package iit.cnr.it.classificationengine.basic;
import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.classifiers.BayesNaive;
import iit.cnr.it.classificationengine.basic.classifiers.C45;
import iit.cnr.it.classificationengine.basic.classifiers.KNN;
import iit.cnr.it.classificationengine.basic.classifiers.SVM;

public class mainTest 
{
	public static void main(String args[])
	{
		KNN<Double> classifierDouble = new KNN<Double>("/home/antonio/file.txt", Double.class, "\n", ",");
		BayesNaive<Double> classifierInteger = new BayesNaive<Double>("/home/antonio/file1.txt", Double.class, "\n", ",");
		C45<Double> classifierDouble1 = new C45<Double>("/home/antonio/hello.txt", Double.class, "\n", ",");
		SVM<Double> classifierDouble2 = new SVM<Double>("/home/antonio/file1.txt", Double.class, "\n", ",");
		//DataSet<Double> data = new DataSet<Double>("/home/antonio/file1.txt", Double.class, "\n", ",") {};
		//DataSet<Double> data1 = new DataSet<Double>("/home/antonio/file.txt", Double.class, "\n", ",") {};
		DataSet<Double> data = DataSet.buildDatasetClassifier("/home/antonio/file.txt", "\n", ",", Double.class);
		DataSet<Double> data1 = DataSet.buildDatasetClassifier("/home/antonio/file.txt", "\n", ",", Double.class);
		
		
		ArrayList<Position> ciInput = new ArrayList<Position>();
		for(int i = 0; i < 6; i++)
			ciInput.add(new Position(i, 0));
		
		ArrayList<Position> cdInput = new ArrayList<Position>();
		for(int i = 6; i < 8; i++)
			cdInput.add(new Position(i, 0));
		
		ArrayList<Position> cd1Input = new ArrayList<Position>();
		for(int i = 0; i < 2; i++)
			cd1Input.add(new Position(i, 1));
		
		Cell<Double> cellD = new Cell<Double>(classifierDouble, new Position(2, 1), cdInput) {};
		Cell<Double> cellI = new Cell<Double>(classifierInteger, new Position(1, 1), ciInput ) {};
		Cell<Double> cellD1 = new Cell<Double>(classifierDouble1, new Position(1, 2), cd1Input) {};
		
		ClassificationMatrix cm = new ClassificationMatrix();
		cm.addCell(cellI);
		cm.addCell(cellD);
		cm.addCell(cellD1);
		
		ClassificationManager cm1 = new ClassificationManager(cm, 2);
		Element<Double> e1 = new Element<Double>(Double.class) {};
		e1.addConvert("2.2");
		e1.addConvert("3.1");
		Element<Integer> e2 = new Element<Integer>(Integer.class) {};
		e2.addConvert("1100");
		e2.addConvert("512");
		e2.addConvert("1500");
		e2.addConvert("0");
		e2.addConvert("1");
		e2.addConvert("1");
		//e2.print();
		ArrayList<Object> al = new ArrayList<Object>();
		for(Integer i : e2.get())
			al.add(i);
		for(Double d : e1.get())
			al.add(d);
		System.out.println("Classification: " + cm1.classify(al));
		
		System.out.println("BayesNaive");
		Evaluation.getMetrics(Evaluation.confusionMatrix(data, classifierInteger), 3);
		System.out.println("KNN");
		Evaluation.getMetrics(Evaluation.confusionMatrix(data1, classifierDouble), 8);
		System.out.println("C45");
		Evaluation.getMetrics(Evaluation.confusionMatrix(data, classifierDouble1), 3);
		System.out.println("SVM");
		Evaluation.getMetrics(Evaluation.confusionMatrix(data, classifierDouble2), 3);
	}
}
