package iit.cnr.it.classificationengine.basic;

public enum TYPE 
{
	CLASSIFIER,
	CLUSTERING;
}
