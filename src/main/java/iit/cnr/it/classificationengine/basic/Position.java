package iit.cnr.it.classificationengine.basic;

/**
 * This class handles positions represented by means of x and y that are integers. 
 * It is a sort of utility class used to represent cell position in the matrix
 * @author antonio
 *
 */
public class Position
{
	//xaxis position
	private int x; 
	//yaxis position
	private int y;
	
	/**
	 * Constructor for Position class
	 * @param x the x-axis position
	 * @param y the y-axis postion
	 */
	public Position(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Copy constructor
	 * @param pos the position to copy 
	 */
	public Position(Position pos)
	{
		this.x = pos.x;
		this.y = pos.y;
	}

	//getter and setter methods
	
	/**
	 * retrieves the x position
	 * @return tohe x position
	 */
	public int X() {return x;}
	
	/**
	 * retrieves the y position
	 * @return the y position
	 */
	public int Y() {return y;}
	
	/**
	 * Set the x position
	 * @param x the x position
	 */
	public void X(int x) 
	{
		if(x >= 0)
			this.x = x;
		else
			ExceptionManager.throwIAE("Invalid x position: " + x);
	}
	
	/**
	 * Sets the y position
	 * @param y the y position
	 */
	public void Y(int y) 
	{
		if(y >= 0)
			this.y = y;
		else
			ExceptionManager.throwIAE("Invalid y position: " + y);
	}

}
