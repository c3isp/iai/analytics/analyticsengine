package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;

/**
 * This abstract class represents all the classifiers we can implement in our
 * project, here we define the common structure. Hence we expect every
 * classifier to share the methods we declare here and every user-defined
 * classifier to extend this abstract class. Since the most popular classifiers
 * library is weka, we want to be compliant with that library too. Hence we wrap
 * our structure around weka's classifiers. In this way you can use this
 * abstract class regardless of the type of classifier you're using. <br>
 * As we've done through all the project, users of this class have to specify
 * the type of things we're classifying: Strings, Double, Integer and so on <br>
 * <b>Supervised Learning</b>
 * 
 * @author antonio
 *
 */
public abstract class Classifier<T> implements Algorithm<T> {
  // This is a DataSet object to be used to train the classifier
  private DataSet<T> trainingSet;
  // to be used for performance calculations
  private DataSet<T> testingSet;
  // this is the type of data the classifier accepts
  private Class<T> tInstance;
  // states if the actual classifier has been trained
  protected boolean isTrained = false;

  /**
   * Retrieves the dataset that forms the training set of this classifier.
   * 
   * @return the training set
   */
  protected DataSet<T> getTrainingSet() {
    return trainingSet;
  }

  /**
   * Set the training set to be used with this classifier.
   * 
   * @param the
   *          training set to be used with this classifier
   */
  protected void setTrainingSet(DataSet<T> trainingSet) {
    this.trainingSet = trainingSet;
  }

  /**
   * Retrieves the dataset that forms the testing set to be used in this
   * classifier.
   * 
   * @return the dataset used as testing set in this classifier
   */
  protected DataSet<T> getTestingSet() {
    return testingSet;
  }

  /**
   * Set the dataset to be used as testing set for this classifier.
   * 
   * @param testingSet
   *          the testing set to be used in this classifier
   */
  protected void setTestingSet(DataSet<T> testingSet) {
    this.testingSet = testingSet;
  }

  @Override
  public Class<T> gettInstance() {
    return tInstance;
  }

  /**
   * Default classifier constructor with only the tInstance class provided. As
   * seen in {@link DataSet<T>, Element<T>} the tInstance parameter is mandatory
   * 
   * @param tInstance
   *          instance of the generic class used as the type for the classifier
   */
  public Classifier(Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (tInstance == null) {
      ExceptionManager.criticalBehavior("Classifier constructor, the parameter is null");
    }
    /* END parameter checking */

    this.tInstance = tInstance;
  }

  /**
   * Constructor for the classifier class, we provide to the classifier the
   * training set and the class instance to be used whenever conversions are
   * necessary.
   * 
   * @param trainingSet
   *          the Set on which we train our classifier
   * @param tInstance
   *          an instance of the class
   * @throws IllegalArgumentException
   *           if one of the arguments is invalid
   */
  public Classifier(DataSet<T> trainingSet, Class<T> tInstance) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (tInstance == null) {
      ExceptionManager.criticalBehavior("Classifier constructor: Instance of template class is null");
    }
    if (trainingSet == null) {
      ExceptionManager.throwIAE("Classifier constructor DataSet object passed is null");
    }
    /* END parameter checking */

    this.trainingSet = trainingSet;
    this.tInstance = tInstance;
  }

  /**
   * Constructor for the classifier class, in this case it is provided to the
   * classifier the path to the training set, hence the trianingSet attribute of
   * the class has to be built up form scratch.
   * 
   * @param trainPathToFile
   *          is the path to the file that serves as training set
   * @param tInstance
   *          is the actual instance of the class-type of the classifier
   * @param eSeparator
   *          is the string used as separator between elements of features
   * @param fSeparator
   *          is the string used as separator between features
   * @throws IllegalArgumentException
   *           if one of the arguments is invalid
   */
  public Classifier(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator) {
    /* BEGIN parameter checking */
    if (tInstance == null) {
      ExceptionManager.criticalBehavior("Classifier constructor: Instance of template class is null");
    }
    /* END parameter checking */

    // other parameters are checked also here
    // trainingSet = new DataSet<T>(trainPathToFile, tInstance, eSeparator,
    // fSeparator) {};
    trainingSet = DataSet.buildDatasetClassifier(trainPathToFile, eSeparator, fSeparator, tInstance);
    this.tInstance = tInstance;
  }

  /**
   * Copy constructor, used to unlink the actual instance of classifier object
   * to the parameter passed. This is useful since, without it, we would have,
   * in some cases two pointers to the same object in the heap
   * 
   * @param classifier
   *          the classifier to copy
   */
  public Classifier(Classifier<T> classifier) {
    /* BEGIN parameter checking */
    if (classifier == null) {
      ExceptionManager.criticalBehavior("The parameter classifier passed " + "to the copy constructor is null");
      /* END parameter checking */
    }

    this.tInstance = classifier.gettInstance();
    if (classifier.trainingSet != null) {
      this.trainingSet = new DataSet<T>(classifier.getTrainingSet()) {
      };
    }
    if (classifier.testingSet != null) {
      this.testingSet = new DataSet<T>(classifier.getTestingSet()) {
      };
    }
    this.isTrained = false;

  }

  // some dummy functions to be implemented by each classifier
  @Override
  public abstract void train();

  public void retrain(DataSet<T> trainingSet) {
  };

  public void addToTrainSet() {
  };

  public void save(String path) {
  };

  public ArrayList<Integer> getClassification(DataSet<T> dataset) {
    return null;
  };

  public String getPerformance() {
    return null;
  };

  @Override
  public int classify(Element<T> f) /* throws IllegalArgumentException */
  {
    return -1;
  }

  public void setParameters(ArrayList<String> a) {
  };

  public String getName() {
    return null;
  };

  public double[] retrieveBelongness(Element<T> elem) throws Exception {
    return null;
  };

  /**
   * Given an attribute returns all the element of that attributes
   * 
   * @param dataset
   *          the dataset to be used
   * @param attribute
   *          the attribute to look for
   * @return ArrayList structure containing all the elements of that class
   */
  public ArrayList<T> getAttribute(DataSet<T> dataset, String attribute) {
    int index = dataset.getIndexFromAttribute(attribute);
    if (index == -1)
      return null;
    // return dataset.getAttributeFeatures(index);
    return dataset.getFeaturesByIndex(index);
  }

  @Override
  public boolean isTrained() {
    return isTrained;
  }

}