package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;

/**
 * This class represents the matrix of cells. Each cell in our vision contains a
 * classifier, hence in this way we implement a matrix of classifiers. As you
 * can see in the image <br>
 * <img src="../classificationMatrix.jpg" alt="Matrix" width=512px height=480px>
 * <br>
 * We assume to have many layers of classification. Each classifier in a layer
 * waits ONLY for upper layer results. <br>
 * By assumption the indexes of the cell start from 1 not from 0. This is done
 * because, by assumption, the features are identified by index 0, hence this is
 * a way to distinguish input from feature from input from an upper cell. <br>
 * NOTE: In this class, to avoid using generic types and since Cells may be of
 * different types, Cells are represented by means of simple Object types masked
 * in the private class rowCell.
 * 
 * @author antonio
 *
 */
public class ClassificationMatrix {

  /**
   * In a matrix classifiers may be of different types since they're able to
   * classify many different things. Hence to mix different types of
   * classifiers, we're forced to use the Object type for the list of cells
   */
  private static class RowCell {
    /**
     * This is a row of cells. We're forced to use the Object type instead of
     * Cell because cells are generic types hence we don't know their runtime
     * type, moreover we want to be able to mix up different types of
     * classifiers
     */
    ArrayList<Cell<?>> row;

    /**
     * default constructor for the rowcell class.
     */
    private RowCell() {
      row = new ArrayList<>();
    }

    /**
     * Adds a cell to the current row in the position specified by the cell if
     * possible, otherwise throws an Exception. This forces users to insert
     * cells in sorting order. <br>
     * NOTE: position starts from 1 not from 0. This because we assume to have
     * the feature vector at position 0
     * 
     * @param cell
     *          the cell to add to the current row
     * @return true if the add is correct
     * @throws IllegalArgumentException
     *           if the position of the cell is not right
     */
    public <T> boolean addCell(Cell<T> cell) throws IllegalArgumentException {
      /* BEGIN parameter checking */
      if (cell.getPosition().X() - 1 > row.size()) {
        ExceptionManager.throwIAE("ClassificationMatrix.addCell:" + " The position X " + cell.getPosition().X()
            + " specified by the parameter cell is not valid, the " + "actual row size is " + row.size());
        /* END parameter checking */
      }

      // append the element at the end of the list
      return row.add(cell);
    }

    /**
     * Gets a reference to the cell at index index.
     * 
     * @param index
     *          the index of the cell we're interested in
     * @return the cell we're interested in
     * @throws IndexOutOfBoundsException
     *           that is managed by ClassificationMatrix
     */
    public Cell<?> getCell(int index) throws IndexOutOfBoundsException {
      return row.get(index);
    }

    /**
     * Size of the list of cells.
     * 
     * @return the size of the list of cells
     */
    public int size() {
      return row.size();
    }
  };

  /* Matrix of cells */
  /**
   * While the rowCell object keeps an entire level of cells, the cellMatrix
   * stores the rowCells as columns. Hence insertion in cellMatrix has to be
   * performed level-by-level, otherwise an exception occurs. This is done to
   * preserve the structure of the matrix and to not mix the classification
   * levels.
   */
  private ArrayList<RowCell> cellMatrix;

  /**
   * This is the default constructor which creates an empty list of rowCells.
   */
  public ClassificationMatrix() {
    cellMatrix = new ArrayList<>();
  }

  /**
   * Adds a cell at the specified position in the cell. At first checks if all
   * parameters are correct and if it is possible to add the cell in that
   * position. If so then add the cell in the specified position, otherwise
   * throw an IllegalArgumentException to signal that the specified position is
   * not available.
   * 
   * @param cell
   *          the cell we want to add to the matrix
   * @return true if everything goes ok, false otherwise
   * @throws IllegalArgumentException
   *           if the parameter provided to the function is not valid
   */
  public <T> boolean addCell(Cell<T> cell) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (cell == null) {
      ExceptionManager.throwIAE("ClassificationMatrix.addCell, " + "the passed parameter is null");
    }
    if ((cell.getPosition().Y() - 1) > cellMatrix.size()) {
      ExceptionManager.throwIAE("ClassificationMatrix.addCell "
          + "the passed parameter is not valid since it belongs to" + "a different level. Parameter level is "
          + cell.getPosition().Y() + " while the matrix of cell is" + " still at level " + cellMatrix.size());
      /* END parameter checking */
    }

    /**
     * Here is not necessary to catch the IndexOutOfBoundsException since we
     * have already performed parameter checking.
     */
    RowCell rc;

    /**
     * check if the cell matrix is empty or if we're inserting a new row in the
     * matrix of cells
     */
    if (cellMatrix.size() == 0 || cellMatrix.size() == cell.getPosition().Y() - 1) {
      // System.out.println("Size1: " + cellMatrix.size());
      rc = new RowCell();
      rc.addCell(cell);
      cellMatrix.add(rc);
    } else {
      // get the row cell we're interested in
      /**
       * Since cells position start from 1 not from 0, we're forced to add the
       * -1 to get the correct cell
       */
      rc = cellMatrix.get(cell.getPosition().Y() - 1);
      rc.addCell(cell);
      // update the cellMatrix row
      cellMatrix.set(cell.getPosition().Y() - 1, rc);
    }
    // System.out.println("size2: " + cellMatrix.size());
    return true;
  }

  /**
   * Retrieve the cell at a certain position in the matrix of cells. At first
   * checks that parameters passed are correct, if so then return the desired
   * cell <br>
   * <b>Remember that indexes start from 1 not from 0</b>
   * 
   * @param xIndex
   *          the x position
   * @param yIndex
   *          the y position
   * @return the cell or null if the xIndex or yIndex are wrong
   */
  public Cell<?> getCell(int xIndex, int yIndex) {
    /* BEGIN parameter checking */
    if (xIndex <= 0 || yIndex <= 0) {
      ExceptionManager
          .throwIAE("ClassificationMatrix.getCell invalid " + "position passed: x = " + xIndex + " y = " + yIndex);
    }
    /* END parameter checking */

    // try to get the requested cell
    try {
      return cellMatrix.get(yIndex - 1).getCell(xIndex - 1);
    } catch (IndexOutOfBoundsException ioobe) {
      if (yIndex - 1 > cellMatrix.size()) {
        ExceptionManager.indexOut(yIndex - 1, cellMatrix.size(), "ClassificationMatrix.getCell, y index");
      } else
        ExceptionManager.indexOut(yIndex - 1, cellMatrix.get(yIndex - 1).size(),
            "ClassificationMatrix.getCell, y index");
      return null;
    }
  }

  /**
   * Copy constructor for a ClassificationMatrix object.
   * 
   * @param classificationMatrix
   *          the object to be copied
   */
  public ClassificationMatrix(ClassificationMatrix classificationMatrix) {
    /* BEGIN parameter checking */
    if (classificationMatrix == null) {
      ExceptionManager.criticalBehavior("ClassificationMatrix copy constructor" + ", the passed parameter is null");
    }
    if (classificationMatrix.cellMatrix == null) {
      ExceptionManager.criticalBehavior("ClassificationManager copy " + "constructor the passed parameter is invalid");
    }
    /* END parameter checking */
    this.cellMatrix = new ArrayList<>(classificationMatrix.cellMatrix);
  }

  /**
   * Return the number of levels of classifiers we have in the actual
   * classification matrix, i.e., how many rowCell objects we have in the
   * cellMatrix list
   * 
   * @return the number of classifier levels in the actual matrix
   */
  public int levels() {
    return cellMatrix.size();
  }

  /**
   * For the row selected via index, return how many cells, hence how many.
   * classifiers are there
   * 
   * @param index
   *          the index of the rowCell we're interested in
   * @return the number of cells in the row, -1 otherwise
   */
  public int rowSize(int index) {
    // go back to position starting from 0
    index = index - 1;
    /* BEGIN parameter checking */
    if (index < 0) {
      ExceptionManager.throwIAE("ClassificationMatrix.rowSize, index not valid");
      /* END parameter checking */
    }

    try {
      return cellMatrix.get(index).size();
    } catch (IndexOutOfBoundsException ioobe) {
      ExceptionManager.indexOut(index, cellMatrix.size(), "ClassificationMatrix.rowSize()");
      return -1;
    }
  }

}
