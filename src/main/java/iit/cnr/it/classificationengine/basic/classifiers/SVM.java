package iit.cnr.it.classificationengine.basic.classifiers;


import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.Classifier;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.classifiers.functions.LibSVM;

import weka.core.Instances;
import weka.core.Instance;

/**
 * This class is the Support Vector Machine classifier. Basically it is a 
 * wrapper around weka SVM classifier. Anyway it uses the more general DataSet 
 * class instead of Instances and conversion is handled. 
 * <br>
 * WEKA: (the libsvm classes, typically the jar file, need to be in the 
 * classpath to use this classifier).<br/>
 * <b>Uses an external resource available at 
 * <a href="http://www.csie.ntu.edu.tw/~cjlin/cgi-bin/libsvm.cgi?+http://www.csie.ntu.edu.tw/~cjlin/libsvm+zip">link</a>
 * that has to be downloaded
 * @author antonio
 *
 * @param <T>
 */

public class SVM<T> extends Classifier<T>
{
	//weka SVM classifier
	private LibSVM svm;

	//training set in instances format
	private Instances TrainingSet;

	/**
	 * return the LibSVM classifier. This is done for two reasons: it avoids us
	 * from wrapping each possible LibSVM functions, thus, if weka adds 
	 * functions to LibSVM, our implementation has not to be modified; moreover
	 * allows users to interact with all the functions of LibSVM. 
	 * @return the LibSVM classifier used
	 */
	public LibSVM getClassifier()
	{
		return svm;
	}

	/**
	 * Retrieve the name of the classifier
	 * @return the name of the classifier
	 */
	@Override
	public String getName()
	{
		return "SVM";
	}
	
	/**
	 * Constructor for SVM classifier, this builds up an empty SVM classifier. 
	 * Of course the tInstance is mandatory and MUST be provided
	 * @throws IllegalArgumentException if the tInstance parameter is null
	 */
	public SVM(Class<T> tInstance) throws IllegalArgumentException
	{
		super(tInstance);
	}

	/**
	 * Constructor for a SVM with an already existing dataset and the template
	 * class instance declared
	 * @param trainingSet is the dataset containing the training set
	 * @param tInstance is the instance of the template class
	 * @throws IllegalArgumentException if one of the argument is invalid
	 */
	public SVM(DataSet<T> trainingSet, Class<T> tInstance)
	{
		//constructs the base-class
		super(trainingSet, tInstance);

		//initialize the Instances object
		TrainingSet = WekaFactory.instancesFromDataset(trainingSet);
		//System.out.println(TrainingSet);
		//sets the class index
		TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);

		//constructs the classifier
		svm = new LibSVM();
	}

	/**
	 * Constructor for SVM classifier , in this case it is provided to 
	 * the classifier the path to the training set, hence the trianingSet 
	 * attribute of the class has to be built up form scratch
	 * 
	 * 
	 * @param trainPathToFile is the path to the file that serves as training set
	 * @param tInstance is the actual instance of the class-type of the classifier
	 * @param eSeparator is the string used as separator between elements of features
	 * @param fSeparator is the string used as separator between features
	 */
	public SVM(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		//construct the base class
		super(trainPathToFile, tInstance, eSeparator,fSeparator);

		//initialize the weka Instances object attribute
		TrainingSet = WekaFactory.instancesFromDataset(this.getTrainingSet());

		svm = new LibSVM();
	}

	/**
	 * Copy constructor for SVM class
	 * @param SVM the SVM object to copy
	 * @throws IllegalArgumentException if the SVM is null 
	 */
	/*public SVM(SVM<T> SVM) throws IllegalArgumentException
	{
		super(SVM);
		if(SVM == null)
			ExceptionManager.criticalBehavior("SVM copy constructor, the object "
					+ "SVM is null");
		this.classifier = SVM.classifier;
		this.TrainingSet = new Instances(SVM.TrainingSet);
	}*/

	/**
	 * Trains the classifier, uses the TrainingSet object to train the
	 * classifier. This function has to be <b>always</b> called before the 
	 * classify function.   
	 */
	@Override
	public void train()
	{
		//System.err.println("Training... ");
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		try
		{
			svm.buildClassifier(TrainingSet);
			isTrained = true;
		}
		//weka exception caught
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
	}

	/**
	 * If modifications to the dataset are applied, then retrain the classifier
	 * @param dataset the dataset on which train the classifier
	 * @throws IllegalArgumentException if the passed dataset is not valid
	 */
	@Override
	public void retrain(DataSet<T> dataset) throws IllegalArgumentException
	{
		if(dataset == null)
			ExceptionManager.throwIAE("SVM.retrain the passed dataset is null");
		//set the passed dataset as trainingset
		setTrainingSet(new DataSet<T>(dataset) {});
		//build up a new classifier
		svm = new LibSVM();
		TrainingSet = WekaFactory.instancesFromDataset(dataset);
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		//System.out.println("Class index: " + TrainingSet.classIndex());
		try
		{
			svm.buildClassifier(TrainingSet);
			isTrained = true;
		}
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
	}

	/**
	 * Classifies an Element object using the just-trained classifier
	 * @param elem the Element object to classify
	 * @throws IllegalArgumentException if the argument passed is invalid
	 */
	@Override
	public int classify(Element<T> elem) throws IllegalArgumentException
	{
		//System.err.println("Classifying....");

		/*BEGIN parameter checking*/
		if(elem == null)
			ExceptionManager.throwIAE("SVM.classify: impossible to classify a null Element");
		if(elem.size() == 0)
			ExceptionManager.throwIAE("SVM.classify: impossible to classify an empty Element");
		/*END parameter checking*/
		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		//elem.print();
		//System.out.println("Instance: " + instance);
		instance.setDataset(TrainingSet);


		double distribution;
		try 
		{
			//classify the element
			distribution = svm.classifyInstance(instance);
			return (int) distribution;
		} 
		catch (Exception e) {
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * The list of parameters provided to the SVM classifier, resembles the 
	 * one provided to the weka LibSVM. Available parameters are as following:
	 * <pre>
	 * -S &lt;int&gt;
	 *  Set type of SVM (default: 0)
	 *    0 = C-SVC
	 *    1 = nu-SVC
	 *    2 = one-class SVM
	 *    3 = epsilon-SVR
	 *    4 = nu-SVR
	 * </pre>
	 * 
	 * <pre>
	 * -K &lt;int&gt;
	 *  Set type of kernel function (default: 2)
	 *    0 = linear: u'*v
	 *    1 = polynomial: (gamma*u'*v + coef0)^degree
	 *    2 = radial basis function: exp(-gamma*|u-v|^2)
	 *    3 = sigmoid: tanh(gamma*u'*v + coef0)
	 * </pre>
	 * 
	 * <pre>
	 * -D &lt;int&gt;
	 *  Set degree in kernel function (default: 3)
	 * </pre>
	 * 
	 * <pre>
	 * -G &lt;double&gt;
	 *  Set gamma in kernel function (default: 1/k)
	 * </pre>
	 * 
	 * <pre>
	 * -R &lt;double&gt;
	 *  Set coef0 in kernel function (default: 0)
	 * </pre>
	 * 
	 * <pre>
	 * -C &lt;double&gt;
	 *  Set the parameter C of C-SVC, epsilon-SVR, and nu-SVR
	 *   (default: 1)
	 * </pre>
	 * 
	 * <pre>
	 * -N &lt;double&gt;
	 *  Set the parameter nu of nu-SVC, one-class SVM, and nu-SVR
	 *   (default: 0.5)
	 * </pre>
	 * 
	 * <pre>
	 * -Z
	 *  Turns on normalization of input data (default: off)
	 * </pre>
	 * 
	 * <pre>
	 * -J
	 *  Turn off nominal to binary conversion.
	 *  WARNING: use only if your data is all numeric!
	 * </pre>
	 * 
	 * <pre>
	 * -V
	 *  Turn off missing value replacement.
	 *  WARNING: use only if your data has no missing values.
	 * </pre>
	 * 
	 * <pre>
	 * -P &lt;double&gt;
	 *  Set the epsilon in loss function of epsilon-SVR (default: 0.1)
	 * </pre>
	 * 
	 * <pre>
	 * -M &lt;double&gt;
	 *  Set cache memory size in MB (default: 40)
	 * </pre>
	 * 
	 * <pre>
	 * -E &lt;double&gt;
	 *  Set tolerance of termination criterion (default: 0.001)
	 * </pre>
	 * 
	 * <pre>
	 * -H
	 *  Turns the shrinking heuristics off (default: on)
	 * </pre>
	 * 
	 * <pre>
	 * -W &lt;double&gt;
	 *  Set the parameters C of class i to weight[i]*C, for C-SVC
	 *  E.g., for a 3-class problem, you could use "1 1 1" for equally
	 *  weighted classes.
	 *  (default: 1 for all classes)
	 * </pre>
	 * 
	 * <pre>
	 * -B
	 *  Generate probability estimates for classification
	 * </pre>
	 * 
	 * <pre>
	 * -seed &lt;num&gt;
	 *  Random seed
	 *  (default = 1)
	 * </pre>
	 * 
	 * @param parameters the list of parameters
	 */
	@Override
	public void setParameters(ArrayList<String> parameters)
	{
		/*BEGIN parameter checking*/
		if(parameters == null)
			ExceptionManager.throwIAE("SVM.setParameters the list of parameters is null");
		if(parameters.size() == 0)
			ExceptionManager.throwIAE("SVM.setParameters the list of parameters is empty");
		/*END parameter checking*/

		try 
		{
			//set required options
			String[] parametersArray = new String[parameters.size()];
			for(int i = 0; i < parameters.size(); i++)
				parametersArray[i] = parameters.get(i);
			svm.setOptions(parametersArray);
		}
		//manage possible exception due to unsupported option
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Unsupported parameter" + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		DataSet<Double> d = DataSet.buildDatasetClassifierWeka("/home/antonio/file.arff", Double.class);


		Element<Double> f = new Element<Double>(Double.class) {};
		f.addConvert("14.1");
		f.addConvert("14.1");
		f.setLabel("Iris");
		//d.add(f);
		//d.add(f);

		SVM<Double> classifier = new SVM<Double>(d, Double.class);
		classifier.train();
		classifier.retrain(d);
		//f.changeFeature(3.4, 0);
		//f.changeFeature(6000.0, 3);
		//f.setAttributes(b);
		System.out.println("Class:"  + classifier.classify(f));

		System.out.println("--------------------------");
	}

	@Override
	public double[] retrieveBelongness(Element<T> elem) throws Exception 
	{
		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		instance.setDataset(TrainingSet);
		return svm.distributionForInstance(instance);
	}
}
