package iit.cnr.it.classificationengine.basic.classifiers;


import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.Classifier;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.classifiers.lazy.IBk;

import weka.core.Instances;
import weka.core.Instance;

/**
 * This class is the K nearest neighbours classifier. Basically it is a 
 * wrapper around weka IBk classifier. Anyway it uses the more general DataSet 
 * class instead of Instances and conversion is handled. 
 * @author antonio
 *
 * @param <T>
 */

public class KNN<T> extends Classifier<T>
{
	//weka IBk classifier
	private IBk ibk;
	
	//training set in instances format
	private Instances TrainingSet;
	
	/**
	 * return the IBk classifier. This is done for two reasons: it avoids us
	 * from wrapping each possible IBk functions, thus, if weka adds 
	 * functions to IBk, our implementation has not to be modified; moreover
	 * allows users to interact with all the functions of IBk. 
	 * @return the IBK classifier used
	 */
	public IBk getClassifier() {
		return ibk;
	}
	
	/**
	 * Retrieve the name of the classifier
	 * @return the name of the classifier
	 */
	@Override
	public String getName()
	{
		return "KNN";
	}
	
	/**
	 * Constructor for KNN classifier, this builds up an empty KNN classifier. 
	 * Of course the tInstance is mandatory and MUST be provided
	 * @throws IllegalArgumentException if the tInstance parameter is null
	 */
	public KNN(Class<T> tInstance) throws IllegalArgumentException
	{
		super(tInstance);
	}
	
	/**
	 * Constructor for a KNN with an already existing dataset and the template
	 * class instance declared
	 * @param trainingSet is the dataset containing the training set
	 * @param tInstance is the instance of the template class
	 * @throws IllegalArgumentException if one of the argument is invalid
	 */
	public KNN(DataSet<T> trainingSet, Class<T> tInstance) 
			throws IllegalArgumentException
	{
		//constructs the base-class
		super(trainingSet, tInstance);
		
		//initialize the Instances object
		TrainingSet = WekaFactory.instancesFromDataset(trainingSet);
		//System.out.println(TrainingSet);
		//sets the class index
		TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		
		//constructs the classifier
		ibk = new IBk();
	}
	
	/**
	 * Constructor for KNN classifier , in this case it is provided to 
	 * the classifier the path to the training set, hence the trianingSet 
	 * attribute of the class has to be built up form scratch
	 * 
	 * 
	 * @param trainPathToFile is the path to the file that serves as training set
	 * @param tInstance is the actual instance of the class-type of the classifier
	 * @param eSeparator is the string used as separator between elements of features
	 * @param fSeparator is the string used as separator between features
	 */
	public KNN(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		//construct the base class
		super(trainPathToFile, tInstance, eSeparator,fSeparator);
		
		//initialize the weka Instances object attribute
		TrainingSet = WekaFactory.instancesFromDataset(this.getTrainingSet());
		
		ibk = new IBk();
	}
	
	/**
	 * Copy constructor for KNN class
	 * @param knn the KNN object to copy
	 * @throws IllegalArgumentException if the knn is null 
	 */
	public KNN(KNN<T> knn) throws IllegalArgumentException
	{
		super(knn);
		if(knn == null)
			ExceptionManager.criticalBehavior("KNN copy constructor, the object "
					+ "knn is null");
		this.ibk = knn.ibk;
		this.TrainingSet = new Instances(knn.TrainingSet);
	}
	
	/**
	 * Trains the classifier, uses the TrainingSet object to train the
	 * classifier. This function has to be <b>always</b> called before the 
	 * classify function.   
	 */
	@Override
	public void train()
	{
		//System.err.println("Training... ");
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		try
		{
			ibk.buildClassifier(TrainingSet);
			isTrained = true;
		}
		//weka exception caught
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
	}
	
	/**
	 * If modifications to the dataset are applied, then retrain the classifier
	 * @param dataset the dataset on which train the classifier
	 * @throws IllegalArgumentException if the passed dataset is not valid
	 */
	@Override
	public void retrain(DataSet<T> dataset) throws IllegalArgumentException
	{
		if(dataset == null)
			ExceptionManager.throwIAE("KNN.retrain the passed dataset is null");
		//set the passed dataset as trainingset
		setTrainingSet(new DataSet<T>(dataset) {});
		//build up a new classifier
		ibk = new IBk();
		TrainingSet = WekaFactory.instancesFromDataset(dataset);
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		//System.out.println("Class index: " + TrainingSet.classIndex());
		try
		{
			ibk.buildClassifier(TrainingSet);
			isTrained = true;
		}
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
	}
	
	/**
	 * Classifies an Element object using the just-trained classifier
	 * @param elem the Element object to classify
	 * @throws IllegalArgumentException if the argument passed is invalid
	 */
	@Override
	public int classify(Element<T> elem) throws IllegalArgumentException
	{
		//System.err.println("Classifying....");
		
		/*BEGIN parameter checking*/
		if(elem == null)
			ExceptionManager.throwIAE("KNN.classify: impossible to classify a null Element");
		if(elem.size() == 0)
			ExceptionManager.throwIAE("KNN.classify: impossible to classify an empty Element");
		/*END parameter checking*/
		
		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		//elem.print();
		//System.out.println("Instance: " + instance);
		instance.setDataset(TrainingSet);
		
		double distribution;
		try 
		{
			//classify the element
			distribution = ibk.classifyInstance(instance);
			return (int) distribution;
		} 
		//manage exception
		catch (Exception e) {
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * The list of parameters provided to the KNN classifier, resembles the 
	 * one provided to the weka IBk. Available parameters are as following:
	 * <pre> -I
	 *  Weight neighbours by the inverse of their distance
	 *  (use when k &gt; 1)</pre>
	 * 
	 * <pre> -F
	 *  Weight neighbours by 1 - their distance
	 *  (use when k &gt; 1)</pre>
	 * 
	 * <pre> -K &lt;number of neighbors&gt;
	 *  Number of nearest neighbours (k) used in classification.
	 *  (Default = 1)</pre>
	 * 
	 * <pre> -E
	 *  Minimise mean squared error rather than mean absolute
	 *  error when using -X option with numeric prediction.</pre>
	 * 
	 * <pre> -W &lt;window size&gt;
	 *  Maximum number of training instances maintained.
	 *  Training instances are dropped FIFO. (Default = no window)</pre>
	 * 
	 * <pre> -X
	 *  Select the number of nearest neighbours between 1
	 *  and the k value specified using hold-one-out evaluation
	 *  on the training data (use when k &gt; 1)</pre>
	 * 
	 * <pre> -A
	 *  The nearest neighbour search algorithm to use (default: weka.core.neighboursearch.LinearNNSearch).
	 * </pre>
	 * 
	 * @param parameters the list of parameters
	 */
	@Override
	public void setParameters(ArrayList<String> parameters)
	{
		/*BEGIN parameter checking*/
		if(parameters == null)
			ExceptionManager.throwIAE("KNN.setParameters the list of parameters is null");
		if(parameters.size() == 0)
			ExceptionManager.throwIAE("KNN.setParameters the list of parameters is empty");
		/*END parameter checking*/
		
		try 
		{
			//set required options
			String[] parametersArray = new String[parameters.size()];
			for(int i = 0; i < parameters.size(); i++)
				parametersArray[i] = parameters.get(i);
			ibk.setOptions(parametersArray);
		}
		//manage possible exception due to unsupported option
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Unsupported parameter" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		DataSet<Double> d = DataSet.buildDatasetClassifierWeka("/home/antonio/file.arff", Double.class);
		
		
		Element<Double> f = new Element<Double>(Double.class) {};
		f.addConvert("14.1");
		f.addConvert("14.1");
		f.setLabel("Iris");
		d.add(f);
		d.add(f);
		
		KNN<Double> classifier = new KNN<Double>(d, Double.class);
		classifier.train();
		KNN<Double> knn1 = new KNN<Double>(classifier);
		classifier.retrain(d);
		knn1.train();
		//f.changeFeature(3.4, 0);
		//f.changeFeature(6000.0, 3);
		//f.setAttributes(b);
		System.out.println("Class:"  + classifier.classify(f));
		System.out.println("Class:"  + knn1.classify(f));
		
		System.out.println("--------------------------");
	}

	@Override
	public double[] retrieveBelongness(Element<T> elem) throws Exception 
	{
		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		instance.setDataset(TrainingSet);
		return ibk.distributionForInstance(instance);
	}
}
