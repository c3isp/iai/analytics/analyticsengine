package iit.cnr.it.classificationengine.basic;
/*
 * Copyright notes go here
 */

//package visibility?

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class represents the features that are considered as the input to our
 * classification engine. This is a <b>generic</b> class since it is up to
 * providers to gather the features and to provide them to the classification
 * engine and because it's not possible to know 'a priori' the type of features
 * provided to the Classification Engine. Since it is possible to build up
 * features in many different ways we provide many constructors to build up the
 * set of features.
 * <p>
 * <b>Label: </b> the label for an element is not mandatory, it is if we need to
 * use the Element object to train a classifier, but we can use Element object
 * also for clustering and in that case labels are useless. <br>
 * In order to provide an easier bridge to weka, the Element<T> class will have
 * an additional attribute aimed at qualifying if the Element<T> we're using is
 * thought to be used for clustering or for classification. Hence in the first
 * case the label attribute, even if present is not considered, in the second
 * one we have the attribute label. The label attribute can be either a String
 * or an object of the same type of the Element<T>, hence we have to manage all
 * these possible cases.
 * </p>
 * Instead of adding more constructors, according to chapter 1 of "Effective
 * JAVA", we will add <i>static factory methods</i>. In this way if the
 * parameters passed to the factory methods do not satisfy the conditions, the
 * object is not created.
 * 
 * An Element object is: <br>
 * <img src=element.jpg height=300px width=800px /> <br>
 * From class perspective an Element<T> object can bee seen in figure <br>
 * <a href="image">
 * <img src="elementClass.jpg" alt="Element object attributes" width=256px
 * height=240px> </a>
 * 
 * <br>
 * Whenever T = String, it is not checked if the feature is valid or not because
 * EVERY STRING IS A VALID STRING. Thus it's not possible to check if the
 * correct separators are used.
 * 
 * @author antonio
 * @see ArrayList
 *
 */

public abstract class Element<T> {
	// ==========================================================================
	// Constants
	// ==========================================================================

	/**
	 * Equivalence classes
	 */
	public final static int SAME_ELEMENT = 0;
	public final static int SAME_ELEMENT_DIFFERENT_CLASS = 1;
	public final static int DIFFERENT_SIZE = 2;
	public final static int DIFFERENT_ELEMENT_SAME_CLASS = 3;
	public final static int DIFFERENT_ELEMENT = 4;

	// ==========================================================================
	// Properties
	// ==========================================================================
	/**
	 * The default qualification for an Element<T> object is the one of
	 * classifier
	 */
	private TYPE qualification = TYPE.CLASSIFIER;

	// special attribute to be considered
	private int columnID = -1;

	/**
	 * In our vision features are represented as a vector which may contain any
	 * possible type of feature. However features in a feature vector must be of
	 * the same type
	 */
	private ArrayList<T> featureVector = null;

	/**
	 * Instance of class template. This is required to perform the cast to the
	 * actual Element object type. In fact it is not possible to gather an
	 * instance of a template class
	 */
	private Class<T> tInstance;

	/**
	 * Contains the attributes types of the Element object, i.e., the quality of
	 * the features
	 */
	private ArrayList<String> attributes = null;

	// --------------------------------------------------------------------------
	// Private class: LABEL
	// --------------------------------------------------------------------------
	/**
	 * Instead of using 3 different attributes to handle a label, maybe the best
	 * option to deal with labels is to use a private class, that only an
	 * Element can use, in charge of managing all the possible operations that
	 * involve the usage of the labels. Labels are sensitive since they can have
	 * a different type than the features, for example we can have String labels
	 * while features are Integer.
	 * 
	 * @author antonio
	 *
	 */
	private class Label {
		// a label may be of the same type of the dataset, or it may be a string
		private String labelValue = "";
		private T label = null;

		// the possible labeltypes are listed above in the element class
		private LABEL_TYPE labelType = LABEL_TYPE.ERROR;

		/**
		 * This function sets a label, basically starting from a label in string
		 * format, it tries to parse it in order to convert it to T class, if it
		 * is not possible, hten the label is a string. Then sets the label type
		 * according to the type of the label
		 * 
		 * @param label
		 *            the label of the actual element
		 */
		void setLabel(String label) {
			try {
				// try to convert the label to the same feature type
				T labelFeatureType = Utility.convert(label, tInstance);
				if (labelFeatureType != null) {
					// set the new label and eventually reset the old one
					this.label = labelFeatureType;
					this.labelValue = "";
					this.labelType = LABEL_TYPE.TEMPLATE;
				} else {
					this.label = null;
					this.labelType = LABEL_TYPE.NOT_PRESENT;
				}
			}
			// if not possible set the stringLabel
			catch (NumberFormatException nfe) {
				this.labelValue = label;
				this.labelType = LABEL_TYPE.STRING;
			}
		}

		/**
		 * Retrieves the label
		 * 
		 * @return the label as an Object, this is done in order to allow the
		 *         label to be both a T or Stirng label
		 */
		Object getLabel() {
			if (labelType.equals(LABEL_TYPE.TEMPLATE))
				return label;
			if (labelType.equals(LABEL_TYPE.STRING))
				return labelValue;
			return null;
		}

		/**
		 * Retrieve the label type
		 * 
		 * @return the labeltype
		 */
		LABEL_TYPE getLabelType() {
			return labelType;
		}

		/**
		 * Set the label type. this function can be called only by an Element
		 * object when its label changes
		 * 
		 * @param labelType
		 *            the labeltype to be setted
		 */
		void setLabelType(LABEL_TYPE labelType) {
			this.labelType = labelType;
		}

		/**
		 * Retrieves the label as a string
		 * 
		 * @return the label as a string
		 */
		String getLabelAsString() {
			Object o = getLabel();
			if (o != null)
				return o.toString();
			else
				return null;
		}

		/**
		 * Clears the Label class
		 */
		void clear() {
			labelType = LABEL_TYPE.NOT_PRESENT;
			labelValue = "";
			label = null;
		}

	};

	Label label = new Label();

	// ==========================================================================
	// Constructors
	// ==========================================================================
	/**
	 * This is the default constructor which initializes an empty feature vector
	 * of the type required by the provider. This vector will be filled next.
	 * The type of feature MUST be declared however <br>
	 * Remember that this is an abstract class hence it can't be instantiated as
	 * is without putting the brackets {}.
	 * 
	 * @param tInstance
	 *            instance of class e.g. String.class
	 * 
	 *            <pre>
	 *  {@code Element<Integer> e = new Element<Integer>(Integer.class) {}}
	 *            </pre>
	 * 
	 *            The tInstance parameter is fundamental in all constructor
	 *            since it is used for type-conversion
	 */
	public Element(Class<T> tInstance) {
		/* BEGIN parameter checking */
		if (tInstance == null) {
			ExceptionManager.criticalBehavior("Element constructor: tInstance parameter is null");
		}
		/* END parameter checking */

		// builds up an empty feature
		featureVector = new ArrayList<T>();
		attributes = new ArrayList<String>();
		this.tInstance = tInstance;
	}

	/**
	 * This constructor builds up an Element object from an already existing
	 * ArrayList of features. To avoid possible misbehaviors here we do a "deep"
	 * copy, hence we build up a new ArrayList class containing the features of
	 * the parameter passed to the constructor. <b> In this case the label is
	 * not provided </b>
	 * 
	 * @param array
	 *            the ArrayList containing the features we want to insert in the
	 *            new class
	 * @param tInstance
	 *            instance of class
	 * @throws IllegalArgumentException
	 *             if a parameter is not correct
	 */
	public Element(ArrayList<T> array, Class<T> tInstance) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (tInstance == null) {
			ExceptionManager.criticalBehavior("Element constructor: tInstance parameter is null");
		}
		if (array == null) {
			ExceptionManager.wrongBehavior("Element constructor: array passed is null");
			array = new ArrayList<T>();
		}
		/* END parameter checking */

		this.tInstance = tInstance;
		featureVector = new ArrayList<T>(array);
		attributes = new ArrayList<String>();
	}

	/**
	 * This constructor builds a new Element class starting form a primitive
	 * array. Also here the label is not provided. In this constructor, if
	 * possible, we try to recover from error
	 * 
	 * @param array
	 *            the pointer to the array
	 * @param size
	 *            the size of the array or the number of elements of the array
	 *            we want to put inside the Element object
	 * @param tInstance
	 *            instance of class
	 */
	public Element(T array[], int size, Class<T> tInstance) {
		/* BEGIN parameter checking */
		if (tInstance == null) {
			ExceptionManager.criticalBehavior("Element constructor: tInstance parameter is null");
		}
		if (array == null) {
			ExceptionManager.wrongBehavior("Element constructor: passed array is null");
			// recover from error
			size = 0;
		}
		if (size < 0 || size > array.length) {
			ExceptionManager.wrongBehavior("Element constructor: size " + size + " invalid");
			// recover from error
			size = 0;
		}
		/* END parameter checking */

		this.tInstance = tInstance;
		boolean result;
		featureVector = new ArrayList<T>();
		attributes = new ArrayList<String>();

		// insert features in the element object
		for (int i = 0; i < size; i++) {
			result = featureVector.add(array[i]);

			// handle invalid insert in ListArray
			if (result == false) {
				featureVector.clear();
				System.err.println("Error in inserting feature");
				return;
			}
		}
	}

	/**
	 * Copy constructor needed to avoid possible misbehaviors when a particular
	 * Element object is requested
	 * 
	 * <pre>
	 * {
	 * 	&#64;code
	 * 	Element<Integer> elem = new Element<Integer>(element);
	 * }
	 * </pre>
	 * 
	 * @param element
	 *            the object to be copied
	 */
	public Element(Element<T> element) {
		/* BEGIN parameter checking */
		if (element == null)
			ExceptionManager.throwIAE("Element copy constructor the argument passed is null");
		/* END parameter checking */

		this.tInstance = element.getInstance();
		this.attributes = new ArrayList<String>();
		this.featureVector = new ArrayList<T>();
		// this.parseAndFill(element.toString(), ",");
		for (T feature : element.featureVector)
			this.addConvert(feature.toString());
		if (element.getLabel() != null)
			this.setLabel(element.getLabelAsString());
		this.qualification = element.qualification;
		this.columnID = element.columnID;
		this.label.setLabelType(element.getLabelType());
	}

	/**
	 * This constructor builds up a new Element object reading file content. We
	 * assume that a file contains ONLY one Element. Here is assumed that
	 * features are separated via an arbitrary character while the element stops
	 * when reading a newline.
	 * 
	 * <b>We assume that the label is the last "feature" </b>
	 * 
	 * @param pathToFile
	 *            the absolute path to the file
	 * @param tInstance
	 *            instance of the actual class type
	 * @param separator
	 *            the char used to separate features
	 * @throws IllegalArgumentException
	 *             if some arguments are invalid
	 */
	public Element(String pathToFile, Class<T> tInstance, String separator) {
		/* BEGIN parameter checking */
		if (tInstance == null)
			ExceptionManager.criticalBehavior("Element constructor: tInstance parameter is null");
		String isNull = (pathToFile == null) ? "path to file" : (separator == null) ? "string separator" : "";
		if (!isNull.isEmpty())
			ExceptionManager.throwIAE("Element.constructor " + isNull + " is null");
		String isEmpty = (pathToFile.isEmpty()) ? "path to file" : (separator.isEmpty()) ? "string separator" : "";
		if (!isEmpty.isEmpty())
			ExceptionManager.throwIAE("Element.constructor " + isEmpty + " is empty");
		if (separator.equals("\n"))
			ExceptionManager.throwIAE("Element.constructor Invalid separator \\n used");
		/* END parameter checking */

		this.tInstance = tInstance;
		featureVector = new ArrayList<T>();
		attributes = new ArrayList<String>();

		// read a line from a file
		String fileLine = readFileLine(pathToFile);

		if (fileLine == null) {
			ExceptionManager.criticalBehavior("FileLine is null an error occurred reading the "
					+ "line from file the element is left uninitialized");
		}

		// effective build of the Element object
		parseAndFill(fileLine, separator);
	}

	// ==========================================================================
	// Static Factory Methods
	// ==========================================================================

	/**
	 * This function is a <i>static factory method</i> basically it constructs
	 * the Element object according to the schema useful for a classifier, hence
	 * it is an element with a label that is assumed to be the last feature.
	 * This is the most simple static factory method to build up an Element to
	 * be given to a classifier
	 * 
	 * <pre>
	 * 	{@code Element<Integer> element = Element.buildClassifierElement("0,1,2,A", ",", Integer.class)}
	 * </pre>
	 * 
	 * @param string
	 *            the string to be parsed in order to find the required features
	 *            that will form the element object
	 * @param fSeparator
	 *            the feature separator string, it may be a single character or
	 *            a multi character
	 * @param tInstance
	 *            the actual instance of the Element object
	 * @return a non null Element if everything goes fine, a null Element
	 *         otherwise
	 */
	public static <T> Element<T> buildClassifierElement(String string, String fSeparator, Class<T> tInstance) {
		if (!Utility.parameterChecking(string, fSeparator, tInstance))
			return null;

		/**
		 * If we arrive at this point it means that the checking previously
		 * performed are all ok, hence we can instantiate the object and parse
		 * the String
		 */
		Element<T> element = new Element<T>(tInstance) {
		};
		// initialize and empty list of attributes
		element.attributes = new ArrayList<String>();
		element.qualification = TYPE.CLASSIFIER;

		/**
		 * Now we can put the features inside the element object, since it is a
		 * classifier element, there should be a label, we assume that the label
		 * is the last of the feature inside the String
		 */
		String parts[] = string.split(fSeparator);
		if (!element.fillFromStrings(parts, parts.length - 1))
			return null;

		// check that the label was correctly added
		if (element.getLabelType().equals(LABEL_TYPE.NOT_PRESENT))
			return null;
		return element;
	}

	/**
	 * This function is an overloading of the previous one, in this case we
	 * explicitly declare which is the ID of the labels (starting form 0) inside
	 * the vector of features passed as a single string.
	 * <p>
	 * In this case we give to the user the possibility of passing a String
	 * without the label, this particular situation has to be signaled by the
	 * user by setting the labelID to -1, this means that in the passed string
	 * there isn't the label, hence the factory method will provide its own,
	 * dummy label, that we decided to name "CLASS". <br>
	 * This possibility is especially required when we have to deal with
	 * predictors, hence we want to know to which class the given element will
	 * belong to, if we use a weka classifier, it forces us to put labels
	 * </p>
	 * 
	 * @param string
	 *            the string to be parsed in order to find the features and
	 *            possibly the label
	 * @param fSeparator
	 *            the string that separates each feature
	 * @param tInstance
	 *            the instance of the Element object we want to create
	 * @param labelID
	 *            the index of the label inside the vector of features
	 * @return null if something goes wrong, an element of the given instance
	 *         with the given features otherwise
	 */
	public static <T> Element<T> buildClassifierElement(String string, String fSeparator, Class<T> tInstance,
			int labelID) {
		if (!Utility.parameterChecking(string, fSeparator, tInstance))
			return null;

		/**
		 * If we arrive at this point it means that the checking previously
		 * performed are all ok, hence we can instantiate the object and parse
		 * the String
		 */
		Element<T> element = new Element<T>(tInstance) {
		};
		// initialize and empty list of attributes
		element.attributes = new ArrayList<String>();
		element.qualification = TYPE.CLASSIFIER;

		/**
		 * Now we can put the features inside the element object, since it is a
		 * classifier element, there should be a label, we assume that the label
		 * is the last of the feature inside the String
		 */
		String parts[] = string.split(fSeparator);
		if (!element.fillFromStrings(parts, labelID))
			return null;

		// check that the label was correctly added
		if (element.getLabelType().equals(LABEL_TYPE.NOT_PRESENT))
			return null;
		return element;
	}

	/**
	 * Build up a new Element to be used inside a clusterer algorithm. Element
	 * of this type do not have a label, hence this has to be taken into account
	 * when constructing this element
	 * 
	 * @param string
	 *            the string containing all the features to be passed to the
	 *            element
	 * @param fSeparator
	 *            the feature separator
	 * @param tInstance
	 *            the instance of the class to be used for this clusterer
	 *            element
	 * @return the element created if everything goes ok, false otherwise
	 */
	public static <T> Element<T> buildClustererElement(String string, String fSeparator, Class<T> tInstance) {
		if (!Utility.parameterChecking(string, fSeparator, tInstance))
			return null;

		/**
		 * If we arrive at this point it means that the checking previously
		 * performed are all ok, hence we can instantiate the object and parse
		 * the String
		 */
		Element<T> element = new Element<T>(tInstance) {
		};
		// initialize and empty list of attributes
		element.attributes = new ArrayList<String>();
		element.qualification = TYPE.CLUSTERING;

		/**
		 * Now we can put the features inside the element object, since this is
		 * an element to be used for clustering, then all the features inside
		 * the string have to be passed
		 */
		String parts[] = string.split(fSeparator);
		if (!element.fillFromStrings(parts, -2))
			return null;

		element.label.setLabelType(LABEL_TYPE.NOT_PRESENT);

		return element;
	}

	// ==========================================================================
	// Private helper functions
	// ==========================================================================
	/**
	 * This helper private function initializes the features for the element
	 * under consideration. Moreover it also manages the label.
	 * <p>
	 * For the labelID there are two special numbers:
	 * <ol>
	 * <li><i>-1</i> if we want to provide a dummy label</li>
	 * <li><i>-2</i> if the label is not required</li>
	 * </ol>
	 * </p>
	 * 
	 * @param array
	 *            the array of strings containing the features
	 * @param labelID
	 *            the id of the label
	 * @return true if everything goes ok, false otherwise
	 */
	private boolean fillFromStrings(String[] array, int labelID) {
		for (int i = 0; i < array.length; i++) {
			if (i == labelID && labelID != -2) {
				this.setLabel(array[i]);
			} else if (!this.addConvert(array[i]))
				return false;
		}
		/**
		 * if the labelID was equal to -1, then it means that we have to provide
		 * a dummy label for the element under consideration.
		 */

		if (labelID == -1) {
			this.setLabel("CLASS");
		}
		return true;
	}

	/**
	 * Read a <b>single</b> line from a file.
	 * 
	 * @param pathToFile
	 *            the path to the file
	 * @return the line read if everything goes ok
	 */
	private String readFileLine(String pathToFile) {
		BufferedReader bufferedReader = null;
		try {
			File f = new File(pathToFile);
			if (bufferedReader == null)
				bufferedReader = new BufferedReader(new FileReader(f));
			String line = bufferedReader.readLine();
			bufferedReader.close();
			return line;
		}

		// file not found
		catch (FileNotFoundException e) {
			ExceptionManager.criticalBehavior("Element reading from file: File NOT found" + pathToFile);
		}
		// file not suitable, no newline found
		catch (IOException e) {
			ExceptionManager.criticalBehavior("Element reading from file: I/O error occured");
		}
		return null;
	}

	/**
	 * Retrieve the last feature of the feature vector which is considered as
	 * label
	 */
	private void retrieveLabel() {
		/**
		 * This is done ONLY if the label is not stored as a String in the
		 * labelValue. By assumption the label is the last "feature". Since we
		 * store the label in a different place, we have also to remove it from
		 * features
		 */
		if (label.getLabelAsString().equals("")) {
			label.setLabel(featureVector.get(featureVector.size() - 1).toString());
			featureVector.remove(featureVector.size() - 1);
		}
	}

	// ==========================================================================
	// Public methods
	// ==========================================================================
	/**
	 * Parses a String containing features separated by the separator parameter
	 * and fills the Element object. In this case the label is assumed to be the
	 * last feature in the String vector.
	 * 
	 * @param vector
	 *            contains all the features plus the label (possibly the last
	 *            feature)
	 * @param separator
	 *            the separator between features
	 * @return true if everything goes ok, false otherwise
	 * @throws IllegalArgumentException
	 *             if one of the parameter is not correct
	 */
	public boolean parseAndFill(String vector, String separator) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		String isNull = (vector == null) ? "string to parse" : (separator == null) ? "string separator" : "";
		if (!isNull.isEmpty())
			ExceptionManager.throwIAE("Element.parseAndFill parameter " + isNull + " is null");
		String isEmpty = (vector.isEmpty()) ? "string to parse" : (separator.isEmpty()) ? "string separator" : "";
		if (!isEmpty.isEmpty())
			ExceptionManager.throwIAE("Element.parseAndFill parameter " + isEmpty + " is Empty");
		/* END parameter checking */

		String[] content = vector.split(separator);

		/**
		 * at this point we have split the line, thus we have to perform the
		 * required cast to put elements into ArrayList
		 */
		for (String element : content) {
			// if the addConvert function fails, then reset everything and tell
			if (this.addConvert(element) != true) {
				System.err.println("Something wrong to the passed vector of " + "features " + vector
						+ " Vector left uninitialized");
				this.clear();
				return false;
			}
		}
		// if label is left uninitialized retrieve it
		retrieveLabel();
		return true;
	}

	/**
	 * Add a new single feature converting it from string
	 * 
	 * @param from
	 *            the string containing the feature
	 * @return true if the add is successful
	 * @throws IllegalArgumentException
	 *             if the parameter is null
	 */
	public boolean addConvert(String from) throws IllegalArgumentException {
		if (from == null)
			ExceptionManager.throwIAE("Element.addConvert parameter is null");
		try {
			// convert the read String to a T object
			T feature = Utility.convert(from.trim(), tInstance);
			if (feature == null) {
				System.err.println("Element: Invalid element read for addConvert" + from);
				return false;
			}
			// add the converted feature to the vector
			return featureVector.add(feature);
		}
		/**
		 * The convert function may throw a number format exception ,this means
		 * that the features are of a numeric type e.g. Integer or Double, while
		 * the label is of type String. If there are more than 2 strings in a
		 * numeric feature vector, hence the dataset contains something wrong.
		 */
		catch (NumberFormatException nfe) {
			System.err.println("NumberFormatException " + from);
			/**
			 * If the read string was not in a numeric format, then this may be
			 * the label we were expecting
			 */
			if (label.getLabelAsString().equals("")) {
				label.setLabel(from);
				return false;
			} else {
				ExceptionManager.criticalBehavior("Element: invalid numeric " + "format " + label.labelValue + " "
						+ from + ". Feature " + "is of class " + tInstance.getName());
				label.labelValue = "";
				return false;
			}
		}
	}

	/**
	 * Clears the class content
	 * 
	 * @return true if the clear was successful, false otherwise
	 */
	public boolean clear() {
		featureVector.clear();
		attributes.clear();
		label.clear();
		return (featureVector.size() == 0 ? true : false);
	}

	/**
	 * Provides the possibility to insert a new feature in the set of features
	 * 
	 * @param feature
	 *            the feature to insert
	 * @return true if the insert was successful, false otherwise
	 * @throws IllegalArgumentException
	 *             if elem is invalid
	 */
	public boolean insertFeature(T feature) throws IllegalArgumentException {
		if (feature == null)
			ExceptionManager.throwIAE("Element.insertFeature parameter is null");
		if (featureVector == null)
			featureVector = new ArrayList<T>();
		// tries to insert a new element
		try {
			return featureVector.add(feature);
		} catch (ClassCastException cce) {
			ExceptionManager
					.wrongBehavior("Element.insertFeature impossible to " + "insert the element, types don't match");
			cce.printStackTrace();
			return false;
		}
	}

	/**
	 * Tries to add the feature passed as parameter in the position passed as
	 * parameter, if index is greater than the size of the list, the feature
	 * will be appended
	 * 
	 * @param feature
	 *            the feature to be inserted
	 * @param index
	 *            the index in which we want to place the feature
	 * @return true if the insert was correctly performed, false otherwise
	 * @throws IllegalArgumentException
	 */
	public boolean insertFeature(T feature, int index) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (feature == null)
			ExceptionManager.throwIAE("Element.insert feature with index, feature is null");
		if (featureVector == null)
			featureVector = new ArrayList<T>();
		if (index > featureVector.size())
			return featureVector.add(feature);
		/* END parameter checking */

		else
			featureVector.add(index, feature);
		return true;
	}

	/**
	 * Changes the value of a single feature in the set of features
	 * 
	 * @param feature
	 *            the feature to insert
	 * @param index
	 *            the index of the feature to change
	 * @return true if everything goes ok, false otherwise
	 * @throws IllegalArgumentException
	 *             if parameters are not valid
	 */
	public boolean changeFeature(T feature, int index) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (feature == null)
			ExceptionManager.throwIAE("Element.changeFeature, feature  is null " + feature + "\t" + this.toString());
		if (index < 0)
			ExceptionManager.throwIAE("Element.changeFeature, index is invalid: " + index);
		/* END parameter checking */

		try {
			featureVector.remove(index);
			featureVector.add(index, feature);
			return true;
		}
		// manage exceptions
		catch (IndexOutOfBoundsException ioobe) {
			ExceptionManager.indexOut(index, featureVector.size(), "Element.changeFeature");
			return false;
		} catch (ClassCastException cce) {
			ExceptionManager
					.wrongBehavior("Element.changeFeature impossible to " + "insert the element, types don't match");
			cce.printStackTrace();
			return false;
		}
	}

	/**
	 * Append a new ArrayList of features to the old one. NOTE: ArrayList type
	 * must be the same
	 * 
	 * @param features
	 *            list of features to append
	 * @return true if the append goes ok
	 * @throws IllegalArgumentException
	 *             if arrayList is null
	 */
	public boolean append(ArrayList<T> features) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (features == null)
			ExceptionManager.throwIAE("Element.append The provided array list is null");
		if (features.size() == 0)
			ExceptionManager.throwIAE("Element.append The provided array list is empty");
		/* END parameter checking */

		// add each feature in the arrayList to the actual feature vector
		for (T feature : features) {
			try {
				insertFeature(feature);
			} catch (IllegalArgumentException iae) {
				ExceptionManager
						.wrongBehavior("Element.append the passed array " + "of features contains a null feature");
				return false;
			}
		}
		return true;
	}

	/**
	 * Append an Element object to the existing one NOTE: don't append
	 * attributes
	 * 
	 * @param element
	 *            the feature to append
	 * @throws IllegalArgumentException
	 *             if the feature is null or empty
	 */
	public boolean append(Element<T> element) {
		/* BEGIN parameter checking */
		if (element == null)
			ExceptionManager.throwIAE("Element.append the passed element is null");
		if (element.size() == 0 || element.get() == null)
			ExceptionManager.throwIAE("Element.append the passed element is empty");
		/* END parameter checking */

		return append(element.get());
	}

	/**
	 * Insert a single attribute name
	 * 
	 * @param name
	 *            the name of the attribute
	 * @return true if everything goes ok, false otherwise
	 * @throws IllegalArgumentException
	 *             if name is null or empty
	 */
	public boolean addAttribute(String name) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (name == null)
			ExceptionManager.throwIAE("Element.addAttribute the passed name is null");
		if (name.isEmpty())
			ExceptionManager.throwIAE("Element.addAttribute the passed name is empty");
		/* END parameter checking */

		return attributes.add(name);
	}

	/**
	 * Prints the feature vector, used mostly to debug and to verify class
	 * behavior
	 */
	public void print() {
		for (T elem : featureVector) {
			System.out.println("Element: " + elem);
		}
		if (!getLabelType().equals(LABEL_TYPE.NOT_PRESENT))
			System.out.println(getLabelAsString());
	}

	/**
	 * Function that removes a feature from the feature vector
	 * 
	 * @param index
	 *            index of the feature to remove
	 * @throws IllegalArgumentException
	 *             if the index is invalid
	 */
	public void removeFeature(int index) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (index < 0)
			ExceptionManager.throwIAE("Element.removeFeature the passed index is invalid");
		/* END parameter checking */

		try {
			featureVector.remove(index);
		} catch (IndexOutOfBoundsException ioobe) {
			ExceptionManager.indexOut(index, featureVector.size(),
					"Element.removeFeature at feature " + this.toString());
			return;
		}

	}

	/**
	 * Compares the element passed as parameter with the actual element. Returns
	 * an integer that states if the two elements are equal or different and in
	 * which aspect they differ
	 * 
	 * @param element
	 *            the element passed as parameter
	 * @return an integer stating in which aspect the two elements are different
	 * @throws IllegalArgumentException
	 */
	public int equals(Element<T> element) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (element == null)
			ExceptionManager.throwIAE("Element.equals the provided argument is null");
		if (element.get().size() == 0 || element.size() != this.size())
			return Element.DIFFERENT_SIZE;
		/* END parameter checking */

		ArrayList<T> features = element.get();
		int i = 0;
		for (T feature : features) {
			if (!feature.equals(featureVector.get(i))) {
				if (element.getLabelAsString().equals(this.getLabelAsString()))
					return Element.DIFFERENT_ELEMENT_SAME_CLASS;
				else
					return Element.DIFFERENT_ELEMENT;
			}
			++i;
		}
		return Element.SAME_ELEMENT;
	}

	// ==========================================================================
	// Accessors
	// ==========================================================================

	/**
	 * Function that counts the number of features contained into the feature
	 * vector
	 * 
	 * @return the number of features in the feature vector
	 */
	public int size() {
		return featureVector.size();
	}

	/**
	 * Swaps the already existing feature vector with the passed one
	 * 
	 * @param arrayList
	 *            the new arrayList to be used
	 * @return true if the operation succeeds, false otherwise
	 * @throws IllegalArgumentException
	 *             the provided ArrayList is null
	 */
	public boolean set(ArrayList<T> arrayList) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (arrayList == null)
			ExceptionManager.throwIAE("Element.set The provided array list is null");
		if (arrayList.size() == 0)
			ExceptionManager.throwIAE("Element.set The provided array list is empty");
		/* END parameter checking */

		// clear the previous feature vector
		featureVector.clear();

		// copy the provided feature vector into the old one
		for (T feature : arrayList) {
			try {
				insertFeature(feature);
			} catch (IllegalArgumentException iae) {
				ExceptionManager.wrongBehavior("Element.set passed aray contains null feature");
				return false;
			}
		}
		return true;
	}

	/**
	 * gather the feature at a certain position
	 * 
	 * @param index
	 *            position of the feature
	 * @return the feature
	 * @throws IllegalArgumentException
	 *             if the index is < 0
	 */
	public T getFeature(int index) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (index < 0)
			ExceptionManager.throwIAE("Element.getFeature the passed index is invalid");
		/* END parameter checking */

		try {
			// System.out.println(featureVector.size());
			return featureVector.get(index);
		} catch (IndexOutOfBoundsException ioobe) {
			ExceptionManager.indexOut(index, featureVector.size(), "Element.getFeature");
			return null;
		}
	}

	/**
	 * Auto-generated getter and setter for the attributes attribute of the
	 * feature class
	 */
	public ArrayList<String> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<String> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Allows other classes to gather a reference to the feature vector
	 * 
	 * @return a copy of the actual feature vector
	 */
	public ArrayList<T> get() {
		return new ArrayList<T>(featureVector);
	}

	/**
	 * Get the value of the feature label as String
	 * 
	 * @return the value of the label
	 */
	public String getLabelAsString() {
		return label.getLabelAsString();
	}

	/**
	 * Get the value of the label
	 * 
	 * @return the value of the label attribute
	 */
	public LABEL_TYPE getLabelType() {
		return label.getLabelType();
	}

	/**
	 * Get the actual label as a generic Object, this is done to avoid other
	 * casting and to not have to check the type of label when not necessary
	 * 
	 * @return the label as an Object
	 */
	public Object getLabel() {
		return label.getLabel();
	}

	/**
	 * Try to parse the passed String to convert it as T object, if succeeds
	 * then the label is of type T, otherwise this is a String Label
	 * 
	 * @param label
	 *            the label to parse.
	 * @throws IllegalArgumentException
	 *             if the label is invalid
	 */
	public void setLabel(String label) {

		/* BEGIN parameter checking */
		if (label == null)
			ExceptionManager.throwIAE("Element.setLabel the passed label is null");
		if (label.isEmpty())
			ExceptionManager.throwIAE("Element.setLabel the passed label is empty");
		/* END parameter checking */
		this.label.setLabel(label);
		if (!this.label.labelType.equals(LABEL_TYPE.NOT_PRESENT))
			this.qualification = TYPE.CLASSIFIER;
	}

	/**
	 * Retrieve the instance of the template class
	 * 
	 * @return instance of template class
	 */
	public Class<T> getInstance() {
		return tInstance;
	}

	/**
	 * Converts the actual feature to String
	 */
	public String toString() {
		// System.out.println("ToString() called");
		StringBuilder stringBuilder = new StringBuilder();
		boolean first = true;
		for (T feature : this.featureVector) {
			if (first) {
				stringBuilder.append("" + feature);
				first = false;
			} else {
				stringBuilder.append("," + feature);
			}
		}
		if (qualification.equals(TYPE.CLASSIFIER))
			stringBuilder.append("," + this.getLabelAsString());
		return stringBuilder.toString();
	}

	public String toString(boolean string) {
		// System.out.println("ToString() called");
		StringBuilder stringBuilder = new StringBuilder();
		boolean first = true;
		for (T feature : this.featureVector) {
			if (first) {
				stringBuilder.append("" + feature);
				first = false;
			} else {
				stringBuilder.append("," + feature);
			}
		}
		if (qualification.equals(TYPE.CLASSIFIER) && string)
			stringBuilder.append("," + this.getLabelAsString());
		return stringBuilder.toString();
	}

	/**
	 * Set the index of the feature to be used as id
	 * 
	 * @param id
	 *            the index of the feature to be used as id
	 */
	public void setColumnID(int id) {
		/* BEGIN parameter checking */
		if (id < 0)
			return;
		/* END parameter checking */
		columnID = id;
	}

	/**
	 * Retrieve the index of the feature to be used as id
	 * 
	 * @return the index of the feature used as id
	 */
	public int getColumnID() {
		return columnID;
	}

	/**
	 * Retrieves the actual qualification of the element
	 * 
	 * @return the actual qualification of the element
	 */
	public TYPE getQualification() {
		return qualification;
	}

	/**
	 * Set the qualification of the element
	 * 
	 * @param qualification
	 *            the new qualification of the element
	 */
	protected void setQualification(TYPE qualification) {
		this.qualification = qualification;
	}

	/**
	 * trial main
	 */
	public static void main(String args[]) {
		String a[] = new String[3];
		a[0] = "hello";
		a[1] = "ciao";
		a[2] = "salut";

		ArrayList<Integer> b = new ArrayList<Integer>();
		b.add(12);
		b.add(2);
		b.add(3);

		Element<Integer> f = new Element<Integer>(b, Integer.class) {
		};
		// f.changeFeature("ole", 0);
		// f.set(b);
		// f.print();

		// Element<Double> f1 = new Element<Double>("/home/antonio/file.txt",
		// Double.class, ",") {};
		// f1.print();
		f.setLabel("12");

		Element<Integer> f1 = new Element<Integer>(b, Integer.class) {
		};
		f1.setLabel("12");
		System.out.println(f1.equals(f) + " " + f + " " + f1);
		f1.changeFeature(5, 2);
		System.out.println(f1.equals(f) + " " + f + " " + f1);
		f.changeFeature(5, 2);
		f1.setLabel("23");
		System.out.println(f1.equals(f) + " " + f + " " + f1);
		f1.changeFeature(7, 2);
		System.out.println(f1.equals(f) + " " + f + " " + f1);
		// UserFeature u = new UserFeature() {};
		// System.out.println(f1.getLabelAsString());
		// System.out.println(f1.getLabel());

		Element<Integer> element = Element.buildClassifierElement("0,1,2,A", ",", Integer.class);
		if (element == null)
			System.out.println("element is null");
		else
			System.out.println(element.toString());
		Element<Integer> element1 = Element.buildClassifierElement("0,1,2", ",", Integer.class, -1);
		System.out.println(element1.toString());
		Element<Integer> element2 = Element.buildClustererElement("0,1,2", ",", Integer.class);
		System.out.println(element2.toString());
	}
}
