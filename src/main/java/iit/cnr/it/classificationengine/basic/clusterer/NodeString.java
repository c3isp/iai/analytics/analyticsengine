package iit.cnr.it.classificationengine.basic.clusterer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.Evaluation;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.TYPE;
import iit.cnr.it.classificationengine.basic.Utility;
import iit.cnr.it.classificationengine.clusterer.cctree.CCTree;
import iit.cnr.it.classificationengine.clusterer.cctree.Entropy;

/**
 * This class represents a NodeString in the CCTree. A NodeString is a structure
 * that contains some data, that are the data according to it was generated.
 * Each NodeString is characterized by its purity. Moreover a NodeString stores
 * the index of the attribute according to it was created. A NodeString is
 * characterized by its own ID and its father ID.
 * 
 * 
 * @author antonio
 *
 */
public class NodeString {

	/* Private class attribute */
	// the list of the elements that compose the NodeString
	private DataSet<String> data;
	// the purity of the NodeString
	private double purity = -1;

	private double externalPurity = -1;

	/**
	 * Couple that fully characterized the NodeString with respect to its father
	 * The attribute index is the attribute that has the highest entropy in the
	 * father's dataset, while attribute value is the value of the attribute
	 * associated to this NodeString
	 */
	// the index of the attribute according to the NodeString was created
	private int attributeIndex = -1;
	// the value of the splitting attribute.
	private String attributeValue = "";

	// states if the NodeString is a leaf or not
	private boolean isLeaf;
	// list of children of this NodeString
	private ArrayList<NodeString> children;

	private String externalLabel = "";

	/**
	 * The next three attributes fully characterize the NodeString in the tree
	 * <ol>
	 * <li>The id is incremented by each NodeString instance</li>
	 * <li>The NodeStringId is the id of the actual NodeString</li>
	 * <li>father is the father's NodeStringId of the actual NodeString</li>
	 * </ol>
	 */
	private static int id = 0;
	int NodeStringId;
	private int father = -1;

	/**
	 * empty NodeString object
	 */
	public NodeString() {
		data = new DataSet<String>(String.class) {
		};
		data.setColumnID(0);
		// data.setQualification(Element.CLUSTERING);
		NodeStringId = id;
		id = id + 1;
		attributeIndex = -1;
	}

	/**
	 * 
	 * @param dataset
	 */
	public NodeString(DataSet<String> dataset) {
		data = dataset;
		// data.setQualification(Element.CLUSTERING);
		data.setColumnID(dataset.getColumnID());
		NodeStringId = id;
		id = id + 1;
		attributeIndex = -1;
	}

	/**
	 * Set the value of the splitting attribute for this NodeString
	 * 
	 * @param value
	 *            the value of the splitting attribute
	 */
	public void setAttributeValue(String value) {
		attributeValue = value;
	}

	/**
	 * Set the NodeStringId of the father of this NodeString
	 * 
	 * @param father
	 *            the NodeStringId of the father
	 */
	public void setFather(int father) {
		this.father = father;
	}

	/**
	 * Retrieves the list of the elements grouped in that NodeString. Basically
	 * this is the list of the elements that share the same attribute value from
	 * the parent
	 * 
	 * @return the list of elements
	 */
	public ArrayList<Element<String>> getElementsList() {
		return data.getElements();
	}

	/**
	 * Check if the actual NodeString is or not a leaf
	 * 
	 * @return true if the NodeString is a leaf, false otherwise
	 */
	public boolean isLeaf() {
		return isLeaf;
	}

	/**
	 * Sets the list of elements grouped in this NodeString
	 * 
	 * @param elementsList
	 *            the list of elements that form the NodeString
	 * @throws IllegalArgumentException
	 *             if the parameter is not valid
	 */
	public void setElementsList(ArrayList<Element<String>> elementsList) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (elementsList == null)
			ExceptionManager.throwIAE("NodeString.setElementsList the provided parameter is null");
		/* END parameter checking */
		this.data = new DataSet<String>(elementsList, String.class) {
		};
		this.data.setColumnID(elementsList.get(0).getColumnID());
	}

	/**
	 * Gets the actual purity of the NodeString
	 * 
	 * @return the purity of the NodeString
	 */
	public double getPurity() {
		return purity;
	}

	/**
	 * Set the purity of the NodeString
	 * 
	 * @param purity
	 *            the purity of the NodeString
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public void setPurity(double purity) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (purity < 0)
			ExceptionManager.throwIAE("NodeString.setPurity the passed purity is invalid");
		/* END parameter checking */
		this.purity = purity;
	}

	/**
	 * Get the index of the attribute according to which the NodeString was
	 * created
	 * 
	 * @return the index of the attribute
	 */
	public int getAttributeIndex() {
		return attributeIndex;
	}

	/**
	 * Set the index of the attribute according to which the NodeString was
	 * created
	 * 
	 * @param attributeIndex
	 *            the index of the attribute
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public void setAttributeIndex(int attributeIndex) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (attributeIndex < 0)
			ExceptionManager.throwIAE("NodeString.setAttributeIndex the passed parameter is invalid");
		/* END parameter checking */
		this.attributeIndex = attributeIndex;
	}

	/**
	 * Constructor for the class NodeString
	 * 
	 * @param elements
	 *            the list of the elements belonging to the actual NodeString
	 * @param purity
	 *            the purity of the NodeString
	 * @param attributeIndex
	 *            the index of the attribute according to the NodeString has
	 *            been created
	 */
	public NodeString(ArrayList<Element<String>> elements, double purity, int attributeIndex, int minElementsThreshold,
			double purityThreshold) {
		// tries to build the NodeString object
		try {
			this.setElementsList(elements);
			this.setPurity(purity);
			this.setAttributeIndex(attributeIndex);
			/* checks if the actual NodeString is a leaf or not */
			if (elements.size() <= minElementsThreshold || purity <= purityThreshold)
				isLeaf = true;
			else
				isLeaf = false;
			// System.out.println(data);
			NodeStringId = id;
			id = id + 1;
		}

		/**
		 * If constructor arguments are not valid then exit
		 */
		catch (IllegalArgumentException aie) {
			ExceptionManager.criticalBehavior(
					"NodeString constructor one or some of the arguments" + " are not valid, the program will stop");
		}
	}

	/**
	 * Constructor of a NodeString which takes as parameter only the elements
	 * contained in that NodeString
	 * 
	 * @param arrayList
	 *            the list of elements contained in that NodeString
	 */
	public NodeString(ArrayList<Element<String>> arrayList) {
		try {
			this.setElementsList(arrayList);
			// System.out.println(data);
			NodeStringId = id;
			id = id + 1;
		} catch (IllegalArgumentException aie) {
			ExceptionManager.criticalBehavior(
					"NodeString constructor elements parameter is invalid, " + "the program will stop");
		}
	}

	/**
	 * Computes the purity of a NodeString looking for the attribute that
	 * provides the maximum value for the entropy
	 * 
	 * @param weights
	 *            the weights for each attribute
	 * @return the index of the attribute with the highest entropy
	 */
	public int computePurity(ArrayList<Double> weights) {
		// System.out.println("\n***********\nNodeStringID: " + NodeStringId);
		// local variables
		double maxEntropy = 0;
		int maxEntropyAttribute = 0;
		double NodeStringPurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		for (int index = 0; index < data.getElement(0).size(); index++) {
			if (index == data.getColumnID() && index < data.getElement(0).size() - 1)
				index = index + 1;
			if (index == data.getColumnID() && index == data.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(data, index);
			if (CCTree.DEBUG == true)
				System.out.println("Entropy of attribute " + index + "is " + tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
				maxEntropyAttribute = index;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			try {
				weight = weights.get(index);
			}
			// this is not an error, weights may not be provided
			catch (NullPointerException npe) {
			}
			// manage the case of an invalid weights list
			catch (IndexOutOfBoundsException ioobe) {
				ExceptionManager.indexOut(index, weights.size(), "NodeString.computePurity");
				return -1;
			}
			// compute NodeString purity
			NodeStringPurity = NodeStringPurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = NodeStringPurity / (data.getElement(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		return maxEntropyAttribute;
	}

	public double computePurity() {
		double maxEntropy = 0;
		double NodeStringPurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		for (int index = 0; index < data.getElement(0).size(); index++) {
			if (index == data.getColumnID() && index < data.getElement(0).size() - 1)
				index = index + 1;
			if (index == data.getColumnID() && index == data.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(data, index);
			// if (CCTree.DEBUG == true)
			// System.out.println("Entropy of attribute " + index + "is " +
			// tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			// compute NodeString purity
			NodeStringPurity = NodeStringPurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = NodeStringPurity / (data.getElement(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		return purity;
	}

	/**
	 * Add a new element to this NodeString
	 * 
	 * @param element
	 *            the element to insert into the dataset of the NodeString
	 * @throws IllegalArgumentException
	 *             if the parameter passed is invalid
	 */
	public void addElement(Element<String> element) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (element == null)
			ExceptionManager.throwIAE("NodeString.addElement, null element provided");
		/* END parameter checking */

		// System.out.println("Reached here " + element.getLabelType() + " " +
		// data.getLabelType());

		data.add(element);
	}

	/**
	 * This function splits the whole dataset creating a NodeString for each
	 * different value of the attribute at index attributeIndex.
	 * 
	 * @param attributeIndex
	 *            the index of the attribute used to split up the dataset
	 * @return the list of NodeStrings just created, i.e, the children of the
	 *         NodeString. If the NodeString can't be further splitted return
	 *         null and signal the actual NodeString as leaf
	 */
	public ArrayList<NodeString> splitAttribute(int attributeIndex, double purityThreshold, int minElementsThreshold) {
		// check if the actual NodeString has to be split or not
		if (!(purity > purityThreshold && data.getElements()
				.size() > minElementsThreshold))/*
												 * (purity != -1 && purity <=
												 * purityThreshold) ||
												 * data.getElements().size() <=
												 * minElementsThreshold)
												 */
		{
			// System.out.println("NULL");
			// signal that the actual NodeString is a leaf
			// System.out.println("LeafId: " + NodeStringId + " purity: " +
			// purity + "
			// size:
			// " + data.getElements().size());
			isLeaf = true;
			return null;
		}

		if (CCTree.DEBUG == true)
			System.out.println("NodeStringId: " + NodeStringId + " Split attribute: " + attributeIndex + " purity: "
					+ purity + " size: " + data.getElements().size());

		// retrieve the possible different value for the attribute index
		ArrayList<String> attributeValues = data.differentFeaturesValue(attributeIndex);

		/**
		 * create a new child NodeString for each possible attribute value and
		 * assign to each child NodeString the corresponding attribute value
		 */
		children = new ArrayList<>();
		for (int i = 0; i < attributeValues.size(); i++) {
			NodeString nodeString = new NodeString();
			nodeString.setQualification(this.data.getQualification());
			nodeString.setAttributeIndex(attributeIndex);
			nodeString.setAttributeValue(attributeValues.get(i));
			nodeString.setFather(NodeStringId);
			children.add(nodeString);
		}

		// System.out.println(attributeValues);

		// insert each element in the correct NodeString
		for (Element<String> element : data.getElements()) {
			// use a new element object in order to not alter the existing ones
			Element<String> tmpElement = new Element<String>(element) {
			};
			/**
			 * retrieve the index of the NodeString that has the same
			 * attributeValue of the considered element
			 */
			int index = Utility.indexOf(attributeValues, element.getFeature(attributeIndex));
			// add the element in the right NodeString
			NodeString tmpNodeString = children.get(index);
			// remove the equal feature
			// tmpElement.removeFeature(attributeIndex);

			tmpElement.setColumnID(data.getColumnID());
			// System.out.println(element);
			tmpNodeString.addElement(tmpElement);
			// swap the stored NodeString with the modified one
			children = Utility.swap(children, tmpNodeString, index);

		}
		if (CCTree.DEBUG == true) {
			System.out.println("\n\nNodeStringID: " + NodeStringId + " AttributeIndex: " + attributeIndex);
			for (int i = 0; i < children.size(); i++) {
				System.out.println(i + ": " + attributeValues.get(i) + " elements: ");
				// for(Element<Integer> element :
				// children.get(i).getElementsList())
				// {
				// System.out.println(element);
				// }
				System.out.println("**************");
			}
		}
		return children;
	}

	/**
	 * Prints the characteristics of a NodeString
	 */
	public void print() {
		System.out.println("--------------------");
		System.out.println("NodeStringId: " + NodeStringId);
		System.out.println(attributeIndex + " " + attributeValue);
		System.out.println(data);
		System.out.println("--------------------");
	}

	/**
	 * Convert a NodeString to String
	 */
	@Override
	public String toString() {
		String s = "\n------------\n";
		s += "Father: " + father;

		/**
		 * checks if the NodeString is a leaf or an effective NodeString
		 */
		String quality = "NodeStringId";
		if (isLeaf)
			quality = "leafId";
		s += "\t" + quality + ": " + NodeStringId + "\t AttributeInfos [index: " + attributeIndex + " value: "
				+ attributeValue + "] purity: " + purity + " size: " + data.getElements().size() + "Label: "
				+ this.externalLabel + "\n";
		s += data.toString();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(s);
		// retrieves the children of this NodeString
		if (children != null) {
			stringBuilder.append("\n{");
			for (NodeString c : children)
				stringBuilder.append(c.toString());
			stringBuilder.append("\n}\n");
		}

		stringBuilder.append("------------\n");

		return stringBuilder.toString();
	}

	/**
	 * Return the number of elements contained in this NodeString
	 */
	public int numberElements() {
		return data.getElements().size();
	}

	/**
	 * Retrieve the id of the NodeString
	 * 
	 * @return the id of the NodeString
	 */
	public int getId() {
		return NodeStringId;
	}

	/**
	 * set the selected NodeString as leaf
	 * 
	 * @param isLeaf
	 *            true to set the NodeString as leaf, false otherwise
	 */
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	/**
	 * retrieve the children of the NodeString under consideration
	 * 
	 * @return the list of NodeString that are the children of the actual
	 *         NodeString
	 */
	public ArrayList<NodeString> getChildren() {
		return children;
	}

	/**
	 * Retrieve the value of the attribute used for splitting this NodeString
	 * 
	 * @return the value of the attribute used for splitting this NodeString
	 */
	public String getAttributeValue() {
		return attributeValue;
	}

	/**
	 * Retrieve the external purity of this NodeString. <br>
	 * If the NodeString of the cctree is used for classification purposes, then
	 * we can compute the external purity from the dataset. In fact, to be used
	 * for classification, the dataset has to be labelled, if it is not, then we
	 * can't apply a classification algorithm on it.
	 * 
	 * @return the external purity of this NodeString
	 */
	public double getExternalPurity() {
		if (data.getQualification().equals(TYPE.CLASSIFIER))
			externalPurity = Evaluation.nodeExternalPurity(data.getLabelAsString());
		return externalPurity;
	}

	/**
	 * Given the list of labels that belong to this NodeString, computes the
	 * external purity of this NodeString
	 * 
	 * @param labels
	 *            the labels of the elements belonging to this NodeString
	 */
	public void computeExternalPurity(ArrayList<String> labels, ArrayList<String> priority) {
		externalPurity = Evaluation.nodeExternalPurity(labels);
		externalPurity = externalPurity * labels.size() / data.getElements().size();
		computeNodeStringLabel(labels, priority);
	}

	public void computeExternalPurity() {
		getExternalPurity();
		computeNodeStringLabel();
	}

	/**
	 * Basing on the labels to be set to this NodeString, tries to assign a
	 * class to this NodeString, the list priority is used in the case in which
	 * we're not able to give a class to the NodeString, in this case it will be
	 * chosen as label the class that is most likely to occur among the ones
	 * belonging to the NodeString under consideration
	 * 
	 * @param labels
	 *            the list of labels of the elements inside this NodeString
	 * @param priority
	 *            the list of possible labels ordered by their likelihood to
	 *            occur
	 */
	private void computeNodeStringLabel(ArrayList<String> labels, ArrayList<String> priority) {
		// System.out.println(labels.toString());
		int[] classes = new int[] { 0, 0, 0, 0, 0, 0 };
		int i = 0;
		ArrayList<String> countedLabels = new ArrayList<>();

		// count the occurrence of each labe that go inside this NodeString
		for (String s : labels) {
			if (!countedLabels.contains(s)) {
				classes[i] = Utility.countOccurrences(s, labels);
				countedLabels.add(s);
				i++;
			}

		}

		int max = 0;
		int maxIndex = -1;
		/**
		 * try to give to this NodeString a class basing on the number of times
		 * each label occurs
		 */
		for (int j = 0; j < classes.length; j++) {

			// System.out.println(seventy + "\t" + data.getElements().size());
			if (classes[j] > max && classes[j] > (labels.size() / 2.0)) {
				max = classes[j];
				maxIndex = j;
			}
		}

		/**
		 * if it was not possible to give a label to this NodeString basing on
		 * the priority list
		 */
		if (maxIndex == -1) {
			// System.out.println("Max index == -1");
			for (String s : priority)
				if (countedLabels.contains(s)) {
					this.externalLabel = s;
					break;
				}
		} else
			this.externalLabel = countedLabels.get(maxIndex);
		// System.out.println("NodeStringID: " + NodeStringId + " Label: " +
		// this.externalLabel);
		/*
		 * System.out.println("Counted Labels: " + countedLabels.toString());
		 * System.out.println("Labels passed: " + labels.toString()); for(int j
		 * = 0; j < classes.length; j++) System.out.print(classes[j] + "\t");
		 * System.out.println("DataSet size: " + data.getElements().size());
		 * System.out.println("ExternalLabel: " + externalLabel);
		 * System.out.println("\n*******");
		 */
	}

	/**
	 * Basing on the labels to be set to this NodeString, tries to assign a
	 * class to this NodeString, the list priority is used in the case in which
	 * we're not able to give a class to the NodeString, in this case it will be
	 * chosen as label the class that is most likely to occur among the ones
	 * belonging to the NodeString under consideration
	 * 
	 * @param labels
	 *            the list of labels of the elements inside this NodeString
	 * @param priority
	 *            the list of possible labels ordered by their likelihood to
	 *            occur
	 */
	private void computeNodeStringLabel() {
		if (data.getQualification() != TYPE.CLASSIFIER) {
			return;
		}
		// System.out.println(labels.toString());
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		// count the occurrence of each labe that go inside this NodeString
		for (Object s : data.getLabel()) {
			if (map.containsKey(s)) {
				int i = map.get(s);
				i += 1;
				map.put(s.toString(), i);
			} else
				map.put(s.toString(), 1);
		}

		int max = 0;
		/**
		 * try to give to this NodeString a class basing on the number of times
		 * each label occurs
		 */
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			// System.out.println(seventy + "\t" + data.getElements().size());
			if (entry.getValue() > max && entry.getValue() > (map.entrySet().size() / 2.0)) {
				max = entry.getValue();
				externalLabel = entry.getKey();
			}
		}

		/**
		 * if it was not possible to give a label to this NodeString basing on
		 * the priority list
		 */

		// System.out.println("NodeStringID: " + NodeStringId + " Label: " +
		// this.externalLabel);
		/*
		 * System.out.println("Counted Labels: " + countedLabels.toString());
		 * System.out.println("Labels passed: " + labels.toString()); for(int j
		 * = 0; j < classes.length; j++) System.out.print(classes[j] + "\t");
		 * System.out.println("DataSet size: " + data.getElements().size());
		 * System.out.println("ExternalLabel: " + externalLabel);
		 * System.out.println("\n*******");
		 */
	}

	/**
	 * Retrieve the external label of the NodeString
	 * 
	 * @return the string representing the label of the NodeString
	 */
	public String getExternalLabel() {
		return externalLabel;
	}

	/**
	 * In this case the splitting operation is not performed basing on an
	 * attribute but on the label and is performed to obtain a leaf that
	 * contains (as much as possible) elements belonging to the same class. In
	 * fact this is very important for the classifier.
	 */
	public ArrayList<NodeString> splitLabel() {
		System.out.println("Split label");
		this.isLeaf = false;
		ArrayList<NodeString> NodeStrings = new ArrayList<>();
		ArrayList<String> differentLabels = data.getLabelDistinct();
		System.out.println(differentLabels.toString());
		// create as many NodeStrings as needed
		for (int i = 0; i < differentLabels.size(); i++) {
			NodeString NodeString = new NodeString();
			NodeString.setFather(NodeStringId);
			NodeString.isLeaf = true;
			NodeStrings.add(NodeString);
		}
		// assign to each NodeString elements of the same label
		for (int i = 0; i < data.getNumberOfElements(); i++) {
			int index = Utility.indexOf(differentLabels, data.getLabel(i, true).toString());
			NodeString NodeString = NodeStrings.get(index);
			NodeString.addElement(data.getElement(i));
			NodeStrings.set(index, NodeString);
		}
		this.children = NodeStrings;
		return NodeStrings;
	}

	public void setQualification(TYPE use) {
		data.setQualification(use);
	}

	public ArrayList<String> getDataId() {
		if (data.getColumnID() != -1) {
			ArrayList<String> ids = new ArrayList<>();
			ids.addAll(data.getFeaturesByIndex(data.getColumnID()));
			return ids;
		}
		return null;
	}
}
