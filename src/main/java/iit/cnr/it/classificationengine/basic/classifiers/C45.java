package iit.cnr.it.classificationengine.basic.classifiers;
import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.Classifier;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This class is a wrapper around the J48 weka classifier.
 *  
 * @author antonio
 *
 * @param <T>
 */
public class C45<T> extends Classifier<T> 
{
	//weka J48 classifier
	private J48 j48;

	//training set in instances format
	private Instances TrainingSet;

	/**
	 * return the J48 classifier. This is done for two reasons: it avoids us
	 * from wrapping each possible J48 functions, thus, if weka adds 
	 * functions to J48, our implementation has not to be modified; moreover
	 * allows users to interact with all the functions of J48. 
	 * @return the J48 classifier used
	 */
	public J48 getClassifier() 
	{
		return j48;
	}
	
	/**
	 * Retrieve the name of the classifier
	 * @return the name of the classifier
	 */
	@Override
	public String getName()
	{
		return "C45";
	}
	
	/**
	 * Constructor for C45 classifier, this builds up an empty C45 classifier. 
	 * Of course the tInstance is mandatory and MUST be provided
	 * @throws IllegalArgumentException if the tInstance parameter is null
	 */
	public C45(Class<T> tInstance) throws IllegalArgumentException
	{
		super(tInstance);
	}

	/**
	 * Constructor for a C45 with an already existing dataset and the template
	 * class instance declared
	 * @param trainingSet is the dataset containing the training set
	 * @param tInstance is the instance of the template class
	 * @throws IllegalArgumentException if one of the argument is invalid
	 */
	public C45(DataSet<T> trainingSet, Class<T> tInstance) 
			throws IllegalArgumentException
	{
		//constructs the base-class
		super(trainingSet, tInstance);

		//initialize the Instances object
		TrainingSet = WekaFactory.instancesFromDataset(trainingSet);
		//System.out.println(TrainingSet);
		//sets the class index
		TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);

		//constructs the classifier
		j48 = new J48();
	}

	/**
	 * Constructor for C45 classifier , in this case it is provided to 
	 * the classifier the path to the training set, hence the trianingSet 
	 * attribute of the class has to be built up form scratch
	 * 
	 * 
	 * @param trainPathToFile is the path to the file that serves as training set
	 * @param tInstance is the actual instance of the class-type of the classifier
	 * @param eSeparator is the string used as separator between elements of features
	 * @param fSeparator is the string used as separator between features
	 */
	public C45(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		//construct the base class
		super(trainPathToFile, tInstance, eSeparator,fSeparator);

		//initialize the weka Instances object attribute
		TrainingSet = WekaFactory.instancesFromDataset(this.getTrainingSet());

		j48 = new J48();
	}

	/**
	 * Copy constructor for C45 class
	 * @param C45 the C45 object to copy
	 * @throws IllegalArgumentException if the C45 is null 
	 */
	public C45(C45<T> C45) throws IllegalArgumentException
	{
		super(C45);
		if(C45 == null)
			ExceptionManager.criticalBehavior("C45 copy constructor, the object "
					+ "C45 is null");
		this.j48 = C45.j48;
		this.TrainingSet = new Instances(C45.TrainingSet);
	}

	/**
	 * Trains the classifier, uses the TrainingSet object to train the
	 * classifier. This function has to be <b>always</b> called before the 
	 * classify function.   
	 */
	@Override
	public void train()
	{
		//System.err.println("Training... ");
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		try
		{
			j48.buildClassifier(TrainingSet);
			isTrained = true;
		}
		//weka exception caught
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
	}

	/**
	 * If modifications to the dataset are applied, then retrain the classifier
	 * @param dataset the dataset on which train the classifier
	 * @throws IllegalArgumentException if the passed dataset is not valid
	 */
	@Override
	public void retrain(DataSet<T> dataset) throws IllegalArgumentException
	{
		if(dataset == null)
			ExceptionManager.throwIAE("C45.retrain the passed dataset is null");
		//set the passed dataset as trainingset
		setTrainingSet(new DataSet<T>(dataset) {});
		//build up a new classifier
		j48 = new J48();
		TrainingSet = WekaFactory.instancesFromDataset(dataset);
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		//System.out.println("Class index: " + TrainingSet.classIndex());
		try
		{
			j48.buildClassifier(TrainingSet);
			isTrained = true;
		}
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
	}

	/**
	 * Classifies an Element object using the just-trained classifier
	 * @param elem the Element object to classify
	 * @throws IllegalArgumentException if the argument passed is invalid
	 */
	@Override
	public int classify(Element<T> elem) throws IllegalArgumentException
	{
		//System.err.println("Classifying....");

		/*BEGIN parameter checking*/
		if(elem == null)
			ExceptionManager.throwIAE("C45.classify: impossible to classify a null Element");
		if(elem.size() == 0)
			ExceptionManager.throwIAE("C45.classify: impossible to classify an empty Element");
		/*END parameter checking*/

		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		//elem.print();
		//System.out.println("Instance: " + instance);
		instance.setDataset(TrainingSet);

		double distribution;
		try 
		{
			//classify the element
			distribution = j48.classifyInstance(instance);
			return (int) distribution;
		} 
		catch (Exception e) {
			ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause() );
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * The list of parameters provided to the C45 classifier, resembles the 
	 * one provided to the weka j48. Available parameters are as following:
	 * -U <br>
	 * Use unpruned tree.<p>
	 *
	 * -C confidence <br>
	 * Set confidence threshold for pruning. (Default: 0.25) <p>
	 *
	 * -M number <br>
	 * Set minimum number of instances per leaf. (Default: 2) <p>
	 *
	 * -R <br>
	 * Use reduced error pruning. No subtree raising is performed. <p>
	 *
	 * -N number <br>
	 * Set number of folds for reduced error pruning. One fold is
	 * used as the pruning set. (Default: 3) <p>
	 *
	 * -B <br>
	 * Use binary splits for nominal attributes. <p>
	 *
	 * -S <br>
	 * Don't perform subtree raising. <p>
	 *
	 * -L <br>
	 * Do not clean up after the tree has been built.
	 *
	 * -A <br>
	 * If set, Laplace smoothing is used for predicted probabilites. <p>
	 *
	 * -Q <br>
	 * The seed for reduced-error pruning. <p>
	 * 
	 * @param parameters the list of parameters
	 */
	@Override
	public void setParameters(ArrayList<String> parameters)
	{
		/*BEGIN parameter checking*/
		if(parameters == null)
			ExceptionManager.throwIAE("C45.setParameters the list of parameters is null");
		if(parameters.size() == 0)
			ExceptionManager.throwIAE("C45.setParameters the list of parameters is empty");
		/*END parameter checking*/

		try 
		{
			//set required options
			String[] parametersArray = new String[parameters.size()];
			for(int i = 0; i < parameters.size(); i++)
				parametersArray[i] = parameters.get(i);
			j48.setOptions(parametersArray);
		}
		//manage possible exception due to unsupported option
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Unsupported parameter" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public double[] retrieveBelongness(Element<T> elem) throws Exception 
	{
		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		instance.setDataset(TrainingSet);
		return j48.distributionForInstance(instance);
	}
	
	
}
