package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * This class handles the content of a cell. To enable concurrent execution on
 * different cells, this class implements the Callable interface, we choose this
 * interface in order to retrieve the result of the classification. <br>
 * To a cellManager corresponds a cell. The fundamental method here is the
 * call(), here we can see the various operations the CellManager is in charge
 * of. At first checks if the classifier in the cell has already been trained,
 * if so retrieves the various inputs from the matrix of features and then
 * classify the element just built up. Hence this object is in charge of
 * training the classifier, filter the inputs and call the classify function.
 * <br>
 * We choose to represent the various features as a matrix because the result of
 * other classifiers may be seen as features for a new one. <br>
 * We can consider the CellManager as the subject while the Cell is the object.
 * 
 * @author antonio
 *
 */
public class CellManager<T> implements Callable<Integer> {
  /**
   * cell associated to the actual cell manager.
   */
  private Cell<T> cell;

  /**
   * This is matrix is composed as follows: <br>
   * at first level we have the features provided to the classificationManager
   * <br>
   * at level i the labels provided by classifiers at level i each in the
   * corresponding position. <br>
   * Please note that, as previously stated, the matrix of classifiers is
   * scanned line by line, thus simplifying the structure.
   */
  private ArrayList<ArrayList<T>> elements = null;

  /**
   * Basic constructor for the CellManager object.
   * 
   * @param cell
   *          the cell associated to this cellManager
   * @param element
   *          the element to classify
   */
  public CellManager(Cell<T> cell, ArrayList<ArrayList<T>> element) {

    /* BEGIN parameter checking */
    String isNull = (cell == null) ? "cell" : (element == null) ? "element" : "";
    if (!isNull.isEmpty()) {
      ExceptionManager.criticalBehavior("CellManager constructor parameter " + isNull + " null");
      /* END parameter checking */
    }

    this.cell = cell;
    this.elements = new ArrayList<>(element);
  }

  /**
   * Implementing method call for the callable interface. The call method
   * performs 3 operations:
   * <ol>
   * <li>checks if the classifier is already trained, if so then go to 2.
   * otherwise trains the classifier with the training set</li> <br>
   * <li>retrieves inputs from elements. The object elements is a matrix from
   * which we gather the necessary features</li> <br>
   * <li>performs the effective classification</li>
   * </ol>
   * 
   * @return the label of the passed element
   */
  @Override
  public Integer call() {
    /**
     * check if the classifier is already trained
     */
    if (!cell.getAlgorithm().isTrained()) {
      cell.getAlgorithm().train();
    }
    // select the vector to classify
    Element<T> elem = selectInput(elements);

    // perform the classify operation
    Integer label = cell.getAlgorithm().classify(elem);
    // System.out.println("label: " + label );
    return label;
  }

  /**
   * Select the inputs to give to the actual classifier.
   * 
   * @param elements
   *          the matrix of all features and of the classes provided by other
   *          classifiers
   * @return the vector of feature to provide to the actual classifier
   */
  private Element<T> selectInput(ArrayList<ArrayList<T>> elements) {
    // build up a new feature vector with the instance stored in the classifier
    Element<T> elem = new Element<T>(cell.getAlgorithm().gettInstance()) {
    };

    /**
     * for each position in the list of inputs gather the corresponding feature
     * and add it to the feature vector
     */
    for (Position position : cell.getInputs()) {
      // System.out.println(position.Y() + "\t" + position.X());
      // add and convert to the correct type the requested feature
      elem.addConvert(elements.get(position.Y()).get(position.X()).toString());
    }
    // set a dummy label
    elem.setLabel("0");
    // System.err.println("Selected inputs " + elem);

    return elem;
  }

}
