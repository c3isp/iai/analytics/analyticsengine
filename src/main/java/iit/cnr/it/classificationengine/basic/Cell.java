package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.classifiers.KNN;

/**
 * This class identifies a cell. In our vision a cell is a container that may
 * contain a classifier or a clusterer as well with some attributes as inputs
 * and position that allow the classifier to select the features it is
 * interested in. Moreover the position is needed to allow the classifier to
 * gather inputs from other classifier in the matrix. <br>
 * To identify inputs we employ the following notation: Features are identified
 * by means of (0, index) where index is the index of the feature in the passed
 * element. Outputs from other classifiers are identified by means of the
 * classifier position. <br>
 * <b>A classifier waits only for previous levels classifier.</b>
 * 
 * @author antonio
 *
 * @param <T>
 *          the template parameter
 */
public abstract class Cell<T> {
  // classifier used in this cell
  // private Algorithm classifier;
  private Algorithm<T> algorithm;
  // this cell position
  private Position position;
  // how to gather inputs to the current classifier
  private ArrayList<Position> inputs;
  // result of classification
  private int result;
  // states if the current classifier has or not a result
  private boolean hasResult = false;

  /**
   * This is the cell constructor. As said before a cell is formed by a
   * classifier, is identified by a position and
   * 
   * @param classifier
   *          the classifier to use in this cell
   * @param position
   *          the cell position in the classifier matrix
   * @param inputs
   *          how to gather the various inputs <br>
   *          The classifier parameter has to be provided as an object already
   *          built up, it is impossible otherwise to know which kind of
   *          classifier you want to have in this cell
   */
  public Cell(Algorithm<T> classifier, Position position, ArrayList<Position> inputs) {

    /* BEGIN parameter checking */
    String isNull = (classifier == null) ? "classifier"
        : (position == null) ? "position" : (inputs == null) ? "array of inputs" : "";
    if (!isNull.isEmpty()) {
      ExceptionManager.criticalBehavior("Error in cell constructor " + isNull + "is null");
    }
    if (!verifyInputs(inputs, position)) {
      ExceptionManager.criticalBehavior("Invalid list of inputs");
      /* END parameter checking */
    }

    this.algorithm = classifier;
    this.position = new Position(position);
    this.inputs = new ArrayList<>(inputs);
  }

  /**
   * Copy constructor NOTE: the two cells will share the same algorithm.
   * 
   * @param cell
   *          the cell to copy
   */
  public Cell(Cell<T> cell) {

    /* BEGIN parameter checking */
    if (cell == null) {
      ExceptionManager.criticalBehavior("Cell copy constructor with null argument");
    }
    if (cell.algorithm == null || cell.position == null || cell.inputs == null) {
      ExceptionManager.criticalBehavior("Cell copy constructor: attributes of parameter are null");
      /* END parameter checking */
    }

    position = new Position(cell.position);
    inputs = new ArrayList<>(cell.inputs);
    algorithm = cell.algorithm;
  }

  /**
   * Verifies if the list of inputs is correct for the classifier. Verifies that
   * the current classifier doesn't have to wait for a next-level or same-level
   * classifier.
   * 
   * @param inputs
   *          the array of position inputs
   * @param position
   *          the actual position of the cell
   * @return true if everything is right, false otherwise
   */
  private boolean verifyInputs(ArrayList<Position> inputs, Position position) {
    for (Position pos : inputs) {
      if (pos.Y() >= position.Y()) {
        ExceptionManager.wrongBehavior("Cell.verifyInputs error on levels for inputs");
        return false;
      }
    }
    return true;
  }

  /**
   * Instead of providing interface to public methods of cell attributes, we
   * provide getter functions that retrieve the objects so that users can
   * interact with them. All methods return a reference to the actual object
   */
  public Algorithm<T> getAlgorithm() {
    return algorithm;
  }

  /**
   * Retrieves the positions of the inputs that go inside this cell.
   * 
   * @return the list of inputs
   */
  public ArrayList<Position> getInputs() {
    return inputs;
  }

  /**
   * Retrieves the position of this cell (x, y).
   * 
   * @return the position of this cell
   */
  public Position getPosition() {
    return position;
  }

  /**
   * Checks if the cell has a result.
   * 
   * @return true if the cell has a result, false otherwise
   */
  public boolean hasResult() {
    return hasResult;
  }

  /**
   * Resets the result of this cell.
   */
  public void resetResult() {
    hasResult = false;
  }

  /**
   * Classifies a feature vector, this is the only public method provided
   * without the usage of the attribute. This is done because the actual cell
   * may need to store the result of the classification, hence
   * 
   * @param element
   *          feature vector to classify
   * @return the class or the label of the feature vector
   * @deprecated since we use Executors, Callable and Future objects
   */
  @Deprecated
  public int classify(Element<T> element) {
    result = algorithm.classify(element);
    hasResult = true;
    return result;
  }

  /**
   * main function.
   * 
   * @param args
   *          arguments
   */
  public static void main(String[] args) {

    ArrayList<Position> ciInput = new ArrayList<>();
    for (int i = 0; i < 6; i++) {
      ciInput.add(new Position(i, 0));
    }

    /*
     * ArrayList<Position> cdInput = new ArrayList<Position>(); for(int i = 6; i
     * < 8; i++) cdInput.add(new Position(i, 0));
     * 
     * ArrayList<Position> cd1Input = new ArrayList<Position>(); for(int i = 0;
     * i < 2; i++) cd1Input.add(new Position(i, 1));
     */
    // Cell<Double> cellD = new Cell<Double>(classifierDouble, new
    // Position(2, 1), cdInput) {};
    // Cell<Double> cellD1 = new Cell<Double>(classifierDouble, new
    // Position(1, 2), cd1Input) {};
    Element<Integer> e2 = new Element<Integer>(Integer.class) {
    };
    e2.addConvert("1100");
    e2.addConvert("512");
    e2.addConvert("1500");
    e2.addConvert("0");
    e2.addConvert("1");
    e2.addConvert("1");
    e2.setLabel("0");
    KNN<Integer> classifierInteger = new KNN<>("/home/antonio/file.txt", Integer.class, "\n", ",");
    Cell<Integer> cellI = new Cell<Integer>(classifierInteger, new Position(1, 1), ciInput) {
    };
    cellI.getAlgorithm().train();
    System.out.println(cellI.getAlgorithm().classify(e2));
  }
}
