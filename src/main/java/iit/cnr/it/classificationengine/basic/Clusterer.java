package iit.cnr.it.classificationengine.basic;
import java.util.ArrayList;

/**
 * This abstract class will be used to represent the unsupervised learning, 
 * aka cluster algorithms. <br>
 * The cluster is provided with an unlabeled dataset, it groups the various elements 
 * according to similarities between elements. 
 * We employ the same dataset object used with Clusterings.   
 * @author antonio
 *
 */
public abstract class Clusterer<T> implements Algorithm<T>
{
	
	//dataset used to build up the cluster
	protected DataSet<T> dataset;
	private boolean isTrained = false;
	
	/**
	 * These are auto-generated getter and setters. These are protected to avoid
	 * any class not-deriving Clustering<T> to access to these functions
	 */
	protected DataSet<T> getdataset() {
		return dataset;
	}

	protected void setdataset(DataSet<T> dataset) {
		this.dataset = dataset;
	}

	@Override
	public Class<T> gettInstance() {
		return dataset.gettInstance();
	}
	
	/**
	 * Default clustering constructor.
	 */
	public Clusterer() {}
	
	/**
	 * Constructor for the Clustering class, we provide to the Clustering
	 * the training set and the class instance to be used whenever conversions
	 * are necessary
	 * @param dataset the Set on which we train our Clustering
	 * @param tInstance an instance of the class
	 * @throws IllegalArgumentException if one of the arguments is invalid 
	 */
	public Clusterer(DataSet<T> dataset) 
			throws IllegalArgumentException
	{
		/*BEGIN parameter checking*/
		if(dataset == null)
			ExceptionManager.throwIAE("Clustering constructor DataSet object passed is null");
		/*END parameter checking*/
		
		this.dataset = dataset;
	}
	
	/**
	 * Constructor for the Clustering class, in this case it is provided to 
	 * the Clustering the path to the training set, hence the trianingSet 
	 * attribute of the class has to be built up form scratch
	 * 
	 * @param trainPathToFile is the path to the file that serves as training set
	 * @param tInstance is the actual instance of the class-type of the Clustering
	 * @param eSeparator is the string used as separator between elements of features
	 * @param fSeparator is the string used as separator between features
	 * @throws IllegalArgumentException if one of the arguments is invalid
	 */
	public Clusterer(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		/*BEGIN parameter checking*/
		if(tInstance == null)
			ExceptionManager.criticalBehavior("Clustering constructor: Instance of template class is null");
		/*END parameter checking*/
		
		//other parameters are checked also here
		//dataset = new DataSet<T>(trainPathToFile, tInstance, eSeparator, fSeparator) {};
		dataset = DataSet.buildDatasetClusterer(trainPathToFile, eSeparator, fSeparator, tInstance);
	}
 
	
	//------------------------------------------------------------------------------------------------------------------
	//abstract functions implemented by each clusterer algorithm
	//------------------------------------------------------------------------------------------------------------------
	@Override
	abstract public void train();
	@Override
	abstract public int classify(Element<T> element);
	abstract public void setParameters(ArrayList<String> a);
	
	public boolean isTrained()
	{
		return isTrained;
	}
}
