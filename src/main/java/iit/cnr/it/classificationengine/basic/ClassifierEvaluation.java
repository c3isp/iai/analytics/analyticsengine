package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This object is needed to store informations about the evaluation of the
 * classifier.
 * <p>
 * Basically all the classifiers that we implement have to be evaluated and the
 * informations about the evaluation of the classifier must be stored somewhere.
 * Instead of providing each classifier with a set of function, one for each
 * different parameter we may be interested in evaluating, we use this class
 * that takes some informations from the calssifier, and from these builds up
 * the metrics needed to evaluate it.
 * 
 * </p>
 * 
 * @author antonio
 *
 */
public class ClassifierEvaluation {
	// the confusion matrix
	int[][] confusionMatrix = null;
	// the precision
	double precision = -1;

	public ClassifierEvaluation() {

	};

	/**
	 * This is a static factory method aimed at building up a new
	 * classifierEvaluation object from the parameters passed to this function.
	 * Basically, since the parameters may differ, we choose this approach
	 * instead of providing a certain number of constructors
	 * 
	 * @param elements
	 *            the original testing set
	 * @param classification
	 *            the classification provided by the classifier for the given
	 *            testing set
	 * @param classes
	 *            the list of classes
	 * @return a Classifier evaluation object
	 */
	public static ClassifierEvaluation buildClassifierEvaluation(ArrayList<Element<Integer>> elements,
			HashMap<Integer, String> classification, ArrayList<String> classes) {
		/* BEGIN parameter checking */
		if (elements == null || elements.size() == 0) {
			ExceptionManager.criticalBehavior(
					"ClassifierEvaluation.buildClassifierEvaluation, the passed testing set is not valid");
			return null;
		}
		if (classification == null || classification.size() == 0) {
			ExceptionManager.criticalBehavior(
					"ClassifierEvaluation.buildClassifierEvaluation, the passed classification set is not valid");
			return null;
		}
		if (classes == null || classes.size() == 0) {
			ExceptionManager.criticalBehavior(
					"ClassifierEvaluation.buildClassifierEvaluation, the passed classes list is not valid");
			return null;
		}
		/* END parameter checking */

		// instantiate a new ClassifierEvaluation
		ClassifierEvaluation classifierEvaluation = new ClassifierEvaluation();

		/**
		 * Instantiate the confusion matrix and fills it with 0s
		 */
		classifierEvaluation.confusionMatrix = new int[classes.size()][classes.size()];
		for (int i = 0; i < classes.size(); i++)
			for (int j = 0; j < classes.size(); j++)
				classifierEvaluation.confusionMatrix[i][j] = 0;

		int correctedClassified = 0;

		/**
		 * For each element in the testing set compares the label that was given
		 * with the original one, if they match then the element is correctly
		 * classified, otherwise it is not. Moreover along this operation, the
		 * cells of the matrix are incremented according to the given label and
		 * the one we expected
		 */
		for (int i = 0; i < elements.size(); i++) {
			String originalLabel = elements.get(i).getLabelAsString().trim();
			String givenLabel = classification.get(elements.get(i).getFeature(0));
			//System.out.println(originalLabel + "\t" + givenLabel);
			int posOriginal = Utility.indexOf(classes, originalLabel);
			int posGiven = Utility.indexOf(classes, givenLabel);
			/**
			 * handles the case in which it was not possible to give a label to
			 * the element
			 */
			if (posOriginal != -1 && posGiven != -1) {
				classifierEvaluation.confusionMatrix[posOriginal][posGiven] += 1;
				if (originalLabel.equals(givenLabel))
					correctedClassified += 1;
			}
		}

		// computes the precision of the classifier
		classifierEvaluation.precision = (double) correctedClassified / elements.size();
		//System.out.println(classifierEvaluation.precision);
		// return the classifier evaluation object
		return classifierEvaluation;
	}

	/**
	 * retrieve the confusion matrix of this classifier evaluation object
	 * 
	 * @return the confusion matrix
	 */
	public int[][] getConfusionMatrix() {
		return confusionMatrix;
	}

	/**
	 * retrieves the precision of this classifier evaluation object
	 * 
	 * @return the precision of this classifier evaluation object
	 */
	public double getPrecision() {
		int correctlyClassified = 0;
		int elements = 0;
		for (int i = 0; i < confusionMatrix.length; i++) {
			for (int j = 0; j < confusionMatrix.length; j++) {
				if (j == i)
					correctlyClassified += confusionMatrix[i][j];
				elements += confusionMatrix[i][j];
			}
		}
		return (double) correctlyClassified / elements;
	}

	/**
	 * Sometimes (especially in foldings) we have pipes of classification whose
	 * results have to be put in common in order to achieve a global look of the
	 * classifier performance, this is why this function comes up. Basically it
	 * sums together two classification matrixes and sums the actual precision
	 * to the one of the just created classifier. If the calssifier object on
	 * which we call the add is empty, its attributes are initilized using as
	 * reference the classifierEvaluation object passed as parameter
	 * 
	 * @param classifierEvaluation
	 *            the classifierEvaluation instance we want to add to the one
	 *            under consideration
	 */
	public void add(ClassifierEvaluation classifierEvaluation) {
		/* BEGIN parameter checking */
		if (classifierEvaluation == null)
			return;
		/* END parameter checking */

		/**
		 * if parameters are not initialized, perform a deep copy of the ones of
		 * the classifierEvaluation passed as reference
		 */
		if (this.confusionMatrix == null && precision <= -1) {
			this.confusionMatrix = new int[classifierEvaluation.confusionMatrix.length][classifierEvaluation.confusionMatrix.length];
			precision = classifierEvaluation.precision;
			for (int i = 0; i < confusionMatrix.length; i++)
				for (int j = 0; j < confusionMatrix.length; j++)
					confusionMatrix[i][j] = classifierEvaluation.confusionMatrix[i][j];

		}
		/**
		 * Otherwise add the confusion matrix and the precision
		 */
		else if (confusionMatrix != null) {
			precision = (precision + classifierEvaluation.precision);
			for (int i = 0; i < confusionMatrix.length; i++)
				for (int j = 0; j < confusionMatrix.length; j++)
					confusionMatrix[i][j] += classifierEvaluation.confusionMatrix[i][j];

		}
	}

	/**
	 * False positive rate is gathered by the formula: FP / (FP + TN)
	 * 
	 * @return the list of false positive rates
	 */
	public ArrayList<Double> getFalsePositiveRate() {
		ArrayList<Double> fprPerColumn = new ArrayList<Double>();
		double fpr = 0.0;
		for (int i = 0; i < confusionMatrix.length; i++) {
			int columnAccumulator = 0;
			int accumulator = 0;
			for (int j = 0; j < confusionMatrix.length; j++) {
				if (i != j) {
					columnAccumulator += confusionMatrix[j][i];
					accumulator += confusionMatrix[j][i];
				}
			}
			for (int j = 0; j < confusionMatrix.length; j++)
				if (j != i)
					accumulator += confusionMatrix[j][j];
			fpr = (double) columnAccumulator / accumulator;
			fprPerColumn.add(fpr);
		}
		return fprPerColumn;
	}

	/**
	 * 
	 * @param matrix
	 */
	public ArrayList<Double> getTruePositiveRate() {
		ArrayList<Double> tprPerColumn = new ArrayList<Double>();
		double tpr = 0.0;
		for (int i = 0; i < confusionMatrix.length; i++) {
			int columnAccumulator = 0;
			int accumulator = 0;
			for (int j = 0; j < confusionMatrix.length; j++) {

				if (i == j)
					columnAccumulator += confusionMatrix[i][j];
				accumulator += confusionMatrix[i][j];
			}
			tpr = (double) columnAccumulator / accumulator;
			tprPerColumn.add(tpr);
		}
		return tprPerColumn;
	}

	/**
	 * 
	 * @param total
	 * @return
	 */
	public double averageWeightTP(int total) {
		ArrayList<Double> tpr = getTruePositiveRate();
		double averageWeight = 0.0;
		int sameClass = 0;
		for (int i = 0; i < tpr.size(); i++) {
			for (int j = 0; j < confusionMatrix.length; j++)
				sameClass += confusionMatrix[i][j];
			//System.out.println("Same class " + sameClass);
			averageWeight += (tpr.get(i) * sameClass);
			sameClass = 0;
		}
		return averageWeight / total;

	}

	/**
	 * 
	 * @param total
	 * @return
	 */
	public double averageWeightFP(int total) {
		ArrayList<Double> fpr = getFalsePositiveRate();
		double averageWeight = 0.0;
		int sameClass = 0;
		for (int i = 0; i < fpr.size(); i++) {
			for (int j = 0; j < confusionMatrix.length; j++)
				sameClass += confusionMatrix[i][j];
			averageWeight += (fpr.get(i) * sameClass);
			//System.out.println("Same class " + averageWeight);
			sameClass = 0;
		}
		return averageWeight / total;
	}

	public void setConfusionMatrix(int[][] matrix) {
		confusionMatrix = new int[matrix.length][matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				confusionMatrix[i][j] = matrix[i][j];
			}
		}
	}

	public void buildFromConfusionMatrix(int[][] matrix) {

	}

	public void generateROC() {

	}
}
