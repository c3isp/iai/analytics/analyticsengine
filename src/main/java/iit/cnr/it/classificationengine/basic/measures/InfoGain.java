package iit.cnr.it.classificationengine.basic.measures;

import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.core.Instances;

/**
 * Wrapper around the infogain class of weka.
 * 
 * <p>
 * The infogain class in weka is the InfoGainAttributeEval.
 * </p>
 * 
 * @author antonio
 *
 */
final public class InfoGain<T> {

	private InfoGainAttributeEval infoGainAttributeEval;

	private DataSet<T> dataset;

	public InfoGain(DataSet<T> dataset, int index) {
		this.dataset = new DataSet<T>(dataset.gettInstance()) {
		};
		this.dataset.setQualification(dataset.getQualification());
		for (Element<T> element : dataset.getElements()) {
			this.dataset.add(element);
		}
		this.dataset.removeAttribute(index);
		infoGainAttributeEval = new InfoGainAttributeEval();
	}

	public ArrayList<Double> retrieveInfoGain() throws Exception {
		// build instances
		Instances instances = WekaFactory.instancesFromDataset(dataset);
		infoGainAttributeEval.buildEvaluator(instances);

		ArrayList<Double> infoGain = new ArrayList<>();

		for (int i = 0; i < instances.numAttributes(); i++) {
			infoGain.add(infoGainAttributeEval.evaluateAttribute(i));
		}
		return infoGain;
	}

}
