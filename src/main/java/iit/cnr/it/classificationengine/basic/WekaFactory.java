package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;
import java.util.Collections;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * In weka all features are represented by means of double, Strings and Nominal
 * attributes are represented by double that are indexes to maps that contain
 * the effective String value. In our implementation of dataset, we've chosen a
 * more general approach where we ask the user to state which type of dataset
 * we're dealing with. In order to use weka classifiers, although, we need a
 * bridge from our implementation to weka one. This is why this class comes up.
 * It provides a way to convert our DataSet<T> or Element<T> Object to weka
 * Instances or Instance respectively. <br>
 * In weka, Instances represent the whole Dataset, while Instance a single
 * element. Attributes state the quality of the feature, i.e., what the feature
 * represents. In our vision the label is not considered as an attribute, while
 * in weka this happens. Hence this has to be taken into account in conversions
 * In weka the label or class is <b>MANDATORY</b> both in Instance and
 * Instances. Even Element object that haven't been classified yet need to have
 * a label. That's why, if the label is not provided, the WekaFactory object is
 * able to provide a dummy label when it's needed. <br>
 * In weka attributes may be Numeric, Nominal or String. The difference between
 * the latter two is that a Nominal attribute is an attribute which can store a
 * certain number of different strings previously declared, a String attribute
 * stores any type of String. See the manual of weka in the section dedicated to
 * API.
 * <p>
 * Instead of implementing this class as a singleton as done in the previous
 * implementation, maybe it is better to implement it as a normal class not
 * instantiable, having only static methods, in this way also the calls to this
 * object become shorter
 * </p>
 * 
 * 
 * @author antonio
 */
public class WekaFactory {
	public static final int WEIGHT = 1;

	// dummy constructor created to avoid the possible instantiation of this
	// class
	private WekaFactory() {

	}

	// ==========================================================================
	// Dataset to Instances
	// ==========================================================================
	/**
	 * This function converts the whole dataset to Instances object needed to
	 * deal with weka classifiers.
	 * 
	 * @param dataset
	 *            the dataset to convert
	 * @return the Instances object
	 * @throws IllegalArgumentException
	 *             via the ExceptionManager Class
	 */
	public static <T> Instances instancesFromDataset(DataSet<T> dataset) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (dataset == null || dataset.getNumberOfFeatures() == 0 || dataset.getNumberOfElements() < 0) {
			ExceptionManager.criticalBehavior("WekaFactory.instancesFromDataset " + "invalid dataset provided");
			return null;
		}
		/* END parameter checking */
		Class<T> clazz = dataset.gettInstance();
		// check if the dataset was built via weka
		if (dataset.getWekaInstances() != null) {
			// get the actual instances object
			Instances instances = dataset.getWekaInstances();
			// set the class index to the features corresponding to attribute
			// "class"
			if (dataset.getQualification().equals(TYPE.CLASSIFIER))
				instances.setClassIndex(instances.attribute("class").index());
			return dataset.getWekaInstances();
		}

		String pathToFile = dataset.getPathToFile();

		/**
		 * Sometimes the dataset may be built up from scratch or with an
		 * unformatted file. Here we have to do a deep copy to manage this case.
		 */
		if (pathToFile.equals("") || (!pathToFile.contains(".csv") || (!pathToFile.contains(".arff")))) {
			Instances instances = deepConversion(dataset, clazz);

			/**
			 * In case of dataset used for classification, it is mandatory to
			 * set the class index to be used in weka to train the classifier,
			 * the first checking has to be performed in order to avoid
			 * exceptions if something goes wrong
			 */
			if (instances != null && dataset.getQualification().equals(TYPE.CLASSIFIER))
				instances.setClassIndex(instances.attribute("class").index());

			return instances;
		}

		// gather the instances from the .arff file
		try {
			DataSource file = new DataSource(pathToFile);
			Instances instances = file.getDataSet();
			instances.setClassIndex(instances.numAttributes() - 1);
			return instances;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("WekaFactory: toinstances: File does not contain a valid dataset");
			return null;
		}
	}

	/**
	 * Handles a deep conversion from the dataset to the weka Instances. Before
	 * performing the conversion we assume that the attributes are set,
	 * otherwise we provide a dummy list of fake attributes att1, att2 and so
	 * on. This because, with weka dataset, attributes are mandatory, especially
	 * to specify the class attribute. <br>
	 * <p>
	 * The steps for a deep conversion are as follows:
	 * <ol>
	 * <li>Convert/Create the list of attributes according to the type of
	 * dataset we're dealing with: attributes in weka may be nominal, numeric,
	 * date and so on, the only ones we take into consideration now are the
	 * nominal (synonimous of categorical) and numeric, they have different
	 * shapes since in the case of a nominal attribute we have to specify the
	 * list of possible values</li>
	 * <li>Convert each element stored inside the dataset into weka Instance
	 * object. To accomplish this task we have created specific functions for
	 * numerical and nominal features</li>
	 * </ol>
	 * </p>
	 * 
	 * @param dataset
	 *            the dataset to convert
	 * @return the weka Instances
	 * 
	 */
	private static <T> Instances deepConversion(DataSet<T> dataset, Class<T> clazz) {
		// ---------------------------------------------------------------------
		// Creating the attribute list
		// ---------------------------------------------------------------------
		FastVector attributesElements = convertAttributesToFastVector(dataset);

		// ---------------------------------------------------------------------
		// Adding the various Instance objects
		// ---------------------------------------------------------------------
		// create an empty Instances object
		Instances instances = new Instances("name", attributesElements, 0);
		// insert Instance object into Instances
		for (Element<T> elem : dataset.getElements()) {
			// System.out.println(elem.getLabelType() + " " +
			// dataset.getLabel().get(index) + " " + label.getClass());
			Instance instance = instanceFromElement(elem, dataset.getQualification(), instances);
			instances.add(instance);
		}

		return instances;
	}

	/**
	 * This function creates the list of dummy attributes, the class is not
	 * taken into account here, since, if we have a class we have also to
	 * specify the possible values for the class unless we're dealing with a
	 * numerical class.
	 * 
	 * @param size
	 *            the number of attributes, equal to the number of features
	 * @param columnID
	 *            the columnID, if there's no column devoted to ID, then this
	 *            parameter is equal to -1
	 * @return the list of dummy attributes
	 */
	private static ArrayList<String> dummyAttributes(int size, int columnID) {
		ArrayList<String> attributes = new ArrayList<String>();
		int numAttributes = size;
		for (int i = 0; i < numAttributes; i++)
			attributes.add("att" + i);
		/* if there is a column id, then modify that attribute */
		if (columnID != -1) {
			attributes.remove(columnID);
			attributes.add(columnID, "ID");
		}
		return attributes;
	}

	/**
	 * Converts the attributes contained into the dataset object passed to this
	 * function to the FastVector format, that is the format widely accepted by
	 * weka.
	 * 
	 * @param dataset
	 *            the dataset object we want to convert
	 * @return a FastVector object containing the attributes
	 */
	private static <T> FastVector convertAttributesToFastVector(DataSet<T> dataset) {
		/**
		 * Create the fast vector structure from the dataset attributes Since we
		 * need to modify in deep the list, then we do a deep copy
		 */
		// ArrayList<String> tmp = dataset.getAttributes();
		ArrayList<String> attributes = new ArrayList<String>(dataset.getAttributes());
		// handle the case in which no attributes are provided by the dataset
		if (attributes.size() == 0)
			attributes = dummyAttributes(dataset.getNumberOfFeatures(), dataset.getColumnID());

		Class<T> clazz = dataset.gettInstance();

		/**
		 * manages the cases of a dataset that contains strings or that is
		 * categorical, in fact, in weka, in this case we have to give the
		 * possible values for each string or for each categorical attribute
		 */
		FastVector attributesElements = null;
		if (clazz.getName().equals("java.lang.String") || dataset.isCategorical()) {
			attributesElements = attributesToFastVector(attributes, true, dataset);
		} else {
			attributesElements = attributesToFastVector(attributes, false, null);
		}

		// managing Label attribute
		FastVector fv = null;
		Attribute att = null;

		/**
		 * In weka Instances, labels are <b>MANDATORY</b> only in the case in
		 * which the dataset is used for classification purposes, hence we have
		 * to check this condition before adding the labels
		 */
		if (dataset.getLabelType().equals(LABEL_TYPE.STRING)
				|| (dataset.gettInstance().getName().equals("java.lang.String")
						&& (dataset.getLabelType().equals(LABEL_TYPE.TEMPLATE)))) {
			/**
			 * Weka classifiers don't work with String labels (as far as we
			 * know) hence we need to provide the possible values of the label
			 * if we have a string label in our dataset.
			 */
			ArrayList<String> possibleLabels = dataset.getLabelDistinct();
			fv = new FastVector(possibleLabels.size());
			for (String a : possibleLabels)
				fv.addElement(a);
			att = new Attribute("class", fv);
		} else
			att = new Attribute("class");

		// the label is added if and only if the dataset is used for
		// classification
		if (dataset.getQualification().equals(TYPE.CLASSIFIER))
			attributesElements.addElement(att);

		return attributesElements;
	}

	/**
	 * Converts a ListArray<String> into a FastVector, this function is used
	 * mostly to convert list of attributes since in weka the FastVector object
	 * is used to store Attributes. To distinguish between string attributes and
	 * not-string attributes, we use the boolean string. This boolean is used
	 * because, in weka, Attributes are built in different ways if the Attribute
	 * stores a numeric value or a String value <br>
	 * <b>From the attribute, in this case, the class or label is
	 * excluded!!!!</b>
	 * 
	 * @param attributes
	 *            the list of attributes of the dataset
	 * @param categoricalORString
	 *            states if the features are of type string or not
	 * @return the constructed FastVector <br>
	 *         NOTE: in our vision, the label is NOT an attribute, while in weka
	 *         a label is one of the attributes
	 */
	private static <T> FastVector attributesToFastVector(ArrayList<String> attributes, boolean categoricalORString,
			DataSet<T> dataset) {
		// allocate a FastVector of the needed dimension
		FastVector attributesElements = new FastVector(attributes.size());
		/**
		 * if the features are of type String or not they have to be created in
		 * different ways
		 */
		// creates NOT STRING features array
		if (!categoricalORString) {
			// add all attributes names into the fastVector
			for (String a : attributes)
				// NOTE: this is how we create a numeric attribute
				attributesElements.addElement(new Attribute(a));
		}

		// manages the case in which we have String or categorical features
		else {
			int i = 0;
			for (String a : attributes) {
				// manage the case of having a possible column used for ID
				if (a.equals("ID")) {
					attributesElements.addElement(new Attribute(a));
					i++;
				} else {
					// dummy fastVector required for type-safety
					FastVector dummy = null;
					ArrayList<String> list = null;
					if (dataset != null) {
						/**
						 * checks if the dataset attribute i can be converted to
						 * categorical, if so retrieve the list of possible
						 * values, if the dataset is already in categorical
						 * format the return value is the list of features
						 * contained in column i
						 */
						list = dataset.isAttributeCategorical(i);
						/**
						 * This sorting is required only to have the weka file
						 * and the one coming from our dataset implementation to
						 * have the same feature number in the same position.
						 */
						if (list != null)
							Collections.sort(list);
						if (list == null) {
							System.out.println("List is null");
							// NOTE: this is how we create a string attribute
							dummy = null;
						}

						// handles the case of categorical features
						else {
							dummy = new FastVector(list.size());
							for (String elem : list)
								dummy.addElement(elem);
						}
						// add a new attribute
						attributesElements.addElement(new Attribute(a, dummy));
						i++;
					}
				}
			}
		}
		return attributesElements;
	}

	// ==========================================================================
	// Element to Instance
	// ==========================================================================

	/**
	 * 
	 * @param element
	 * @param qualification
	 * @param instances
	 * @return
	 */
	public static <T> Instance instanceFromElement(Element<T> element, TYPE qualification, Instances instances) {
		// BEGIN parameter checking
		if (element == null) {
			ExceptionManager.criticalBehavior("WekaFactory.instanceFromElement invalid parameters passed");
			return null;
		}
		// END parameter checking

		if (element.getInstance().getName().equals("java.lang.String"))
			return instanceFromStringElement(element, qualification, instances);
		else
			return instanceFromNumericElement(element, qualification, instances);
	}

	/**
	 * Convert a numeric element, i.e., an element containing numeric features,
	 * to instance. In this case it is provided also the attributes list, hence
	 * we qualify also the various features. The label is a separated argument
	 * to manage both String or numeric labels. <br>
	 * For numeric features only, it is possible to have an empty list of
	 * attributes in weka, hence the list of attributes is null in this case.
	 * 
	 * @param attributes
	 *            this is the list of attributes
	 * @param listFeature
	 *            the list of features
	 * @param label
	 *            the class of the element
	 * @param labelType
	 *            the type of the label @see Element<T> for possible values of
	 *            label type
	 * @param tInstance
	 *            the instance of T class to distinguish between the numeric
	 *            classes
	 * @return the created instance object
	 * 
	 */
	private static <T> Instance instanceFromNumericElement(Element<T> element, TYPE qualification,
			Instances instances) {
		/**
		 * NOTE: in weka each Instance object has a reference to an Instances
		 * object, this is done moslty for consistency reasons on attributes,
		 * this is why we have to create a dummy Instances object
		 */
		if (instances != null) {
			/**
			 * Now that we have the dataset, we can build up the instance that
			 * uses that attributes
			 */
			// empty instance with required number of attributes
			Instance data = new Instance(instances.numAttributes());
			data.setDataset(instances);

			/**
			 * Perform a conversion to double data type. As said in the header
			 * in weka everything is represented by means of double
			 */
			int index = 0;
			for (T feature : element.get()) {
				data.setValue(index, Double.parseDouble(feature.toString()));
				++index;
			}

			/**
			 * <b>MANAGE LABELS</b> Luckily we don't have to perform the
			 * conversion from String to double, since weka is perfectly able to
			 * perform this task. Hence to add the label we have only to specify
			 * the attribute which refers to the label in weka and we have to
			 * cast the label, which is a generic object to the appropriate type
			 */
			if (qualification.equals(TYPE.CLASSIFIER)) {
				Attribute attribute = instances.attribute("class");
				// use label since in that local variable we already stored the
				// dummy label
				if (element.getLabelType().equals(LABEL_TYPE.STRING))
					data.setValue(attribute, element.getLabelAsString());
				else
					data.setValue(attribute, (Double) element.getLabel());
			}
			return data;
		}

		/**
		 * in this case no attributes are provided, hence create an instance
		 * with no attributes. In this case the label may be of T type only,
		 * where T is Integer or Double
		 */
		else {
			/**
			 * The +1 is needed to prepare space for the label which is not set
			 * still. Without it an IndexOutOfBoundsException occurs
			 */
			double[] appo = null;
			if (qualification.equals(TYPE.CLUSTERING))
				appo = new double[element.get().size()];
			else
				appo = new double[element.get().size() + 1];

			/**
			 * convert the features represented as an ArrayList to the double
			 * primitive data type
			 */
			int index = 0;
			/**
			 * Adapt our data to weka. As previously said, weka requires all
			 * features to be represented as double and Instance has a
			 * constructor which takes an array of double
			 */
			for (T feature : element.get()) {
				appo[index] = Double.parseDouble(feature.toString());
				++index;
			}
			if (qualification.equals(TYPE.CLASSIFIER))
				appo[index] = (Double) element.getLabel();

			/**
			 * Since we don't have any attributes, the instance can not specify
			 * an Instances class
			 */
			Instance data = new Instance(WEIGHT, appo);
			return data;
		}
	}

	/**
	 * Function that creates a new Instance object starting from the list of
	 * features, the label of the element and the instances class used to have
	 * feature attributes. <br>
	 * For more reference see
	 * {@link #instanceFromNumericElement(ArrayList, Object, int, Instances)}
	 * 
	 * @param listFeatures
	 *            the list of features
	 * @param label
	 *            the label of the element
	 * @param instances
	 *            the instances object the new Instance refers to
	 * @param labelType
	 *            the type of the label
	 * @return the just created instance object
	 */
	private static <T> Instance instanceFromStringElement(Element<T> element, TYPE qualification, Instances instances) {
		if (instances != null)
			return stringInstanceInstances(element, qualification, instances);
		else {
			FastVector attributesElements = attributesToFastVector(
					dummyAttributes(element.size(), element.getColumnID()), true, null);

			// Create the weka class attribute
			Attribute att = null;
			FastVector fv = new FastVector(1);
			fv.addElement(element.getLabelAsString());
			att = new Attribute("class", fv);
			attributesElements.addElement(att);

			// create an empty dataset with the previous attributes
			instances = new Instances("dataset", attributesElements, 0);

			Instance data = new Instance(element.get().size());

			// insert the string along with its Attribute object
			for (int index = 0; index < element.get().size(); index++) {
				String valueName = (String) element.get().get(index);
				data.setValue((Attribute) attributesElements.elementAt(index), valueName);
			}

			// add the label value
			data.setValue(att, element.getLabelAsString());

			data.setDataset(instances);
			return data;
		}
	}

	/**
	 * 
	 * @param element
	 * @param qualification
	 * @param instances
	 * @return
	 */
	private static <T> Instance stringInstanceInstances(Element<T> element, TYPE qualification, Instances instances) {
		int index = 0;
		// empty instance with required number of attributes
		Instance data = new Instance(instances.numAttributes());
		// exclude the label from attributes
		int numAttributes = instances.numAttributes() + ((qualification.equals(TYPE.CLUSTERING)) ? 0 : -1);
		// insert the string along with its Attribute object
		for (index = 0; index < numAttributes; index++) {
			String valueName = (String) element.get().get(index);
			// System.out.println(valueName);
			data.setValue(instances.attribute(index), valueName);
		}

		// add the label
		data.setValue(instances.attribute("class"), element.getLabelAsString());

		data.setDataset(instances);
		return data;
	}

	/**
	 * Retrieves an Instances object reading the content of a formatted file
	 * 
	 * @param pathToFile
	 *            the path to the weka formatted file
	 * @return the Instances object
	 */
	public static Instances buildFromWeka(String pathToFile) {
		try {
			Instances wekaInstances;
			// initialize weka instances
			DataSource file = new DataSource(pathToFile);
			wekaInstances = file.getDataSet();
			return wekaInstances;
		} catch (Exception e) {
			System.err.println("DataSet -> Constructor from file: File does not contain a valid dataset");
			e.printStackTrace();
			return null;
		}
	}

}
