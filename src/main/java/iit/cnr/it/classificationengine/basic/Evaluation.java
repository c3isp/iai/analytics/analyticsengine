package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.lazy.KStar;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;

/**
 * This class is in charge of evaluating the performance of a classifier.
 * Performance are measured in terms of confusion matrix, ROC curve and so on.
 * 
 * @author antonio
 *
 */
public class Evaluation {
	/**
	 * Checks and returns the label used in the dataset. If labels are sparse,
	 * weka doesn't provide a confusionMatrix. In our case however, we decide to
	 * provide a confusion matrix with all the used labels
	 * 
	 * @param dataset
	 *            the dataset to verify
	 * @return the list of labels used
	 * @throws Exception
	 *             if the labels are not compliant with confusion matrix
	 */
	// TODO
	/*
	 * private static <T> ArrayList<?> checkLabel(DataSet<T> dataset,
	 * Classifier<T> classifier) throws Exception { /*If the label is of type
	 * string, then the dataset can be employed
	 */
	/*
	 * if(dataset.getLabelType() == Element.LABEL_STRING) return
	 * dataset.getLabelDistinct();
	 * 
	 * //retrieves the labels ArrayList<Integer> list = new
	 * ArrayList<Integer>(); int maxLabel = 0; for(Object o :
	 * dataset.getLabel()) { int label; //handles the case in which the label is
	 * of type double if(classifier.gettInstance().getName() ==
	 * "java.lang.Double") { Double d = (Double) o; double tmp = d; label =
	 * (int) tmp; } else label = (int) o;
	 * 
	 * //if the label is not contained in the list then add it
	 * if(!list.contains(label)) list.add(label); }
	 * 
	 * return list;
	 * 
	 * }
	 */

	/**
	 * converts an object to double, it is an utility function for indexOf
	 * 
	 * @param o
	 *            the object to convert
	 * @return the converted object
	 */
	static Object convertDouble(Object o) {
		if (o instanceof String)
			return o;
		else
			return (Double) Utility.convert(o.toString(), Double.class);
	}

	/**
	 * Forming the confusion matrix is quite straightforward. Starting from a
	 * dataset we repeatedly perform classification on already labeled elements.
	 * Once this has been done, we record in a matrix to which class elements
	 * belonging to a certain label belong to.
	 * 
	 * <br>
	 * To obtain a confusion matrix the following steps are followed:
	 * <ol>
	 * <li>classify all the elements in the dataset</li>
	 * <li>construct a matrix where at position (i, j) there are elements of
	 * class i classified as elements of class j</li>
	 * </ol>
	 * 
	 * @param dataset
	 *            the dataset to use to build up the confusion matrix, may be
	 *            the training set itself
	 * @param classifier
	 *            the classifier we're interested in measure performance
	 * @return the confusion matrix
	 */
	public static <T> int[][] confusionMatrix(DataSet<T> dataset, Classifier<T> classifier)
			throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (classifier == null)
			ExceptionManager.criticalBehavior("No classifer provided");
		if (dataset == null)
			ExceptionManager.wrongBehavior(
					classifier.getName() + ".confusionMatrix, provided dataset null, training set will be used");
		// recover from a null dataset case
		if (classifier.getTrainingSet() == null)
			ExceptionManager.criticalBehavior("KNN.confusionMatrix, null training set");
		else
			dataset = classifier.getTrainingSet();
		if (dataset.getLabelType().equals(LABEL_TYPE.NOT_PRESENT))
			ExceptionManager.throwIAE("KNN.confusionMatrix, passed dataset is unlabeled");
		/* END parameter checking */

		// at first train the classifier
		if (classifier.isTrained == false)
			classifier.train();

		// list of labels as provided by the classifier
		ArrayList<Integer> label = new ArrayList<Integer>();
		// classify each element in the dataset
		for (Element<T> element : dataset.getElements()) {
			label.add(classifier.classify(element));
		}

		ArrayList<Object> distinctLabels = dataset.getLabel();
		int numDistinctLabels = dataset.getLabelDistinct().size();
		// build up the matrix with all zeros
		int[][] confusionMatrix = new int[numDistinctLabels][numDistinctLabels];
		for (int i = 0; i < numDistinctLabels; i++) {
			for (int j = 0; j < numDistinctLabels; j++) {
				confusionMatrix[i][j] = 0;
			}
		}

		// check element classification
		for (int i = 0; i < dataset.getElements().size(); i++) {
			int x = Utility.indexOf(dataset.getLabel(i, true), distinctLabels);
			int y;
			// System.out.println(distinctLabels.get(index));
			if (dataset.getLabelType().equals(LABEL_TYPE.STRING))
				y = Utility.indexOf(distinctLabels.get(label.get(i)), distinctLabels);
			else
				y = Utility.indexOf((Integer) label.get(i), distinctLabels);
			if (x != -1 && y != -1)
				confusionMatrix[x][y]++;
		}

		// print
		// for(int i = 0; i < numDistinctLabels; i++)
		// System.out.print("\t" + dataset.getLabelDistinct().get(i) + "|\t");
		// System.out.println();
		for (int i = 0; i < numDistinctLabels; i++) {
			boolean first = true;
			for (int j = 0; j < numDistinctLabels; j++) {
				if (first) {
					// System.out.print(dataset.getLabelDistinct().get(i) + "|"
					// + confusionMatrix[i][j] + "\t");
					first = false;
				} else
					System.out.print("|" + confusionMatrix[i][j] + "\t");
			}
			System.out.print("\n------------------------------\n");
		}

		return confusionMatrix;
	}

	/**
	 * Extract metrix from the confusion matrix. These metrics are True
	 * Positive, False Positive, True Negative, False Negative and overall
	 * Accuracy
	 * 
	 * @param confusionMatrix
	 *            the confusion matrix to be used
	 * @param size
	 *            the size of the confusion matrix (only the number of rows), in
	 *            fact the confusion matrix is a square matrix.
	 * @throws IllegalArgumentException
	 *             if one of the parameters is invalid
	 */
	public static void getMetrics(int[][] confusionMatrix, int size) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (confusionMatrix == null || size <= 0)
			ExceptionManager.throwIAE("getMetrix parameters not valid");
		/* END parameter checking */

		// retrieve the overall number of elements in the dataset
		int numberOfElements = 0;
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				numberOfElements += confusionMatrix[i][j];

		// list to store the various metrics
		ArrayList<Integer> truePositive = new ArrayList<Integer>();
		ArrayList<Integer> falsePositive = new ArrayList<Integer>();
		ArrayList<Integer> trueNegative = new ArrayList<Integer>();
		ArrayList<Integer> falseNegative = new ArrayList<Integer>();

		// compute the various metrics
		for (int i = 0; i < size; i++) {
			truePositive.add(confusionMatrix[i][i]);
			int fp = 0, fn = 0;
			for (int j = 0; j < size; j++) {
				if (j == i)
					;
				else {
					fp += confusionMatrix[i][j];
					fn += confusionMatrix[j][i];
				}
			}
			falsePositive.add(fp);
			falseNegative.add(fn);
			trueNegative.add(numberOfElements - (truePositive.get(i) + falsePositive.get(i) + falseNegative.get(i)));
			System.out.println("TP: " + truePositive.get(i) + "; FP: " + falsePositive.get(i) + "; FN: "
					+ falseNegative.get(i) + ";TN: " + trueNegative.get(i));
		}

		// compute accuracy
		Integer sumTruePositive = 0, sumTrueNegative = 0, sum = 0;
		for (int i = 0; i < size; i++) {
			sumTruePositive += truePositive.get(i);
			sumTrueNegative += trueNegative.get(i);
			sum += numberOfElements;
		}
		double accuracy = (double) (sumTruePositive + sumTrueNegative) / sum;
		System.out.println("Accuracy :" + accuracy);
	}

	/* BEGIN first possibility for f-measure */

	/**
	 * Basically in this function we want to compute the F-Measure.
	 * <p>
	 * In order to compute the F-measure we need the number of items belonging
	 * to the desired clusters, and this is the first parameter where we have
	 * the list of families that belong to the desired cluster. Instead in the
	 * second parameter we need the actual configuration returned by the
	 * clustering algorithm, here we have again an HashMap in which as key we
	 * have the id of the node and as value the list of families to which the
	 * elements stored inside the node belong to. <br>
	 * <b>Example:</b> in the case of malware classification we will have as key
	 * the classes identified in the MADAM paper {Rootkit, Installer... } and as
	 * parameter the families that belong to that class. As second parameter we
	 * will have the id of the node and the list of families to which the
	 * elements present inside that node belong to
	 * </p>
	 * <p>
	 * In the following we write the formula used to compute the F-Measure for
	 * desired cluster C_i, as C'_j will be identified each resulting cluster.
	 * The intersection parameter will be identified by the letter V, instead
	 * the |*| operator is the modulus one, in the set operations this means the
	 * number of elements that form the set <br>
	 * F(i) = max_(i,j) [(2 * |C_i V C'_j|) / (|C_i| + |C'_j|)] <br>
	 * The F-measure is a value between 0 and 1
	 * </p>
	 * 
	 * @param desiredFamilies
	 * @param obtainedClusters
	 */
	public static <K, V> double fMeasure(ArrayList<V> desiredFamilies, HashMap<K, ArrayList<V>> obtainedClusters) {
		/* BEGIN parameter checking */
		if (desiredFamilies == null || obtainedClusters == null)
			ExceptionManager.throwIAE("Invalid parameters passed");
		/* END parameter checking */

		/**
		 * For each of the obtained clusters compute the F-measure and then
		 * retrieve the maximum F-measure
		 */
		double maxFmeasure = 0.0;
		for (Map.Entry<K, ArrayList<V>> entry : obtainedClusters.entrySet()) {
			double actualFmeasure = 0.0;
			/**
			 * <b>Step 1:</b> retrieve the intersection between the families
			 * belonging to the desired cluster and the ones belonging to the
			 * actual cluster
			 */
			int intersectionDimension = Utility.retrieveIntersection(desiredFamilies, entry.getValue()).size();

			/**
			 * <b>Step2: multiply by two the intersection dimension</b>
			 */
			actualFmeasure = 2 * intersectionDimension;
			/**
			 * <b>Step 3:</b>Divide for the sum of the cardinality of the two
			 * sets
			 */
			actualFmeasure = actualFmeasure / (desiredFamilies.size() + entry.getValue().size());
			/**
			 * <b>Step 4: compute the maximum</b>
			 */
			maxFmeasure = Math.max(maxFmeasure, actualFmeasure);
		}
		return maxFmeasure;
	}

	/**
	 * This function is in charge of computing the globalMeasure of the result
	 * of the clustering algorithm.
	 * <p>
	 * We pass to this function the map of the desired clusters in which we have
	 * as key the name of the class and as list the list of families belonging
	 * to that cluster, the second parameter is the map of clusters as returned
	 * by the clustering algorithm.
	 * </p>
	 * <p>
	 * The global fMeasure will be computed according to the following formula,
	 * where C_i is a cluster inside the set of desired clusters, k is the
	 * number of desired clusters <br>
	 * F = sum_{i,k}(F(i) * |C_i| / | U_(j, k) C_j |)
	 * </p>
	 * 
	 * @param desiredClusters
	 *            the map of the desiredClusters
	 * @param obtainedClusters
	 *            the map of obtained clusters
	 * @return a double representing the global F-measure
	 */
	public static <K, V> double globalFMeasure(HashMap<K, ArrayList<V>> desiredClusters,
			HashMap<K, ArrayList<V>> obtainedClusters) {
		/* BEGIN parameter checking */
		if (desiredClusters == null || obtainedClusters == null)
			ExceptionManager.throwIAE("globalFMeasure: One of the passed parameters is null");
		/* END parameter checking */

		double globalFmeasure = 0.0;

		int globalSize = 0;
		double tmp = 0.0;
		for (Map.Entry<K, ArrayList<V>> entry : desiredClusters.entrySet()) {
			globalSize += entry.getValue().size();
			System.out.println("GlobalSize: " + globalSize);
		}
		for (Map.Entry<K, ArrayList<V>> entry : desiredClusters.entrySet()) {
			double actual = fMeasure(entry.getValue(), obtainedClusters);
			System.out.println("Actual : " + actual);
			actual = actual * entry.getValue().size() / globalSize;
			tmp += actual * entry.getValue().size();
			// System.out.println("Actual after division: " + actual + " " +
			// entry.getValue().size());
			globalFmeasure += actual;
			// System.out.println("FMEASURE: " + globalFmeasure);
		}
		System.out.println("TMP: " + tmp + "FM: " + globalFmeasure);

		return globalFmeasure;
	}

	/* END first possibility for fmeasure */

	/* BEGIN second possibility for fmeasure */

	/**
	 * This function is the second possibility for computing the F-measure. In
	 * this case we have the name of the desired clustering with the number of
	 * items that belong to it and the set of clustering returned by our
	 * clustering algorithm already classified. For more informations see the
	 * above function <br>
	 * Basically the main differnece between this shape and the other is that in
	 * this case the obtained clusters parameter has all the elements already
	 * labelled with the name of the class and it is exaclty this name that form
	 * the list of String, in the other it was given to the obtained cluster the
	 * family to which each element belonged to
	 * 
	 * @param clusterName
	 *            the name of the desired cluster
	 * @param clusterSize
	 *            the size of the desired cluster
	 * @param obtainedClusters
	 *            the set of clusters returned by the algorithm
	 */
	public static double fMeasure(String clusterName, int clusterSize,
			HashMap<Integer, ArrayList<String>> obtainedClusters) {
		// System.out.println("Fmeasure of: " + clusterName);

		/* BEGIN parameter checking */
		if (obtainedClusters == null)
			ExceptionManager.throwIAE("fMeasure second shape the obtaiendClustersSet is null");
		/* END parameter checking */

		double fMeasure = 0.0;
		/**
		 * for each obtained cluster compute the f-measure according to the
		 * formula seen above
		 */
		for (Map.Entry<Integer, ArrayList<String>> entry : obtainedClusters.entrySet()) {
			double actualMeasure = 0.0;
			/**
			 * instead of doing a really intersection, here it is sufficient to
			 * count the number of occurrences that have the same name of
			 * clusterName
			 */
			double intersection = Utility.countOccurrences(clusterName, entry.getValue());
			actualMeasure = 2 * intersection / (clusterSize + entry.getValue().size());
			// System.out.println(actualMeasure + "\t" + intersection + "\t" +
			// (clusterSize + entry.getValue().size()));
			fMeasure = Math.max(actualMeasure, fMeasure);
		}
		// if(fMeasure != 0)
		// System.out.println(clusterName + " FMeasure: " + fMeasure + " " +
		// clusterSize);
		return fMeasure;
	}

	/**
	 * This is a function to compute the globl fmeasure, different from the
	 * other one, basically they use the same formula but they use different
	 * sets of parameters
	 * 
	 * @param globalSize
	 *            the global size of the elements we're measuring
	 * @param desiredClusters
	 *            the desired clusters, a string representing the label and an
	 *            integer representin how many elements belong to each label
	 * @param obtainedClusters
	 *            the clusters obtained by the algorithm
	 * @return a double representing the global F-measure
	 */
	public static double globalFMeasure(int globalSize, HashMap<String, Integer> desiredClusters,
			HashMap<Integer, ArrayList<String>> obtainedClusters) {
		/* BEGIN parameter checking */
		if (desiredClusters == null || obtainedClusters == null || globalSize == 0)
			ExceptionManager.throwIAE("globlaFMeasure second shape parameters are null");
		/* END parameter checking */

		// System.out.println(globalSize);

		double globalFMeasure = 0.0;

		for (Map.Entry<String, Integer> entry : desiredClusters.entrySet()) {
			double actualValue = fMeasure(entry.getKey(), entry.getValue(), obtainedClusters);
			// System.out.println("Actual Value: " + actualValue);
			if (actualValue != 0) {
				actualValue = actualValue * entry.getValue();
				// actualValue = actualValue / globalSize;
				globalFMeasure += actualValue;
				// System.out.println(globalFMeasure);
			}
		}
		return globalFMeasure / globalSize;
	}

	/**
	 * Computes the globa external purity of the cluster.
	 * <p>
	 * By globalExternalPurity we mean the following: once all the elements have
	 * been labeled, and we have computed the external purity for each node (see
	 * the following function) we average the result by the number of elements
	 * present in each node so that to have an idea of how much similars are the
	 * elements inside the cluster
	 * </p>
	 * 
	 * @return the global external purity a value between 0 and 1
	 */
	public static double globalExternalPurity(HashMap<Integer, ArrayList<String>> nodeLabels) {
		/* BEGIN parameter checking */
		if (nodeLabels == null)
			ExceptionManager.throwIAE("Argument of externalPurity is null");
		/* END parameter checking */

		// global values used to compute the average
		int globalSize = 0;
		double globalExternalPurity = 0.0;

		// for each node that has been labeled
		for (Map.Entry<Integer, ArrayList<String>> entry : nodeLabels.entrySet()) {
			// compute the node external purity
			// System.out.print(entry.getKey() + "\t" + entry.getValue());
			double nodeExternalPurity = nodeExternalPurity(entry.getValue());
			// System.out.println("\t" + nodeExternalPurity + "\n");
			// weight the node external purity with the number of elements
			// inside the node
			globalExternalPurity += nodeExternalPurity * entry.getValue().size();
			globalSize += entry.getValue().size();
		}
		// return the average
		// System.out.println("Sum of ext purity: " + globalExternalPurity + "
		// GS: " + globalSize);
		return globalExternalPurity / globalSize;
	}

	/**
	 * This function computes the nodeExternalPurity.
	 * <p>
	 * Basically once we have all the elements inside a node labeled, we want to
	 * measure how much the label that occurs the most in the cluster represents
	 * the whole cluster. That is why we at first compute the label that occurs
	 * the most, then we divide the number of times that label occurs by the
	 * total number of elements in the cluster.
	 * </p>
	 * 
	 * @param labels
	 *            the labels inside a node
	 * @return the percentage of the most occurring label with respect to the
	 *         number of elements present in the node
	 */
	public static double nodeExternalPurity(ArrayList<String> labels) {
		HashMap<String, Integer> labelOccurrences = new HashMap<String, Integer>();

		int maxOccurrences = 1;

		// for each label
		for (String s : labels) {
			// update if needed the number of times it occurs
			if (labelOccurrences.containsKey(s)) {
				Integer occurrence = labelOccurrences.get(s);
				occurrence += 1;
				maxOccurrences = (occurrence > maxOccurrences) ? occurrence : maxOccurrences;
				labelOccurrences.put(s, occurrence);
			}
			// or insert it if it was not present
			else
				labelOccurrences.put(s, 1);
		}
		// System.out.print(labels.toString() + "\tmaxOcc: " + maxOccurrences);
		/**
		 * Divide the number of times the label that occurs the most appears by
		 * the total number of elements in the cluster
		 */
		double result = (double) maxOccurrences / labels.size();
		// System.out.print("\t" + labels.toString() + "Ext purity: " + result +
		// "\n");
		// System.out.print("\tresult: " + result + "\n");
		return result;
	}

	/**
	 * Performes cross validation on a certain training set using the algorithm
	 * provided as parameter. This code was generated looking at <a href=
	 * "https://weka.wikispaces.com/Generating+cross-validation+folds+%28Java+approach%29">
	 * weka guide</a>
	 * 
	 * @param algorithm
	 *            the algorithm to be used to evaluate the training set
	 * @param trainingSet
	 *            the provided training set to be divided
	 * @return a dummy double
	 * @throws Exception
	 *             the exception that may be thrown by weka
	 */
	public static double retrieveCrossValidation(String algorithm, Instances trainingSet) throws Exception {
		int seed = 23;
		int folds = 10;
		Random rand = new Random(seed);
		trainingSet.randomize(rand);
		if (trainingSet.classAttribute().isNominal())
			trainingSet.stratify(folds);

		weka.classifiers.Evaluation wekaEvaluation = new weka.classifiers.Evaluation(trainingSet);

		if (algorithm.equals("C45")) {
			J48 j48 = new J48();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				J48 j48Copy = (J48) J48.makeCopy(j48);
				j48Copy.buildClassifier(train);
				wekaEvaluation.evaluateModel(j48Copy, test);
			}
			System.out.println("C45");
			System.out.println(wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("BayesNaive")) {
			NaiveBayes naiveBayes = new NaiveBayes();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				NaiveBayes naiveBayesCopy = (NaiveBayes) NaiveBayes.makeCopy(naiveBayes);
				naiveBayesCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(naiveBayesCopy, test);
			}
			System.out.println("BayesNaive");
			System.out.println(wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("KNN")) {
			IBk ibk = new IBk();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				IBk ibkCopy = (IBk) IBk.makeCopy(ibk);
				ibkCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(ibkCopy, test);
			}
			System.out.println("KNN");
			System.out.println(wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("RandomTreeForest")) {
			RandomForest randomForest = new RandomForest();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				RandomForest randomForestCopy = (RandomForest) RandomForest.makeCopy(randomForest);
				randomForestCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(randomForestCopy, test);
			}
			System.out.println("RandomForest\n"
					+ wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("SVM")) {
			LibSVM svm = new LibSVM();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				LibSVM svmCopy = (LibSVM) LibSVM.makeCopy(svm);
				svmCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(svmCopy, test);
			}
			System.out.println(
					"SVM\n" + wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("BayesNet")) {
			BayesNet bayesNet = new BayesNet();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				BayesNet bayesNetCopy = (BayesNet) BayesNet.makeCopy(bayesNet);
				bayesNetCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(bayesNetCopy, test);
			}
			System.out.println("BayesNet\n"
					+ wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("MultiLayerPerceptron")) {
			MultilayerPerceptron mlp = new MultilayerPerceptron();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				MultilayerPerceptron mlpCopy = (MultilayerPerceptron) MultilayerPerceptron.makeCopy(mlp);
				mlpCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(mlpCopy, test);
			}
			System.out.println("MultilayerPerceptron\n"
					+ wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}

		if (algorithm.equals("KStar")) {
			KStar kstar = new KStar();
			for (int i = 0; i < folds; i++) {
				Instances train = trainingSet.trainCV(folds, i);
				Instances test = trainingSet.testCV(folds, i);
				KStar kstarCopy = (KStar) KStar.makeCopy(kstar);
				kstarCopy.buildClassifier(train);
				wekaEvaluation.evaluateModel(kstarCopy, test);
			}
			System.out.println(
					"Kstar\n" + wekaEvaluation.toSummaryString("=== " + folds + "-fold Cross-validation ===", false));
		}
		return 0.0;
	}

}
