package iit.cnr.it.classificationengine.basic.classifiers;
import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.Classifier;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This class is a wrapper around the weka NaiveBayes class. Basically we exploit
 * functions already implemented in weka to enlarge the classifier to be used
 * also with not-weka file format and types. 
 * In order to use the weka functions, the wekaFactory class has to be called
 * to convert Dataset to Instances and Element to instance
 * @author antonio
 * <br>
 * NOTE: this type of classifier requires a NOMINAL type of label, hence 
 * the dataset MUST have labels of type String, if the file format provided is
 * .arff then the attribute MUST be declared, if the dataset is in txt format 
 * then it is sufficient that labels are of type string.
 *
 * @param <T>
 */
public class BayesNaive<T extends Comparable<T>> extends Classifier<T>
{
	
	/**
	 * This is the training set provided to the classifier converted to instances,
	 * this is done to avoid alway converting whenever not necessary
	 */
	Instances TrainingSet;
	
	/**
	 * This is the effective weka classifier
	 */
	NaiveBayes naiveBayes;
	
	/**
	 * In Bayes classifiers we don't have a specific class, but a probability 
	 * that the element belongs to a particular class. In order to not change 
	 * the structure of our classifiers, the vector of probability is stored
	 * per se, while the return value of the classifier is the index of the class
	 * with the higher probability 
	 */
	double[] membershipProbabilities;
	
	/**
	 * return the NaiveBayes classifier. This is done for two reasons: it avoids us
	 * from wrapping each possible weka functions, thus, if weka adds 
	 * functions to the NaiveBayes, our implementation has not to be modified; moreover
	 * allows users to interact with all the functions of NaiveBayes.  
	 * @return the NaiveBayes classifier used
	 */
	public NaiveBayes getClassifier() 
	{
		return naiveBayes;
	}
	
	/**
	 * Retrieve the name of the classifier
	 * @return the name of the classifier
	 */
	@Override
	public String getName()
	{
		return "BayesNaive";
	}
	
	/**
	 * Constructor for NaiveBayes classifier, this builds up an empty classifier. 
	 * Of course the tInstance is mandatory and MUST be provided
	 * @throws IllegalArgumentException if the tInstance parameter is null
	 */
	public BayesNaive(Class<T> tInstance) throws IllegalArgumentException
	{
		super(tInstance);
	}
	
	/**
	 * Constructor for a BayesNaive with an already existing dataset and the template
	 * class instance declared
	 * @param trainingSet is the dataset containing the training set
	 * @param tInstance is the instance of the template class
	 * @throws IllegalArgumentException if one of the argument is invalid
	 */
	public BayesNaive(DataSet<T> trainingSet, Class<T> tInstance) 
			throws IllegalArgumentException
	{
		//constructs the base-class
		super(trainingSet, tInstance);
		
		//initialize the Instances object
		TrainingSet = WekaFactory.instancesFromDataset(trainingSet);
		//System.out.println(TrainingSet);
		//sets the class index
		TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		
		//constructs the classifier
		naiveBayes = new NaiveBayes();
	}
	
	/**
	 * Constructor for NaiveBayes classifier , in this case it is provided to 
	 * the classifier the path to the training set, hence the trianingSet 
	 * attribute of the class has to be built up form scratch
	 * 
	 * 
	 * @param trainPathToFile is the path to the file that serves as training set
	 * @param tInstance is the actual instance of the class-type of the classifier
	 * @param eSeparator is the string used as separator between elements of features
	 * @param fSeparator is the string used as separator between features
	 */
	public BayesNaive(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		//construct the base class
		super(trainPathToFile, tInstance, eSeparator,fSeparator);
		
		//initialize the weka Instances object attribute
		TrainingSet = WekaFactory.instancesFromDataset(this.getTrainingSet());
		
		naiveBayes = new NaiveBayes();
	}
	
	/**
	 * Trains the classifier, uses the TrainingSet object. This function has to be <b>always</b> called before the 
	 * classify function.   
	 */
	@Override
	public void train()
	{
		//System.err.println("Training... ");
		//TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
		try
		{
			naiveBayes.buildClassifier(TrainingSet);
			isTrained = true;
		}
		catch(weka.core.UnsupportedAttributeTypeException wekaUate)
		{
			ExceptionManager.criticalBehavior(wekaUate.getMessage());
		}
		//weka exception caught
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Exception caught in weka " + e.getCause() );
			e.printStackTrace();
		}
	}
	
	/**
	 * Update the classifier with the given element if possible
	 * @param element the element to add to update the classifier
	 */ 
	public void updateClassifier(Element<T> element)
	{
		/*BEGIN parameter checking*/
		if(element == null)
			ExceptionManager.throwIAE("BayesNaive.updateClassifier the parameter "
					+ "passed is null");
		/*END parameter checking*/
		
		Instance instance = WekaFactory.instanceFromElement(element, element.getQualification(), null);
		//uodate the classifier
		try 
		{
			naiveBayes.updateClassifier(instance);
		} 
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("NaiveBayes.updateClassifier, "
					+ "the element is invalid.");
			e.printStackTrace();
		}
	}
	
	/**
	 * Classifies a given element, in weka a classifier has to implement 
	 * either the classify function or the distribution for instance, while the 
	 * abstract class Classifier is in charge of retrieving the class via the
	 * classifyInstance method.
	 * @param element the element to classify
	 * @return the class of the element
	 * @throws IllegalArgumentException if the passed argument is not valid
	 */
	@Override
	public int classify(Element<T> element)
	{
		/*BEGIN parameter checking*/
		if(element == null)
			ExceptionManager.throwIAE("BayesNaive.classify: impossible to classify a null Element");
		if(element.size() == 0)
			ExceptionManager.throwIAE("BayesNaive.classify: impossible to classify an empty Element");
		/*END parameter checking*/
		
		Instance instance = WekaFactory.instanceFromElement(element, element.getQualification(), null);
		//element.print();
		//System.out.println("Instance: " + instance);
		instance.setDataset(TrainingSet);
		
		double distribution;
		try
		{
			distribution = naiveBayes.classifyInstance(instance);
			return (int) distribution;
		}
		catch(Exception e)
		{
			ExceptionManager.wrongBehavior("BayesNaive.classify: Exception caught in weka " + e.getCause() );
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * The list of parameters provided to the BayesNaive classifier, resembles the 
	 * one provided to the weka NaiveBayes. Available parameters are as following:
	 * <pre> -K
	 *  Use kernel density estimator rather than normal
	 *  distribution for numeric attributes</pre>
	 * 
	 * <pre> -D
	 *  Use supervised discretization to process numeric attributes
	 * </pre>
	 *
	 * <pre> -O
	 *  Display model in old format (good when there are many classes)
	 * </pre>
	 * 
	 * @param parameters the list of parameters
	 */
	@Override
	public void setParameters(ArrayList<String> parameters)
	{
		/*BEGIN parameter checking*/
		if(parameters == null)
			ExceptionManager.throwIAE("BayesNaive.setParameters the list of parameters is null");
		if(parameters.size() == 0)
			ExceptionManager.throwIAE("BayesNaive.setParameters the list of parameters is empty");
		/*END parameter checking*/
		
		try 
		{
			//set required options
			String[] parametersArray = new String[parameters.size()];
			for(int i = 0; i < parameters.size(); i++)
				parametersArray[i] = parameters.get(i);
			naiveBayes.setOptions(parametersArray);
		}
		//manage possible exception due to unsupported option
		catch (Exception e) 
		{
			ExceptionManager.wrongBehavior("Unsupported parameter" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public double[] retrieveBelongness(Element<T> elem) throws Exception 
	{
		Instance instance = WekaFactory.instanceFromElement(elem, elem.getQualification(), null);
		instance.setDataset(TrainingSet);
		return naiveBayes.distributionForInstance(instance);
	}
		
}
