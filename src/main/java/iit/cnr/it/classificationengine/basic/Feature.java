package iit.cnr.it.classificationengine.basic;
/*
 * Copyright notes go here
 */

//package visibility?

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * This class represents the features that are considered as the input to our 
 * classification engine. 
 * This is a <b>generic</b> class since it is up to providers to gather the 
 * features and to provide them to the classification engine. This is a generic 
 * class because it's not possible to know 'a priori' the type of features 
 * provided to the Classification Engine.  
 * Since it is possible to build up features in many different ways we provide
 * many constructors to build up the set of features.
 * 
 * As assumption the last element read will be considered as the label, however
 * we give the user the possibility to specify which is the label via some functions
 * 
 * @author antonio
 * @see ArrayList
 * 
 * @deprecated
 *
 */ 

public abstract class Feature<T> {
	
	public static final int LABEL_STRING = 1;
	public static final int LABEL_T = 2;
	public static final int NO_LABEL = 0;
	public static final int LABEL_ERROR = -1;
	
	
	/**
	 * In our vision features are represented as a vector which may contain 
	 * different types of element. However all elements contained into a feature
	 * vector must have the same type
	 */
	private ArrayList<T> featureVector = null;
	
	
	/**
	 * Instance of class template. 
	 * This is required to perform the cast from the read String to the Feature
	 * actual type.  
	 */
	private Class<T> tInstance;
	
	/**
	 * Contains the attributes types for the selected feature, i.e., the quality
	 * of the elements in the feature.
	 */
	private ArrayList<String> attributes = null;
	
	/**
	 * For the label we have two possibilities, one is to have a String label, 
	 * with the features of another type, or a Label that is of the same type of
	 * the features
	 */
	private String labelValue = "";
	private T label = null;
	
	/**
	 * This is the default constructor which initializes an empty feature vector
	 * of the type required by the provider. This vector will be filled next.
	 * The type of feature MUST be declared however
	 * 
	 *  @param tInstance instance of class e.g. String.class
	 */
	public Feature(Class<T> tInstance)
	{
		featureVector = new ArrayList<T>();
		attributes = new ArrayList<String>();
		this.tInstance = tInstance; 
	}
	
	/**
	 * This constructor builds up a Feature class from an already existing 
	 * ArrayList of features. To avoid possible misbehaviors here we do a "deep" 
	 * copy, hence we build up a new ArrayList class containing the elements of
	 * the parameter passed to the constructor.
	 * <b> In this case only the features are provided. </b>  
	 * @param array the ArrayList containing the features we want to insert in the 
	 * new class
	 * @param tInstance instance of class  
	 */
	public Feature(ArrayList<T> array, Class<T> tInstance)
	{
		if(array == null)
		{
			System.err.println("Null parameter passed");
			return;
		}
		this.tInstance = tInstance;
		featureVector = new ArrayList<T>(array);
		attributes = new ArrayList<String>();
	}
	
	/**
	 * This constructor builds a new Feature class starting form a primitive
	 * array. Also here the label is not provided
	 * @param array the pointer to the array
	 * @param size the size of the array
	 * @param tInstance instance of class
	 */
	public Feature(T array[], int size, Class<T> tInstance)
	{
		this.tInstance = tInstance;
		boolean result;
		featureVector = new ArrayList<T>();
		attributes = new ArrayList<String>();
		
		//parameter checking
		if(size <= 0 || array == null)
		{
			System.err.println("Invalid parameter passed, empty structure will be created");
		}
		
		//insert elements in the vector in the set of features
		else
		{
			for(int i = 0; i < size; i++)
			{
				result = featureVector.add(array[i]);
				
				//handle invalid insert in ListArray
				if(result == false)
				{
					featureVector.clear();
					System.err.println("Error in inserting element");
					return;
				}
			}
		}
	}
	
	/**
	 * This function handles the conversion from a String to a Template type T
	 * basically we check the type of the instance of the generic class 
	 * tInstance and, basing on its type we behave accordingly. 
	 * 
	 * NOTE: We expect any user defined type to implement the method fromString()
	 * to this aim we provide the baseClass UserFeature and we oblige user classes
	 * to extend the base one
	 * @param from the string to be converted
	 * @return the template element 
	 * 
	 * @throws NumberFormatException
	 * TODO extend user defined type conversion
	 */
	public static <T> T convert(String from, Class<T> tInstance)
	throws NumberFormatException
	{
		
		String s = tInstance.getName().toString();
		
		if(from.equals(""))
			return tInstance.cast(null);
		
		//try the possible cast conversions
		try
		{
			/**
			 * if we have read a string and the feature type is string then do a
			 * dummy cast for type-safe
			 */
			if (s.equals("java.lang.String"))
			{
				return tInstance.cast(from);
			}

			/**
			 * If we have a string but the desired value is an Integer than we
			 * parse the string to obtain an integer and then perform a dummy cast
			 * for type-safe
			 */
			if( s.equals("java.lang.Integer"))
			{
				return tInstance.cast(Integer.parseInt(from));
			}

			/**
			 * Same written for integer holds also for Double and Boolean
			 */
			if(s.equals("java.lang.Double"))
			{
				return tInstance.cast(Double.parseDouble(from));
			}

			//do some testing in order to check the appropriate type
			if(s.equals("UserFeature"))
			{
				return tInstance.cast(UserFeature.fromString(from));
			}
		}
		catch(ClassCastException cce)
		{
			System.err.println("Cast: check file format and type used" + from);
			return null;
		}
		/**
		 * In this case we throw an exception since the considered String may 
		 * store the label, hence we need to manage this possibility 
		 */
		catch(NumberFormatException nfe)
		{
			throw new NumberFormatException("Invalid number format");
		}
		catch(NullPointerException npe)
		{
			System.err.println("Cast: read string is null" + from);
			return null;
		}
		//in case no type found
		return null;
	}
	
	/**
	 * Read a single line from a file. 
	 * @param pathToFile the path to the file
	 * @return the line read if everything goes ok
	 */
	private String readFileLine(String pathToFile)
	{
		BufferedReader bufferedReader = null;
		try
		{
			File f = new File(pathToFile);
			if(bufferedReader == null)
				bufferedReader = new BufferedReader(new FileReader(f));
			String line = bufferedReader.readLine();
			return line;
		}
		
		//file not found
		catch (FileNotFoundException e) 
		{
			System.err.println("Feature -> Constructor from file: File NOT found" + pathToFile);
		}
		//file not suitable, no newline found
		catch (IOException e) 
		{
			System.err.println("Feature -> Constructor from file: I/O error occured, feature vector not initialized");
			featureVector.clear();
		}
		
		//closing streams
		finally
		{
			try
			{
				bufferedReader.close();
			}
			catch(IOException e)
			{
				System.err.println("Illegal operation" + "bufferedReader.close()");
			}
		}
		return null;
	}
	
	/**
	 * This constructor builds up a new feature reading file content. We assume
	 * that a file contains ONLY one feature. 
	 * Here is assumed that feature elements are separated via an arbitrary
	 * character while the feature stops when reading a newline.
	 * 
	 * <b>We assume that the label is the last "feature" </b>
	 * 
	 * @param pathToFile the absolute path to the file
	 * @param tInstance instance of the actual class type
	 * @param separator the char used to separate features
	 */
	public Feature(String pathToFile, Class<T> tInstance, String separator)
	{
		this.tInstance = tInstance;
		featureVector = new ArrayList<T>();
		attributes = new ArrayList<String>();
		
		/*Parameter checking*/
		if(separator == null || pathToFile == null)
		{
			System.err.println("Null parameter passed");
			return;
		}
		if(separator.equals("\n") || separator.equals("") || separator.equals('.'))
		{
			System.err.println("Invalid separator used");
			return;
		}
		
		//read a line from a file
		String fileLine = readFileLine(pathToFile);
		
		if(fileLine == null)
		{
			System.err.println("FileLine is null an error occurred reading the "
					+ "line from file the element is left uninitialized");
			return;
		}
		
		//effective build of the Feature object
		parseAndFill(fileLine, separator);
	}
	
	/**
	 * Constructor of a Feature object starting from a String containing all 
	 * the features plus the label. 
	 * @param vector contains all the features plus the label (possibly the last feature)
	 * @param separator the separator between features
	 * @return true if everything goes ok, false otherwise
	 */
	public boolean parseAndFill(String vector, String separator)
	{
		/*Parameter checking*/
		if(vector == null || separator == null)
		{
			System.err.println("null parameters passed");
			return false;
		}
		if(vector.isEmpty() || separator.isEmpty())
		{
			System.err.println("Empty parameters passed");
			return false;
		}
		
		String[] content = vector.split(separator);
		
		/**
		 * at this point we have split the line, thus we have to perform
		 * the required cast to put elements into ArrayList 
		 */
		for(String element : content)
		{
			//if the addConvert function fails, then reset everything and tell
			if(this.addConvert(element) != true)
			{
				System.err.println("Something wrong to the passed vector of "
						+ "features " + vector + " Vector left uninitialized");
				this.clear();
				return false;
			}
		}
		/**
		 * At this point from the String we've read from the file we extract the 
		 * label. This is done ONLY if the label is not stored as a String
		 * in the labelValue. By assumption the label is the last "feature". 
		 * Since we store the label in a different place, we have also to 
		 * remove it from features
		 */
		if(labelValue.equals(""))
		{
			try
			{
				label = featureVector.get(featureVector.size() - 1);
				featureVector.remove(featureVector.size() - 1);
			}
			catch(IndexOutOfBoundsException ioobe)
			{
				System.err.println("Something wrong happened in accessing the "
						+ "feature vector, the Feature Object will be left "
						+ "uninitialized");
				clear();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Add a new feature element converting it from string
	 * @param from the string containing the element
	 * @return true if the add is successful
	 */
	public boolean addConvert(String from)
	{
		try
		{
			//convert the read String to a T object
			T feature = convert(from, tInstance);
			if(feature == null)
			{
				System.err.println("Feature: Invalid element read for addConver" + from);
				return false;
			}
			//add the converted feature to the vector
			return featureVector.add(feature);
		}
		/**
		 * The convert function may throw a number format exception ,this means 
		 * that the features are of a numeric type e.g. Integer or Double, while
		 * the label is of type String. If there are more than 2 strings in a 
		 * numeric feature vector, hence the dataset contains something wrong.
		 */
		catch(NumberFormatException nfe)
		{
			/**
			 * If the read string was not in a numeric format, then this
			 * may be the label we were expecting
			 */
			if(labelValue.equals(""))
			{
				labelValue = from;
				return true;
			}
			else
			{
				System.err.println("Feature: invalid numeric "
						+ "format" + labelValue + " " + from);
				labelValue = "";
				return false;
			}
		}
	}
	/**
	 * Allows other classes to gather a reference to the feature vector
	 * @return a copy of the actual feature vector
	 */
	public ArrayList<T> get()
	{
		return new ArrayList<T>(featureVector);
	}
	
	/**
	 * Clears the class content
	 *  @return true if the clear was successful, false otherwise
	 */
	public boolean clear() 
	{
		if(featureVector == null)
		{
			featureVector = new ArrayList<T>();
			return true;
		}
		featureVector.clear();
		label = null;
		labelValue = "";
		return (featureVector.size() == 0 ? true : false);
	}
	
	/**
	 * Provides the possibility to insert a new element in the set of features
	 * @param elem the feature to insert
	 * @return true if the insert was successful, false otherwise
	 */
	public boolean insertElement(T elem)
	{
		if(featureVector == null)
			featureVector = new ArrayList<T>();
		return featureVector.add(elem);
	}
	
	/**
	 * Changes the value of a single element in the set of features
	 * @param elem the feature to insert
	 * @param index the index of the feature to change
	 * @return true if everything goes ok, false otherwise
	 */
	public boolean changeElement(T elem, int index)
	{
		if(featureVector == null)
		{
			System.err.println("Feature -> changeElement empty feature");
			return false;
		}
		try
		{
			featureVector.remove(index);
			featureVector.add(index, elem);
			return true;
		}
		catch(IndexOutOfBoundsException ioobe)
		{
			System.err.println("Invalid index " + index + "Feature vector size is: " + featureVector.size() );
			return false;
		}
	}
	
	/**
	 * Swaps the already existing feature vector with the passed one
	 * @param arrayList the new arrayList to be used
	 * @return true if the operation succeeds, false otherwise
	 */
	public boolean set(ArrayList<T> arrayList)
	{
		//clear the previous feature vector
		featureVector.clear();
		boolean result = true;
		
		//copy the provided feature vector into the old one
		for(T elem : arrayList)
		{
			result = featureVector.add(elem);
			if(result == false)
			{
				featureVector.clear();
				System.out.println("feature Vector cleared");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Append a new ArrayList of feature to the old one. 
	 * NOTE: ArrayList type must be the same
	 * @param arrayList ArrayList to append
	 * @return true if the append goes ok
	 */
	public boolean append(ArrayList<T> arrayList)
	{
		boolean result;
		for(T elem : arrayList)
		{
			result = featureVector.add(elem);
			if(result == false)
			{
				System.err.println("Invalid append");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Append a feature vector  to the existing one
	 * @param feature the feature to append
	 */
	public boolean append(Feature<T> feature)
	{
		return append(feature.get());
	}
	
	/**
	 * gather the feature at a certain position
	 * @param index position of the feature
	 * @return the feature 
	 */
	public T get(int index)
	{
		if(index < 0 || index > featureVector.size())
		{
			System.err.println("Invalid position");
			return null;
		}
		
		try
		{
			return featureVector.get(index);
		}
		catch(IndexOutOfBoundsException ioobe)
		{
			System.err.println("Feature -> get: index out of bounds");
			return null;
		}
	}
	
	/**
	 * Auto-generated getter and setter for the attributes attribute of the 
	 * feature class
	 */
	public ArrayList<String> getAttributes() 
	{
		return attributes;
	}

	public void setAttributes(ArrayList<String> attributes) 
	{
		this.attributes = attributes;
	}
	
	/**
	 * Insert a single attribute name
	 * @param name the name of the attribute
	 * @return true if everything goes ok, false otherwise
	 */
	public boolean addAttribute(String name)
	{
		if(name.isEmpty() || name == null)
		{
			System.err.println("Invalid attribute name chosen " + name );
		}
		return attributes.add(name);
	}
	
	/**
	 * Prints the feature vector, used mostly to debug and to verify 
	 * class behavior
	 */
	public void print()
	{
		if(featureVector == null)
		{
			System.err.println("Null vector of features");
		}
		for(T elem : featureVector)
		{
			System.out.println("Element: " + elem);
		}
		if(getLabelType() != NO_LABEL)
			System.out.println(getLabelAsString());
	}
	
	/**
	 * Function that counts the number of elements contained into the feature vector
	 * @return the number of elements in the feature vector
	 */
	public int size()
	{
		if(featureVector == null)
			featureVector = new ArrayList<T>();
		return featureVector.size();
	}
	
	/**
	 * Function that removes an element from the feature
	 * @param index index of the element to remove
	 */
	public void removeElement(int index)
	{
		try
		{
			featureVector.remove(index);
		}
		catch(IndexOutOfBoundsException ioobe)
		{
			System.err.println("Feature -> removeElement index out of bounds"
					+ index + " FeatureVector size " + featureVector.size());
			return;
		}
	}
	
	/**
	 * Get the value of the feature label as String
	 * @return the value of the label
	 */
	public String getLabelAsString()
	{
		if(this.getLabelType() == LABEL_STRING)
			return labelValue;
		if(this.getLabelType() == LABEL_T)
			return label.toString();
		return null;
	}
	
	/**
	 * Get the value of the label 
	 * @return the value of the label attribute
	 */
	public int getLabelType()
	{
		if(!labelValue.equals(""))
			return LABEL_STRING;
		if(label != null)
			return LABEL_T;
		else
			return NO_LABEL;
	}
	
	/**
	 * Get the actual label as a generic Object, this is done to avoid other 
	 * casting and to not have to check the type of label when not necessary
	 * @return
	 */
	public Object getLabel()
	{
		if(getLabelType() == LABEL_T)
			return label;
		if(getLabelType() == LABEL_STRING)
			return labelValue;
		return null;
	}
	
	/**
	 * Try to parse the passed String to convert it as T object, if succeeds then
	 * the label is of type T, otherwise this is a String Label
	 * TODO
	 * @param s
	 */
	public void setLabel(String s)
	{
		labelValue = s;
	}
	
	
	/**
	 * trial main
	 */
	public static void main(String args[])
	{
		String a[] = new String[3];
		a[0] = "hello";
		a[1] = "ciao";
		a[2] = "salut";
		
		ArrayList<String> b = new ArrayList<String>();
		b.add("12");
		b.add("2");
		b.add("3");
		
		Feature<String> f = new Feature<String>(a, 3, String.class) {};
		f.changeElement("ole", 0);
		f.set(b);
		f.print();
		
		Feature<Double> f1 = new Feature<Double>("/home/antonio/file.txt", Double.class, ",") {};
		f1.print();

		//UserFeature u = new UserFeature() {};
		System.out.println(f1.getLabelAsString());
		System.out.println(f1.getLabel());	
	}
}
