package iit.cnr.it.classificationengine.basic.clusterer;

import java.io.File;
import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.Clusterer;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.clusterers.Cobweb;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

/**
 * Like the classifiers that are derived from weka, also this one is a wrapper
 * around the weka implementation of the cobweb clusterer. 
 * <p>
 * For the implementation we will use the same approach used in classifiers: 
 * we will rely on the abstract class Clusterer and we will use the functions
 * provided by the weka class to implement the action that the clusterer has to
 * perform. 
 * 
 * <br>
 * NOTE: the Cobweb algorithm works with categorical data, so the dataset passed
 * to the constructor has to be categorical
 * </p>
 * TODO
 * @author antonio
 *
 */
public class CobWeb<T> extends Clusterer<T> 
{
	//weka instances to be used as training set to build up the clusterer
	Instances trainingSet;
	
	//real implementation of the cobweb clusterer provided in weka
	Cobweb wekaCobweb;
	
	/**
	 * Basic constructor for the Cobweb class, we pass as parameter the dataset 
	 * that has to be used to build up the clusterer
	 * @param dataset the dataset to be used to build up the clusterer
	 */
	public CobWeb(DataSet<T> dataset)
	{
		super(dataset);
		wekaCobweb = new Cobweb();
	}
	
	/**
	 * Another constructor for the class CobWeb. In this case we pass to the class
	 * all the parameters required to let the object, by itself, build up the
	 * DataSet object
	 * @param pathToFile the absolute path to the file
	 * @param tInstance the instance of the dataset
	 * @param eSeparator the element separator
	 * @param fSeparator the feature separator
	 */
	public CobWeb(String pathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		super(pathToFile, tInstance, eSeparator, fSeparator);
		wekaCobweb = new Cobweb();
	}
	
	/**
	 * This is the train function. 
	 * <p>
	 * This function has been named train to be compliant with the train function
	 * of classifiers, to be more precise it is the buildClusterer function, hence
	 * this is the function in which starting from the dataset passed to the 
	 * object we build up the clusterer. 
	 * Since we use the weka implementation, the first step is to translate the
	 * dataset object into a Instances object that can be read by weka. 
	 * <br>
	 * In case we're using a categorical dataset, maybe the best option is to 
	 * store the dataset in arff format somewhere and then load weka instances
	 * from that file. 
	 * <br>
	 * If a column ID is needed, then the best option is to write the file without 
	 * the id already given, ask weka to add a new column id and then load 
	 * the dataset directly from weka.
	 * <br> 
	 * FIXME: this is just a starting point, later on we will provide ways to
	 * pass directly from categorical dataset to weka instances
	 * </p>
	 */
	@Override
	public void train() 
	{
		Instances instances = WekaFactory.instancesFromDataset(dataset);
		ArffSaver arffSaver = new ArffSaver();
		
		try 
		{
			File file = new File("/home/antonio/cobweb.arff");
			arffSaver.setDestination(file);
			arffSaver.setFile(file);
			arffSaver.setInstances(instances);
			arffSaver.writeBatch();
			//wekaCobweb.buildClusterer(instances);
			//System.out.println(wekaCobweb.toString());
			//System.out.println(wekaCobweb.graph());
			//ClusterEvaluation clusterEvaluation = new ClusterEvaluation();
			//clusterEvaluation.setClusterer(wekaCobweb);
			//clusterEvaluation.evaluateClusterer(instances);
			//System.out.println(clusterEvaluation.toString());
			//System.out.println("Results to string: " + clusterEvaluation.clusterResultsToString());
			//System.out.println(instances.toString());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	
	@Override
	public void setParameters(ArrayList<String> a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int classify(Element<T> element) {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	
}
