package iit.cnr.it.classificationengine.basic;

/**
 * This class interface represents the skeleton for every possible algorithm we
 * pick for classification or for clustering. We choose this approach in order
 * to allow a classification matrix to hold not only classification algorithm
 * but also clustering algorithms which may provide additional knowledge, hence
 * additional features can be provided to other classifiers
 * 
 * @author antonio
 *
 * @param <T>
 *          template parameter
 */
public interface Algorithm<T> {
  public abstract int classify(Element<T> elem);

  public abstract void train();

  public abstract boolean isTrained();

  public abstract Class<T> gettInstance();
}
