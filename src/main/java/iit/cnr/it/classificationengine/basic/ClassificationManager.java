package iit.cnr.it.classificationengine.basic;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * This is the ClassificationManager object. This is the entity in charge of
 * managing the classificationMatrix. Basically it is built up with an already
 * existing matrix. Then, for each cell, it is in charge of providing inputs and
 * managing training sets. <br>
 * In deep, as we have seen in the classificationMatrix class, the cells that
 * represent a classifier are put in a matrix. In the matrix, communication
 * between cells is allowed only up to down. Since each cell can be seen as a
 * separate unit, the classification process is intrinsically parallel. Thus by
 * means of a thread pool (of a user-defined size) the classification manager
 * invokes CellManager. <br>
 * <img src=classificationManager.jpg width=780px height=480px> <br>
 * The ClassificationManager is the subject of the operations involving the
 * object ClassificationMatrix
 * 
 * @author antonio
 */
public class ClassificationManager {
  /**
   * this is the object that is managed by this manager.
   */
  private ClassificationMatrix classificationMatrix;

  /**
   * In order to efficiently use the resources, the classificationManager will
   * use a thread pool. Every time it needs to classify an element or to train a
   * classifier, one of the thread in the pool will be in charge of performing
   * the task. Instead of spawning a thread for each operation we follow this
   * approach because we expect to use this library on constrained devices too,
   * hence having many threads may not be the right choice in these
   * environments. This attribute is in charge of storing the pool of executors
   */
  ExecutorService executors;

  /**
   * Constructor of a new ClassificationManagaer.
   * 
   * @param classificationMatrix
   *          the matrix the manager handles
   * @param maxExecutors
   *          the max number of threads the device can handle
   */
  public ClassificationManager(ClassificationMatrix classificationMatrix, int maxExecutors) {
    /* BEGIN parameter checking */
    if (classificationMatrix == null) {
      ExceptionManager.criticalBehavior("ClassificationManager constr classificationMatrix null");
    }
    if (maxExecutors <= 0) {
      ExceptionManager.criticalBehavior("ClassMan maxExecutors " + maxExecutors + " invalid");
      /* END parameter checking */
    }

    // instantiate a new object
    this.classificationMatrix = new ClassificationMatrix(classificationMatrix);
    // build up a new thread pool
    executors = Executors.newFixedThreadPool(maxExecutors);
  }

  /**
   * Prints the matrix of features.
   * 
   * @param array
   *          the matrix of features
   */
  public static void print(ArrayList<ArrayList<Object>> array) {
    for (ArrayList<Object> a : array) {
      for (Object o : a) {
        System.err.print(o.toString() + " ");
      }
      System.err.println();
    }
  }

  /**
   * Performs the classification operation, to do that, the
   * ClassificationManager deals with CellManager object. Basically the
   * classification operation is performed as follows:
   * <ol>
   * <li>Prepare the matrix of features</li>
   * <li>If there are executors available in the pool:</li>
   * <ol type=i>
   * <li>Assign the executor a new CellManager object which is the effective
   * thread</li>
   * <li>Retrieve the result of each classifier and build up the new vector of
   * features</li>
   * <li>Add the new vector of features in the matrix</li>
   * <li>Once all classifiers at a certain level have finished go to next level
   * and then go to 1.</li>
   * </ol>
   * <li>wait for an executor to become free</li>
   * </ol>
   * Once all the levels have performed their job, return the result of the last
   * classifier.
   * 
   * @param element
   *          the element we want to classify
   * @return the class of the element
   */
  public Integer classify(ArrayList<Object> element) {
    /* BEGIN parameter checking */
    if (element == null) {
      ExceptionManager.throwIAE("ClassificationManager.classify, the pased parameter is null");
      /* END parameter checking */
    }

    // creates the matrix of features
    ArrayList<ArrayList<Object>> features = new ArrayList<>();
    // put the passed feature vector at first position
    features.add(0, element);

    /**
     * for each cell in the matrix, starting from the first row, spawn a new
     * thread in charge of handling the classification for that classifier
     */
    for (int i = 1; i <= classificationMatrix.levels(); i++) {
      // list to manage classification results, each row has its own list of
      // results
      ArrayList<Future<Integer>> fs = new ArrayList<>();
      for (int j = 1; j <= classificationMatrix.rowSize(i); j++) {
        /* ask a thread to classify the feature vector */
        try {
          /**
           * here the suppress warnings is allowed since we're converting an
           * object from a wildcard to the Object type.
           */
          @SuppressWarnings("unchecked")
          Cell<Object> cell = (Cell<Object>) classificationMatrix.getCell(i, j);
          // explicit conversion from wildcard to Cell<Object>
          fs.add(executors.submit(new CellManager<>(cell, features)));
        } catch (IndexOutOfBoundsException ioobe) {
          ExceptionManager.wrongBehavior("ClassManager.classify" + " index out of bounds detected");
          return null;
        }

      }

      /**
       * now in the fs list we have the list of classification results hence we
       * build up a new list with classification results and then we add it to
       * the features matrix
       */
      try {
        Integer integer = 0;
        ArrayList<Object> classificationResults = new ArrayList<>();
        for (Future<Integer> f : fs) {
          // System.out.println( "LABEL: " + integer.toString() );
          // String s = f.get().toString();
          /**
           * The get function performed on a Future object make the task wait
           * for the result to arrive, here we've spawned all the necessary
           * threads to compute operations at a certain level. To go to the next
           * level we have to wait all the previous classifications to complete.
           * Doing this with Future object we avoid using wait() and notify().
           */
          integer = f.get();

          // add the actual label to the list of labels
          classificationResults.add(integer);
        }

        // add the new vector to the matrix of features
        features.add(classificationResults);

        // if we are at the last classification ,we have a single class
        if (i == classificationMatrix.levels()) {
          // System.err.println("Last classifier");
          return Integer.parseInt(classificationResults.get(0).toString());
        }
      } catch (InterruptedException ie) {
        ExceptionManager.wrongBehavior("ClassMan.classify " + "thread has been interrupted");
        return null;
      } catch (ExecutionException exc) {
        ExceptionManager.wrongBehavior("ClassMan.classify retrieving result from a task aborted");
        exc.printStackTrace();
        System.out.println("-------\n" + exc.getCause());
        return null;
      }
    }
    return null;
  }

}
