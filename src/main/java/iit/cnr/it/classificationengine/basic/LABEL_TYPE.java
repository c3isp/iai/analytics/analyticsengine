package iit.cnr.it.classificationengine.basic;

public enum LABEL_TYPE 
{
	STRING,
	TEMPLATE,
	NOT_PRESENT,
	ERROR;
}
