package iit.cnr.it.classificationengine.basic.classifiers;

import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.Classifier;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.LABEL_TYPE;
import iit.cnr.it.classificationengine.basic.WekaFactory;
import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This class is a wrapper around the RandomForest weka classifier.
 * 
 * @author antonio
 *
 * @param <T>
 */
public class RandomTreeForest<T> extends Classifier<T> {
  // weka RandomForest classifier
  private RandomForest randomForest;

  // training set in instances format
  private Instances TrainingSet;

  /**
   * return the RandomForest classifier. This is done for two reasons: it avoids
   * us from wrapping each possible RandomForest functions, thus, if weka adds
   * functions to RandomForest, our implementation has not to be modified;
   * moreover allows users to interact with all the functions of RandomForest.
   * 
   * @return the RandomForest classifier used
   */
  public RandomForest getClassifier() {
    return randomForest;
  }

  /**
   * Constructor for RandomTreeForest classifier, this builds up an empty
   * RandomTreeForest classifier. Of course the tInstance is mandatory and MUST
   * be provided
   * 
   * @throws IllegalArgumentException
   *           if the tInstance parameter is null
   */
  public RandomTreeForest(Class<T> tInstance) throws IllegalArgumentException {
    super(tInstance);
  }

  /**
   * Constructor for a RandomTreeForest with an already existing dataset and the
   * template class instance declared
   * 
   * @param trainingSet
   *          is the dataset containing the training set
   * @param tInstance
   *          is the instance of the template class
   * @throws IllegalArgumentException
   *           if one of the argument is invalid
   */
  public RandomTreeForest(DataSet<T> trainingSet, Class<T> tInstance)
      throws IllegalArgumentException {
    // constructs the base-class
    super(trainingSet, tInstance);

    // initialize the Instances object
    TrainingSet = WekaFactory.instancesFromDataset(trainingSet);
    // System.out.println(TrainingSet);
    // sets the class index
    TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);

    // constructs the classifier
    randomForest = new RandomForest();
  }

  /**
   * Constructor for RandomTreeForest classifier , in this case it is provided
   * to the classifier the path to the training set, hence the trianingSet
   * attribute of the class has to be built up form scratch
   * 
   * 
   * @param trainPathToFile
   *          is the path to the file that serves as training set
   * @param tInstance
   *          is the actual instance of the class-type of the classifier
   * @param eSeparator
   *          is the string used as separator between elements of features
   * @param fSeparator
   *          is the string used as separator between features
   */
  public RandomTreeForest(String trainPathToFile, Class<T> tInstance,
      String eSeparator, String fSeparator) {
    // construct the base class
    super(trainPathToFile, tInstance, eSeparator, fSeparator);

    // initialize the weka Instances object attribute
    TrainingSet = WekaFactory.instancesFromDataset(this.getTrainingSet());

    randomForest = new RandomForest();
  }

  /**
   * Copy constructor for RandomTreeForest class
   * 
   * @param RandomTreeForest
   *          the RandomTreeForest object to copy
   * @throws IllegalArgumentException
   *           if the RandomTreeForest is null
   */
  public RandomTreeForest(RandomTreeForest<T> RandomTreeForest)
      throws IllegalArgumentException {
    super(RandomTreeForest);
    if (RandomTreeForest == null)
      ExceptionManager
          .criticalBehavior("RandomTreeForest copy constructor, the object "
              + "RandomTreeForest is null");
    this.randomForest = RandomTreeForest.randomForest;
    this.TrainingSet = new Instances(RandomTreeForest.TrainingSet);
  }

  /**
   * Trains the classifier, uses the TrainingSet object to train the classifier.
   * This function has to be <b>always</b> called before the classify function.
   */
  @Override
  public void train() {
    // System.err.println("Training... ");
    // TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
    try {
      randomForest.buildClassifier(TrainingSet);
      isTrained = true;
    }
    // weka exception caught
    catch (Exception e) {
      ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause());
      e.printStackTrace();
    }
  }

  /**
   * If modifications to the dataset are applied, then retrain the classifier
   * 
   * @param dataset
   *          the dataset on which train the classifier
   * @throws IllegalArgumentException
   *           if the passed dataset is not valid
   */
  @Override
  public void retrain(DataSet<T> dataset) throws IllegalArgumentException {
    if (dataset == null)
      ExceptionManager
          .throwIAE("RandomTreeForest.retrain the passed dataset is null");
    // set the passed dataset as trainingset
    setTrainingSet(new DataSet<T>(dataset) {
    });
    // build up a new classifier
    randomForest = new RandomForest();
    TrainingSet = WekaFactory.instancesFromDataset(dataset);
    // TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
    // System.out.println("Class index: " + TrainingSet.classIndex());
    try {
      randomForest.buildClassifier(TrainingSet);
      isTrained = true;
    } catch (Exception e) {
      ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause());
      e.printStackTrace();
    }
  }

  /**
   * Classifies an Element object using the just-trained classifier
   * 
   * @param elem
   *          the Element object to classify
   * @throws IllegalArgumentException
   *           if the argument passed is invalid
   */
  @Override
  public int classify(Element<T> elem) throws IllegalArgumentException {
    // System.err.println("Classifying....");

    /* BEGIN parameter checking */
    if (elem == null)
      ExceptionManager.throwIAE(
          "RandomTreeForest.classify: impossible to classify a null Element");
    if (elem.size() == 0)
      ExceptionManager.throwIAE(
          "RandomTreeForest.classify: impossible to classify an empty Element");
    /* END parameter checking */

    Instance instance = WekaFactory.instanceFromElement(elem,
        elem.getQualification(), TrainingSet);
    // elem.print();
    // System.out.println("Instance: " + instance);
    instance.setDataset(TrainingSet);

    double distribution;
    try {
      // classify the element
      distribution = randomForest.classifyInstance(instance);
      return (int) distribution;
    }
    // manage exception
    catch (Exception e) {
      ExceptionManager.wrongBehavior("Exception caught in weka" + e.getCause());
      e.printStackTrace();
    }
    return -1;
  }

  /**
   * The list of parameters provided to the RandomTreeForest classifier,
   * resembles the one provided to the weka RandomForest. Available parameters
   * are as following:
   * 
   * <pre>
   * -I &lt;number of trees&gt;
   *  Number of trees to build.
   * </pre>
   * 
   * <pre>
   * -K &lt;number of features&gt;
   *  Number of features to consider (&lt;0 = int(log_2(#predictors)+1)).
   * </pre>
   * 
   * <pre>
   * -S
   *  Seed for random number generator.
   *  (default 1)
   * </pre>
   * 
   * <pre>
   * -depth &lt;num&gt;
   *  The maximum depth of the trees, 0 for unlimited.
   *  (default 0)
   * </pre>
   * 
   * <pre>
   * -D
   *  If set, classifier is run in debug mode and
   *  may output additional info to the console
   * </pre>
   * 
   * @param parameters
   *          the list of parameters
   */
  @Override
  public void setParameters(ArrayList<String> parameters) {
    /* BEGIN parameter checking */
    if (parameters == null)
      ExceptionManager.throwIAE(
          "RandomTreeForest.setParameters the list of parameters is null");
    if (parameters.size() == 0)
      ExceptionManager.throwIAE(
          "RandomTreeForest.setParameters the list of parameters is empty");
    /* END parameter checking */

    try {
      // set required options
      String[] parametersArray = new String[parameters.size()];
      for (int i = 0; i < parameters.size(); i++)
        parametersArray[i] = parameters.get(i);
      randomForest.setOptions(parametersArray);
    }
    // manage possible exception due to unsupported option
    catch (Exception e) {
      ExceptionManager.wrongBehavior("Unsupported parameter" + e.getMessage());
      e.printStackTrace();
    }
  }

  /**
   * Checks if the labels stored in the dataset are compliant with the confusion
   * matrix.
   * 
   * @param dataset
   *          the dataset to verify
   * @return the number of distinct label if everything goes ok
   * @throws Exception
   *           if the labels are not compliant with confusion matrix
   */
  private int checkLabel(DataSet<T> dataset) throws Exception {
    /* If the label is of type string, then the dataset can be employed */
    if (dataset.getLabelType().equals(LABEL_TYPE.STRING))
      return dataset.getLabelDistinct().size();

    // looks for the max value for the label
    int maxLabel = 0;
    for (Object o : dataset.getLabel()) {
      int label;
      // handles the case in which the label is of type double
      if (gettInstance().getName() == "java.lang.Double") {
        Double d = (Double) o;
        double tmp = d;
        label = (int) tmp;
      } else
        label = (int) o;
      // update maxlabel if necessary
      if (label > maxLabel)
        maxLabel = label;
    }

    // checks if max label is coherent with the number of distinct values
    if (maxLabel > dataset.getLabelDistinct().size()) {
      ExceptionManager.throwException(
          "Can't apply confusion matrix to the provided dataset, labels are sparse");
    }
    return dataset.getLabelDistinct().size();

  }

  /**
   * Forming the confusion matrix is quite straightforward. Starting from a
   * dataset we repeatedly perform classification on already labeled elements.
   * Once this has been done, we record in a matrix to which class elements
   * belonging to a certain label belong to.
   * 
   * <br>
   * To obtain a confusion matrix the following steps are followed:
   * <ol>
   * <li>classify all the elements in the dataset</li>
   * <li>construct a matrix where at position (i, j) there are elements of class
   * i classified as elements of class j</li>
   * </ol>
   * 
   * @param dataset
   *          the dataset to use to build up the confusion matrix, may be the
   *          training set itself
   * @return the confusion matrix
   */
  public int[][] confusionMatrix(DataSet<T> dataset)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (getTrainingSet() == null) {
      if (dataset == null) {
        ExceptionManager
            .criticalBehavior("KNN.confusionMatrix, null training set");
      } else {
        if (dataset.getLabelType().equals(LABEL_TYPE.NOT_PRESENT)) {
          ExceptionManager
              .throwIAE("KNN.confusionMatrix, passed dataset is unlabeled");
        } else {
          TrainingSet = WekaFactory.instancesFromDataset(dataset);
          // System.out.println(TrainingSet);
          // sets the class index
          TrainingSet.setClassIndex(TrainingSet.numAttributes() - 1);
        }
      }
    }
    /* END parameter checking */

    // at first train the classifier
    if (isTrained == false) {
      train();
    }

    // list of labels as provided by the classifier
    ArrayList<Integer> label = new ArrayList<Integer>();
    // classify each element in the dataset
    for (Element<T> element : dataset.getElements()) {
      label.add(classify(element));
    }

    int numDistinctLabels;
    /**
     * try to retrieve the number of distinct labels, if labels are compliant
     * with confusion matrix, otherwise an exception is thrown and it is handled
     * in the next block of code
     */
    try {
      numDistinctLabels = checkLabel(dataset);
    }
    // handles possible exception
    catch (Exception e) {
      ExceptionManager.wrongBehavior(e.getMessage());
      return null;
    }

    // build up the matrix with all zeros
    int[][] confusionMatrix = new int[numDistinctLabels][numDistinctLabels];
    for (int i = 0; i < numDistinctLabels; i++) {
      for (int j = 0; j < numDistinctLabels; j++) {
        confusionMatrix[i][j] = 0;
      }
    }

    // check element classification
    for (int i = 0; i < dataset.getElements().size(); i++) {
      // cases in which direct conversion Object->int is possible
      if (gettInstance().getName() == "java.lang.Integer"
          || gettInstance().getName() == "java.lang.String")
        confusionMatrix[(int) dataset.getLabel(i, false)][label.get(i)]++;

      // direct conversion not possible
      if (gettInstance().getName() == "java.lang.Double") {
        Double d = (Double) dataset.getLabel(i, false);
        double tmp = d;
        int label1 = (int) tmp;
        confusionMatrix[label1][label.get(i)]++;
      }
    }

    // print
    for (int i = 0; i < numDistinctLabels; i++) {
      for (int j = 0; j < numDistinctLabels; j++)
        System.out.print("|" + confusionMatrix[i][j] + "\t");
      System.out.print("\n------------------------------\n");
    }
    return confusionMatrix;
  }

  public static void main(String[] args) {
    DataSet<Double> d = DataSet
        .buildDatasetClassifierWeka("/home/antonio/file.arff", Double.class);

    Element<Double> f = new Element<Double>(Double.class) {
    };
    f.addConvert("14.1");
    f.addConvert("14.1");
    f.setLabel("Iris");
    d.add(f);
    d.add(f);

    RandomTreeForest<Double> classifier = new RandomTreeForest<Double>(d,
        Double.class);
    classifier.train();
    RandomTreeForest<Double> RandomTreeForest1 = new RandomTreeForest<Double>(
        classifier);
    classifier.retrain(d);
    RandomTreeForest1.train();
    // f.changeFeature(3.4, 0);
    // f.changeFeature(6000.0, 3);
    // f.setAttributes(b);
    System.out.println("Class Forest:" + classifier.classify(f));
    System.out.println("Class:" + RandomTreeForest1.classify(f));

    System.out.println("--------------------------");
  }

  @Override
  public double[] retrieveBelongness(Element<T> elem) throws Exception {
    Instance instance = WekaFactory.instanceFromElement(elem,
        elem.getQualification(), null);
    instance.setDataset(TrainingSet);
    return randomForest.distributionForInstance(instance);
  }

}
