package iit.cnr.it.classificationengine.basic;
import java.util.ArrayList;

/**
 * This abstract class will be used to represent the unsupervised learning, 
 * aka cluster algorithms. <br>
 * The cluster is provided with an unlabeled dataset, it groups the various elements 
 * according to similarities between elements. 
 * We employ the same dataset object used with Clusterings.   
 * @author antonio
 * @deprecated
 */
public abstract class Clustering {
	
	//dataset used to build up the cluster
	protected DataSet<?> dataset;
	
	/**
	 * These are auto-generated getter and setters. These are protected to avoid
	 * any class not-deriving Clustering<T> to access to these functions
	 */
	protected DataSet<?> getdataset() {
		return dataset;
	}

	protected void setdataset(DataSet<?> dataset) {
		this.dataset = dataset;
	}

	protected Class<?> gettInstance() {
		return dataset.getClass();
	}
	
	/**
	 * Default clustering constructor.
	 */
	public Clustering() {}
	
	/**
	 * Constructor for the Clustering class, we provide to the Clustering
	 * the training set and the class instance to be used whenever conversions
	 * are necessary
	 * @param dataset the Set on which we train our Clustering
	 * @param tInstance an instance of the class
	 * @throws IllegalArgumentException if one of the arguments is invalid 
	 */
	public Clustering(DataSet<?> dataset) 
			throws IllegalArgumentException
	{
		/*BEGIN parameter checking*/
		if(dataset == null)
			ExceptionManager.throwIAE("Clustering constructor DataSet object passed is null");
		/*END parameter checking*/
		
		this.dataset = dataset;
	}
	
	/**
	 * Constructor for the Clustering class, in this case it is provided to 
	 * the Clustering the path to the training set, hence the trianingSet 
	 * attribute of the class has to be built up form scratch
	 * 
	 * @param trainPathToFile is the path to the file that serves as training set
	 * @param tInstance is the actual instance of the class-type of the Clustering
	 * @param eSeparator is the string used as separator between elements of features
	 * @param fSeparator is the string used as separator between features
	 * @throws IllegalArgumentException if one of the arguments is invalid
	 */
	public <T extends Comparable<T>> Clustering(String trainPathToFile, Class<T> tInstance, String eSeparator, String fSeparator)
	{
		/*BEGIN parameter checking*/
		if(tInstance == null)
			ExceptionManager.criticalBehavior("Clustering constructor: Instance of template class is null");
		/*END parameter checking*/
		
		//other parameters are checked also here
		//dataset = new DataSet<T>(trainPathToFile, tInstance, eSeparator, fSeparator) {};
		dataset = DataSet.buildDatasetClusterer(trainPathToFile, eSeparator, fSeparator, tInstance);
	}
 
	
	//dummy function implemented by each cluster algorithm
	public void train() {};
	public int classify(Element<?> element) {return -1;};
	public void setParameters(ArrayList<String> a){};
	
}
