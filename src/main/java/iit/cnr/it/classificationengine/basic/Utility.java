package iit.cnr.it.classificationengine.basic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import weka.core.Instances;

//TODO add utility functions to this class and refactor
/**
 * This class contains only static methods used for utility. <br>
 * This class is devoted to all those methods that are widely used in the
 * project, such as reading from a file, or writing to a file and so on. In
 * order to avoid the possible instantiation of this class, its constructor is
 * private so that also the class that derive from utility can't instantiate it.
 * 
 * @author antonio
 *
 */
public class Utility {
	/**
	 * Private constructor in order to avoid possible instantiation of this
	 * class
	 */
	private Utility() {

	}

	/**
	 * This function handles the conversion from a String to a Template type T
	 * basically we check the type of the instance of the generic class
	 * tInstance and, basing on its type we behave accordingly.
	 * 
	 * NOTE: We expect any user defined type to implement the method
	 * fromString() to this aim we provide the baseClass UserFeature and we
	 * oblige user classes to extend the base one <br>
	 * Example usage is the following :
	 * 
	 * <pre>
	 * {@code Integer i = Element.convert("22", Integer.class)}
	 * </pre>
	 * 
	 * @param from
	 *            the string to be converted
	 * @param tInstance
	 *            the instance of generic class used to convert from String to
	 *            generic
	 * @param <T>
	 *            signals that this is a static function that uses generic types
	 * @return the template element
	 * 
	 * @throws NumberFormatException
	 *             if the String has to be converted to number but is malformed
	 * @throws IllegalArgumentException
	 *             if some arguments are invalid TODO extend user defined type
	 *             conversion
	 */
	public static <T> T convert(String from, Class<T> tInstance)
			throws NumberFormatException, IllegalArgumentException {
		/* BEGIN parameter checking */
		String isNull = (from == null) ? "String to convert" : (tInstance == null) ? "Instance of converter class" : "";
		if (!isNull.isEmpty())
			ExceptionManager.throwIAE("Element.convert " + isNull + " isNull");
		if (from.isEmpty())
			ExceptionManager.throwIAE("Element.convert empty String parameter");
		/* END parameter checking */

		String s = tInstance.getName().toString();

		if (from.equals(""))
			return tInstance.cast(null);

		// try the possible cast conversions
		try {
			/**
			 * if we have read a string and the feature type is string then do a
			 * dummy cast for type-safe
			 */
			if (s.equals("java.lang.String")) {
				return tInstance.cast(from);
			}

			/**
			 * If we have a string but the desired value is an Integer than we
			 * parse the string to obtain an integer and then perform a dummy
			 * cast for type-safe
			 */
			if (s.equals("java.lang.Integer")) {
				return tInstance.cast(Integer.parseInt(from));
			}

			/**
			 * Same written for integer holds also for Double
			 */
			if (s.equals("java.lang.Double")) {
				return tInstance.cast(Double.parseDouble(from));
			}

			// do some testing in order to check the appropriate type
			if (s.equals("UserFeature")) {
				return tInstance.cast(UserFeature.fromString(from));
			}
		} catch (ClassCastException cce) {
			ExceptionManager.wrongBehavior("Element.cast: check file format and type used at" + from);
			return null;
		}
		/**
		 * In this case we throw an exception since the considered String may
		 * store the label, hence we need to manage this possibility
		 */
		catch (NumberFormatException nfe) {
			throw new NumberFormatException("Invalid number format");
		}

		// in case no type found
		System.err.println("Unable to convert, undefined conversion for class " + s);
		return null;
	}

	/**
	 * Converts the dataset in String format to a matrix of double where each
	 * double represents an index to a String stored into an hashmap.
	 * 
	 * @param dataset
	 *            the dataset to be converted
	 * @return the matrix of doubles
	 */
	static <T extends Comparable<T>> double[][] asDoubleMatrixFromString(DataSet<T> dataset) {
		ArrayList<Element<T>> featureMatrix = dataset.getElements();
		int x = featureMatrix.size();
		int y = featureMatrix.get(0).size();
		double[][] matrix = new double[x][y];

		// iterates over rows
		int rowIndex = 0;
		for (Element<T> element : featureMatrix) {
			// iterates over columns
			int columnIndex = 0;

			/**
			 * We need to have an hashmap for each attribute, we know we're
			 * storing strings
			 */
			ArrayList<HashMap<Integer, String>> hashList = new ArrayList<HashMap<Integer, String>>();
			for (T feature : element.get()) {
				/**
				 * checks if the feature is already contained in the
				 * corresponding hash table, if so then retrieve the index at
				 * which the feature is stored
				 */
				if (hashList.get(columnIndex).containsValue(feature.toString())) {
					// iterates over rows
					for (int j = 0; j < hashList.get(columnIndex).size(); j++) {
						if (hashList.get(columnIndex).get(j).equals(feature.toString())) {
							matrix[rowIndex][columnIndex] = j;
						}
					}
				}
				/**
				 * otherwise insert the feature in the hashmap
				 */
				else {
					int index = hashList.get(columnIndex).size();
					hashList.get(columnIndex).put(index, feature.toString());
					matrix[rowIndex][columnIndex] = index;
				}
				columnIndex++;
			}
			/**
			 * In case of String label, the label is already stored in an
			 * hashmap and its index is stored into a list of indexes. Hence we
			 * retrieve the index to the map if the label is of type String or
			 * the label itself
			 */
			Double d = convert(dataset.getLabel(rowIndex, false).toString(), Double.class);
			matrix[rowIndex][columnIndex] = d;

			rowIndex++;
		}
		return matrix;
	}

	/**
	 * Convert the actual dataset to a Matrix of doubles, same approach used
	 * also in weka.
	 * 
	 * @return the matrix of double
	 * @throws IllegalArgumentException
	 */
	public static <T extends Comparable<T>> double[][] asDoubleMatrix(DataSet<T> dataset, Class<T> tInstance)
			throws IllegalArgumentException {
		/* BEGIN parameter checking */
		String isNull = (dataset == null) ? "dataset" : (tInstance == null) ? "tInstance" : "";
		if (!isNull.isEmpty())
			ExceptionManager.throwIAE("Utility.asDoubleMatrix: " + isNull + "is null");
		if (dataset.getDimensions() == 0)
			ExceptionManager.throwIAE("Utility.asDoubleMatrix the passed dataset is empty");
		/* END parameter checking */

		ArrayList<Element<T>> featureMatrix = dataset.getElements();

		/**
		 * The case of String dataset has to be treated separately
		 */
		if (tInstance.getName().equals("java.lang.String"))
			return asDoubleMatrixFromString(dataset);

		else {
			// first of all state matrix dimensions
			int x = featureMatrix.size();
			/**
			 * the number of columns is equal to the number of features per
			 * element the plus 1 to take into account the label.
			 */
			int y = featureMatrix.get(0).size() + 1;

			// the matrix of doubles
			double[][] doubleMatrix = new double[x][y];

			int rowIndex = 0;

			// scan matrix by row to convert arrayList of elements to arrays of
			// double
			for (Element<T> element : featureMatrix) {
				int columnIndex = 0;
				// scan feature
				for (T elem : element.get()) {
					// convert each feature and add it to the right position in
					// the matrix
					Double d = convert(elem.toString(), Double.class);
					doubleMatrix[rowIndex][columnIndex] = d;
					columnIndex++;
				}

				/**
				 * In case of String label, the label is already stored in an
				 * hashmap and its index is stored into a list of indexes. Hence
				 * we retrieve the index to the map if the label is of type
				 * String or the label itself if it is stored as Double or
				 * Integer and store that index into the matrix
				 */
				Double d = convert(dataset.getLabel(rowIndex, false).toString(), Double.class);
				doubleMatrix[rowIndex][columnIndex] = d;

				rowIndex++;
			}

			// return the matrix
			return doubleMatrix;
		}
	}

	/**
	 * Given an object returns its index inside a list. To compare object tries
	 * to put the object at the same type
	 * 
	 * @param object
	 *            the object we're finding
	 * @param listObjects
	 *            the list of objects
	 * @return the index of the object, -1 if the object is not found
	 */

	public static int indexOf(Object object, ArrayList<Object> listObjects) {
		int i = 0;
		for (Object o : listObjects) {
			// Object a = Evaluation.convertDouble(o);
			// Object b = Evaluation.convertDouble(object);
			// System.out.println(a + " " + b);
			if (o.equals(object))
				return i;
			i++;
		}

		return -1;
	}

	/**
	 * Retrieves the index of the object passed as parameter inside the list
	 * list passed as parameter. In case the object is not present inside the
	 * list it returns -1
	 * 
	 * @param list
	 *            the list in which we're loking for
	 * @param object
	 *            the object to be searched
	 * @return the index of the object if it is present, -1 otherwise
	 */
	public static <T> int indexOf(ArrayList<T> list, T object) {
		int i = 0;
		for (T elem : list) {
			if (elem.equals(object))
				return i;
			i++;
		}
		// System.err.println(object + "not found in " + list);
		return -1;
	}

	/**
	 * Swaps the object at position index with the one passed as parameter.
	 * Returns the modified list
	 * 
	 * @param list
	 *            the list in which the object is stored
	 * @param object
	 *            the new value for the object stored in the list
	 * @param index
	 *            the index of the object in the list
	 * @return the modified list
	 * @throws IndexOutOfBoundsException
	 */
	public static <T> ArrayList<T> swap(ArrayList<T> list, T object, int index) throws IndexOutOfBoundsException {
		list.remove(index);
		list.add(index, object);
		return list;
	}

	/**
	 * This function is used in order to chain together two lists, the first
	 * parameter is the list to which we have to append the other one passed as
	 * parameter
	 * 
	 * @param dest
	 *            the list to be used as destination
	 * @param src
	 *            the list to be used as source
	 * @return the chained list
	 */
	public static <T> ArrayList<T> chainTogether(ArrayList<T> dest, ArrayList<T> src) {
		// check if the chaining is necessary
		if (src == null)
			return dest;

		// chain together the two lists
		else {
			for (T node : src)
				dest.add(node);
		}
		return dest;
	}

	/**
	 * Retrieve the intersection of two lists
	 * 
	 * @param desiredFamilies
	 *            the list of desired families
	 * @param clusterFamilies
	 *            the list of the families present in the cluster
	 * @return the intersection list
	 */
	public static <V> ArrayList<V> retrieveIntersection(ArrayList<V> desiredFamilies, ArrayList<V> clusterFamilies) {
		ArrayList<V> intersection = new ArrayList<V>();
		for (V dFamily : desiredFamilies)
			for (V cFamily : clusterFamilies)
				if (cFamily.equals(dFamily))
					intersection.add(cFamily);
		return intersection;
	}

	public static <T> int countOccurrences(T clusterName, ArrayList<T> values) {
		int occurrences = 0;
		for (T s : values)
			if (s.equals(clusterName))
				occurrences += 1;
		return occurrences;
	}

	/**
	 * This function is in charge of verifying the parameters passed to the
	 * various function, in this way it is possible to avoid having to write the
	 * same lines of code over and over again
	 * 
	 * @param objs
	 *            the list of parameters we want to verify
	 * @return true if everything goes fine, false otherwise
	 */
	public static boolean parameterChecking(String functionName, Object... objs) {

		for (Object o : objs) {
			if (o == null) {
				ExceptionManager.throwIAE(functionName + "parameter is null");
				return false;
			}
			if (o.getClass().toString().equals("java.lang.String")) {
				String s = o.toString();
				if (s.equals("")) {
					ExceptionManager.throwIAE(functionName + "String parameter is empty");
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Retrieves the content of the file stored into pathToFile. Basically this
	 * function reads the file content using as separator the eSeparator String
	 * passed as parameter and returns back the list of lines that form the
	 * file. In this case a line is not necessary a sequence of words separated
	 * by "\n", they may be separated by whichever character or group of
	 * characters the user decides.
	 * 
	 * @param pathToFile
	 *            the absolute path to the file to read
	 * @param eSeparator
	 *            the line separator
	 * @return the list of lines forming the file
	 */
	public static ArrayList<String> retrieveFileContent(String pathToFile, String eSeparator) {
		ArrayList<String> fileContent = new ArrayList<String>();
		try {
			Scanner scanner = new Scanner(new File(pathToFile));
			scanner.useDelimiter(eSeparator);
			while (scanner.hasNext()) {
				fileContent.add(scanner.next());
			}
			scanner.close();
			return fileContent;
		} catch (FileNotFoundException fnfe) {
			ExceptionManager.wrongBehavior("Utility.retrieveFileContent fileNotFoundException " + pathToFile);
			return null;
		}
	}

	/**
	 * Converts weka instances to a list of String
	 * 
	 * @param instances
	 *            the Instances object to be converted
	 * @return the list of string forming the instances object
	 */
	public static ArrayList<String> instancesToStrings(Instances instances) {
		if (instances == null)
			return null;
		ArrayList<String> instancesString = new ArrayList<String>();
		for (int i = 0; i < instances.numInstances(); i++)
			instancesString.add(instances.instance(i).toString());
		return instancesString;
	}
	
	public static ArrayList<String> read(String path) {
		try {
			ArrayList<String> lines = new ArrayList<>();
		InputStream stream = Utility.class.getClassLoader()
			    .getResourceAsStream(path);
			BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));
			String line = "";
			
			while ((line = buffer.readLine()) != null) {
				lines.add(line);
			}
			buffer.close();
			stream.close();
			return lines;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
