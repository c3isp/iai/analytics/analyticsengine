package iit.cnr.it.classificationengine.basic;
/**
 * This is a utility class that manages some type of exceptions. Basically is 
 * job is to print error messages, and, in some cases: or end the program or 
 * throw an exception. 
 * This has been created to enforce application portability
 * 
 * @author antonio
 *
 */

public class ExceptionManager {
	
	/**
	 * Prints a message in case of null pointer exception
	 * @param isNull the null object
	 * @param function function that generated the exception
	 */
	public static void nullPointer(String isNull, String function)
	{
		System.err.println("Error in " + function + " accessing the object "
				+ isNull + ". The object is null");
	}
	
	/**
	 * Prints a message in case of IndexOutOfBoundsException
	 * @param index the index out of buonds
	 * @param size maximum bounds
	 * @param function function that generated the exception
	 */
	public static void indexOut(int index, int size, String function)
	{
		System.err.println("Error in " + function + " index: " + index + 
				" is out of bounds of list/array of size " + size);
	}
	
	/**
	 * Handles the case in which something really wrong happened and we have to
	 * stop the program to avoid damage
	 * @param message the message to print
	 */
	public static void criticalBehavior(String message)
	{
		System.err.println("Wrong behavior detected: " + message);
		System.exit(-1);
	}
	
	/**
	 * Throws an IllegalArgumentException in case some argument is not valid
	 * @param message the message to print
	 */
	public static void throwIAE(String message) throws IllegalArgumentException
	{
		throw new IllegalArgumentException(message);
	}
	
	/**
	 * Simple utility function that prints an error message
	 * @param message the message to print
	 */
	public static void wrongBehavior(String message)
	{
		System.err.println(message);
	}
	
	public static void throwException(String message) throws Exception
	{
		throw new Exception(message);
	}

}
