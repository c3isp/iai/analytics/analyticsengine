package iit.cnr.it.classificationengine.basic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.Saver;

/**
 * This class is designed to deal with datasets or files containing sets of
 * features, basically it represents a <b>matrix of features</b>. This too is a
 * generic class since it has to deal with possibly different types of features.
 * Here we imagine that features may be of 4 possible types: String, Double,
 * Integer and User, where the User type has been added in order to deal with
 * user-defined set of features. To deal with it we provide an Interface that
 * all user-defined set of feature <b>must</b> follow to implement their own
 * defined type.
 * <p>
 * In the revision we applied some modifications to the original structure,
 * basically we leave only some of the constructors, (the most basic ones) and
 * decide to reorganize the constructors structure using the static factory
 * methods. This choise is taken because they've become messy and because we
 * need to distinguish between the constructors used for clustering and the ones
 * used for classifier and we don't want to force the user in a strict structure
 * like it has been done in weka. <br>
 * Basically we leave all the constructors as they're except the one that
 * constructs a dataset from file, this will be splitted into 3 static factory
 * methods. The first two are used for dataset used for classification (same
 * thing that was done in the Element class) and the other is for clustering.
 * </p>
 * 
 * This too is a <b>generic</b> class since it has to be compliant with the
 * previously defined possible types of features
 * 
 * @author antonio
 * 
 *         <br>
 *         <img src=dataset1.jpg height=500px width=800px />
 * 
 * @see userFeature interface
 */
public abstract class DataSet<T> {
  // ==========================================================================
  // Constants
  // ==========================================================================
  /**
   * error codes used in order to verify the featureMatrix characteristics.
   */
  public static final int WRONG_CHARACTERISTICS = -1;
  public static final int WRONG_LABEL = -2;
  public static final int EVERYTHING_FINE = 1;

  public static final int SYNTHETIC_LABEL = -1;
  public static final int NO_LABEL = -2;
  public static final int DEFAULT_LABEL_POS = -3;

  // ==========================================================================
  // Properties
  // ==========================================================================

  /**
   * This is an ArrayList of Elements, thus, since each element is an arrayList
   * of features, this is a matrix of features.
   */
  private ArrayList<Element<T>> featureMatrix;

  // Contains the list of attributes
  private ArrayList<String> attributes;

  // path to the file from which we derived the dataset
  private String pathToFile = "";

  // instance of the template class, this is necessary for conversion purpose
  private Class<T> classInstance;

  // instances needed to deal with weka files
  private Instances wekaInstances = null;

  // actual qualification of the dataset, if it is used for clustering or
  // classifier
  private TYPE qualification;

  private LABEL_TYPE labelsType = LABEL_TYPE.NOT_PRESENT;

  // states if the current dataset is categorical or not
  private boolean isCategorical = false;

  private int columnID = -1;

  private int numberOfFeatures = -1;

  private ArrayList<String> distinctLabels;

  // ==========================================================================
  // Constructors
  // ==========================================================================
  /**
   * Constructor for object dataset. It builds an empty dataset, however it must
   * be specified an instance of the class type to be used in conversions.
   * 
   * @param tInstance
   *          the class instance to be used
   */
  public DataSet(Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (tInstance == null) {
      ExceptionManager
          .criticalBehavior("Dataset constructor with tInstance null");
    }
    /* END parameter checking */

    this.classInstance = tInstance;
    featureMatrix = new ArrayList<>();
    attributes = new ArrayList<>();
  }

  /**
   * constructor for dataset object. It builds a new dataset object with
   * pre-defined elements
   * 
   * @param elementsList
   *          the list of elements to store in the dataset
   * @param tInstance
   *          the template class instance
   */
  public DataSet(ArrayList<Element<T>> elementsList, Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (elementsList == null || tInstance == null) {
      ExceptionManager
          .criticalBehavior("Dataset constructor, "
              + "null parameter provided, the program will stop");
    }
    /* END parameter checking */
    this.featureMatrix = new ArrayList<>(elementsList);
    this.classInstance = tInstance;
    manageLabels();
  }

  /**
   * Copy constructor, performs a deep copy of the dataset passed as parameter
   * 
   * <pre>
   * {
   *   &#64;code
   *   Dataset<T> dataset = new Dataset<T>(data) {
   *   };
   * }
   * </pre>
   * 
   * @param dataset
   *          the dataset object for which we want to compute a deep copy
   */
  public DataSet(DataSet<T> dataset) {
    /* BEGIN parameter checking */
    if (dataset == null) {
      ExceptionManager
          .throwIAE("Dataset copy constructor the passed parameter is null");
    } else {
      this.qualification = dataset.qualification;
      this.isCategorical = dataset.isCategorical;
      this.labelsType = dataset.labelsType;
      this.classInstance = dataset.classInstance;
      if (dataset.attributes != null) {
        this.attributes = new ArrayList<>(dataset.attributes);
      }
      this.featureMatrix = new ArrayList<Element<T>>();
      for (Element<T> element : dataset.featureMatrix) {
        this.featureMatrix.add(new Element<T>(element) {
        });
      }
      // this.featureMatrix = new ArrayList<>(dataset.featureMatrix);
    }
  }

  // ==========================================================================
  // Static Factory Methods
  // ==========================================================================

  /**
   * Builds up a dataset to be used with a classifier. To reach this target the
   * user passes as parameters the path to the file in which the dataset is
   * stored, the element separator, the feature separator and the instance of
   * the class of the generic dataset
   * 
   * <pre>
   * {
   *   &#64;code
   *   Dataset<Integer> dataset = Dataset.buildDatasetClassifier(
   *       "/home/antonio/dataset.txt", "\n", ",",
   *       Integer.class);
   * }
   * </pre>
   * 
   * @param pathToFile
   *          the path to the file to be used as dataset
   * @param eSeparator
   *          the element separator
   * @param fSeparator
   *          the feature separator
   * @param tInstance
   *          the isntance of the class used by the generic
   * @return
   * @return true if the dataset is correctly initialized, false otherwise
   */
  public static <T> DataSet<T> buildDatasetClassifier(String pathToFile,
      String eSeparator, String fSeparator,
      Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (!Utility.parameterChecking("Dataset.buildDatasetClassifier", pathToFile,
        eSeparator, fSeparator,
        tInstance)) {
      return null;
    }
    if (pathToFile.contains(".arff")) {
      return buildDatasetClassifierWeka(pathToFile, tInstance);
    }
    /* END parameter checking */
    /* creates new dataset object */
    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLASSIFIER;
    dataset.pathToFile = pathToFile;

    // retrieve the content of the file
    ArrayList<String> fileContent = Utility.retrieveFileContent(pathToFile,
        eSeparator);
    /**
     * Try to fill the dataset using the file content, if everything goes ok,
     * then manage the labels, otherwise return null
     */
    if (dataset.fillFromStrings(fileContent, fSeparator, -3)) {
      /**
       * Manage the labels of the dataset, if something goes wrong then return
       * null
       */
      if (dataset.manageLabels() && dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }
  
  /**
   * Builds up a dataset to be used with a classifier. To reach this target the
   * user passes as parameters the path to the file in which the dataset is
   * stored, the element separator, the feature separator and the instance of
   * the class of the generic dataset
   * 
   * <pre>
   * {
   *   &#64;code
   *   Dataset<Integer> dataset = Dataset.buildDatasetClassifier(
   *       "/home/antonio/dataset.txt", "\n", ",",
   *       Integer.class);
   * }
   * </pre>
   * 
   * @param pathToFile
   *          the path to the file to be used as dataset
   * @param eSeparator
   *          the element separator
   * @param fSeparator
   *          the feature separator
   * @param tInstance
   *          the isntance of the class used by the generic
   * @return
   * @return true if the dataset is correctly initialized, false otherwise
   */
  public static <T> DataSet<T> buildDatasetClassifier(ArrayList<String> fileContent,
      String eSeparator, String fSeparator,
      Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (!Utility.parameterChecking("Dataset.buildDatasetClassifier", fileContent,
        eSeparator, fSeparator,
        tInstance)) {
      return null;
    }
    /* END parameter checking */
    /* creates new dataset object */
    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLASSIFIER;

    /**
     * Try to fill the dataset using the file content, if everything goes ok,
     * then manage the labels, otherwise return null
     */
    if (dataset.fillFromStrings(fileContent, fSeparator, -3)) {
      /**
       * Manage the labels of the dataset, if something goes wrong then return
       * null
       */
      if (dataset.manageLabels() && dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }

  /**
   * This is the same function as before, the only difference is that in this
   * case we pass to the function also the ID of the label, basically this is
   * the index of the column at which we will find the label of the element.
   * 
   * @param pathToFile
   *          the absolute path to the file
   * @param eSeparator
   *          the element separator
   * @param fSeparator
   *          the feature separator
   * @param tInstance
   *          the tInstance object used to initialize the dataset
   * @param labelID
   *          the index of the label
   * @return the dataset object if everything goes ok, null otherwise
   */
  public static <T> DataSet<T> buildDatasetClassifier(String pathToFile,
      String eSeparator, String fSeparator,
      Class<T> tInstance, int labelID) {
    /* BEGIN parameter checking */
    if (!Utility.parameterChecking("Dataset.buildDatasetClassifier", pathToFile,
        eSeparator, fSeparator,
        tInstance)) {
      return null;
    }
    if (pathToFile.contains(".arff")) {
      return buildDatasetClassifierWeka(pathToFile, tInstance);
      /* END parameter checking */
    }

    /* creates new dataset object */
    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLASSIFIER;
    dataset.pathToFile = pathToFile;

    // retrieve the content of the file
    ArrayList<String> fileContent = Utility.retrieveFileContent(pathToFile,
        eSeparator);
    /**
     * Try to fill the dataset using the file content, if everything goes ok,
     * then manage the labels, otherwise return null
     */
    if (dataset.fillFromStrings(fileContent, fSeparator, labelID)) {
      /**
       * Manage the labels of the dataset, if something goes wrong then return
       * null
       */
      if (dataset.manageLabels() && dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }
  
  /**
   * This is the same function as before, the only difference is that in this
   * case we pass to the function also the ID of the label, basically this is
   * the index of the column at which we will find the label of the element.
   * 
   * @param pathToFile
   *          the absolute path to the file
   * @param eSeparator
   *          the element separator
   * @param fSeparator
   *          the feature separator
   * @param tInstance
   *          the tInstance object used to initialize the dataset
   * @param labelID
   *          the index of the label
   * @return the dataset object if everything goes ok, null otherwise
   */
  public static <T> DataSet<T> buildDatasetClassifier(ArrayList<String> fileContent,
      String eSeparator, String fSeparator,
      Class<T> tInstance, int labelID) {
    /* BEGIN parameter checking */
    if (!Utility.parameterChecking("Dataset.buildDatasetClassifier", fileContent,
        eSeparator, fSeparator,
        tInstance)) {
      return null;
    }
    /*END parameter checking*/

    /* creates new dataset object */
    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLASSIFIER;

    // retrieve the content of the file
    /**
     * Try to fill the dataset using the file content, if everything goes ok,
     * then manage the labels, otherwise return null
     */
    if (dataset.fillFromStrings(fileContent, fSeparator, labelID)) {
      /**
       * Manage the labels of the dataset, if something goes wrong then return
       * null
       */
      if (dataset.manageLabels() && dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }

  /**
   * In this case we build our dataset starting from weka instances. Basically
   * here at first we instantiate an Instances object used to parse the arff
   * file in weka, then, starting from it, we will build up our dataset
   * 
   * @param pathToFile
   *          the absolute path to the .arff file
   * @param tInstance
   *          the instance of the class to be used in the dataset
   * @return the dataset if everything goes ok, null otherwise
   */
  public static <T> DataSet<T> buildDatasetClassifierWeka(String pathToFile,
      Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (!Utility.parameterChecking("Dataset.buildDatasetClassifierWeka",
        pathToFile, tInstance)) {
      return null;
    }
    if (!pathToFile.contains(".arff")) {
      return null;
      /* END parameter checking */
    }

    /* Initialize Instances object */
    Instances wekaInstances = WekaFactory.buildFromWeka(pathToFile);
    if (wekaInstances == null) {
      return null;
    }

    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLASSIFIER;
    dataset.pathToFile = pathToFile;

    /**
     * Try to fill the dataset using the file content, if everything goes ok,
     * then manage the labels, otherwise return null
     */
    if (dataset.initializeMatrixFromWeka()) {
      /**
       * Manage the labels of the dataset, if something goes wrong then return
       * null
       */
      if (dataset.manageLabels() && dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }

  /**
   * In this case the dataset we want to build up is meant to be used for
   * clustering, hence in this case no labels are present.
   * <p>
   * Here we assume to have the whole dataset stored inside a file, having an
   * element separator eSeparator and a feature separator fSeparator.
   * </p>
   * 
   * @param pathToFile
   *          the absolute path to the file in which the dataset is stored
   * @param eSeparator
   *          the string used to separate the elements
   * @param fSeparator
   *          the string used to separate the features
   * @param tInstance
   *          the instance of the generic class
   * @return an object of type dataset if everything goes ok
   */
  public static <T> DataSet<T> buildDatasetClusterer(String pathToFile,
      String eSeparator, String fSeparator,
      Class<T> tInstance) {
    // BEGIN parameter checking
    if (!Utility.parameterChecking("DataSet.buildDatasetClusterer", pathToFile,
        eSeparator, fSeparator,
        tInstance)) {
      return null;
    }
    // END parameter checking

    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLUSTERING;
    dataset.attributes = new ArrayList<>();
    dataset.pathToFile = pathToFile;

    ArrayList<String> fileContent = Utility.retrieveFileContent(pathToFile,
        eSeparator);
    /**
     * Using the file content fills the dataset object that will be returned
     * <br>
     * here it is not necessary to manage the labels since we do not have any
     */
    if (dataset.fillFromStrings(fileContent, fSeparator, NO_LABEL)) {
      if (dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }
  
  /**
   * In this case the dataset we want to build up is meant to be used for
   * clustering, hence in this case no labels are present.
   * <p>
   * Here we assume to have the whole dataset stored inside a file, having an
   * element separator eSeparator and a feature separator fSeparator.
   * </p>
   * 
   * @param pathToFile
   *          the absolute path to the file in which the dataset is stored
   * @param eSeparator
   *          the string used to separate the elements
   * @param fSeparator
   *          the string used to separate the features
   * @param tInstance
   *          the instance of the generic class
   * @return an object of type dataset if everything goes ok
   */
  public static <T> DataSet<T> buildDatasetClusterer(ArrayList<String> fileContent,
      String eSeparator, String fSeparator,
      Class<T> tInstance) {
    // BEGIN parameter checking
    if (!Utility.parameterChecking("DataSet.buildDatasetClusterer", fileContent,
        eSeparator, fSeparator,
        tInstance)) {
      return null;
    }
    // END parameter checking

    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLUSTERING;
    dataset.attributes = new ArrayList<>();
    /**
     * Using the file content fills the dataset object that will be returned
     * <br>
     * here it is not necessary to manage the labels since we do not have any
     */
    if (dataset.fillFromStrings(fileContent, fSeparator, NO_LABEL)) {
      if (dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }

  /**
   * This is a mirror function of the buildDatasetClassifierWeka, in this case
   * the only difference is that here we do not have a class label, hence at
   * first we populate the Instances object using weka API, then we convert it
   * into a dataset object as was previously done.
   * 
   * @param pathToFile
   *          the absolute path to the file
   * @param tInstance
   *          the instance of the generic class
   * @return the dataset object constructed
   */
  public static <T> DataSet<T> buildDatasetClustererWeka(String pathToFile,
      Class<T> tInstance) {
    /* BEGIN parameter checking */
    if (!Utility.parameterChecking("Dataset.buildDatasetClassifierWeka",
        pathToFile, tInstance)) {
      return null;
    }
    if (!pathToFile.contains(".arff")) {
      return null;
    }
    /* END parameter checking */

    /* Initialize Instances object */
    Instances wekaInstances = WekaFactory.buildFromWeka(pathToFile);
    if (wekaInstances == null) {
      return null;
    }

    DataSet<T> dataset = new DataSet<T>(tInstance) {
    };
    dataset.qualification = TYPE.CLUSTERING;
    dataset.pathToFile = pathToFile;

    /**
     * Try to fill the dataset using the file content, if everything goes ok,
     * then manage the labels, otherwise return null
     */
    if (dataset.initializeMatrixFromWeka()) {
      if (dataset.checkCharacteristics()) {
        return dataset;
      }
    }
    return null;
  }

  /**
   * The labels management was a really nightmare in the old version of this
   * code, to improve the code we decided to follow this approach: <br>
   * since labels are already stored inside the Element object, it is not
   * necessary to have them also in the Dataset object, it will be a duplicated
   * information that improves only confusion. For this reason the only thing
   * that this function will do is to check the type of the label for each
   * element, if it finds labels of different types, a warning will be sent, if
   * there are some elements without label, the program will be stopped
   * 
   * @return true if everything is alright, false otherwise
   */
  private boolean manageLabels() {
    LABEL_TYPE labelType = featureMatrix.get(0).getLabelType();
    for (Element<T> element : featureMatrix) {
      if (!labelType.equals(element.getLabelType())) {
        ExceptionManager.criticalBehavior("The labels do not match");
        return false;
      }
    }
    labelsType = labelType;
    return true;
  }

  /**
   * This function is used to fill the dataset with a list of String. We create
   * an Element for each String in the list and then we add it to the list of
   * elements to form the dataset
   * 
   * @param fileContent
   *          the content of the file as an array of strings
   * @param fSeparator
   *          the feature separator to be passed in order to build up the
   *          element
   * @param labelID
   *          the id of the label. This last value has 3 values already
   *          assigned:
   *          <ol>
   *          <li>-1 stands for synthetic label</li>
   *          <li>-2 stands for cluster element</li>
   *          <li>-3 stands for label in the default position</li>
   *          </ol>
   * @return true if everything goes ok, false otherwise
   */
  private boolean fillFromStrings(ArrayList<String> fileContent,
      String fSeparator, int labelID) {
    Element<T> element = null;
    for (String s : fileContent) {
      if (s.contains("?")) {
        ;
      } else {
        if (labelID == DEFAULT_LABEL_POS) {
          element = Element.buildClassifierElement(s, fSeparator,
              classInstance);
        }
        if (labelID == NO_LABEL) {
          element = Element.buildClustererElement(s, fSeparator, classInstance);
        }
        if (element == null) {
          element = Element.buildClassifierElement(s, fSeparator, classInstance,
              labelID);
        }
        if (element != null) {
          featureMatrix.add(element);
        } else {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Checks that all the elements have the same number of features to avoid
   * possible misbehaviors due to the fact that some elements may be bigger or
   * shorter.
   * 
   * @return true if everything goes ok, false otherwise
   */
  private boolean checkCharacteristics() {
    // retrieves the number of features of the first element
    if (featureMatrix.size() == 0)
      return false;
    numberOfFeatures = featureMatrix.get(0).size();
    // checks if all the elements have the same number of features
    for (Element<T> element : featureMatrix) {
      // if an element is smaller or bigger shows a message and return
      // false
      if (element.size() != numberOfFeatures) {
        ExceptionManager
            .criticalBehavior("Element " + element.toString()
                + " has a different number of features"
                + " original number: " + numberOfFeatures + " actual: "
                + element.size());
        return false;
      }
    }

    // check if the number of attributes is the same of the number of
    // features
    if (attributes != null && attributes.size() > 0) {
      if (attributes.size() != numberOfFeatures) {
        ExceptionManager.criticalBehavior(
            "Attributes size " + attributes.size() + " and feature size: "
                + numberOfFeatures + "doesn't match");
        return false;
      }
    }

    return true;
  }

  // ==========================================================================
  // Public functions
  // ==========================================================================

  /**
   * Add a new Element object to the dataset. Even if an already existing
   * Element object is passed, to avoid possible misbehaviors, a brand new one
   * is created.
   * 
   * @param element
   *          the Element object to add
   * @return true if the add is correct, false otherwise
   * @throws IllegalArgumentException
   *           if the feature parameter is not valid
   */
  public boolean add(Element<T> element) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (element == null || element.get() == null || element.size() <= 0
        || !element.getQualification().equals(qualification)) {
      ExceptionManager.throwIAE("Dataset.add element parameter is not valid "
          + element.toString() + " "
          + element.getQualification() + "\t" + this.getQualification());
      /* END parameter checking */
    }

    // checking allocation of featurematrix
    if (featureMatrix == null) {
      featureMatrix = new ArrayList<>();
    }

    if (!compatible(element)) {
      return false;
    }

    /**
     * Set attributes names from the passed feature if the dataset doesn't have
     * any attributes declared while the feature does
     */
    if (element.getAttributes() != null
        && (attributes == null || attributes.size() == 0)) {
      attributes = new ArrayList<>(element.getAttributes());
    }

    // if the add goes ok
    Element<T> tmp = new Element<T>(element) {
    };
    if (featureMatrix.add(tmp)) {
      labelsType = element.getLabelType();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Checks if the passed element object can be inserted inside this dataset.
   * 
   * @param element
   *          the element we want to insert
   * @return true if the element can be inserted inside the dataset, false
   *         otherwise
   */
  private boolean compatible(Element<T> element) {
    /**
     * Verify the parameter if the feature matrix already contains some elements
     */
    if (featureMatrix.size() > 0) {
      // verify number of features in element
      if (element.size() != numberOfFeatures && numberOfFeatures != -1) {
        ExceptionManager.throwIAE(
            "Dataset.add number of features in the "
                + "passed element is not compatible with the dataset "
                + element.size() + "-" + numberOfFeatures);
        return false;
      }
      // verify that the label has the same type
      if (element.getLabelType() != labelsType) {
        System.out.println(this.toString());
        System.out.println(element.toString());
        ExceptionManager.throwIAE("Dataset.add the label of the element"
            + " object " + element.getLabelType()
            + " is not " + "compatible with " + labelsType);
        return false;
      }
    }

    // checks if attributes are compatible
    if (attributes != null && attributes.size() > 0) {
      if (element.getAttributes() != null
          && element.getAttributes().size() > 0) {
        if (element.getAttributes().size() != attributes.size()) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Append two datasets, this may be useful when appending training set and
   * testing set for example for cross validation.
   * 
   * @param dataset
   *          the dataset to append to the existing one
   * @return true if the append is successful
   * @throws IllegalArgumentException
   *           if the dataset parameter is not valid
   */
  public boolean append(DataSet<T> dataset) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (dataset == null || dataset.getElements() == null
        || dataset.getElements().size() == 0) {
      ExceptionManager.throwIAE(
          "Dataset.append the passed dataset is invalid " + dataset.toString());
      /* END parameter checking */
    }

    /**
     * Verify that datasets are compliant, we check if they share the same label
     * type and each feature contains the same elements
     */
    if (featureMatrix.size() > 0) {
      if (dataset.getLabelType() != this.getLabelType()) {
        return false;
      }
      if (dataset.getElement(0).size() != this.getElement(0).size()) {
        return false;
      }
    }

    // perform a deep copy
    for (Element<T> e : dataset.getElements()) {
      if (this.add(e) == false) {
        ExceptionManager
            .criticalBehavior("Dataset.append: Append was unsuccessful");
        return false;
      }
    }
    return true;
  }

  /**
   * Clears the actual dataset.
   * 
   * @return true if the clear was successful
   */
  public boolean clear() {
    // reset some attributes
    labelsType = LABEL_TYPE.NOT_PRESENT;
    featureMatrix.clear();
    attributes = new ArrayList<>();
    boolean result = (featureMatrix.size() == 0) ? true : false;
    if (result == false) {
      ExceptionManager.criticalBehavior(
          "DataSet -> clear: Something wrong happened, clear stopped");
      return false;
    }
    return true;
  }

  /**
   * Removes one attribute from the list of attributes, for consistency reasons,
   * removes the corresponding feature too.
   * 
   * @param index
   *          the index of the attribute
   * @throws IllegalArgumentException
   *           if the index parameter is not valid
   */
  public void removeAttribute(int index) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (index < 0) {
      ExceptionManager.throwIAE(
          "Dataset.removeAttribute parameter index is not valid " + index);
      /* END parameter checking */
    }

    /**
     * Try to remove the attribute and the corresponding features. Since the
     * dataset has been changed, the wekaInstances isn't compliant anymore to
     * the actual dataset.
     */
    try {
      // remove the attribute
      if (attributes != null && attributes.size() > 0) {
        attributes.remove(index);
      }
      // remove the corresponding features
      this.removeFeatures(index);
      reset();
    } catch (IndexOutOfBoundsException ioobe) {
      ExceptionManager.indexOut(index, attributes.size(),
          "Dataset.removeAttribute");
      return;
    }
  }

  /**
   * Removes a feature from the matrix of features.
   * 
   * @param index
   *          index of the features NOTE: In order to not leave the dataset
   *          inconsistent, if a feature is removed, then remove the relative
   *          attribute too, if the attribute list is present
   * @throws IllegalArgumentException
   *           if the index is not valid
   */
  private void removeFeatures(int index) throws IllegalArgumentException {
    // remove every feature at passed index
    for (int i = 0; i < featureMatrix.size(); i++) {
      Element<T> element = featureMatrix.get(i);
      element.removeFeature(index);
      featureMatrix.remove(i);
      featureMatrix.add(i, element);
    }
  }

  /**
   * Function to save the dataset to a File. If there are attributes declared,
   * the file will be saved along with an header to make it simpler to load the
   * dataset from the file. Otherwise a very simple format will be used to store
   * the dataset to a file.
   * 
   * @param pathToFile
   *          the path to the file on which store the dataset
   * @return true if the file was read correctly false otherwise
   * @throws IllegalArgumentException
   *           if the parameter is not valid
   */
  public boolean store(String pathToFile) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (pathToFile == null || pathToFile.isEmpty()) {
      ExceptionManager.throwIAE("Dataset.store parameter pathToFile is null");
      /* END parameter checking */
    }

    /* Create a string containing all attributes */
    StringBuffer att = new StringBuffer();
    if (attributes != null && attributes.size() > 0) {
      for (String a : attributes) {
        att.append("$attribute " + a + "\n");
      }
    }

    BufferedWriter bw;
    try {
      // open a file, create a new file if necessary
      File file = new File(pathToFile);
      if (!file.exists()) {
        if (!file.createNewFile()) {
          return false;
        }
      }

      // write dataset to file
      FileWriter fw = new FileWriter(file.getAbsoluteFile());
      bw = new BufferedWriter(fw);
      if (!att.toString().equals("")) {
        bw.write(att.toString());
      }
      bw.write(toString());
      bw.close();
    } catch (IOException ioe) {
      System.err.println("Dataset -> store I/O Error occured");
      return false;
    }

    return true;
  }

  /**
   * Function to store the file as a .arff file, hence here we're using weka
   * functions to store the file according to our needs
   * 
   * @param pathToFile
   *          the absolute path to the file
   * @throws IllegalArgumentException
   *           if the parameter pathToFile is invalid TODO
   */
  public boolean saveAsArff(String pathToFile) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (pathToFile == null) {
      ExceptionManager
          .throwIAE("Dataset.saveAsArff parameter pathToFile is null");
    }
    if (pathToFile.isEmpty()) {
      ExceptionManager
          .throwIAE("Dataset.saveAsArff parameter pathToFile is empty");
      /* END parameter checking */
    }

    /**
     * In order to store the dataset in a format acceptable for weka, we need to
     * remove the ID column because it has to be signaled in some way to weka
     */
    if (columnID != -1) {
      this.removeFeatures(columnID);
      columnID = -1;
    }

    if (isCategorical) {
      try {
        return storeCategorical(pathToFile);
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    }

    // check if the dataset is a weka instances too, if not convert dataset
    // to
    // instances
    if (wekaInstances == null) {
      wekaInstances = WekaFactory.instancesFromDataset(this);
    }

    /**
     * This is how in weka Instances are stored into arff files.
     */
    ArffSaver as = new ArffSaver();
    as.setRetrieval(Saver.BATCH);
    try {
      as.setFile(new File(pathToFile));
      as.setInstances(wekaInstances);
      as.writeBatch();
      return true;
    } catch (IOException ie) {
      System.err.println("Dataset.saveAsArff: I/O exception occured");
      return false;
    }
  }

  /**
   * Function that loads a previously written file which stores a dataset. In
   * this case the file has been formatted as seen before, hence we have a
   * structure: "$attribute attribute_1" "$attribute attribute_2" ...
   * feature,feature,feature,feature, LABEL feature,feature,feature,feature,
   * LABEL ... so the label is assumed to be the LAST element in the feature
   * vector. Hence we have to parse this structure to retrieve attributes and
   * then to retrieve the complete dataset
   * 
   * @param pathToFile
   *          the path to the file
   * @return true if it was possible to load the dataset, false otherwise
   * @throws IllegalArgumentException
   *           if the parameter pathToFile is invalid TODO
   */
  public boolean loadDataset(String pathToFile, boolean classifier)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (pathToFile == null || pathToFile.isEmpty()) {
      ExceptionManager
          .throwIAE("Dataset.loadDataset parameter pathToFile is not valid");
      /* END parameter checking */
    }

    /**
     * check if we're dealing with an arff file, in this case use weka functions
     * to deal with it
     */
    if (pathToFile.contains(".arff") || pathToFile.contains(".csv")) {
      wekaInstances = WekaFactory.buildFromWeka(pathToFile);
      initializeMatrixFromWeka();
      return true;
    }

    // read file and build the dataset
    ArrayList<String> fileContent = Utility.retrieveFileContent(pathToFile,
        "\n");

    // until there are lines to be read
    for (int i = 0; i < fileContent.size(); i++) {
      String line = fileContent.get(i);
      /// change token before attribute
      if (line.charAt(0) == '$') {
        parseAndAddAttribute(line);
      }

      // add the remaining part of the file to the dataset
      else {
        if (classifier) {
          this.fillFromStrings(
              new ArrayList<>(fileContent.subList(i, fileContent.size())), ",",
              DataSet.DEFAULT_LABEL_POS);
        } else {
          this.fillFromStrings(
              new ArrayList<>(fileContent.subList(i, fileContent.size())), ",",
              DataSet.NO_LABEL);
        }
        i = fileContent.size();
        // break;
      }
    }
    return true;
  }

  /**
   * Private function that initializes the dataset starting from the instances
   * matrix of weka. The wekaInstances class should be initialized using an
   * .arff file, once it has been initialized, we use this object to build up
   * our dataset object
   * 
   * @return true if everything goes ok, false otherwise
   */
  private boolean initializeMatrixFromWeka() {
    /* BEGIN parameter checking */
    if (wekaInstances == null) {
      ExceptionManager
          .throwIAE("In constructing the dataset, the weka file is invalid");
      return false;
    }
    /* END parameter checking */

    int labelID = wekaInstances.classIndex();
    attributes = new ArrayList<>();
    /**
     * Manage the attributes, since in weka they're mandatory, do not consider
     * the class attribute, since it will be put directly inside the element
     * object so it is not necessary to have it now
     */
    for (int i = 0; i < wekaInstances.numAttributes(); i++) {
      if (i != labelID) {
        attributes.add(wekaInstances.attribute(i).name());
      }
    }

    // set the qualification accordingly
    if (labelID != -1) {
      this.qualification = TYPE.CLASSIFIER;
    } else {
      this.qualification = TYPE.CLUSTERING;
    }

    ArrayList<String> instances = Utility.instancesToStrings(wekaInstances);
    // copies the instances stored inside the weka class
    if (wekaInstances.classIndex() != -1) {
      return fillFromStrings(instances, ",", wekaInstances.classIndex());
    } else {
      return fillFromStrings(instances, ",", DataSet.NO_LABEL);
    }
  }

  /**
   * Given a string read from a stored dataset, retrieve the attributes.
   * 
   * @param line
   *          a string of the form "$attribute attribute_name"
   * @throws IllegalArgumentException
   *           if the parameter element is null
   */
  public void parseAndAddAttribute(String line)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (line == null || line.isEmpty()) {
      ExceptionManager
          .throwIAE("Dataset.parseAndAddAttribute parameter element is null");
    }
    if (line.isEmpty()) {
      ExceptionManager
          .throwIAE("Dataset.parseAndAddAttribute parameter element is empty");
      /* END parameter checking */
    }

    if (attributes == null) {
      attributes = new ArrayList<>();
    }
    try {
      String[] array = line.split(" ");
      attributes.add(array[1]);
      reset();
    } catch (IndexOutOfBoundsException ioobe) {
      System.err.println("String " + line + " not formatted properly");
      return;
    }
  }

  /**
   * Function that converts the whole dataset into a string, useful for printing
   * informations or to save the dataset.
   * 
   * @return String containing the dataset
   */
  @Override
  public String toString() {
    /// add attribute to the string
    StringBuilder dataset = new StringBuilder();
    if (attributes != null) {
      for (String a : attributes)
        dataset.append("$attribute\t" + a + "\n");
    }

    // scan the featureMatrix
    for (Element<T> element : featureMatrix)
      dataset.append(element.toString() + "\n");
    // convert each feature to String and append it to the String dataset
    return dataset.toString();
  }

  /**
   * change the name of the selected attribute
   * 
   * @param index
   *          the attribute index
   * @param name
   *          the attribute name
   * @throws IllegalArgumentException
   *           if one of the passed parameters is invalid
   */
  public void changeAttributeName(int index, String name)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (name == null || name.isEmpty())
      ExceptionManager
          .throwIAE("Dataset.changeAttributeName parameter name is not valid");
    if (index < 0 || index > numberOfFeatures)
      ExceptionManager.throwIAE(
          "Dataset.changeAttributeName parameter index is not valid: " + index);
    /* END parameter checking */

    // set the attribute name
    attributes.set(index, name);
    reset();
  }

  /**
   * Tries to insert the actual labels as feature, this function is needed
   * whenever we assume a feature to be a label
   * 
   * @return true if it was possible to insert the labels in the feature matrix,
   *         false otherwise
   */
  public boolean labelsAsFeature() {
    /* BEGIN parameter checking */
    if (labelsType.equals(LABEL_TYPE.TEMPLATE)) {
      for (int i = 0; i < featureMatrix.size(); i++) {
        Element<T> element = featureMatrix.get(i);
        element.addConvert(element.getLabelAsString());
        element.setLabel("CLASS");
        featureMatrix.set(i, element);
      }
      return true;
    }
    /* END parameter checking */
    else
      return false;
  }

  /**
   * Removes a single element, i.e., a single row, from the feature matrix.
   * Removes the corresponding label too
   * 
   * @param index
   *          the index of the row to delete
   * @return true if everything goes ok, false otherwise
   */
  public boolean removeElement(int index) {
    /* BEGIN parameter checking */
    if (index < 0 || index > featureMatrix.size()) {
      ExceptionManager.throwIAE("Dataset.removeElement invalid index " + index);
      return false;
    }
    featureMatrix.remove(index);
    reset();
    return true;
  }

  /**
   * Utility function to be called whenever we modify the dataset hence the
   * informations stored in the dataset are no more compliant with the content
   * of the file and/or the wekaInstances object. This utility function is
   * useful whnever we need to convert from dataset to weka.
   */
  private void reset() {
    setPathToFile("");
    if (wekaInstances != null)
      wekaInstances = null;
  }

  /**
   * This function checks if the attribute column i contains categorical
   * features. If so returns all the possible values for that column.
   * 
   * @param index
   *          the index of the attribute to check
   * @return the new attributes value if everything goes ok, null otherwise
   * @throws IllegalArgumentException
   */
  public ArrayList<String> isAttributeCategorical(int index)
      throws IllegalArgumentException {
    ArrayList<String> returnValue = new ArrayList<>();
    /* BEGIN parameter checking */
    if (index < 0 || index >= featureMatrix.get(0).size())
      ExceptionManager.throwIAE(
          "Dataset.retrieveAttributeAsCategorical index not valid " + index);
    /* END parameter checking */
    // retrieve all the features for an attribute

    // first check if the dataset is already into categorical format
    if (isCategorical) {
      /**
       * the warning can be suppressed here since we now that the features we're
       * sending out are already into integer format
       */
      if (classInstance.getName().equals("java.lang.Integer")) {
        for (T feature : differentFeaturesValue(index))
          returnValue.add(feature.toString());
        return returnValue;
      }
      /**
       * if the dataset is already into categorical format, but the type of
       * dataset with which we're dealing with is not Integer, then return null
       */
      else
        return null;
    } else {
      if (differentFeaturesValue(index).size() < getFeaturesByIndex(index)
          .size()) {
        for (T feature : differentFeaturesValue(index))
          returnValue.add(feature.toString());
        return returnValue;
      } else
        return null;
    }
  }

  /**
   * This function returns an Array of Integer, where to each integer
   * corresponds a feature in the dataset, basically this is the most practical
   * way to gather features that are not categorical (e.g. string features) into
   * categorical format
   * 
   * @param index
   *          the index of the attribute in which we're interested into
   * @return the list of features as categorical, where to each number
   *         corresponds the real feature
   */
  public ArrayList<Integer> categoricalValues(int index) {
    System.out.println("Convert attribute to categorical " + isCategorical);
    ArrayList<Integer> intFeatures = new ArrayList<>();
    ArrayList<String> possibleValues = null;
    possibleValues = isAttributeCategorical(index);
    if (possibleValues == null) {
      return null;
    }

    ArrayList<T> features = getFeaturesByIndex(index);
    for (T feature : features) {
      int featureIndex = Utility.indexOf(possibleValues, feature.toString());
      if (!intFeatures.contains(featureIndex))
        intFeatures.add(featureIndex);
    }
    Collections.sort(intFeatures);
    return intFeatures;
  }

  /**
   * Convert the whole dataset to Categorical.
   * <p>
   * <b>EXAMPLE</b> if we have as features "horse", "dog", ... it translates
   * them into 0,1...
   * </p>
   * 
   * @param intervals
   *          this is an optional parameter that can to be used whenever we deal
   *          with integer or Double data
   * @return a vector of integer elements
   */
  public ArrayList<ArrayList<Integer>> toCategorical() {
    ArrayList<ArrayList<Integer>> dataset = new ArrayList<>();
    // build up a copy of the actual dataset
    DataSet<T> data = new DataSet<T>(this) {
    };
    for (int i = 0; i < numberOfFeatures; i++)
      dataset.add(data.categoricalValues(i));
    return dataset;

  }

  /**
   * Retrieve an arrayList containing all the different values for the feature
   * with index index
   * 
   * @param index
   *          the index of the feature we're interested in
   * @return the list of all the differnet values
   */
  public ArrayList<T> differentFeaturesValue(int index) {
    /* BEGIN parameter checking */
    if (index < 0 || index > numberOfFeatures) {
      ExceptionManager.criticalBehavior(
          "Dataset.differentFeatureValues index " + index + "not valid");
      return null;
    }
    /* END parameter checking */
    ArrayList<T> values = new ArrayList<>();
    // checks if the feature is already in the list
    for (Element<T> element : featureMatrix) {
      if (!values.contains(element.getFeature(index)))
        values.add(element.getFeature(index));
    }
    // System.out.println(values.toString());
    return values;
  }

  /**
   * Converts the dataset to String. The label of each element is put in the
   * string only if label is true
   * 
   * @param label
   *          states if the label has to be stored or not in the string
   * @return the dataset as a String
   */
  public String toString(boolean label) {
    if (label)
      return this.toString();
    else {
      StringBuilder dataset = new StringBuilder();
      // we need to provide a different print for the first feature of
      // each
      // element
      boolean first = true;

      // scan the featureMatrix
      for (Element<T> element : featureMatrix) {
        // convert each feature to String and append it to the String
        // dataset
        for (T feature : element.get()) {
          if (first) {
            dataset.append("" + feature.toString());
            first = false;
          } else
            dataset.append("," + feature.toString());
        }
        dataset.append("\n");
        first = true;
      }
      dataset.append("\n");
      return dataset.toString();
    }
  }

  /**
   * In this case this function modifies the actual dataset that stores numeric
   * values into a dataset that stores categorical values. The way in which each
   * numeric attribute has to be converted into the correspondent categorical
   * counterpart is stated by the categorical set
   * 
   * @param categoricalSet
   *          the set to which the various numeric attributes have to be
   *          restricted to
   */
  @SuppressWarnings("unchecked")
  public void convertToCategorical(
      HashMap<Integer, ArrayList<Integer>> categoricalSet) {
    /* BEGIN parameter checking */
    if (categoricalSet == null)
      return;
    if (this.classInstance != Integer.class)
      return;
    /* END parameter checking */

    /**
     * For each element we have to convert the numerical features into
     * categorical ones so that we can apply algorithms that employ categorical
     * features
     */
    int elementIndex = 0;
    for (Element<T> element : featureMatrix) {
      for (Map.Entry<Integer, ArrayList<Integer>> entry : categoricalSet
          .entrySet()) {
        Integer value = (Integer) element.getFeature(entry.getKey());
        Integer featureValue = 0;
        for (Integer boundary : entry.getValue()) {
          if (value <= boundary) {
            featureMatrix.get(elementIndex).changeFeature((T) featureValue,
                entry.getKey());
            break;
          } else
            featureValue += 1;
        }
      }
      elementIndex += 1;
    }
    isCategorical = true;
  }

  /**
   * In this case we have to store the dataset in an arff format keeping in mind
   * that we're dealing with a categorical dataset, hence, in order to be read
   * from weka, we need to use nominal attributes declaring the values of each
   * nominal attribute. If an ID was present, the relative column was removed in
   * the calling saveAsArff function.
   * <p>
   * The storing operation is composed by two differnet parts:
   * <ol>
   * <li>In the first part we write the header as required from weka:
   * <ul>
   * <li>the name of the dataset</li>
   * <li>the name of the attributes and the list of possible values for each
   * attribute</li>
   * <li>the '@data' to effectively start the dataset</li>
   * </ul>
   * </li>
   * <li>In the second we effectively write the content of the dataset</li>
   * </ol>
   * </p>
   * 
   * @param pathToFile
   *          the absolute path to the file to which we want to write
   * @return true if everything goes ok, false otherwise
   * @throws Exception
   */
  private boolean storeCategorical(String pathToFile) throws Exception {
    try {
      File file = new File(pathToFile);
      FileWriter fileWriter = new FileWriter(file);

      // step 1: write the name of the dataset
      fileWriter.write("@relation 'dataset'\n");

      // step2: retrieve the possible values for each feature
      for (int i = 0; i < featureMatrix.get(0).size(); i++) {
        /**
         * for each attribute build up a String (according to weka format) that
         * holds the possible values for the attribute
         */
        ArrayList<T> differentValues = this.differentFeaturesValue(i);
        StringBuilder values = new StringBuilder();
        values.append("{");
        for (T elem : differentValues)
          values.append(elem + ", ");
        // remove the last two dummy characters from the string
        String valuesString = values.toString();
        valuesString = valuesString.substring(0, values.length() - 2);
        valuesString += "}";
        String attribute = "att" + i;
        // step4: write the name of the attribute and the string
        if (attributes != null && attributes.size() > 0)
          attribute = attributes.get(i);

        fileWriter.write("@attribute " + attribute + "\t" + values + "\n");
      }
      fileWriter.write("\n@data\n");
      fileWriter.write(this.toString(false));
      fileWriter.flush();
      fileWriter.close();
      return true;

    } catch (IOException ioe) {
      ExceptionManager.throwException("Dataset.storeCategorical, IOException");
      return false;
    }
  }

  /**
   * Retrieves the list of all the elements having the same value for the
   * feature at index index
   * 
   * @param value
   *          the value of the feature
   * @param index
   *          the index of the feature
   * @return the list of elements having the same value at feature in index
   */
  public ArrayList<Element<T>> getElementByFeature(T value, int index) {
    ArrayList<Element<T>> returnedList = new ArrayList<>();
    for (Element<T> element : featureMatrix) {
      if (element.getFeature(index).equals(value))
        returnedList.add(new Element<T>(element) {
        });
    }
    return returnedList;
  }

  /**
   * Here we split the dataset according to a proportion 80-20, where the 80% of
   * the dataset is used for training and the other 20 is used for classifying.
   * In the splitting we have to keep the proportions of the labels. Basically
   * if in the training set there are 20 elements with label A and 10 of label
   * B, in the training set there will be 16 elements with label A and 8 of
   * label B, the remaining will go in the testing set. <br>
   * We can consider this function really useful in computing cross validation
   * 
   * @param proportion
   *          the proportion of the dataset to be used for the training set
   * @return a vector of two ArrayList, the first one is meant to be used for
   *         training, the other for testing, basically here, we return the
   *         lists of id
   */
  public ArrayList<ArrayList<T>> retrieveTrainingTestingID(double proportion) {
    /* BEGIN parameter checking */
    if (proportion >= 1)
      return null;
    /* END parameter checking */

    /**
     * Instantiate the list to be used in the function to store the IDs
     */
    ArrayList<ArrayList<T>> ids = new ArrayList<>();
    ids.add(new ArrayList<T>());
    ids.add(new ArrayList<T>());

    /**
     * First of all we retrieve all the elements by label, and store them in an
     * hashMap using as key the label of the element and as value the list of
     * elements which share the same label
     */
    HashMap<String, ArrayList<Element<T>>> sameLabel = sameLabel();

    /**
     * For each entry in the hashMap respect the proportion passed as parameter
     * so that, in the end, the proportion of labels between the testing and the
     * training set will be respected
     */
    for (Map.Entry<String, ArrayList<Element<T>>> entry : sameLabel
        .entrySet()) {
      ArrayList<Element<T>> elements = entry.getValue();
      // mix up the elements
      Collections.shuffle(elements);
      int trainingSize = (int) Math.round(elements.size() * proportion);
      for (int i = 0; i < trainingSize; i++)
        ids.get(0).add(elements.get(i).getFeature(0));
      for (int i = trainingSize; i < elements.size(); i++)
        ids.get(1).add(elements.get(i).getFeature(0));
    }

    // last mixing
    Collections.shuffle(ids.get(0));
    Collections.shuffle(ids.get(1));

    // return the matrix of IDs
    return ids;
  }

  /**
   * This function groups the element by label, basically it constructs an
   * HashMap having as key the label and as value the list of elements.
   * 
   * @return the HashMap having as key the label and as value the list of
   *         elements having the same label
   */
  private HashMap<String, ArrayList<Element<T>>> sameLabel() {
    HashMap<String, ArrayList<Element<T>>> sameLabel = new HashMap<>();
    // required in order to mix the elements inside the dataset
    Collections.shuffle(featureMatrix);
    for (Element<T> element : featureMatrix) {
      /**
       * if the label is not contained inside the hashMap then instantiate an
       * arrayList to hold the elements having the same label
       */
      if (!sameLabel.containsKey(element.getLabelAsString())) {
        ArrayList<Element<T>> list = new ArrayList<>();
        list.add(element);
        sameLabel.put(element.getLabelAsString(), list);
      }
      /**
       * Otherwise add the element into one of the existing lists
       */
      else {
        ArrayList<Element<T>> list = sameLabel.get(element.getLabelAsString());
        list.add(element);
        sameLabel.put(element.getLabelAsString(), list);
      }
    }
    return sameLabel;
  }

  /**
   * This function computes the folding set. <br>
   * We provide as parameter the number of folds we want to obtain from the
   * dataset under consideration, this function will split the dataset into as
   * many foldings as needed keeping the proportions of labels as much as
   * possible. Instead of returning the complete Element, this function will
   * return the id of the elements that go in each folding.
   * 
   * @param foldsNumber
   *          the number of foldings
   * @return the id matrix of the elements that go inside the various foldings
   */
  public ArrayList<ArrayList<T>> folding(int foldsNumber) {
    /* BEGIN parameter checking */
    if (foldsNumber < 2)
      return null;
    /* END parameter checking */

    /* Instantiate as many foldings as needed */
    ArrayList<ArrayList<T>> ids = new ArrayList<>();
    for (int i = 0; i < foldsNumber; i++)
      ids.add(new ArrayList<T>());

    // group the elements stored in the dataset by label
    HashMap<String, ArrayList<Element<T>>> sameLabel = sameLabel();

    /**
     * Divide the elements stored inside the list in as many parts as needed
     * trying to keep the proportions between labels
     */
    for (Map.Entry<String, ArrayList<Element<T>>> entry : sameLabel
        .entrySet()) {
      int size = entry.getValue().size();
      int startingPoint = 0;
      for (int i = 0; i < foldsNumber; i++) {
        int parts = (int) Math.round((double) size / (foldsNumber - i));
        for (int j = startingPoint; j < parts + startingPoint; j++) {
          ids.get(i).add(entry.getValue().get(j).getFeature(0));
        }
        Collections.shuffle(ids.get(i));
        startingPoint += parts;
        size -= parts;
      }
    }

    // return the matrix of ids
    return ids;
  }

  // ==========================================================================
  // Accessors
  // ==========================================================================

  /**
   * Retrieves the tInstance
   * 
   * @return tInstance
   */
  public Class<T> gettInstance() {
    return classInstance;
  }

  /**
   * Retrieves the whole dataset as a matrix of features
   * 
   * @return the dataset as a matrix of features
   */
  public ArrayList<ArrayList<T>> getMatrixOfFeatures() {
    ArrayList<ArrayList<T>> matrix = new ArrayList<>();

    for (Element<T> f : featureMatrix) {
      // insert the features
      ArrayList<T> tmp = f.get();
      // insert the label
      matrix.add(tmp);
    }
    return matrix;
  }

  /**
   * Retrieves the list of the distinct label values as String, we use to return
   * them as String since Strings are more manageable
   * 
   * @return the list of distinct label values
   */
  public ArrayList<String> getLabelDistinct() {
    distinctLabels = new ArrayList<>();

    // check if labels are present
    if (labelsType.equals(LABEL_TYPE.NOT_PRESENT)
        || this.qualification.equals(TYPE.CLUSTERING)) {
      System.err.println("No labels provided");
      return null;
    }
    for (Element<T> element : featureMatrix)
      if (!distinctLabels.contains(element.getLabelAsString()))
        distinctLabels.add(element.getLabelAsString());
    return distinctLabels;
  }

  /**
   * Set the labels of the actual feature Matrix, via the passed array of
   * labels. If the elementsLabels has been already initialized, then the actual
   * labels will be put in the featureMatrix. NOTE: The actual label are
   * consdered as feature
   * 
   * @param labelArray
   *          the vector of the new labels
   * @param size
   *          the dimensions of the labelArray
   * @return true if everything goes ok, false otherwise
   * @throws IllegalArgumentException
   *           if one of the parameters is not valid
   */
  public boolean setLabels(String[] labelArray)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (labelArray == null || labelArray.length != featureMatrix.size())
      ExceptionManager
          .throwIAE("Dataset.setLabels parameter labelArray is not valid");
    /* END parameter checking */

    for (int i = 0; i < labelArray.length; i++) {
      Element<T> element = featureMatrix.get(i);
      element.setLabel(labelArray[i].toString());
      featureMatrix.set(i, element);
    }
    return true;

  }

  /**
   * Return the label selected by the index as a String
   * 
   * @param rowIndex
   *          the index of the element that has the label we're interested in
   * @param asString
   *          if the label is String states if we want the label as string or
   *          not
   * @return the label as a String
   */
  public Object getLabel(int rowIndex, boolean asString) {
    /* BEGIN parameter checking */
    if (rowIndex < 0)
      ExceptionManager.throwIAE(
          "Dataset.getLabel parameter rowIndex is not valid: " + rowIndex);
    if (this.qualification.equals(TYPE.CLUSTERING))
      ExceptionManager.throwIAE(
          "Dataset.getLabel, the dataset is meant to be used in clustering");
    /* END parameter checking */
    if (asString == false) {
      int i = 0;
      for (String string : distinctLabels) {
        if (string.equals(this.getElement(rowIndex).getLabel())) {
          return new Integer(i);
        } else {
          i += 1;
        }
      }
      System.err.println("LABEL NOT FOUND");
      return "";
    } else
      return this.getElement(rowIndex).getLabel().toString();
  }

  /**
   * Function that reads all the label values from the dataset. NOTE: here we
   * are obliged to have an ArrayList of generics object because sometimes the
   * labels are String but the class template is Integer or Double
   * 
   * @return the label vectors, null if label undefined
   */
  public ArrayList<Object> getLabel() {
    if (this.qualification.equals(TYPE.CLUSTERING))
      return null;
    // gather all label values if there are any
    ArrayList<Object> values = new ArrayList<>();
    for (int i = 0; i < featureMatrix.size(); i++) {
      Object label = getLabel(i, true);
      // insert the label only if it is not null
      if (label != null)
        values.add(label);
    }
    return values;
  }

  public ArrayList<String> getLabelAsString() {
    if (this.qualification.equals(TYPE.CLUSTERING))
      return null;
    // gather all label values if there are any

    ArrayList<String> values = new ArrayList<>();
    for (int i = 0; i < featureMatrix.size(); i++) {
      Object label = getLabel(i, true);
      // insert the label only if it is not null
      if (label != null)
        values.add(label.toString());
    }
    return values;
  }

  /**
   * Retrieve the number of features in the feature matrix, hence if the number
   * of elements per row is n and the number of elements per column is m, it
   * returns m x n
   * 
   * @return the elements in the matrix if the matrix is correct, -1 otherwise
   */
  public int getDimensions() {
    return (checkCharacteristics())
        ? featureMatrix.size() * featureMatrix.get(0).size()
        : 0;
  }

  /**
   * This function aims at selecting one of the column as label for the dataset.
   * At first checks if it is possible to select the column as Label .This is
   * possible in only one case: that in which labels and features are of the
   * same type
   * 
   * @param index
   *          the index of the elements to set as Label
   * @return true if it was possible to set the selected index as label
   * @throws IllegalArgumentException
   *           if the index is not valid
   */
  public boolean setLabelIndex(int index) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (index < 0 || this.qualification.equals(TYPE.CLUSTERING)
        || index > numberOfFeatures) {
      ExceptionManager
          .throwIAE("Dataset.setLabelIndex can't set label at: " + index);
      return false;
    }
    /* END parameter checking */

    // for each element gather the feature at index, this is the label
    for (int i = 0; i < featureMatrix.size(); i++) {
      Element<T> element = featureMatrix.get(i);
      T label = element.getFeature(index);
      element.setLabel(label.toString());
      featureMatrix.set(i, element);
    }
    return true;
  }

  /**
   * Gets all the features with the same attribute. Basically retrieves from the
   * matrix the column containing all the features specified in the index, which
   * is a column index of course, building an arrayList of these.
   * 
   * @param index
   *          index of the class
   * @return the ArrayList containing all the elements of that class
   * @throws IllegalArgumentException
   *           iae
   */
  public ArrayList<T> getFeaturesByIndex(int index)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (index < 0)
      ExceptionManager
          .throwIAE("Dataset.getAttributesFeatures " + index + " is not valid");
    /* END parameter checking */

    /**
     * For each element, retrieve the requested feature and build an ArrayList
     * of these. In this way we're scanning the dataset by column
     */
    ArrayList<T> appo = new ArrayList<>();
    for (Element<T> element : featureMatrix) {
      try {
        T feature = element.getFeature(index);
        if (feature == null)
          ExceptionManager.criticalBehavior(
              "Dataset.getFeaturesByIndex: NULLFEATURE: " + element);
        if (!appo.add(feature)) {
          ExceptionManager
              .wrongBehavior("Dataset.getAttributeFeatures: Error in adding");
          return null;
        }
      }
      // manage possible exception
      catch (IndexOutOfBoundsException ioobe) {
        ExceptionManager.indexOut(index, element.size(),
            "Dataset.getAttributeFeatures");
        return null;
      }
    }
    return appo;
  }

  /**
   * Function that given an Attribute name returns the index.
   * 
   * @param name
   *          the name of the attribute
   * @return the index of the attribute, -1 if the attribute is not present
   */
  public int getIndexFromAttribute(String name)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (name == null || name.isEmpty() || attributes == null) {
      ExceptionManager
          .throwIAE("Dataset.getIndexFromAttribute parameter name is null");
      return -1;
    }
    /* END parameter checking */

    // System.out.println("getIndexFromAttribute " + name);
    int index = 0;
    for (String s : attributes) {
      // System.out.println(s);
      if (s.equals(name))
        return index;
      ++index;
    }
    return -1;
  }

  /**
   * Auto generated getter and setter
   * 
   * @return the wekaInstances otherwise null
   */
  public Instances getWekaInstances() {
    return wekaInstances;
  }

  /**
   * returns the arraylist of attributes
   * 
   * @return a list containing the names of the attributes
   */
  public ArrayList<String> getAttributes() {
    // for safety reasons we perform a copy of the attributes list
    if (attributes != null)
      return new ArrayList<>(attributes);
    return null;
  }

  /**
   * sets the attributes for the dataset
   * 
   * @param attributes
   *          the list of attributes
   * @return true if the provided list may be a valid attributes list
   * @throws IllegalArgumentException
   *           if the parameter is invalid
   */
  public boolean setAttributes(ArrayList<String> attributes)
      throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (attributes == null || attributes.size() != numberOfFeatures)
      ExceptionManager
          .throwIAE("Dataset.setAttributes parameter attributes is not valid");
    /* END parameter checking */

    /* perform a deep copy of attributes */
    this.attributes = new ArrayList<>(attributes);
    return true;
  }

  /**
   * Method that sets the path to the dataset file
   * 
   * @param p
   *          the path to the file
   * @throws IllegalArgumentException
   *           if parameter p is null
   */
  public void setPathToFile(String p) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (p == null)
      ExceptionManager.throwIAE("Dataset.setPathToFile parameter p is null");
    /* END parameter checking */

    pathToFile = p;
    if (wekaInstances != null)
      wekaInstances = null;
  }

  /**
   * Gathers the feature matrix ArrayList
   * 
   * @return returns a deep copy of the actual feature Matrix
   */
  public ArrayList<Element<T>> getElements() {
    try {
      // perform a deep copy of the actual feature matrix
      return new ArrayList<>(featureMatrix);
    } catch (NullPointerException npe) {
      System.err.println("The matrix of features is null");
      return null;
    }
  }

  public void insertElement(Element<T> element, int index) {
    // BEGIN parameter checking
    if (element == null || index < 0) {
      return;
    }
    // END parameter checking
    featureMatrix.add(index, element);
  }

  /**
   * Get a Element object from a certain index. The Element object represents an
   * element, i.e., a vector of features plus the label
   * 
   * @param index
   *          element position in the matrix
   * @return the Element object if the index is correct, null otherwise
   * @throws IllegalArgumentsException
   *           if the index is not valid <br>
   *           NOTE: to avoid possible future misbehaviors between the elements,
   *           the returned element is a copy of the requested one
   * 
   */
  public Element<T> getElement(int index) throws IllegalArgumentException {
    /* BEGIN parameter checking */
    if (index < 0)
      ExceptionManager
          .throwIAE("Dataset.getElement index" + index + " not valid");
    /* END parameter checking */

    try {
      return new Element<T>(featureMatrix.get(index)) {
      };
    } catch (IndexOutOfBoundsException ioobe) {
      System.err
          .println("Dataset.getElement index " + index + " out of bounds");
      return null;
    }
  }

  /**
   * Retrieves if or not the actual dataset is categorical
   * 
   * @return true if the dataaset is categorical, false otherwise
   */
  public boolean isCategorical() {
    return isCategorical;
  }

  /**
   * Set the actual dataset to categorical
   * 
   * @param cathegorical
   *          true if the dataset is categorical, false otherwise
   */
  public void setCategorical(boolean categorical) {
    this.isCategorical = categorical;
  }

  /**
   * Retrieves the label type of the dataset
   * 
   * @return
   */
  public LABEL_TYPE getLabelType() {
    return labelsType;
  }

  /**
   * This function has to take into account the possibility of having a column
   * devoted to the ID of the Element stored into the dataset. This column
   * 
   * @param index
   *          the index of the column id
   */
  public void setColumnID(int index) {
    /* BEGIN parameter checking */
    if (index < 0)
      return;
    /* END parameter checking */
    columnID = index;
    for (Element<T> element : featureMatrix)
      element.setColumnID(index);
  }

  /**
   * Retrieve the column ID
   * 
   * @return the column ID, -1 if there is no column ID
   */
  public int getColumnID() {
    return columnID;
  }

  /**
   * Retrieves if the dataset is used for classifier or for clustering
   * 
   * @return the qualification of the dataset
   */
  public TYPE getQualification() {
    return qualification;
  }

  public void setQualification(TYPE q) {
    qualification = q;
    if (qualification.equals(TYPE.CLUSTERING))
      labelsType = LABEL_TYPE.NOT_PRESENT;
  }

  public void setNumberOfFeatures(int n) {
    if (n <= 0)
      return;
    numberOfFeatures = n;
  }

  /**
   * Return the path to the file according to which the dataset was created
   * 
   * @return the path to the file according to which the dataset was created
   */
  public String getPathToFile() {
    return pathToFile;
  }

  /**
   * Retrieves the number of features of the dataset
   * 
   * @return the number of features
   */
  public int getNumberOfFeatures() {
    if (featureMatrix.size() > 0)
      numberOfFeatures = featureMatrix.get(0).size();
    return numberOfFeatures;
  }

  /**
   * Retrieves the number of elements in the dataset
   * 
   * @return the number of elements in the dataset
   */
  public int getNumberOfElements() {
    if (featureMatrix == null)
      return -1;
    return featureMatrix.size();
  }

  // --------------------------------------------------------------------------
  // MAIN
  // --------------------------------------------------------------------------
  public static void main(String args[]) {
    /*
     * ArrayList<String> b = new ArrayList<String>(); b.add("ha1"); b.add("c1");
     * b.add("s2"); b.add("ha12"); b.add("c12"); b.add("dsa");
     * b.add(Feature.labelKeyword);
     */
    // DataSet<String> d = new DataSet<String>("/home/antonio/hello.txt",
    // String.class, "\n", ",") {};
    // d.setLabelIndex(2);
    // System.out.println("Set attributes result " + d.setAttributes(b));
    // ald = d.getClass(2);
    // System.out.println(d.toString());
    // System.out.println(d.getLabelType());
    // d.store("/home/antonio/file1.txt");
    // System.out.println(d.getDimensions());
    // ArrayList<Object> l = d.getLabel();
    // for(Object a : l )
    // System.out.println((String) a);
    // DataSet<String> d1 = new DataSet<String>(String.class) {};
    // d1.loadDataset("/home/antonio/file1.txt");
    // System.out.println("Label type " + d1.getLabelType());
    // d1.saveAsArff("/home/antonio/f.arff");
    // DataSet<String> d2 = new DataSet<String>("/home/antonio/f.arff",
    // String.class, "\n", ",") {};
    // Element<Double> f = d1.getElement(2);
    // f.setLabel("LABEL");
    // System.err.println("Printing feature");
    // f.print();
    // d1.add(f);
    // d1.print();
    // System.out.println("THE END");

    // Instances wekaI = WekaFactory.getInstance().instancesFromDataset(d1,
    // Double.class);
    // System.out.println(wekaI);
    // d1.saveAsArff("/home/antonio/file1.arff");

    // DataSet<String> d2 = new DataSet<String>("/home/antonio/file1.arff",
    // String.class, "\n", ",") {};
    // d2.setPathToFile("");

    // Instances i = d1.getWekaInstances();
    // System.out.println(i);
    // d1.removeElement(1);
    // ArrayList<ArrayList<Double>> matrix = d1.getWholeDataSet();
    // for(ArrayList<Double> a : matrix)
    // {
    // for(Double h : a)
    // {
    // Syst in unable to start intentem.out.print(h + " ");
    // }
    // System.out.println("\n----------");
    // }
    // d.print();
    // d1.print();
    // d2.print();
    // System.out.println(d.getIndexFromAttribute("double"));*/
    // DataSet<Integer> dataset = new
    // DataSet<Integer>("/home/antonio/cpu.txt",
    // Integer.class, "\n", ",") {};
    // System.out.println(dataset.getElements().size());
    // dataset.removeFeatures(5);
    // dataset.removeFeatures(3);
    // dataset.retrieveUnique();
    // System.out.println(dataset.getAttributeFeatures(5));
    // System.out.println(Entropy.entropyFromFeatures(dataset, 5));
  }

  // ==========================================================================
  // Unique class
  // ==========================================================================
  /* COUNT UNIQUE */
  private class UniqueElement {
    Element<T> element;
    int classA = 0; // <=50K
    int classB = 0; // >50K

    /**
     * Build up a new UniqueElement with a predefined value
     * 
     * @param element
     */
    public UniqueElement(Element<T> element) {
      this.element = element;
    }

    public void setClass(Element<T> element, int i) {

      if (this.element.equals(element) == Element.SAME_ELEMENT) {
        // System.out.println("Equals " + i);
        String label = element.getLabelAsString();

        if (label.contains("<=50K"))
          this.classA++;
        else
          this.classB++;
      }
    }

    public UniqueElement setClass(ArrayList<Element<T>> list) {
      int i = 0;
      for (Element<T> element : list) {
        this.setClass(element, i);
        if (this.element.equals(element) == Element.SAME_ELEMENT)
          i++;
      }
      // System.out.println(this.classA + "\t" + this.classB);
      return this;
    }

    @Override
    public String toString() {
      String s = element.toString();
      s += "\t" + classA + "\t" + classB;
      return s;
    }

  }

  public void retrieveUnique() {
    ArrayList<UniqueElement> list = new ArrayList<>();
    ArrayList<String> parsedElements = new ArrayList<>();
    for (Element<T> element : featureMatrix) {
      UniqueElement u = new UniqueElement(element);
      if (parsedElements.contains(element.toString()))
        ;
      else {
        parsedElements.add(element.toString());
        list.add(u.setClass(featureMatrix));
      }
    }
    StringBuilder s = new StringBuilder();

    for (UniqueElement u : list) {
      s.append(u.toString() + "\n");
    }
    System.out.println(s.toString());
  }

  /**
   * This function removes the confusing Elements, hence all those elements
   * which have the same features values but different label, those element can
   * bias the classifier, hence they have to be removed before the training set
   * is provided to the classifier. When there are confusing elements, only the
   * ones belonging to the minority class will be removed. <br>
   * This is a very high time consuming operation, to make it faster, the first
   * thing we will compare is the label of the elements, so that we already
   * apply a filter
   * 
   * @param priority
   *          the list of class ordered by the number of elements per class
   */
  public void removeConfusing(ArrayList<String> priority) {
    if (priority == null || priority.size() == 0)
      return;

    // scan the list of elements
    for (int i = 0; i < featureMatrix.size(); i++) {
      Element<T> comparing = featureMatrix.get(i);
      for (int j = 0; j < featureMatrix.size(); j++) {
        Element<T> compared = featureMatrix.get(j);
        if (!comparing.getLabelAsString().equals(compared.getLabelAsString())) {
          ArrayList<T> features;
          features = comparing.get();
          if (columnID != -1)
            features.remove(columnID);
          String comparingFeatures = features.toString();
          features = compared.get();
          if (columnID != -1)
            features.remove(columnID);
          String comparedFeatures = features.toString();
          features = null;
          // System.out.println(comparedFeatures + "\t" +
          // comparingFeatures);
          if (comparingFeatures.equals(comparedFeatures)) {

            int comparingIndex = Utility.indexOf(priority,
                comparing.getLabelAsString());
            int comparedIndex = Utility.indexOf(priority,
                compared.getLabelAsString());
            if (comparingIndex < comparedIndex)
              featureMatrix.remove(j);
            else {
              featureMatrix.remove(i);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * ADD comments
   * 
   * @param map
   * @return
   */
  public DataSet<Integer> convertToCategorical(
      ArrayList<HashMap<String, Integer>> maps) {
    DataSet<Integer> dataset = new DataSet<Integer>(Integer.class) {
    };
    dataset.setCategorical(true);
    dataset.setQualification(TYPE.CLUSTERING);
    // for each element in the old dataset
    for (int i = 0; i < this.getNumberOfElements(); i++) {
      // create a new element
      Element<Integer> element = new Element<Integer>(Integer.class) {
      };
      element.setQualification(TYPE.CLUSTERING);
      // for each feature
      for (int j = 0; j < this.getNumberOfFeatures(); j++) {
        /**
         * insert in the element an integer feature that corresponds to the
         * value gathered using as key the feature of the old dataset.
         */
        element
            .insertFeature(maps.get(j).get(this.getElement(i).getFeature(j)));
      }
      dataset.add(element);
    }
    return dataset;

  }

  /**
   * Splits the dataset in n equal parts
   */
  public ArrayList<DataSet<T>> splitEqual(int number) {
    HashMap<String, ArrayList<Integer>> map = new HashMap<>();
    int index = 0;
    for (Element<T> element : featureMatrix) {
      String label = element.getLabelAsString();
      if (label == null) {
        label = "null";
      }
      if (!map.containsKey(element.getLabelAsString())) {
        map.put(element.getLabelAsString(), new ArrayList<Integer>());
      }
      map.get(element.getLabelAsString()).add(index);
      index += 1;
    }

    for (Map.Entry<String, ArrayList<Integer>> entry : map.entrySet()) {
      Collections.shuffle(entry.getValue());
    }

    ArrayList<DataSet<T>> list = new ArrayList<DataSet<T>>();
    LinkedList<Element> used = new LinkedList<>();
    for (int i = 0; i < number; i++) {
      DataSet<T> dataset = new DataSet<T>(classInstance) {
      };
      dataset.setQualification(getQualification());
      for (Map.Entry<String, ArrayList<Integer>> entry : map.entrySet()) {
        int elem = entry.getValue().size() / (3 - i);
        for (int j = 0; j < elem;) {
          Integer a = entry.getValue().remove(j);
          Element<T> element = featureMatrix.get(a);
          if (used.contains(element)) {
            System.out.println("USED");
            try {
              System.in.read();
            } catch (IOException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
          }
          used.add(element);
          dataset.add(element);
          elem -= 1;
        }
      }
      list.add(dataset);
    }
    // Collections.sort(used);
    // System.out.println(used.toString());
    return list;
  }

}