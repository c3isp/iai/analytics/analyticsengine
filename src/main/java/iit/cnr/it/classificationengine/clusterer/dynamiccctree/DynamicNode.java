package iit.cnr.it.classificationengine.clusterer.dynamiccctree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.Evaluation;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.TYPE;
import iit.cnr.it.classificationengine.basic.Utility;
import iit.cnr.it.classificationengine.clusterer.cctree.Entropy;

/**
 * This class represents a node in the DynamicCCTree. A node is a structure that
 * contains some data, that are the data according to it was generated. Each
 * node is characterized by its purity. Moreover a node stores the index of the
 * attribute according to it was created. A node is characterized by its own ID
 * and its father ID.
 * <p>
 * In the case of a DynamicCCTRee all the logic is put inside a node, it is up
 * to a node to perform all the actions required to build up the cctree. For
 * this reason a DynamicNode holds also a train function that has a similar
 * shape to the train function that is present in the CCTree. Basically, every
 * time one of the conditions does not hold, the train function is called.
 * </p>
 * 
 * @author antonio
 *
 */
public class DynamicNode {

	public static int rootRegenerated = 0;
	private static int costDenominator = 0;

	// -----------------------------------------------------------------------------------------------------------------
	// STOP CONDITIONS
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * This attribute should describe how many elements have to be added to the
	 * CCTree before we can start reevaluating the node under consideration
	 */
	private int startingSize = -1;

	/**
	 * As we add more eleemtns to the actual node, once both the starting size
	 * and the purity condition hold, we evaluate the difference between the
	 * entropy of the attribute selected for splitting before adding the
	 * elements and the one we actually have. If the absolute value of that
	 * difference is higher than this threshold, then we should call the cctree
	 * algorithm on this set of data
	 */
	private double entropyThreshold = -1;

	/**
	 * This is the purity threshold to be used for this node
	 */
	private double purityThreshold = -1;

	/**
	 * This is the minimum size, i.e., the minimum number of elements a node can
	 * contain before splitting
	 */
	private int minimumSize = -1;

	/**
	 * This is the delat purity threshold for not applying the cctree to the
	 * leaf
	 */
	private double deltaPurityThreshold = -1;
	// -----------------------------------------------------------------------------------------------------------------
	// ATTRIBUTES
	// -----------------------------------------------------------------------------------------------------------------
	// this is the list of elements for which the node was originally created
	private DataSet<Integer> originalData;
	// this is the list of elements that have been added to the node
	private DataSet<Integer> addedData;
	// This value represents the actual purity of the node
	private double purity = -1;

	private double externalPurity = -1;
	private ArrayList<Double> weights = null;
	/**
	 * Couple that fully characterized the node with respect to its father The
	 * attribute index is the attribute that has the highest entropy in the
	 * father's dataset, while attribute value is the value of the attribute
	 * associated to this node
	 */
	// the index of the attribute according to the node was created
	private int attributeIndex = -1;
	// the value of the splitting attribute.
	private int attributeValue = -1;
	// the index according to which the children of this node were created
	private int maxEntropyAttribute = -1;

	// states if the node is a leaf or not
	private boolean isLeaf = true;
	// list of children of this node
	private ArrayList<DynamicNode> children;

	private String externalLabel = "";

	private double splitAttributeEntropy = -1;

	/**
	 * The next three attributes fully characterize the node in the tree
	 * <ol>
	 * <li>The id is incremented by each node instance</li>
	 * <li>The nodeId is the id of the actual node</li>
	 * <li>father is the father's nodeId of the actual node</li>
	 * </ol>
	 */
	private static int id = 0;
	int nodeId;
	private int father = -1;

	// =================================================================================================================
	// CONSTRUCTORS
	// =================================================================================================================
	/**
	 * empty node object
	 */
	public DynamicNode() {
		originalData = new DataSet<Integer>(Integer.class) {
		};
		originalData.setQualification(TYPE.CLUSTERING);
		addedData = new DataSet<Integer>(Integer.class) {
		};
		addedData.setQualification(TYPE.CLUSTERING);
		// data.setQualification(Element.CLUSTERING);
		nodeId = id;
		id = id + 1;
		attributeIndex = -1;
	}

	/**
	 * 
	 * @param dataset
	 */
	public DynamicNode(DataSet<Integer> dataset) {
		if (dataset.gettInstance() != Integer.class)
			ExceptionManager.criticalBehavior("Wrong instance of the dataset");
		originalData = dataset;
		// data.setQualification(Element.CLUSTERING);
		originalData.setColumnID(dataset.getColumnID());
		addedData.setColumnID(dataset.getColumnID());
		addedData.setQualification(dataset.getQualification());
		originalData.setQualification(dataset.getQualification());
		nodeId = id;
		id = id + 1;
		attributeIndex = -1;
	}

	/**
	 * Constructor for the class Node
	 * 
	 * @param elements
	 *            the list of the elements belonging to the actual node
	 * @param purity
	 *            the purity of the node
	 * @param attributeIndex
	 *            the index of the attribute according to the node has been
	 *            created
	 */
	public DynamicNode(ArrayList<Element<Integer>> elements, double purity, int attributeIndex,
			int minElementsThreshold, double purityThreshold) {
		// tries to build the node object
		try {
			this.setElementsList(elements);
			this.setPurity(purity);
			this.setAttributeIndex(attributeIndex);
			/* checks if the actual node is a leaf or not */
			if (elements.size() <= minElementsThreshold || purity <= purityThreshold)
				isLeaf = true;
			else
				isLeaf = false;
			// System.out.println(data);
			nodeId = id;
			id = id + 1;
			addedData.setQualification(elements.get(0).getQualification());
			originalData.setQualification(elements.get(0).getQualification());
		}

		/**
		 * If constructor arguments are not valid then exit
		 */
		catch (IllegalArgumentException aie) {
			ExceptionManager.criticalBehavior(
					"Node constructor one or some of the arguments" + " are not valid, the program will stop");
		}
	}

	/**
	 * Constructor of a node which takes as parameter only the elements
	 * contained in that node
	 * 
	 * @param arrayList
	 *            the list of elements contained in that node
	 */
	public DynamicNode(ArrayList<Element<Integer>> arrayList) {
		try {
			this.setElementsList(arrayList);
			// System.out.println(data);
			nodeId = id;
			id = id + 1;
			addedData.setQualification(arrayList.get(0).getQualification());
			originalData.setQualification(arrayList.get(0).getQualification());
		} catch (IllegalArgumentException aie) {
			ExceptionManager
					.criticalBehavior("Node constructor elements parameter is invalid, " + "the program will stop");
		}
	}

	// =================================================================================================================
	// FUNCTIONS
	// =================================================================================================================

	/**
	 * Computes the purity of a node looking for the attribute that provides the
	 * maximum value for the entropy
	 * 
	 * @param weights
	 *            the weights for each attribute
	 * @return the index of the attribute with the highest entropy
	 */
	public int computeMAXEntropyAttribute(ArrayList<Double> weights) {
		// System.out.println("\n***********\nNodeID: " + nodeId);
		// local variables
		double maxEntropy = 0;
		int maxEntropyAttribute = 0;
		double nodePurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		if (originalData.getNumberOfElements() == 0)
			return -1;
		for (int index = 0; index < originalData.getElement(0).size(); index++) {
			if (index == originalData.getColumnID() && index < originalData.getElement(0).size() - 1)
				index = index + 1;
			if (index == originalData.getColumnID() && index == originalData.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(originalData, index);
			if (DynamicCCTree.DEBUG == true)
				System.out.println("Entropy of attribute " + index + "is " + tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
				maxEntropyAttribute = index;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			try {
				weight = weights.get(index);
			}
			// this is not an error, weights may not be provided
			catch (NullPointerException npe) {
			}
			// manage the case of an invalid weights list
			catch (IndexOutOfBoundsException ioobe) {
				ExceptionManager.indexOut(index, weights.size(), "Node.computePurity");
				return -1;
			}
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = nodePurity / (originalData.getElement(0).size());
		this.maxEntropyAttribute = maxEntropyAttribute;
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		// System.out.println("Max entropy attribute: " + maxEntropyAttribute);
		return maxEntropyAttribute;
	}

	/**
	 * Computes the purity of a node looking for the attribute that provides the
	 * maximum value for the entropy
	 * 
	 * @param weights
	 *            the weights for each attribute
	 * @return the index of the attribute with the highest entropy
	 */
	public int computeOverallMAXEntropyAttribute(ArrayList<Double> weights) {
		// System.out.println("\n***********\nNodeID: " + nodeId);
		// local variables
		double maxEntropy = 0;
		int maxEntropyAttribute = 0;
		double nodePurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		ArrayList<Element<Integer>> elements = new ArrayList<>();
		elements.addAll(originalData.getElements());
		elements.addAll(addedData.getElements());
		if (originalData.getNumberOfElements() == 0)
			return -1;
		for (int index = 0; index < originalData.getElement(0).size(); index++) {
			if (index == originalData.getColumnID() && index < originalData.getElement(0).size() - 1)
				index = index + 1;
			if (index == originalData.getColumnID() && index == originalData.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(elements, index);
			if (DynamicCCTree.DEBUG == true)
				System.out.println("Entropy of attribute " + index + "is " + tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
				maxEntropyAttribute = index;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			try {
				weight = weights.get(index);
			}
			// this is not an error, weights may not be provided
			catch (NullPointerException npe) {
			}
			// manage the case of an invalid weights list
			catch (IndexOutOfBoundsException ioobe) {
				ExceptionManager.indexOut(index, weights.size(), "Node.computePurity");
				return -1;
			}
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		// purity = nodePurity / (originalData.getElement(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		// System.out.println("Overall Max Entropy Attribute " +
		// maxEntropyAttribute);
		return maxEntropyAttribute;
	}

	/**
	 * Computes the purity over the original set of elements
	 * 
	 * @return the purity of the original set of elements
	 */
	public double computePurity() {
		double maxEntropy = 0;
		double nodePurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		for (int index = 0; index < originalData.getElement(0).size(); index++) {
			if (index == originalData.getColumnID() && index < originalData.getElement(0).size() - 1)
				index = index + 1;
			if (index == originalData.getColumnID() && index == originalData.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(originalData, index);
			// if (CCTree.DEBUG == true)
			// System.out.println("Entropy of attribute " + index + "is " +
			// tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = nodePurity / (originalData.getElement(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		return purity;
	}

	/**
	 * Computes the overall purity of both the original data and the added ones
	 * 
	 * @return the overall purity of the data stored inside the node
	 */
	public double computeOverallPurity() {
		double maxEntropy = 0;
		double nodePurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy

		ArrayList<Element<Integer>> elements = new ArrayList<Element<Integer>>();
		elements.addAll(originalData.getElements());
		elements.addAll(addedData.getElements());
		for (int index = 0; index < elements.get(0).size(); index++) {
			if (index == originalData.getColumnID() && index < elements.get(0).size() - 1)
				index = index + 1;
			if (index == originalData.getColumnID() && index == elements.get(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(elements, index);
			if (DynamicCCTree.DEBUG == true)
				System.out.println("Entropy of attribute " + index + "is " + tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = nodePurity / (elements.get(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		// System.out.println("Overall Purity " + purity);
		return purity;
	}

	/**
	 * Computes the overall purity of both the original data and the added ones
	 * 
	 * @return the overall purity of the data stored inside the node
	 */
	public double computeOriginalPurity() {
		double maxEntropy = 0;
		double nodePurity = 0;
		double tmpPurity = 0.0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy

		ArrayList<Element<Integer>> elements = new ArrayList<Element<Integer>>();
		elements.addAll(originalData.getElements());
		for (int index = 0; index < elements.get(0).size(); index++) {
			if (index == originalData.getColumnID() && index < elements.get(0).size() - 1)
				index = index + 1;
			if (index == originalData.getColumnID() && index == elements.get(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(elements, index);
			if (DynamicCCTree.DEBUG == true)
				System.out.println("Entropy of attribute " + index + "is " + tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		tmpPurity = nodePurity / (elements.get(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		return tmpPurity;
	}

	/**
	 * Retrieve the entropy of the attribute used for splitting the dataset on
	 * the original Data
	 * 
	 * @return the entropy of the attribute used for splitting the dataset on
	 *         the original data
	 */
	private double computeOriginalSplitAttributeEntropy() {
		if (splitAttributeEntropy != -1)
			return splitAttributeEntropy;
		if (maxEntropyAttribute == -1)
			return -1;
		if (originalData.getNumberOfElements() == 0)
			return 0;
		return Entropy.entropyFromFeatures(originalData, maxEntropyAttribute);
	}

	/**
	 * Computes the entropy of the attribute used for splitting by considering
	 * both dataset, the one of added data and the original one.
	 * 
	 * @return the entropy of the attribute used for splitting when considering
	 *         both dataset
	 */
	private double computeOverallSplitAttributeEntropy() {
		if (maxEntropyAttribute == -1)
			return -1;
		DataSet<Integer> dataset = new DataSet<Integer>(Integer.class) {
		};
		dataset.setQualification(originalData.getQualification());
		dataset.append(originalData);
		if (addedData.getNumberOfElements() > 0)
			dataset.append(addedData);
		dataset.setColumnID(originalData.getColumnID());
		dataset.setQualification(originalData.getQualification());
		double entropy = Entropy.entropyFromFeatures(dataset, maxEntropyAttribute);
		return entropy;
	}

	/**
	 * Add a new element to this node
	 * 
	 * @param element
	 *            the element to insert into the dataset of the node
	 * @throws IllegalArgumentException
	 *             if the parameter passed is invalid
	 */
	public void addElement(Element<Integer> element) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (element == null)
			ExceptionManager.throwIAE("Node.addElement, null element provided");
		/* END parameter checking */

		// System.out.println("Reached here " + element.getLabelType() + " " +
		// data.getLabelType());

		addedData.add(element);
		// if (this.father == -1 && originalData.getNumberOfElements() > 0) {
		// divergenceKullbackLeibler();
		// }
		if (originalData.getNumberOfElements() == 0) {
			originalData.append(addedData);
			resetAddedData();

		} else if (addedData.getNumberOfElements() >= getStartingSize()) {
			// System.out.println("Size: " + addedData.getNumberOfElements() +
			// "\t" + getStartingSize());
			// CHECKED
			if (computeOverallPurity() <= purityThreshold && !isLeaf()) {
				// System.out.println("Purity lower than threshold");
				rejoin();
				originalData.append(addedData);
				resetAddedData();
				// CHECKED
			} else if (computeOverallPurity() > purityThreshold && isLeaf()) { //
				System.out.println("PURITY TRIGGERS for SPLITTING " + computeOverallPurity());
				rejoin();
				originalData.append(addedData);
				resetAddedData();
				cctree();
			} else if ((computeMAXEntropyAttribute(null) != computeOverallMAXEntropyAttribute(null))
					|| computeOverallSplitAttributeEntropy() == -1) {
				/*
				 * else if (isLeaf() && Math.abs(computeOriginalPurity() -
				 * computeOverallPurity()) > deltaPurityThreshold) { //
				 * System.out.println("Delta purity triggers " + //
				 * computeOriginalPurity() + "\t" + computeOverallPurity());
				 * originalData.append(addedData); resetAddedData(); cctree(); }
				 */if (!isLeaf()
						&& ((Math.abs(computeOriginalSplitAttributeEntropy()
								- computeOverallSplitAttributeEntropy()) > entropyThreshold))
						|| computeOverallSplitAttributeEntropy() == -1) {
					// System.out.println("ENTROPY TRIGGERS ");
					// computeOverallSplitAttributeEntropy() + "\t"
					// + computeOriginalSplitAttributeEntropy() + "\t"
					// + Math.abs(computeOriginalSplitAttributeEntropy() -
					// computeOverallSplitAttributeEntropy())
					// + "\t" + entropyThreshold);
					originalData.append(addedData);
					resetAddedData();
					/*
					 * if (maxEntropyAttribute != -1 &&
					 * this.computeMAXEntropyAttribute(weights) ==
					 * maxEntropyAttribute) {
					 * System.out.println("SAME ATTRIBUTE"); if
					 * (maxEntropyAttribute != -1) {
					 * System.out.println("GOING DOWN"); int value =
					 * element.getFeature(maxEntropyAttribute); boolean found =
					 * false; if (children != null) { for (DynamicNode node :
					 * children) {
					 * 
					 * if (node.getAttributeValue() == value) { found = true;
					 * node.addElement(element); break; } }
					 * 
					 * if (found == false) { rejoin(); cctree(); }
					 * 
					 * } }
					 */
					// } else {
					rejoin();
					cctree();
					// }
					return;
				}
			} else if (!isLeaf && children != null) {
				// System.out.println("NOT LEAF ");
				int value = element.getFeature(maxEntropyAttribute);
				boolean found = false;
				if (children != null) {
					for (DynamicNode node : children) {
						if (node.getAttributeValue() == value) {
							found = true;
							node.addElement(element);
							break;
						}
					}

					if (found == false) {
						DynamicNode node = new DynamicNode();
						node.setQualification(this.originalData.getQualification());
						node.setAttributeIndex(maxEntropyAttribute);
						node.setAttributeValue(element.getFeature(maxEntropyAttribute));
						node.setFather(nodeId);
						node.setMinimumSize(minimumSize);
						node.setEntropyThreshold(entropyThreshold);
						node.setPurityThreshold(purityThreshold);
						node.setStartingSize(startingSize);
						node.setDeltaPurityThreshold(deltaPurityThreshold);
						node.originalData.setColumnID(originalData.getColumnID());
						node.addedData.setColumnID(originalData.getColumnID());
						node.addedData.setQualification(originalData.getQualification());
						node.isLeaf = true;
						node.addElement(element);
						originalData.add(element);
						addedData.removeElement(addedData.getNumberOfElements() - 1);
						children.add(node);
					}
				}
			} /*
				 * else { System.out.println("Do not know what to do");
				 * rejoin(); originalData.append(addedData); resetAddedData();
				 * cctree(); try { System.in.read(); } catch (IOException e) {
				 * // TODO Auto-generated catch block e.printStackTrace(); } } }
				 */ else if (maxEntropyAttribute != -1) {
				int value = element.getFeature(maxEntropyAttribute);
				if (children != null) {
					for (DynamicNode node : children) {
						if (node.getAttributeValue() == value) {
							node.addElement(element);
							break;
						}
					}
				} else {
					rejoin();
					originalData.append(addedData);
					resetAddedData();
					cctree();
				}
			}
		}

	}

	private void resetAddedData() {
		addedData = new DataSet<Integer>(Integer.class) {
		};
		addedData.setQualification(originalData.getQualification());
		addedData.setColumnID(originalData.getColumnID());

	}

	/**
	 * This is the rejoin function.
	 * <p>
	 * In our case this function is very simple since we store the whole dataset
	 * in each node, hence to perform a rejoin the only thing to do is to remove
	 * all the children and put this node as a leaf
	 * </p>
	 */
	private void rejoin() {
		// System.out.println("REJOIN");
		if (father == -1) {
			System.out.println("ROOT REGENERATION " + rootRegenerated);
			rootRegenerated += 1;
		}
		// double cost = computeCost();
		// System.out.println("Cost: " + cost);
		children = null;
		this.isLeaf = true;
		maxEntropyAttribute = -1;
	}

	private double computeCost() {
		// System.out.println("ComputeCost: ");
		double numberElement = getNumberOfElements();
		if (costDenominator == 0) {
			System.out.println("CD is 0");
			return -1;
		} else {
			// System.out.println("TOTAL COST on " + nodeId + " : " +
			// (numberElement / costDenominator));
			return numberElement / costDenominator;
		}

	}

	public int getNumberOfElements() {
		// System.out.println(nodeId + " GetElements");
		if (isLeaf || children == null) {
			return addedData.getNumberOfElements() + originalData.getNumberOfElements();
		} else {
			int tot = addedData.getNumberOfElements() + originalData.getNumberOfElements();
			for (DynamicNode node : children) {
				tot += node.getNumberOfElements();
			}
			return tot;
		}
	}

	public static int getCostDenominator() {
		return costDenominator;
	}

	public static void setCostDenominator(int costDenominator) {
		// System.out.println("CD: " + costDenominator);
		DynamicNode.costDenominator = costDenominator;
	}

	/**
	 * This function is used to build up the classifier. In this case we have
	 * unlabeled data and we are interested in building up a tree able to group
	 * the dataset by means of attributes that provide the maximum entropy,
	 * i.e., the maximum amount of information. Building up the tree is
	 * performed in the following steps:
	 * <ul>
	 * <li>initialization phase: Root node N0 takes all data points Dk
	 * <ol>
	 * <li>For each node Ni != leafNode</li>
	 * <li><b>if</b> nodePurity < purityThreshold || numElement < maxNumElement
	 * </li>
	 * <ul>
	 * <li>label Ni as leaf</li>
	 * </ul>
	 * <li><b> else </b></li>
	 * <ol>
	 * <li>for each attribute Aj do</li>
	 * <li>if Aj yields maxShannonEntropy then</li>
	 * <li>use Aj to divide data of Ni and generate new t Nodes with t possible
	 * values for Aj
	 * </ol>
	 * </ol>
	 * <ul>
	 * TODO
	 */
	private void cctree() {
		isLeaf = false;
		ArrayList<DynamicNode> nodes = new ArrayList<DynamicNode>();
		nodes.add(this);
		// System.out.println("CCTREE");
		for (int i = 0; i < nodes.size(); i++) {
			DynamicNode node = nodes.get(i);

			// compute purity and then split nodes int attributeIndex =
			int attributeIndex = node.computeMAXEntropyAttribute(weights);
			nodes.get(i).setMaxEntropyAttribute(attributeIndex);
			// System.out.println("AttributeIndex: " + attributeIndex);
			ArrayList<DynamicNode> tmpNodes = node.splitAttribute(attributeIndex, purityThreshold, minimumSize);
			if (tmpNodes != null) {
				// System.out.println("Node created from " + i + ": " +
				// tmpNodes.size());
				nodes.get(i).children = tmpNodes;
			}
			// add the just created node in the nodes list nodes =
			Utility.chainTogether(nodes, tmpNodes); // if(tmpNodes != null) //
		}

		// prints the tree // System.out.println(root);
		// System.out.println("END training...");
	}

	/**
	 * This function splits the whole dataset creating a node for each different
	 * value of the attribute at index attributeIndex.
	 * 
	 * @param attributeIndex
	 *            the index of the attribute used to split up the dataset
	 * @return the list of nodes just created, i.e, the children of the node. If
	 *         the node can't be further splitted return null and signal the
	 *         actual node as leaf
	 */
	public ArrayList<DynamicNode> splitAttribute(int attributeIndex, double purityThreshold, int minElementsThreshold) {
		// check if the actual node has to be split or not
		if (!(purity > purityThreshold && originalData.getElements()
				.size() > minElementsThreshold))/*
												 * (purity != -1 && purity <=
												 * purityThreshold) ||
												 * data.getElements().size() <=
												 * minElementsThreshold)
												 */
		{
			// signal that the actual node is a leaf
			isLeaf = true;
			return null;
		}

		if (DynamicCCTree.DEBUG == true)
			System.out.println("NodeId: " + nodeId + " Split attribute: " + attributeIndex + " purity: " + purity
					+ " size: " + originalData.getElements().size());

		// retrieve the possible different value for the attribute index
		ArrayList<Integer> attributeValues = originalData.differentFeaturesValue(attributeIndex);

		/**
		 * create a new child node for each possible attribute value and assign
		 * to each child node the corresponding attribute value
		 */
		children = new ArrayList<>();
		for (int i = 0; i < attributeValues.size(); i++) {
			DynamicNode node = new DynamicNode();
			node.setQualification(this.originalData.getQualification());
			node.setAttributeIndex(attributeIndex);
			node.setAttributeValue(attributeValues.get(i));
			node.setFather(nodeId);
			node.setMinimumSize(minimumSize);
			node.setEntropyThreshold(entropyThreshold);
			node.setPurityThreshold(purityThreshold);
			node.setStartingSize(startingSize);
			node.setDeltaPurityThreshold(deltaPurityThreshold);
			node.originalData.setColumnID(originalData.getColumnID());
			node.addedData.setColumnID(originalData.getColumnID());
			node.addedData.setQualification(originalData.getQualification());
			children.add(node);
		}

		// System.out.println(attributeValues);
		// insert each element in the correct node
		for (Element<Integer> element : originalData.getElements()) {
			// use a new element object in order to not alter the existing ones
			Element<Integer> tmpElement = new Element<Integer>(element) {
			};

			/**
			 * retrieve the index of the node that has the same attributeValue
			 * of the considered element
			 */
			int index = Utility.indexOf(attributeValues, element.getFeature(attributeIndex));
			// add the element in the right node
			DynamicNode tmpNode = children.get(index);
			// remove the equal feature
			// tmpElement.removeFeature(attributeIndex);

			tmpElement.setColumnID(originalData.getColumnID());
			tmpNode.originalData.add(tmpElement);
			// swap the stored node with the modified one
			children = Utility.swap(children, tmpNode, index);
		}
		if (DynamicCCTree.DEBUG == true) {
			System.out.println("\n\nNodeID: " + nodeId + " AttributeIndex: " + attributeIndex);
			for (int i = 0; i < children.size(); i++) {
				System.out.println(i + ": " + attributeValues.get(i) + " elements: ");
				// for(Element<Integer> element :
				// children.get(i).getElementsList())
				// {
				// System.out.println(element);
				// }
				System.out.println("**************");
			}
		}
		return children;
	}

	/**
	 * Prints the characteristics of a node
	 */
	public void print() {
		System.out.println("--------------------");
		System.out.println("NodeId: " + nodeId);
		System.out.println(attributeIndex + " " + attributeValue);
		System.out.println(originalData);
		System.out.println("--------------------");
	}

	/**
	 * Convert a node to String
	 */
	@Override
	public String toString() {
		String s = "\n------------\n";
		s += "Father: " + father;

		/**
		 * checks if the node is a leaf or an effective node
		 */
		String quality = "nodeId";
		if (isLeaf) {
			quality = "leafId";
		}
		s += "\t" + quality + ": " + nodeId + "\t AttributeInfos [index: " + attributeIndex + " value: "
				+ attributeValue + "] purity: " + purity + " size: "
				+ (originalData.getElements().size() + addedData.getNumberOfElements()) + "\n";
		s += "O: " + originalData.toString();
		s += "+++++++++++\n";
		s += "A: " + addedData.toString() + "\n";

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(s);
		// retrieves the children of this node
		if (children != null) {
			stringBuilder.append("\n{");
			for (DynamicNode c : children)
				stringBuilder.append(c.toString());
			stringBuilder.append("\n}\n");
		}

		stringBuilder.append("------------\n");

		return stringBuilder.toString();
	}

	/**
	 * Given the list of labels that belong to this node, computes the external
	 * purity of this node
	 * 
	 * @param labels
	 *            the labels of the elements belonging to this node
	 */
	public void computeExternalPurity(ArrayList<String> labels, ArrayList<String> priority) {
		externalPurity = Evaluation.nodeExternalPurity(labels);
		externalPurity = externalPurity * labels.size() / originalData.getElements().size();
		computeNodeLabel(labels, priority);
	}

	/**
	 * Basing on the labels to be set to this node, tries to assign a class to
	 * this node, the list priority is used in the case in which we're not able
	 * to give a class to the node, in this case it will be chosen as label the
	 * class that is most likely to occur among the ones belonging to the node
	 * under consideration
	 * 
	 * @param labels
	 *            the list of labels of the elements inside this node
	 * @param priority
	 *            the list of possible labels ordered by their likelihood to
	 *            occur
	 */
	private void computeNodeLabel(ArrayList<String> labels, ArrayList<String> priority) {
		// System.out.println(labels.toString());
		int[] classes = new int[] { 0, 0, 0, 0, 0, 0 };
		int i = 0;
		ArrayList<String> countedLabels = new ArrayList<>();

		// count the occurrence of each labe that go inside this node
		for (String s : labels) {
			if (!countedLabels.contains(s)) {
				classes[i] = Utility.countOccurrences(s, labels);
				countedLabels.add(s);
				i++;
			}

		}

		int max = 0;
		int maxIndex = -1;
		/**
		 * try to give to this node a class basing on the number of times each
		 * label occurs
		 */
		for (int j = 0; j < classes.length; j++) {

			// System.out.println(seventy + "\t" + data.getElements().size());
			if (classes[j] > max && classes[j] > (labels.size() / 2.0)) {
				max = classes[j];
				maxIndex = j;
			}
		}

		/**
		 * if it was not possible to give a label to this node basing on the
		 * priority list
		 */
		if (maxIndex == -1) {
			// System.out.println("Max index == -1");
			for (String s : priority)
				if (countedLabels.contains(s)) {
					this.externalLabel = s;
					break;
				}
		} else
			this.externalLabel = countedLabels.get(maxIndex);
		// System.out.println("NodeID: " + nodeId + " Label: " +
		// this.externalLabel);
		/*
		 * System.out.println("Counted Labels: " + countedLabels.toString());
		 * System.out.println("Labels passed: " + labels.toString()); for(int j
		 * = 0; j < classes.length; j++) System.out.print(classes[j] + "\t");
		 * System.out.println("DataSet size: " + data.getElements().size());
		 * System.out.println("ExternalLabel: " + externalLabel);
		 * System.out.println("\n*******");
		 */
	}

	/**
	 * In this case the splitting operation is not performed basing on an
	 * attribute but on the label and is performed to obtain a leaf that
	 * contains (as much as possible) elements belonging to the same class. In
	 * fact this is very important for the classifier.
	 */
	public ArrayList<DynamicNode> splitLabel() {
		System.out.println("Split label");
		this.isLeaf = false;
		ArrayList<DynamicNode> nodes = new ArrayList<>();
		ArrayList<String> differentLabels = originalData.getLabelDistinct();
		System.out.println(differentLabels.toString());
		// create as many nodes as needed
		for (int i = 0; i < differentLabels.size(); i++) {
			DynamicNode node = new DynamicNode();
			node.setFather(nodeId);
			node.isLeaf = true;
			nodes.add(node);
		}
		// assign to each node elements of the same label
		for (int i = 0; i < originalData.getNumberOfElements(); i++) {
			int index = Utility.indexOf(differentLabels, originalData.getLabel(i, true).toString());
			DynamicNode node = nodes.get(index);
			node.addElement(originalData.getElement(i));
			nodes.set(index, node);
		}
		this.children = nodes;
		return nodes;
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Getters and setters
	// -----------------------------------------------------------------------------------------------------------------
	public double getPurityThreshold() {
		return purityThreshold;
	}

	public void setPurityThreshold(double purityThreshold) {
		this.purityThreshold = purityThreshold;
	}

	public int getMinimumSize() {
		return minimumSize;
	}

	public void setMinimumSize(int minimumSize) {
		this.minimumSize = minimumSize;
	}

	/**
	 * Set the value of the splitting attribute for this node
	 * 
	 * @param value
	 *            the value of the splitting attribute
	 */
	public void setAttributeValue(int value) {
		attributeValue = value;
	}

	/**
	 * Set the nodeId of the father of this node
	 * 
	 * @param father
	 *            the nodeId of the father
	 */
	public void setFather(int father) {
		this.father = father;
	}

	/**
	 * Retrieves the list of the elements grouped in that node. Basically this
	 * is the list of the elements that share the same attribute value from the
	 * parent
	 * 
	 * @return the list of elements
	 */
	public ArrayList<Element<Integer>> getElementsList() {
		ArrayList<Element<Integer>> list = originalData.getElements();
		list.addAll(addedData.getElements());
		return list;
	}

	/**
	 * Check if the actual node is or not a leaf
	 * 
	 * @return true if the node is a leaf, false otherwise
	 */
	public boolean isLeaf() {
		return isLeaf;
	}

	/**
	 * Sets the list of elements grouped in this node
	 * 
	 * @param elementsList
	 *            the list of elements that form the node
	 * @throws IllegalArgumentException
	 *             if the parameter is not valid
	 */
	public void setElementsList(ArrayList<Element<Integer>> elementsList) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (elementsList == null)
			ExceptionManager.throwIAE("Node.setElementsList the provided parameter is null");
		/* END parameter checking */
		this.originalData = new DataSet<Integer>(elementsList, Integer.class) {
		};
		this.originalData.setColumnID(elementsList.get(0).getColumnID());
	}

	/**
	 * Gets the actual purity of the node
	 * 
	 * @return the purity of the node
	 */
	public double getPurity() {
		return purity;
	}

	/**
	 * Set the purity of the node
	 * 
	 * @param purity
	 *            the purity of the node
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public void setPurity(double purity) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (purity < 0)
			ExceptionManager.throwIAE("Node.setPurity the passed purity is invalid");
		/* END parameter checking */
		this.purity = purity;
	}

	/**
	 * Get the index of the attribute according to which the node was created
	 * 
	 * @return the index of the attribute
	 */
	public int getAttributeIndex() {
		return attributeIndex;
	}

	/**
	 * Set the index of the attribute according to which the node was created
	 * 
	 * @param attributeIndex
	 *            the index of the attribute
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public void setAttributeIndex(int attributeIndex) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (attributeIndex < 0)
			ExceptionManager.throwIAE("Node.setAttributeIndex the passed parameter is invalid");
		/* END parameter checking */
		this.attributeIndex = attributeIndex;
	}

	/**
	 * Retrieve the id of the node
	 * 
	 * @return the id of the node
	 */
	public int getId() {
		return nodeId;
	}

	/**
	 * set the selected node as leaf
	 * 
	 * @param isLeaf
	 *            true to set the node as leaf, false otherwise
	 */
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	/**
	 * retrieve the children of the node under consideration
	 * 
	 * @return the list of node that are the children of the actual node
	 */
	public ArrayList<DynamicNode> getChildren() {
		return children;
	}

	/**
	 * Retrieve the value of the attribute used for splitting this node
	 * 
	 * @return the value of the attribute used for splitting this node
	 */
	public int getAttributeValue() {
		return attributeValue;
	}

	/**
	 * Retrieve the external purity of this node. <br>
	 * If the node of the cctree is used for classification purposes, then we
	 * can compute the external purity from the dataset. In fact, to be used for
	 * classification, the dataset has to be labelled, if it is not, then we
	 * can't apply a classification algorithm on it.
	 * 
	 * @return the external purity of this node
	 */
	public double getExternalPurity() {
		if (originalData.getQualification().equals(TYPE.CLASSIFIER))
			externalPurity = Evaluation.nodeExternalPurity(originalData.getLabelAsString());
		return externalPurity;
	}

	/**
	 * Retrieve the external label of the node
	 * 
	 * @return the string representing the label of the node
	 */
	public String getExternalLabel() {
		return externalLabel;
	}

	public void setQualification(TYPE use) {
		originalData.setQualification(use);
		addedData.setQualification(use);
	}

	public int getStartingSize() {
		return startingSize;
	}

	public void setStartingSize(int startingSize) {
		// System.out.println("Set starting size: " + startingSize);
		this.startingSize = startingSize;
	}

	public double getEntropyThreshold() {
		return entropyThreshold;
	}

	public void setEntropyThreshold(double entropyThreshold) {
		this.entropyThreshold = entropyThreshold;
	}

	/**
	 * Return the number of elements contained in this node
	 */
	public int numberElements() {
		return originalData.getElements().size();
	}

	public ArrayList<Double> getWeights() {
		return weights;
	}

	public void setWeights(ArrayList<Double> weights) {
		this.weights = weights;
	}

	public int getMaxEntropyAttribute() {
		return maxEntropyAttribute;
	}

	public void setMaxEntropyAttribute(int maxEntropyIndex) {
		this.maxEntropyAttribute = maxEntropyIndex;
	}

	public void setColumnId(int id) {
		this.originalData.setColumnID(id);
		this.addedData.setColumnID(id);
	}

	public ArrayList<Integer> getDataId() {
		if (originalData.getColumnID() != -1) {
			ArrayList<Integer> ids = new ArrayList<Integer>();
			ids.addAll(originalData.getFeaturesByIndex(originalData.getColumnID()));
			ids.addAll(addedData.getFeaturesByIndex(originalData.getColumnID()));
			return ids;
		}
		return null;
	}

	public double getDeltaPurityThreshold() {
		return deltaPurityThreshold;
	}

	public void setDeltaPurityThreshold(double deltaPurityThreshold) {
		this.deltaPurityThreshold = deltaPurityThreshold;
	}

	/**
	 * This function is used to build up the classifier. In this case we have
	 * unlabeled data and we are interested in building up a tree able to group
	 * the dataset by means of attributes that provide the maximum entropy,
	 * i.e., the maximum amount of information. Building up the tree is
	 * performed in the following steps:
	 * <ul>
	 * <li>initialization phase: Root node N0 takes all data points Dk
	 * <ol>
	 * <li>For each node Ni != leafNode</li>
	 * <li><b>if</b> nodePurity < purityThreshold || numElement < maxNumElement
	 * </li>
	 * <ul>
	 * <li>label Ni as leaf</li>
	 * </ul>
	 * <li><b> else </b></li>
	 * <ol>
	 * <li>for each attribute Aj do</li>
	 * <li>if Aj yields maxShannonEntropy then</li>
	 * <li>use Aj to divide data of Ni and generate new t Nodes with t possible
	 * values for Aj
	 * </ol>
	 * </ol>
	 * <ul>
	 * TODO
	 */
	/*
	 * public void train() { System.out.println("training..."); //
	 * initialization phase root = new DynamicNode(dataset); // list of all
	 * nodes in the tree nodes = new ArrayList<DynamicNode>(); nodes.add(root);
	 * 
	 * /** To perform splitting of nodes we decided to have a list of all nodes,
	 * since this list will be modified at runtime we cant't rely on iterators,
	 * thus we rely on the dimension of the list that will become bigger and
	 * bigger as the nodes are split up.
	 * 
	 * for (int i = 0; i < nodes.size(); i++) { DynamicNode node = nodes.get(i);
	 * 
	 * // compute purity and then split nodes int attributeIndex =
	 * node.computePurity(weights); ArrayList<DynamicNode> tmpNodes =
	 * node.splitAttribute(attributeIndex, purityThreshold,
	 * minElementsThreshold); if (tmpNodes != null && DEBUG == true)
	 * System.out.println("Node created from " + i + ": " + tmpNodes.size());
	 * 
	 * // add the just created node in the nodes list nodes =
	 * Utility.chainTogether(nodes, tmpNodes); // if(tmpNodes != null) //
	 * System.out.println("Size: " + tmpNodes.size()); }
	 * 
	 * // prints the tree // System.out.println(root);
	 * System.out.println("END training..."); }
	 */

	public double divergenceKullbackLeibler(DataSet<Integer> dataset1, DataSet<Integer> dataset2) {
		// System.out.println("KLSTARTING " + Math.log(1.0));
		ArrayList<Double> divergences = new ArrayList<Double>();
		double overallDivergences = 0.0;
		// pre-check before applying divergence
		for (int i = 1; i < dataset1.getNumberOfFeatures() - 1; i++) {
			HashMap<Integer, Integer> originalMap = new HashMap<Integer, Integer>();
			for (int j = 0; j < dataset1.getNumberOfElements(); j++) {
				if (originalMap.containsKey(dataset1.getElement(j).getFeature(i))) {
					originalMap.put(dataset1.getElement(j).getFeature(i),
							originalMap.get(dataset1.getElement(j).getFeature(i)) + 1);
				} else {
					originalMap.put(dataset1.getElement(j).getFeature(i), 1);
				}
			}
			HashMap<Integer, Integer> addedMap = new HashMap<Integer, Integer>();
			if (dataset2.getNumberOfElements() == 1)
				return -1;
			for (int j = 0; j < dataset2.getNumberOfElements(); j++) {
				if (addedMap.containsKey(dataset2.getElement(j).getFeature(i))) {
					addedMap.put(dataset2.getElement(j).getFeature(i),
							addedMap.get(dataset2.getElement(j).getFeature(i)) + 1);
				} else {
					addedMap.put(dataset2.getElement(j).getFeature(i), 1);
				}
				if (!originalMap.containsKey(dataset2.getElement(j).getFeature(i))) {
					// System.out.println("Feature Absent");
					// return -1;
					originalMap.put(dataset2.getElement(j).getFeature(i), 1);
				}
			}
			// System.out.println("ORIG: " + originalMap.toString());
			// System.out.println("ADDED: " + addedMap.toString());

			ArrayList<Integer> originalValues = new ArrayList<Integer>();
			ArrayList<Integer> addedValues = new ArrayList<Integer>();
			for (Map.Entry<Integer, Integer> entry : originalMap.entrySet()) {
				originalValues.add(entry.getValue());
				Integer integer = entry.getKey();
				if (addedMap.containsKey(integer)) {
					addedValues.add(addedMap.get(integer));
				} else {
					// System.out.println("ADD 0");
					addedValues.add(0);
				}
			}

			ArrayList<Double> originalProbs = Entropy.computeProbabilities(originalValues);
			ArrayList<Double> addedProbs = Entropy.computeProbabilities(addedValues);

			// System.out.println("Original: " + originalProbs.toString());
			// System.out.println("Added: " + addedProbs.toString());

			double divergence = 0.0;
			for (int t = 0; t < originalProbs.size(); t++) {
				if (addedProbs.get(t) == 0) {
					divergence += 0;
				} else {
					// System.out.println(addedProbs.get(t) + "\t" +
					// originalValues.get(t) + "\t"
					// + Math.log(addedProbs.get(t) / originalProbs.get(t)));
					divergence += addedProbs.get(t) * Math.log(addedProbs.get(t) / originalProbs.get(t));
				}
			}
			divergences.add(divergence);
			overallDivergences += divergence;
		}
		System.out.println("DIVERGENCES: " + divergences + "\nDivergence: " + overallDivergences / divergences.size());

		return overallDivergences / divergences.size();
	}

	public void simulateInsertion(DataSet<Integer> dataset) {
		for (Element<Integer> element : dataset.getElements()) {
			addedData.add(element);
		}

		if (originalData.getNumberOfElements() == 0) {
			originalData.append(addedData);
			resetAddedData();

		} else if (addedData.getNumberOfElements() >= getStartingSize()) {
			// System.out.println("Size: " + addedData.getNumberOfElements() +
			// "\t" + getStartingSize());
			// CHECKED
			if (computeOverallPurity() <= purityThreshold && !isLeaf()) {
				// System.out.println("Purity lower than threshold");
				rejoin();
				// CHECKED
			} else if (computeOverallPurity() > purityThreshold && isLeaf()) { //
				System.out.println("PURITY TRIGGERS for SPLITTING " + computeOverallPurity());
				System.out.println("COST: " + nodeId);
				computeCost();
			}
		}
		System.out.println("# of Elements: " + dataset.getElements().size());
		// try {
		// System.in.read();
		// } catch (IOException e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		ArrayList<String> list = new ArrayList<String>();
		for (Element<Integer> element : dataset.getElements()) {
			// System.out.println("*******************InsertELEMENT***********");
			String s = simulateInsertion(element);
			System.out.println("ReturnedString: " + s);
			if (!s.equals("")) {
				list.add(s);
			}
		}
		HashMap<Integer, Double> hashMap = new HashMap<Integer, Double>();
		for (String s : list) {
			Integer index = Integer.parseInt(s.split(",")[0]);
			Double cost = Double.parseDouble(s.split(",")[1]);
			if (hashMap.containsKey(index)) {
				if (cost > hashMap.get(index)) {
					hashMap.put(index, cost);
				}
			} else {
				hashMap.put(index, cost);
			}
		}
		double cost = 0.0;
		for (Map.Entry<Integer, Double> entry : hashMap.entrySet()) {
			System.out.println("Cost: " + entry.getValue());
			cost += entry.getValue();
		}
		System.out.println("Total cost: " + cost);

	}

	public String simulateInsertion(Element<Integer> element) {
		if (addedData.getElementByFeature(element.getFeature(0), 0) != null) {
			addedData.add(element);
		}
		if (originalData.getNumberOfElements() == 0) {
			// originalData.append(addedData);
			// resetAddedData();

		} else /* if (addedData.getNumberOfElements() >= getStartingSize()) */ {
			// System.out.println("Size: " + addedData.getNumberOfElements() +
			// "\t" + getStartingSize());
			// CHECKED
			if (computeOverallPurity() <= purityThreshold && !isLeaf()) {
				// System.out.println("Purity lower than threshold");
				// System.out.println("COST on: " + nodeId);
				double t = computeCost();
				// System.out.println("Cost: " + t);
				return nodeId + "," + t;
				// rejoin();
				// CHECKED
			} else if (computeOverallPurity() > purityThreshold && isLeaf()) { //
				// System.out.println("PURITY TRIGGERS for SPLITTING " +
				// computeOverallPurity());
				// System.out.println("COST: " + nodeId);
				computeCost();
				double t = computeCost();
				// System.out.println("Cost: " + t);
				return nodeId + "," + t;
			} else if ((maxEntropyAttribute != computeOverallMAXEntropyAttribute(null))
					|| computeOverallSplitAttributeEntropy() == -1) {
				/*
				 * else if (isLeaf() && Math.abs(computeOriginalPurity() -
				 * computeOverallPurity()) > deltaPurityThreshold) { //
				 * System.out.println("Delta purity triggers " + //
				 * computeOriginalPurity() + "\t" + computeOverallPurity());
				 * originalData.append(addedData); resetAddedData(); cctree(); }
				 */if (!isLeaf()
						&& ((Math.abs(computeOriginalSplitAttributeEntropy()
								- computeOverallSplitAttributeEntropy()) > entropyThreshold))
						|| computeOverallSplitAttributeEntropy() == -1) {
					// System.out.println("ENTROPY TRIGGERS " +
					// maxEntropyAttribute);
					// computeOverallSplitAttributeEntropy() + "\t"
					// + computeOriginalSplitAttributeEntropy() + "\t"
					// + Math.abs(computeOriginalSplitAttributeEntropy() -
					// computeOverallSplitAttributeEntropy())
					// + "\t" + entropyThreshold);
					// System.out.println("COST: " + nodeId);
					computeCost();
					maxEntropyAttribute = computeOverallMAXEntropyAttribute(null);
					System.out.println("New MAx entropy Attribute on " + nodeId + ": " + maxEntropyAttribute);
					// originalData.append(addedData);
					// resetAddedData();
					/*
					 * if (maxEntropyAttribute != -1 &&
					 * this.computeMAXEntropyAttribute(weights) ==
					 * maxEntropyAttribute) {
					 * System.out.println("SAME ATTRIBUTE"); if
					 * (maxEntropyAttribute != -1) {
					 * System.out.println("GOING DOWN"); int value =
					 * element.getFeature(maxEntropyAttribute); boolean found =
					 * false; if (children != null) { for (DynamicNode node :
					 * children) {
					 * 
					 * if (node.getAttributeValue() == value) { found = true;
					 * node.addElement(element); break; } }
					 * 
					 * if (found == false) { rejoin(); cctree(); }
					 * 
					 * } }
					 */
					// } else {
					// rejoin();
					// cctree();
					// }
					double t = computeCost();
					// System.out.println("Cost: " + t);
					return nodeId + "," + t;
				}
			} else if (!isLeaf && children != null) {
				// System.out.println("NOT LEAF ");
				int value = element.getFeature(maxEntropyAttribute);
				boolean found = false;
				if (children != null) {
					for (DynamicNode node : children) {
						if (node.getAttributeValue() == value) {
							found = true;
							return node.simulateInsertion(element);
							// break;
						}
					}

					if (found == false) {
						/*
						 * DynamicNode node = new DynamicNode();
						 * node.setQualification(this.originalData.
						 * getQualification());
						 * node.setAttributeIndex(maxEntropyAttribute);
						 * node.setAttributeValue(element.getFeature(
						 * maxEntropyAttribute)); node.setFather(nodeId);
						 * node.setMinimumSize(minimumSize);
						 * node.setEntropyThreshold(entropyThreshold);
						 * node.setPurityThreshold(purityThreshold);
						 * node.setStartingSize(startingSize);
						 * node.setDeltaPurityThreshold(deltaPurityThreshold);
						 * node.originalData.setColumnID(originalData.
						 * getColumnID());
						 * node.addedData.setColumnID(originalData.getColumnID()
						 * ); node.addedData.setQualification(originalData.
						 * getQualification()); node.isLeaf = true;
						 * node.simulateInsertion(element);
						 * originalData.add(element);
						 * addedData.removeElement(addedData.getNumberOfElements
						 * () - 1); children.add(node);
						 */
					}
				}
			} else if (maxEntropyAttribute != -1) {
				int value = element.getFeature(maxEntropyAttribute);
				if (children != null) {
					for (DynamicNode node : children) {
						if (node.getAttributeValue() == value) {
							return node.simulateInsertion(element);
							// break;
						}
					}
				} else {
					// System.out.println("COST: " + nodeId);
					// computeCost();
					// rejoin();
					// originalData.append(addedData);
					// resetAddedData();
					// cctree();
				}
			} else {
				// System.out.println("Do not know what to do");
				// System.out.println("COST: " + nodeId);
				// computeCost();
				// rejoin();
				// originalData.append(addedData);
				// resetAddedData();
				// cctree();
				// try {
				// System.in.read();
				// } catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				// }
			}
		}
		return "";
	}

}
