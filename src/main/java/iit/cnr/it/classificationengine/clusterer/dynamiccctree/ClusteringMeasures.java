package iit.cnr.it.classificationengine.clusterer.dynamiccctree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;

//SAAME CLUSTER, SAME LABEL {write function for labeling, the label is the id of the leaf}
/**
 * This is a class with static methods used to evaluate the clustering
 * algorithm.
 * 
 * @author antonio
 *
 */
public class ClusteringMeasures {
	/**
	 * The siulhouette function is computed following <a href=
	 * "https://en.wikipedia.org/wiki/Silhouette_(clustering)">wikipedia</a> In
	 * our case we pass the leaf to the function and compute the silhouette of
	 * the cluster held by the leaf
	 * <p>
	 * This function computes the average silhouette of each element in the
	 * dataset of the cctree
	 * </p>
	 * 
	 * @param node
	 *            the leaf on which we want to compute the silhouette function
	 */
	public static double silhouette(ArrayList<DynamicNode> nodes) {
		// sum of the silhouettes of each single node
		double silhouettes = 0.0;
		int elementNumbers = 0;
		for (int i = 0; i < nodes.size(); i++) {
			DynamicNode node = nodes.get(i);
			// the silhouette can be computed for leaves only
			if (node.isLeaf()) {
				// System.out.println("Node is leaf");
				ArrayList<DynamicNode> externalNodes = new ArrayList<DynamicNode>();
				externalNodes.addAll(nodes.subList(0, i));
				externalNodes.addAll(nodes.subList(i + 1, nodes.size()));
				for (int j = 0; j < node.getElementsList().size(); j++) {
					Element<Integer> element = node.getElementsList().get(j);
					ArrayList<Element<Integer>> others = new ArrayList<Element<Integer>>();
					others.addAll(node.getElementsList().subList(0, j));
					others.addAll(node.getElementsList().subList(j + 1, node.getElementsList().size()));
					silhouettes += elementSilhouette(element, others, externalNodes);
					// System.out.println("Silhouette: " + silhouettes);
					elementNumbers += 1;
				}
			}
		}
		// System.out.println("Silhouette: " + silhouettes + " elementNumbers: "
		// + elementNumbers );
		return silhouettes / elementNumbers;
	}

	/**
	 * This function computes the Hamming distance, according to
	 * <a href="https://it.wikipedia.org/wiki/Distanza_di_Hamming">Wikipedia</a>
	 * the Hamming distance is the number of swaps necessary to make two strings
	 * look equal, in our case it should be the number of features that are
	 * diffenret between two element
	 * 
	 * @param source
	 *            the element used for comparison
	 * @param destination
	 *            the other element used for comparison
	 * @return the distance in terms of features that have different values
	 *         between the two
	 */
	public static <T> int hammingDistance(Element<T> source, Element<T> destination) {
		/* BEGIN parameter checking */
		if (source.size() != destination.size())
			ExceptionManager.throwIAE("The size of the elements is different");
		/* END parameter checking */

		int distance = 0;

		// for each feature
		for (int i = 1; i < source.size(); i++) {
			// if the features in the same position are different then the
			// distance increases
			if (!source.getFeature(i).equals(destination.getFeature(i)))
				distance += 1;
		}
		return distance;
	}

	public static <T> double hammingDistanceNormalized(Element<T> source, Element<T> destination) {
		double hammingDistance = hammingDistance(source, destination);
		double result = hammingDistance / source.size();
		// System.out.println("HD: " + hammingDistance + "size: " +
		// source.size() + "\t" + result );
		return result;
	}

	/**
	 * Here we compute the average dissimilarity of an element and a cluster, it
	 * may be the cluster to which the node belongs or an external one. these
	 * averages are necessary in order to extract the silhouette of the
	 * clustering algorithm
	 * 
	 * @param element
	 *            the element for which we want to compute the
	 *            averageDissimilarity
	 * @param node
	 *            the cluster to be considered (it may be or not the cluster to
	 *            which the node belongs to)
	 * @return a double representing the average dissimilarity
	 */
	private static <T> double averageDissimilarity(Element<Integer> element, ArrayList<Element<Integer>> others) {
		/* BEGIN parameter checking */
		if (element == null || others == null)
			ExceptionManager.throwIAE("Invalid arguments for computing the average dissimilarity");
		/* END parameter checking */

		double dissimilarity = 0.0;
		int elements = 0;
		double result = 0.0;
		// scan the elements contained into the node passed as parameter
		for (Element<Integer> destination : others) {
			dissimilarity += hammingDistanceNormalized(element, destination);
			elements += 1;
			// System.out.println(dissimilarity + "\t" + elements);
		}
		if (elements != 0) {
			// System.out.print("Elements: " + elements + "\t" + dissimilarity);
			result = dissimilarity / elements;
			// System.out.print("\t" + result + "\n");
		}
		return result;
	}

	/**
	 * Computes the silhouette of the element passed as parameter.
	 * <p>
	 * The elemnt passed as parameter belongs to the object node, hence that is
	 * the internal cluster to which we compute the internal dissimilarity,
	 * instead the externalNodes list is the list of the nodes external to the
	 * one we're considering
	 * </p>
	 * 
	 * @param element
	 *            the element for which we want to compute the silhouette
	 * @param node
	 *            the cluster to which the element belogns to
	 * @param externalNodes
	 *            the list of nodes external to the node
	 * @return a double representing the silhouette of the element
	 */
	private static double elementSilhouette(Element<Integer> element, ArrayList<Element<Integer>> others,
			ArrayList<DynamicNode> externalNodes) {
		/* BEGIN compute the average internal dissimilarity */
		double internalDissimilarity = 0.0;
		internalDissimilarity = averageDissimilarity(element, others);
		// System.out.println("InternalDissimilarity: " +
		// internalDissimilarity);
		/* END compute the average internal dissimilarity */

		/* BEGIN compute the lowest average external dissimilarity */
		double averageExternalDissimilarity = 0.0;
		double lowestDissimilarity = 100.0;
		for (DynamicNode extNode : externalNodes) {
			if (extNode.isLeaf()) {
				// System.out.println(extNode.nodeId + "ext is leaf");
				averageExternalDissimilarity = averageDissimilarity(element, extNode.getElementsList());
				if (averageExternalDissimilarity < lowestDissimilarity)
					lowestDissimilarity = averageExternalDissimilarity;
			}
		}
		/* END compute the lowest average external dissimilarity */
		double max = Math.max(lowestDissimilarity, internalDissimilarity);
		// System.out.println("Lowest: " + lowestDissimilarity + "\tinternal " +
		// internalDissimilarity + "\tmax: " + max);
		double silhouette = 0.0;
		if (max != 0.0)
			silhouette = (lowestDissimilarity - internalDissimilarity) / max;
		// else
		// System.out.println("Max dissimilarity is 0");
		return silhouette;
	}

	/**
	 * This function is called whenever we use weka to generate the cluster, in
	 * that case we can rely only on the measures that weka provides us, but
	 * sometimes these measures are not good enough. Hence this function is in
	 * charge of performing the following things:
	 * <ol>
	 * <li>Parse the file removing the additions performed by weka
	 * <ul>
	 * <li>headers</li>
	 * <li>attributes and class</li>
	 * <ul>
	 * </li>
	 * <li>Build up the tree basing on the informations provided by the
	 * file</li>
	 * </ol>
	 * 
	 * @param pathToFile
	 *            the absolute path to the file
	 */
	public static <T> ArrayList<DynamicNode> analyzeWekaResults(String pathToFile) {
		/* BEGIN parameter checking */
		if (pathToFile == null || pathToFile.equals(""))
			return null;
		/* END parameter checking */

		// build up the dataset from the file
		DataSet<Integer> dataset = DataSet.buildDatasetClassifier(pathToFile, "\n", ",", Integer.class);
		// remove the not interesting column
		dataset.removeAttribute(0);
		dataset.setColumnID(0);

		return buildTree(dataset);
	}

	/**
	 * Build the tree from the passed dataset object.
	 * <p>
	 * By assumption the dataset is the dataset retrieved from weka, so we can
	 * use the label to assess the node, hence at first we retrieve the
	 * structure of the tree using an HashMap where the key is the label and the
	 * value is the node and we update the nodes while we scan the dataset. Once
	 * we have this structure, we convert it into an arrayList in order to make
	 * it more maneagable.
	 * </p>
	 * 
	 * @param dataset
	 *            the dataset object from whci hwe want to build the tree
	 * @return the list of nodes that form the cluster
	 */
	private static ArrayList<DynamicNode> buildTree(DataSet<Integer> dataset) {
		HashMap<String, DynamicNode> nodeMap = new HashMap<String, DynamicNode>();
		ArrayList<DynamicNode> nodes = new ArrayList<DynamicNode>();

		// retrieve the elements from the dataset
		for (Element<Integer> element : dataset.getElements()) {
			String label = element.getLabelAsString();
			if (nodeMap.containsKey(label)) {
				DynamicNode node = nodeMap.get(label);
				node.addElement(element);
				node.setLeaf(true);
				nodeMap.put(label, node);
			} else {
				DynamicNode node = new DynamicNode();
				node.addElement(element);
				node.setLeaf(true);
				nodeMap.put(label, node);
			}
		}

		// for the map, build up the arraylist
		for (Map.Entry<String, DynamicNode> entry : nodeMap.entrySet())
			nodes.add(entry.getValue());

		// System.out.println(nodes.toString());

		return nodes;
	}

	public static double computeAveragePurity(ArrayList<DynamicNode> nodes) {
		double purity = 0.0;
		int leaves = 0;
		for (DynamicNode node : nodes) {
			if (node.isLeaf()) {
				// System.out.print(node.getPurity() + "\t" +
				// node.getElementsList().size());
				purity += (node.computeOverallPurity() * node.getElementsList().size());
				leaves += node.getElementsList().size();
				// System.out.println(purity + "\t" + leaves + "\n");
			}
		}
		// System.out.println(purity + "\t" + leaves);
		return purity / leaves;
	}

	public static void main(String[] args) {
		ArrayList<DynamicNode> nodes = ClusteringMeasures.analyzeWekaResults("/home/antonio/cobweb.arff");
		System.out.println(nodes.toString());
	}

	/**
	 * Count the leaves of the clusterer algorithm, it outputs also the number
	 * of outliers (i.e. the number of node that contain a single element)
	 * 
	 * @param structure
	 *            the list of nodes that form the clusterer algorithm
	 * @return the number of leaves
	 */
	public static int countLeaves(ArrayList<DynamicNode> structure) {
		int leaves = 0;
		int oneElement = 0;
		for (DynamicNode node : structure) {
			if (node.isLeaf())
				leaves++;
			if (node.getElementsList().size() < 2)
				oneElement++;
		}
		System.out.println("One element " + oneElement);
		return leaves;
	}

	/**
	 * Retrieves the id of the elements that fall in the outliers category,
	 * hence this function returns the list of elements that form outliers in
	 * the cctree
	 * 
	 * @param nodes
	 *            the nodes of the cctree
	 * @return the list of elements that are outliers
	 */
	public static ArrayList<Integer> retrieveOutliersID(ArrayList<DynamicNode> nodes) {
		// the list that will be returned of ids
		ArrayList<Integer> ids = new ArrayList<Integer>();

		/**
		 * Scan the list of nodes looking for outliers
		 */
		for (DynamicNode node : nodes)
			if (node.isLeaf())
				if (node.getElementsList().size() == 1) {
					Integer nextId = node.getElementsList().get(0).getFeature(0);
					boolean inserted = false;
					// ordered insertion
					for (int i = 0; i < ids.size() && inserted == false; i++)
						if (nextId > ids.get(i)) {
							ids.add(i, nextId);
							inserted = true;
						}
					if (inserted == false)
						ids.add(nextId);
				}
		// System.out.println(ids.toString());
		return ids;
	}
}
