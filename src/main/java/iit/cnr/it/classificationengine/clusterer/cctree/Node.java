package iit.cnr.it.classificationengine.clusterer.cctree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.Evaluation;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.TYPE;
import iit.cnr.it.classificationengine.basic.Utility;

/**
 * This class represents a node in the CCTree. A node is a structure that
 * contains some data, that are the data according to it was generated. Each
 * node is characterized by its purity. Moreover a node stores the index of the
 * attribute according to it was created. A node is characterized by its own ID
 * and its father ID.
 * 
 * 
 * @author antonio
 *
 */
public class Node {

	/* Private class attribute */
	// the list of the elements that compose the node
	private DataSet<Integer> data;
	// the purity of the node
	private double purity = -1;

	private double externalPurity = -1;

	/**
	 * Couple that fully characterized the node with respect to its father The
	 * attribute index is the attribute that has the highest entropy in the
	 * father's dataset, while attribute value is the value of the attribute
	 * associated to this node
	 */
	// the index of the attribute according to the node was created
	private int attributeIndex = -1;
	// the value of the splitting attribute.
	private int attributeValue = -1;

	// states if the node is a leaf or not
	private boolean isLeaf;
	// list of children of this node
	private ArrayList<Node> children;

	private String externalLabel = "";

	/**
	 * The next three attributes fully characterize the node in the tree
	 * <ol>
	 * <li>The id is incremented by each node instance</li>
	 * <li>The nodeId is the id of the actual node</li>
	 * <li>father is the father's nodeId of the actual node</li>
	 * </ol>
	 */
	private static int id = 0;
	int nodeId;
	private int father = -1;

	/**
	 * empty node object
	 */
	public Node() {
		data = new DataSet<Integer>(Integer.class) {
		};
		data.setColumnID(0);
		// data.setQualification(Element.CLUSTERING);
		nodeId = id;
		id = id + 1;
		attributeIndex = -1;
	}

	/**
	 * 
	 * @param dataset
	 */
	public Node(DataSet<Integer> dataset) {
		if (dataset.gettInstance() != Integer.class)
			ExceptionManager.criticalBehavior("Wrong instance of the dataset");
		data = dataset;
		// data.setQualification(Element.CLUSTERING);
		data.setColumnID(dataset.getColumnID());
		nodeId = id;
		id = id + 1;
		attributeIndex = -1;
	}

	/**
	 * Set the value of the splitting attribute for this node
	 * 
	 * @param value
	 *            the value of the splitting attribute
	 */
	public void setAttributeValue(int value) {
		attributeValue = value;
	}

	/**
	 * Set the nodeId of the father of this node
	 * 
	 * @param father
	 *            the nodeId of the father
	 */
	public void setFather(int father) {
		this.father = father;
	}

	/**
	 * Retrieves the list of the elements grouped in that node. Basically this
	 * is the list of the elements that share the same attribute value from the
	 * parent
	 * 
	 * @return the list of elements
	 */
	public ArrayList<Element<Integer>> getElementsList() {
		return data.getElements();
	}

	/**
	 * Check if the actual node is or not a leaf
	 * 
	 * @return true if the node is a leaf, false otherwise
	 */
	public boolean isLeaf() {
		return isLeaf;
	}

	/**
	 * Sets the list of elements grouped in this node
	 * 
	 * @param elementsList
	 *            the list of elements that form the node
	 * @throws IllegalArgumentException
	 *             if the parameter is not valid
	 */
	public void setElementsList(ArrayList<Element<Integer>> elementsList) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (elementsList == null)
			ExceptionManager.throwIAE("Node.setElementsList the provided parameter is null");
		/* END parameter checking */
		this.data = new DataSet<Integer>(elementsList, Integer.class) {
		};
		this.data.setColumnID(elementsList.get(0).getColumnID());
	}

	/**
	 * Gets the actual purity of the node
	 * 
	 * @return the purity of the node
	 */
	public double getPurity() {
		return purity;
	}

	/**
	 * Set the purity of the node
	 * 
	 * @param purity
	 *            the purity of the node
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public void setPurity(double purity) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (purity < 0)
			ExceptionManager.throwIAE("Node.setPurity the passed purity is invalid");
		/* END parameter checking */
		this.purity = purity;
	}

	/**
	 * Get the index of the attribute according to which the node was created
	 * 
	 * @return the index of the attribute
	 */
	public int getAttributeIndex() {
		return attributeIndex;
	}

	/**
	 * Set the index of the attribute according to which the node was created
	 * 
	 * @param attributeIndex
	 *            the index of the attribute
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public void setAttributeIndex(int attributeIndex) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (attributeIndex < 0)
			ExceptionManager.throwIAE("Node.setAttributeIndex the passed parameter is invalid");
		/* END parameter checking */
		this.attributeIndex = attributeIndex;
	}

	/**
	 * Constructor for the class Node
	 * 
	 * @param elements
	 *            the list of the elements belonging to the actual node
	 * @param purity
	 *            the purity of the node
	 * @param attributeIndex
	 *            the index of the attribute according to the node has been
	 *            created
	 */
	public Node(ArrayList<Element<Integer>> elements, double purity, int attributeIndex, int minElementsThreshold,
			double purityThreshold) {
		// tries to build the node object
		try {
			this.setElementsList(elements);
			this.setPurity(purity);
			this.setAttributeIndex(attributeIndex);
			/* checks if the actual node is a leaf or not */
			if (elements.size() <= minElementsThreshold || purity <= purityThreshold)
				isLeaf = true;
			else
				isLeaf = false;
			// System.out.println(data);
			nodeId = id;
			id = id + 1;
		}

		/**
		 * If constructor arguments are not valid then exit
		 */
		catch (IllegalArgumentException aie) {
			ExceptionManager.criticalBehavior(
					"Node constructor one or some of the arguments" + " are not valid, the program will stop");
		}
	}

	/**
	 * Constructor of a node which takes as parameter only the elements
	 * contained in that node
	 * 
	 * @param arrayList
	 *            the list of elements contained in that node
	 */
	public Node(ArrayList<Element<Integer>> arrayList) {
		try {
			this.setElementsList(arrayList);
			// System.out.println(data);
			nodeId = id;
			id = id + 1;
		} catch (IllegalArgumentException aie) {
			ExceptionManager
					.criticalBehavior("Node constructor elements parameter is invalid, " + "the program will stop");
		}
	}

	/**
	 * Computes the purity of a node looking for the attribute that provides the
	 * maximum value for the entropy
	 * 
	 * @param weights
	 *            the weights for each attribute
	 * @return the index of the attribute with the highest entropy
	 */
	public int computePurity(ArrayList<Double> weights) {
		// System.out.println("\n***********\nNodeID: " + nodeId);
		// local variables
		double maxEntropy = 0;
		int maxEntropyAttribute = 0;
		double nodePurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		for (int index = 0; index < data.getElement(0).size(); index++) {
			if (index == data.getColumnID() && index < data.getElement(0).size() - 1)
				index = index + 1;
			if (index == data.getColumnID() && index == data.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(data, index);
			if (CCTree.DEBUG == true)
				System.out.println("Entropy of attribute " + index + "is " + tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
				maxEntropyAttribute = index;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			try {
				weight = weights.get(index);
			}
			// this is not an error, weights may not be provided
			catch (NullPointerException npe) {
			}
			// manage the case of an invalid weights list
			catch (IndexOutOfBoundsException ioobe) {
				ExceptionManager.indexOut(index, weights.size(), "Node.computePurity");
				return -1;
			}
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = nodePurity / (data.getElement(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		return maxEntropyAttribute;
	}

	public double computePurity() {
		double maxEntropy = 0;
		double nodePurity = 0;

		// dummy vairable to be used if weights list lacks
		double weight = 1;

		// for each attribute in the dataset compute the entropy
		for (int index = 0; index < data.getElement(0).size(); index++) {
			if (index == data.getColumnID() && index < data.getElement(0).size() - 1)
				index = index + 1;
			if (index == data.getColumnID() && index == data.getElement(0).size() - 1)
				break;
			double tmpEntropy = Entropy.entropyFromFeatures(data, index);
			// if (CCTree.DEBUG == true)
			// System.out.println("Entropy of attribute " + index + "is " +
			// tmpEntropy + "\n\n");
			// checks if the actual entropy is higher than the maximum one
			if (maxEntropy < tmpEntropy) {
				// update maxEntropy and the index of the attribute with higher
				// entropy
				maxEntropy = tmpEntropy;
			}
			// System.out.println("Index: " + index + " Entropy: " +
			// tmpEntropy);
			/**
			 * try to retrieve the weight of the current attribute, the weights
			 * list is not mandatory
			 */
			// compute node purity
			nodePurity = nodePurity + weight * tmpEntropy;
		}
		// average the purity over the number of attributes
		purity = nodePurity / (data.getElement(0).size());
		// if(CCTree.DEBUG == true)
		// System.out.println("Purity: " + purity + " max entropy attribute: " +
		// maxEntropyAttribute + "\t" + data.getElements().size());
		return purity;
	}

	/**
	 * Add a new element to this node
	 * 
	 * @param element
	 *            the element to insert into the dataset of the node
	 * @throws IllegalArgumentException
	 *             if the parameter passed is invalid
	 */
	public void addElement(Element<Integer> element) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (element == null)
			ExceptionManager.throwIAE("Node.addElement, null element provided");
		/* END parameter checking */

		// System.out.println("Reached here " + element.getLabelType() + " " +
		// data.getLabelType());

		data.add(element);
	}

	/**
	 * This function splits the whole dataset creating a node for each different
	 * value of the attribute at index attributeIndex.
	 * 
	 * @param attributeIndex
	 *            the index of the attribute used to split up the dataset
	 * @return the list of nodes just created, i.e, the children of the node. If
	 *         the node can't be further splitted return null and signal the
	 *         actual node as leaf
	 */
	public ArrayList<Node> splitAttribute(int attributeIndex, double purityThreshold, int minElementsThreshold) {
		// check if the actual node has to be split or not
		if (!(purity > purityThreshold && data.getElements()
				.size() > minElementsThreshold))/*
												 * (purity != -1 && purity <=
												 * purityThreshold) ||
												 * data.getElements().size() <=
												 * minElementsThreshold)
												 */
		{
			// System.out.println("NULL");
			// signal that the actual node is a leaf
			// System.out.println("LeafId: " + nodeId + " purity: " + purity + "
			// size:
			// " + data.getElements().size());
			isLeaf = true;
			return null;
		}

		if (CCTree.DEBUG == true)
			System.out.println("NodeId: " + nodeId + " Split attribute: " + attributeIndex + " purity: " + purity
					+ " size: " + data.getElements().size());

		// retrieve the possible different value for the attribute index
		ArrayList<Integer> attributeValues = data.differentFeaturesValue(attributeIndex);

		/**
		 * create a new child node for each possible attribute value and assign
		 * to each child node the corresponding attribute value
		 */
		children = new ArrayList<>();
		for (int i = 0; i < attributeValues.size(); i++) {
			Node node = new Node();
			node.setQualification(this.data.getQualification());
			node.setAttributeIndex(attributeIndex);
			node.setAttributeValue(attributeValues.get(i));
			node.setFather(nodeId);
			children.add(node);
		}

		// System.out.println(attributeValues);

		// insert each element in the correct node
		for (Element<Integer> element : data.getElements()) {
			// use a new element object in order to not alter the existing ones
			Element<Integer> tmpElement = new Element<Integer>(element) {
			};
			/**
			 * retrieve the index of the node that has the same attributeValue
			 * of the considered element
			 */
			int index = Utility.indexOf(attributeValues, element.getFeature(attributeIndex));
			// add the element in the right node
			Node tmpNode = children.get(index);
			// remove the equal feature
			// tmpElement.removeFeature(attributeIndex);

			tmpElement.setColumnID(data.getColumnID());
			// System.out.println(element);
			tmpNode.addElement(tmpElement);
			// swap the stored node with the modified one
			children = Utility.swap(children, tmpNode, index);

		}
		if (CCTree.DEBUG == true) {
			System.out.println("\n\nNodeID: " + nodeId + " AttributeIndex: " + attributeIndex);
			for (int i = 0; i < children.size(); i++) {
				System.out.println(i + ": " + attributeValues.get(i) + " elements: ");
				// for(Element<Integer> element :
				// children.get(i).getElementsList())
				// {
				// System.out.println(element);
				// }
				System.out.println("**************");
			}
		}
		return children;
	}

	/**
	 * Prints the characteristics of a node
	 */
	public void print() {
		System.out.println("--------------------");
		System.out.println("NodeId: " + nodeId);
		System.out.println(attributeIndex + " " + attributeValue);
		System.out.println(data);
		System.out.println("--------------------");
	}

	/**
	 * Convert a node to String
	 */
	@Override
	public String toString() {
		String s = "\n------------\n";
		s += "Father: " + father;

		/**
		 * checks if the node is a leaf or an effective node
		 */
		String quality = "nodeId";
		if (isLeaf)
			quality = "leafId";
		s += "\t" + quality + ": " + nodeId + "\t AttributeInfos [index: " + attributeIndex + " value: "
				+ attributeValue + "] purity: " + purity + " size: " + data.getElements().size() + "Label: "
				+ this.externalLabel + "\n";
		s += data.toString();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(s);
		// retrieves the children of this node
		if (children != null) {
			stringBuilder.append("\n{");
			for (Node c : children)
				stringBuilder.append(c.toString());
			stringBuilder.append("\n}\n");
		}

		stringBuilder.append("------------\n");

		return stringBuilder.toString();
	}

	/**
	 * Return the number of elements contained in this node
	 */
	public int numberElements() {
		return data.getElements().size();
	}

	/**
	 * Retrieve the id of the node
	 * 
	 * @return the id of the node
	 */
	public int getId() {
		return nodeId;
	}

	/**
	 * set the selected node as leaf
	 * 
	 * @param isLeaf
	 *            true to set the node as leaf, false otherwise
	 */
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	/**
	 * retrieve the children of the node under consideration
	 * 
	 * @return the list of node that are the children of the actual node
	 */
	public ArrayList<Node> getChildren() {
		return children;
	}

	/**
	 * Retrieve the value of the attribute used for splitting this node
	 * 
	 * @return the value of the attribute used for splitting this node
	 */
	public int getAttributeValue() {
		return attributeValue;
	}

	/**
	 * Retrieve the external purity of this node. <br>
	 * If the node of the cctree is used for classification purposes, then we
	 * can compute the external purity from the dataset. In fact, to be used for
	 * classification, the dataset has to be labelled, if it is not, then we
	 * can't apply a classification algorithm on it.
	 * 
	 * @return the external purity of this node
	 */
	public double getExternalPurity() {
		if (data.getQualification().equals(TYPE.CLASSIFIER))
			externalPurity = Evaluation.nodeExternalPurity(data.getLabelAsString());
		return externalPurity;
	}

	/**
	 * Given the list of labels that belong to this node, computes the external
	 * purity of this node
	 * 
	 * @param labels
	 *            the labels of the elements belonging to this node
	 */
	public void computeExternalPurity(ArrayList<String> labels, ArrayList<String> priority) {
		externalPurity = Evaluation.nodeExternalPurity(labels);
		externalPurity = externalPurity * labels.size() / data.getElements().size();
		computeNodeLabel(labels, priority);
	}

	public void computeExternalPurity() {
		getExternalPurity();
		computeNodeLabel();
	}

	/**
	 * Basing on the labels to be set to this node, tries to assign a class to
	 * this node, the list priority is used in the case in which we're not able
	 * to give a class to the node, in this case it will be chosen as label the
	 * class that is most likely to occur among the ones belonging to the node
	 * under consideration
	 * 
	 * @param labels
	 *            the list of labels of the elements inside this node
	 * @param priority
	 *            the list of possible labels ordered by their likelihood to
	 *            occur
	 */
	private void computeNodeLabel(ArrayList<String> labels, ArrayList<String> priority) {
		// System.out.println(labels.toString());
		int[] classes = new int[] { 0, 0, 0, 0, 0, 0 };
		int i = 0;
		ArrayList<String> countedLabels = new ArrayList<>();

		// count the occurrence of each labe that go inside this node
		for (String s : labels) {
			if (!countedLabels.contains(s)) {
				classes[i] = Utility.countOccurrences(s, labels);
				countedLabels.add(s);
				i++;
			}

		}

		int max = 0;
		int maxIndex = -1;
		/**
		 * try to give to this node a class basing on the number of times each
		 * label occurs
		 */
		for (int j = 0; j < classes.length; j++) {

			// System.out.println(seventy + "\t" + data.getElements().size());
			if (classes[j] > max && classes[j] > (labels.size() / 2.0)) {
				max = classes[j];
				maxIndex = j;
			}
		}

		/**
		 * if it was not possible to give a label to this node basing on the
		 * priority list
		 */
		if (maxIndex == -1) {
			// System.out.println("Max index == -1");
			for (String s : priority)
				if (countedLabels.contains(s)) {
					this.externalLabel = s;
					break;
				}
		} else
			this.externalLabel = countedLabels.get(maxIndex);
		// System.out.println("NodeID: " + nodeId + " Label: " +
		// this.externalLabel);
		/*
		 * System.out.println("Counted Labels: " + countedLabels.toString());
		 * System.out.println("Labels passed: " + labels.toString()); for(int j
		 * = 0; j < classes.length; j++) System.out.print(classes[j] + "\t");
		 * System.out.println("DataSet size: " + data.getElements().size());
		 * System.out.println("ExternalLabel: " + externalLabel);
		 * System.out.println("\n*******");
		 */
	}

	/**
	 * Basing on the labels to be set to this node, tries to assign a class to
	 * this node, the list priority is used in the case in which we're not able
	 * to give a class to the node, in this case it will be chosen as label the
	 * class that is most likely to occur among the ones belonging to the node
	 * under consideration
	 * 
	 * @param labels
	 *            the list of labels of the elements inside this node
	 * @param priority
	 *            the list of possible labels ordered by their likelihood to
	 *            occur
	 */
	private void computeNodeLabel() {
		if (data.getQualification() != TYPE.CLASSIFIER) {
			return;
		}
		// System.out.println(labels.toString());
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		// count the occurrence of each labe that go inside this node
		for (Object s : data.getLabel()) {
			if (map.containsKey(s)) {
				int i = map.get(s);
				i += 1;
				map.put(s.toString(), i);
			} else
				map.put(s.toString(), 1);
		}

		int max = 0;
		/**
		 * try to give to this node a class basing on the number of times each
		 * label occurs
		 */
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			// System.out.println(seventy + "\t" + data.getElements().size());
			if (entry.getValue() > max && entry.getValue() > (map.entrySet().size() / 2.0)) {
				max = entry.getValue();
				externalLabel = entry.getKey();
			}
		}

		/**
		 * if it was not possible to give a label to this node basing on the
		 * priority list
		 */

		// System.out.println("NodeID: " + nodeId + " Label: " +
		// this.externalLabel);
		/*
		 * System.out.println("Counted Labels: " + countedLabels.toString());
		 * System.out.println("Labels passed: " + labels.toString()); for(int j
		 * = 0; j < classes.length; j++) System.out.print(classes[j] + "\t");
		 * System.out.println("DataSet size: " + data.getElements().size());
		 * System.out.println("ExternalLabel: " + externalLabel);
		 * System.out.println("\n*******");
		 */
	}

	/**
	 * Retrieve the external label of the node
	 * 
	 * @return the string representing the label of the node
	 */
	public String getExternalLabel() {
		return externalLabel;
	}

	/**
	 * In this case the splitting operation is not performed basing on an
	 * attribute but on the label and is performed to obtain a leaf that
	 * contains (as much as possible) elements belonging to the same class. In
	 * fact this is very important for the classifier.
	 */
	public ArrayList<Node> splitLabel() {
		System.out.println("Split label");
		this.isLeaf = false;
		ArrayList<Node> nodes = new ArrayList<>();
		ArrayList<String> differentLabels = data.getLabelDistinct();
		System.out.println(differentLabels.toString());
		// create as many nodes as needed
		for (int i = 0; i < differentLabels.size(); i++) {
			Node node = new Node();
			node.setFather(nodeId);
			node.isLeaf = true;
			nodes.add(node);
		}
		// assign to each node elements of the same label
		for (int i = 0; i < data.getNumberOfElements(); i++) {
			int index = Utility.indexOf(differentLabels, data.getLabel(i, true).toString());
			Node node = nodes.get(index);
			node.addElement(data.getElement(i));
			nodes.set(index, node);
		}
		this.children = nodes;
		return nodes;
	}

	public void setQualification(TYPE use) {
		data.setQualification(use);
	}

	public ArrayList<Integer> getDataId() {
		if (data.getColumnID() != -1) {
			ArrayList<Integer> ids = new ArrayList<Integer>();
			ids.addAll(data.getFeaturesByIndex(data.getColumnID()));
			return ids;
		}
		return null;
	}
}
