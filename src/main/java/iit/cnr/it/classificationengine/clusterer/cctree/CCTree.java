package iit.cnr.it.classificationengine.clusterer.cctree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import iit.cnr.it.classificationengine.basic.ClassifierEvaluation;
import iit.cnr.it.classificationengine.basic.Clusterer;
import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.LABEL_TYPE;
import iit.cnr.it.classificationengine.basic.TYPE;
import iit.cnr.it.classificationengine.basic.Utility;

/**
 * This class represents the CCTree. The CCTree is basically a clustering
 * algorithm that operates on categorical data. It was presented in this
 * <a href=
 * "http://link.springer.com/chapter/10.1007%2F978-3-319-24858-5_1">paper</a>.
 * It basically builds up a tree starting from a categorical dataset trying to
 * put similar data into the same nodes. The evaluation of similarity is
 * performed basing on the Shannon entropy of each attribute. <br>
 * In a cctree we set two parameters:
 * <ol>
 * <li><i>purity</i> the internal purity of the node computed basing on the
 * entropy of each attribute</li>
 * <li><i>min elements threshold</i> the minimum number of elements that have to
 * be hosted inside a node</li>
 * </ol>
 * <br>
 * The CCTree can be used also for classification, in this case the set of data
 * must be labelled, moreover, in order to consider a node as a leaf, we have to
 * look also at the labels it contains, we can't consider a node as a leaf if
 * the external purity for that node is above a certain threshold. For this
 * reason there are two slightly different ways of training a CCTree, one is the
 * standard basing on the purity of the nodes, the other one, that can be
 * applied only in the cases in which we are using the CCTree for classification
 * purposes takes into account also the purity of the labels that fall inside
 * the same leaf
 * 
 * @author antonio
 */

public class CCTree extends Clusterer<Integer> {
	public static boolean DEBUG = false;
	// list of weights for each attribute
	private ArrayList<Double> weights;
	/* list of nodes that form the tree */

	/**
	 * The CCTree works onlhy with categorical data, for this reason we may need
	 * to have a data structure that helps us in categorizing the dataset.
	 * Instead of a list we pick an HashMap because we can have mixed datasets
	 * in which some data are categorical and others are not-categorical
	 */
	HashMap<Integer, ArrayList<Integer>> categoricalSets;

	// some constants that can be set up by user
	private int minElementsThreshold = 2;
	private double purityThreshold = 0.01;

	private double externalPurityThreshold = 1.0;

	// the root node of the tree
	private Node root;
	private ArrayList<Node> nodes;
	/**
	 * In this list we will hold the id of the nodes that we will use to build
	 * up the small knowledge
	 */
	private ArrayList<Integer> smallKnowledgeNodeIDs = new ArrayList<Integer>();
	// public int notConsider = -1;

	/* BASIC constructors */

	/**
	 * this is the empty CCTree constructor
	 */
	public CCTree() {
	}

	/**
	 * The CCTree is built up from a pre-existing dataset
	 * 
	 * @param dataset
	 *            the dataset on which we want to build up the tree
	 */
	public CCTree(DataSet<Integer> dataset) {
		super(dataset);
	}

	/**
	 * This function is used to build up the classifier. In this case we have
	 * unlabeled data and we are interested in building up a tree able to group
	 * the dataset by means of attributes that provide the maximum entropy,
	 * i.e., the maximum amount of information. Building up the tree is
	 * performed in the following steps:
	 * <ul>
	 * <li>initialization phase: Root node N0 takes all data points Dk
	 * <ol>
	 * <li>For each node Ni != leafNode</li>
	 * <li><b>if</b> nodePurity < purityThreshold || numElement < maxNumElement
	 * </li>
	 * <ul>
	 * <li>label Ni as leaf</li>
	 * </ul>
	 * <li><b> else </b></li>
	 * <ol>
	 * <li>for each attribute Aj do</li>
	 * <li>if Aj yields maxShannonEntropy then</li>
	 * <li>use Aj to divide data of Ni and generate new t Nodes with t possible
	 * values for Aj
	 * </ol>
	 * </ol>
	 * <ul>
	 * TODO
	 */
	public void train() {
		System.out.println("training...");
		// initialization phase
		root = new Node(dataset);
		// list of all nodes in the tree
		nodes = new ArrayList<Node>();
		nodes.add(root);

		/**
		 * To perform splitting of nodes we decided to have a list of all nodes,
		 * since this list will be modified at runtime we cant't rely on
		 * iterators, thus we rely on the dimension of the list that will become
		 * bigger and bigger as the nodes are split up.
		 */
		for (int i = 0; i < nodes.size(); i++) {
			Node node = nodes.get(i);

			// compute purity and then split nodes
			int attributeIndex = node.computePurity(weights);
			ArrayList<Node> tmpNodes = node.splitAttribute(attributeIndex, purityThreshold, minElementsThreshold);
			if (tmpNodes != null && DEBUG == true)
				System.out.println("Node created from " + i + ": " + tmpNodes.size());

			// add the just created node in the nodes list
			nodes = Utility.chainTogether(nodes, tmpNodes);
			// if(tmpNodes != null)
			// System.out.println("Size: " + tmpNodes.size());
		}

		// prints the tree
		// System.out.println(root);
		System.out.println("END training...");
	}

	/**
	 * As said in the header of this class, the training part of the CCTree in
	 * case of classification is similar to the training part of the CCTree in
	 * case of clustering. The only difference is that before considering a node
	 * a leaf we have also to take into account the labels of the elements
	 * inside that node. If they do not respect a threshold stated a priori,
	 * then we have to perform another split
	 */
	public void trainClassifier() {
		// the CCTree is used for classification

		System.out.println("training classifier... " + dataset.getQualification());
		root = new Node(dataset);
		root.setQualification(TYPE.CLASSIFIER);
		// list of all nodes in the tree
		nodes = new ArrayList<Node>();
		nodes.add(root);

		/**
		 * To perform splitting of nodes we decided to have a list of all nodes,
		 * since this list will be modified at runtime we cant't rely on
		 * iterators, thus we rely on the dimension of the list that will become
		 * bigger and bigger as the nodes are split up.
		 */
		for (int i = 0; i < nodes.size(); i++) {
			Node node = nodes.get(i);
			node.setQualification(TYPE.CLASSIFIER);

			// compute purity and then split nodes
			if (node.getExternalPurity() < externalPurityThreshold) {
				int attributeIndex = node.computePurity(weights);
				ArrayList<Node> tmpNodes = node.splitAttribute(attributeIndex, 0, 0);
				if (tmpNodes != null && DEBUG == true)
					System.out.println("Node created from " + i + ": " + tmpNodes.size());

				// add the just created node in the nodes list
				nodes = Utility.chainTogether(nodes, tmpNodes);
			}

			else {
				node.setLeaf(true);
				nodes.set(i, node);
			}
			// if(tmpNodes != null)
			// System.out.println("Size: " + tmpNodes.size());
		}

		// prints the tree
		// System.out.println(root);
		System.out.println("END training...");

	}

	/**
	 * The list of parameters provided to the CCTree cluster.
	 * 
	 * <pre>
	 * -w w1,w2.... To set up the weights for the various attributes
	 * 
	 * <pre>
	 * -p purity To set up the purity threshold
	 * 
	 * <pre>
	 * -e maxElements To set up the max number of elements
	 * 
	 * @param parameters
	 *            the list of parameters
	 */
	@Override
	public void setParameters(ArrayList<String> parameters) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (parameters == null)
			ExceptionManager.throwIAE("CCTree.setParameters null parameter");
		if (parameters.size() == 0)
			return;
		/* END parameter checking */

		for (String s : parameters) {
			// retrieve the argument
			String[] args = s.split(" ");
			// check the required operation and perform it
			if (args[0].contains("-w"))
				setWeights(args[1]);
			if (args[0].contains("-p"))
				setPurityThreshold(Utility.convert(args[1], Double.class));
			if (args[0].contains("-e"))
				setMinElements(Utility.convert(args[1], Integer.class));
		}
	}

	/**
	 * Creates the list of weights NOTE: since it is a static function, setting
	 * up weights for a CCTree object leads to all the CCTree objects having the
	 * same weights
	 * 
	 * @param param
	 *            the list of weights in String format
	 */
	public void setWeights(String param) {
		String[] weight = param.split(",");
		weights = new ArrayList<Double>();
		int length = weight.length;
		for (int i = 0; i < length; i++) {
			weights.add(i, Utility.convert(weight[i], Double.class));
		}

	}

	/**
	 * Set the purity threshold for each node
	 * 
	 * @param purity
	 */
	public void setPurityThreshold(Double purity) {
		purityThreshold = purity;
	}

	/**
	 * Set the minimum number of elements for each node, if a node stores less
	 * than this value it doesn't make any sense to split it up
	 * 
	 * @param minElements
	 *            the minimum number of elements above which the node can be
	 *            split
	 */
	public void setMinElements(Integer minElements) {
		minElementsThreshold = minElements;
	}

	/**
	 * Removes nodes by size, i.e., all nodes that contain a number of elements
	 * less than size will be removed from the tree
	 * 
	 * @param size
	 *            the minimum size of nodes
	 */
	public void removeNodes(int size) {
		int outlayers = 0, clusters = 0;
		ArrayList<Node> tmp = new ArrayList<Node>();
		for (Node node : nodes) {
			// System.out.println(node.numberElements());
			if (node.isLeaf()) {
				if (node.numberElements() <= size)
					outlayers++;
				else {
					clusters++;
					tmp.add(node);
				}
			} else
				tmp.add(node);
		}
		System.out.println("Outlayers: " + outlayers + " Clusters: " + clusters);
		nodes = new ArrayList<Node>(tmp);
		System.out.println("Nodes: " + nodes.size());
	}

	public int getSize() {
		return nodes.size();
	}

	/**
	 * Convert the dataset held by the CCTree to categorical (if possible)
	 * 
	 * @param categoricalSets
	 *            the hashMap of categorical sets
	 */
	public void convertToCategorical(HashMap<Integer, ArrayList<Integer>> categoricalSets) {
		if (categoricalSets == null)
			return;
		this.categoricalSets = categoricalSets;
		this.dataset.convertToCategorical(this.categoricalSets);
	}

	/**
	 * Retrieve the average purity of the CCTree
	 * 
	 * @return the average purity of the CCTree
	 */
	public double averagePurity() {
		double purity = 0.0;
		int leaves = 0;
		for (Node node : nodes) {
			if (node.isLeaf()) {
				// System.out.print(node.getPurity() + "\t" +
				// node.getElementsList().size());
				purity += (node.getPurity() * node.getElementsList().size());
				leaves += node.getElementsList().size();
				// System.out.println("\t" + purity + "\t" + leaves + "\n");
			}
		}
		// System.out.println(purity + "\t" + leaves);
		return purity / leaves;
	}

	public int countLeaves() {
		int leaf = 0;
		for (Node node : nodes)
			if (node.isLeaf())
				leaf++;
		return leaf;
	}

	/**
	 * Retrieve the structure of the CCTree
	 * 
	 * @return the list of nodes that form the CCTree
	 */
	public ArrayList<Node> getStructure() {
		return this.nodes;
	}

	/**
	 * Retrieve the root of the CCTree
	 * 
	 * @return the root of the CCTree
	 */
	public Node getRoot() {
		return root;
	}

	/**
	 * For each node present in the cctree and in the map nodeLabels which has
	 * as key the ID of the node and as Value the list of labels related to that
	 * node, compute the external purity of that node
	 * 
	 * @param nodeLabels
	 *            the node id and the list of labels related to that node
	 */
	public void computeNodesExternalPurity(HashMap<Integer, ArrayList<String>> nodeLabels, ArrayList<String> priority) {
		for (Node node : nodes) {
			if (nodeLabels.containsKey(node.getId()))
				node.computeExternalPurity(nodeLabels.get(node.getId()), priority);
		}
	}

	public void computeExternalPurity() {
		for (Node node : nodes) {
			if (node.isLeaf()) {
				node.computeExternalPurity();
			}
		}
	}

	/**
	 * Tries to classify the element passed as parameter. Since after the
	 * clustering phase in the cctree we may have elements belonging to
	 * different classes inside the same node, then this function always returns
	 * an error, it is better to use the other function in which we provide a
	 * list so that, if we do not know to which class an element belongs to, we
	 * have a list of priorities
	 * 
	 * @param element
	 *            the element we want to classify
	 * @param int
	 *            the result of the classification
	 * @deprecated
	 */
	@Override
	public int classify(Element<Integer> element) {
		return -1;
	}

	/**
	 * This is the function to use if we want to classify an element using the
	 * CCTree. We have to provide not only the
	 * 
	 * @param element
	 *            the element we want to classify
	 * @param priority
	 *            the priority list
	 * @return a String representing the class to which the element passed
	 *         belongs to
	 */
	public String classify(Element<Integer> element, ArrayList<String> priority) {
		return classify(element, true);
	}

	// not static
	public static void main(String[] args) throws IOException {
		/* TODO add parameter to state if dataset is categorical or numeric */
		DataSet<Integer> dataset = DataSet.buildDatasetClusterer(
				"/home/antonio/malwareClassification/StaticFeaturesID" + ".txt", "\n", ",", Integer.class);
		// System.out.println(dataset);
		dataset.labelsAsFeature();

		// dataset.setCathegorical(true);
		dataset.setColumnID(0);
		// dataset.removeFeatures(0);
		// dataset.removeFeatures(dataset.getElement(0).size() - 1);
		// dataset.removeFeatures(dataset.getElement(0).size() - 1);
		// dataset.setLabelIndex(0);
		// this has to be performed because the dataset doesn't have any label

		// dataset.removeFeatures(20);
		CCTree cctree = new CCTree(dataset);
		cctree.convertToCategorical(null);
		cctree.train();
		double avgs = ClusteringMeasures.silhouette(cctree.nodes);
		System.out.println("Avg silhouette is: " + avgs);
		// System.out.println("Nodes: " + cctree.getSize() + " ");
		// cctree.removeNodes(1);
		// System.out.println("Nodes: " + cctree.getSize() + " ");
		// cctree.print();
		File file = new File("/home/antonio/malwareClassification/cctreeSFIDmin5purity1.txt");
		try {
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(cctree.root.toString());
			fileWriter.flush();
			fileWriter.close();
			System.out.println("finished");
			System.out.println("Average purity is " + cctree.averagePurity());
		} catch (Exception e) {

		}
		System.out.println("Clusters: " + cctree.countLeaves());
		cctree.removeNodes(1);
		// System.out.println(cctree.root);
		// System.out.println("HELLO!");
	}

	/**
	 * This function is in charge of classifying a list of elements with an
	 * already provieded label, moreover we pass to this function a list of
	 * classes in order to compute the confusion matrix
	 * 
	 * @param elements
	 *            the list of elements to be considered as testing set
	 * @param classes
	 *            the list of classes
	 * @return ClassifierEvaluation object in charge of holding the various
	 *         performance of this classifier
	 */
	public ClassifierEvaluation classify(ArrayList<Element<Integer>> elements, ArrayList<String> classes) {
		/* BEGIN parameter checking */
		if (elements == null || elements.size() == 0)
			return null;
		if (classes == null || classes.size() == 0)
			return null;
		/* END parameter checking */

		HashMap<Integer, String> classification = new HashMap<Integer, String>();
		for (Element<Integer> element : elements) {
			// for each element perform the classification
			String className = classify(element, true);
			//System.out.println("CLASS: " + className);
			if (!className.equals("")) {
				//System.out.println(element.getLabel() + "\t" + className);
				classification.put(element.getFeature(0), className.trim());
			}
		}
		// System.out.println(classification.toString());
		return ClassifierEvaluation.buildClassifierEvaluation(elements, classification, classes);
	}

	/**
	 * Classify an element, the additional parameter asString is added in order
	 * to allow overloading
	 * 
	 * @param element
	 *            the element to be classified
	 * @param asString
	 *            states that we want the label in string format
	 * @return the name of the class, an empty string will be returned if it was
	 *         not possible to classify the element
	 */
	private String classify(Element<Integer> element, boolean asString) {
		Node node = root;
		// scan the tree in the deep
		while (true) {
			// retrieve the children of the actual node
			ArrayList<Node> children = node.getChildren();
			/**
			 * For each child retrieve the attribute value according to which
			 * the child was created
			 */
			// System.out.println("##########################################");
			// System.out.println("Classification of " + element.toString() );
			boolean found = false;
			if (children == null)
				return "";
			for (Node child : children) {
				found = false;
				int attributeIndex = child.getAttributeIndex();
				int attributeValue = child.getAttributeValue();
				// System.out.println("attIndex: " + attributeIndex + " value: "
				// + attributeValue + "NodeID: " + child.getId() + "\t" +
				// element.getFeature(attributeIndex));
				Integer featureValue = Integer.parseInt(element.getFeature(attributeIndex).toString());
				/**
				 * If the attribute value according to which the node was
				 * created is the same of the Element passed as parameter, then
				 * we have to consider the node
				 */
				if (attributeValue == featureValue.intValue()) {
					found = true;
					node = child;
					// if the node is a leaf, then the search can stop
					if (node.isLeaf()) {
						//System.out.println("NodeID: " + node.getId() + " Label: " + node.getExternalLabel() + "\t"
							//	+ node.getPurity() + "\t" + node.getElementsList().size());
						//System.out
							//	.println("ElemID: " + element.getFeature(0) + " Label: " + element.getLabelAsString());
						//System.out.println("********************");
						// System.out.println(element);
						// System.out.println("DATASET");
						// System.out.println(node.toString());
						// System.out.println("#########################################");
						return node.getExternalLabel();
					}
					break;
				}
			}
			/**
			 * In the CCTree it may happen that sometimes the element we're
			 * looking for is not allocated in the tree because it may be an
			 * outlier, in this particular case we have to stop to consider that
			 * particular element
			 */
			if (found == false) {
				//System.out.println(element.getFeature(0) + " Outlier found");
				return "";
			}
		}
	}

	// ------------------------------------------------------------------------------------------------------------------
	// SMALL KNOWLEDGE
	// ------------------------------------------------------------------------------------------------------------------

	/**
	 * Given an element search in the cctree the cluster to which this labeled
	 * element belongs. Once found gives to all the other elements present in
	 * the same cluster the same label of the element passed as parameter.
	 * Moreover it verifies that the node can be elected as a node containing a
	 * portion of the training set. This decision is taken by looking at the two
	 * double parameter passed to this function, they're the internal purity of
	 * the node and the external purity of it
	 * 
	 * @param element
	 *            the element to be searched
	 * @param internalPurity
	 * @param externalPurity
	 * @return the list of elements that will form the training set
	 */
	private ArrayList<Element<Integer>> retrieveClusterElements(Element<Integer> element, double internalPurity,
			double externalPurity) {
		// elements present in the same cluster to which the element passed as
		// parameter belongs to
		ArrayList<Element<Integer>> clusterElements = new ArrayList<Element<Integer>>();
		Node node = root;
		// scan the tree in the deep

		while (true) {
			boolean found = false;
			// retrieve the children of the actual node
			ArrayList<Node> children = node.getChildren();
			/**
			 * For each child retrieve the attribute vale according to which the
			 * child was created
			 */
			for (Node child : children) {
				int attributeIndex = child.getAttributeIndex();
				int attributeValue = child.getAttributeValue();
				// System.out.println("attIndex: " + attributeIndex + " value: "
				// + attributeValue );
				Integer featureValue = Integer.parseInt(element.getFeature(attributeIndex).toString());
				/**
				 * If the attribute value according to which the node was
				 * created is the same of the Element passed as parameter, then
				 * we have to consider the node
				 */
				if (attributeValue == featureValue) {
					node = child;
					found = true;
					// if the node is a leaf, then the search can stop
					if (node.isLeaf()) {
						// System.out.println("looking for: " +
						// element.toString() + " node id: " + node.getId());
						boolean rightNode = (node.getPurity() > internalPurity
								|| node.getExternalPurity() < externalPurity) ? false : true;
						if (rightNode == false) {
							// System.out.println("Not a right node: " +
							// node.getId() + "\t" + node.getExternalPurity() +
							// "\t" + node.getPurity());
							// System.out.println(node.toString());
							clusterElements.add(element);
						} else {
							// System.out.println("Right node: " + node.getId()
							// + "\t" + node.getExternalPurity() + "\t" +
							// node.getPurity() + "\t" +
							// node.getExternalLabel());
							// if(smallKnowledgeNodeIDs.contains(node.getId()))
							// System.out.println("ID " + node.getId() +
							// "alreadyPresent " );
							smallKnowledgeNodeIDs.add(node.getId());
							// System.out.println("Reached " + node.getId() + "
							// by Label: " + element.getLabelAsString() );
							// System.out.println("Add elements " +
							// node.getElementsList().toString());
							for (Element<Integer> elem : node.getElementsList()) {
								elem.setLabel(element.getLabelAsString());
								clusterElements.add(elem);
							}
						}
						return clusterElements;
					}
					// otherwise stop looking inside the list of siblings and go
					// deep
					else
						break;
				}
			}
			if (found == false)
				break;
		}
		return clusterElements;
	}

	/**
	 * Retrieve the list of IDs of nodes to be used as SmallKnowledge
	 * 
	 * @return the list of ids
	 */
	public ArrayList<Integer> getSKNodeIDs() {
		return smallKnowledgeNodeIDs;
	}

	/**
	 * The list of clusters not included inside the SmallKnowledge
	 * 
	 * @return the list of nodes
	 */
	public ArrayList<Node> getClusters() {
		ArrayList<Node> clusters = new ArrayList<Node>();
		for (Node node : nodes) {
			if (node.isLeaf()) {
				if (smallKnowledgeNodeIDs.contains(node.getId()))
					;
				else
					clusters.add(node);
			}
		}
		return clusters;
	}

	/**
	 * Once we have selected the elements via small knowledge we have to discard
	 * duplicated elements that may bias the result. This is the task of this
	 * function
	 * 
	 * @param elements
	 *            the elements retrieved from small knowledge
	 * @param id
	 *            the index of the column id
	 * @return a list of elements without duplicates
	 */
	private ArrayList<Element<Integer>> filterID(ArrayList<Element<Integer>> elements, int id) {
		ArrayList<Element<Integer>> training = new ArrayList<Element<Integer>>();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Element<Integer> elem : elements) {
			if (ids.contains(elem.getFeature(id)))
				;
			else {
				training.add(elem);
				ids.add(elem.getFeature(id));
			}
		}
		return training;
	}

	/**
	 * This function is in charge of computing a training dataset starting from
	 * small knowledge about the data.
	 * <p>
	 * Basically we provide a subset of data that have been already labeled and
	 * we follow their path in the tree. Once they've reached a cluster we
	 * verify some characteristics of the arrival cluster, the internal purity
	 * and the external purity, if both these measures are good for our
	 * purposes, then the elements forming the cluster will be retrieved. All
	 * the elements retrieved in this way will form the training set.
	 * </p>
	 * 
	 * @param data
	 *            the data to be used as small knowledge
	 * @param internalPurity
	 *            the internal purity maximum threshold for the arriving node
	 * @double externalPurity the minimum external purity threshold for the
	 *         arriving node
	 * @return the list of elements that will form the training set
	 */
	public ArrayList<Element<Integer>> smallKnowledge(ArrayList<Element<Integer>> data, double internalPurity,
			double externalPurity) {
		// variable to store the retrieved training set
		ArrayList<Element<Integer>> trainingSet = new ArrayList<Element<Integer>>();
		for (Element<Integer> element : data) {
			System.out.println("Element");
			if (element.getLabelType().equals(LABEL_TYPE.NOT_PRESENT))
				ExceptionManager.criticalBehavior("Element with no label " + element.toString());
			trainingSet.addAll(retrieveClusterElements(element, internalPurity, externalPurity));
		}

		// filter elements with the same id
		trainingSet = filterID(trainingSet, 0);
		return trainingSet;
	}

	public double averageNumberOfElementsPerLeaf() {
		int leaves = 0;
		double elements = 0;
		for (Node node : nodes) {
			if (node.isLeaf()) {
				leaves += 1;
				elements += node.numberElements();
			}
		}

		return elements / leaves;
	}

	public int getNumberOutliers() {
		int outliers = 0;
		for (Node node : nodes) {
			if (node.isLeaf()) {
				if (node.numberElements() <= 2) {
					outliers += 1;
				}
			}
		}
		return outliers;
	}

}
