package iit.cnr.it.classificationengine.clusterer.cctree;

import java.util.ArrayList;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.ExceptionManager;
import iit.cnr.it.classificationengine.basic.Utility;

/**
 * This class is aimed to compute the Entropy or split rules for the clustering
 * trees
 * 
 * @author antonio
 *
 */
public class Entropy {

	/**
	 * Computes the Shannon Entropy once provided the array list of the various
	 * probabilities Validated via Weka.core.contingencyTables.entropy
	 * 
	 * @param probabilitiesList
	 *            the list of probabilities
	 * @return the Shannon entropy
	 * @throws IllegalArgumentException
	 *             if the passed parameter is not valid
	 */
	public static Double computeShannonEntropy(ArrayList<Double> probabilitiesList) throws IllegalArgumentException {
		/* BEGIN parameter checking */
		if (probabilitiesList == null)
			ExceptionManager.throwIAE("Computing Shannon entropy, the provided list " + "of probabilities is null");
		if (probabilitiesList.size() == 0)
			ExceptionManager.throwIAE("Computing Shannon entropy, the provided list " + "of probabilities is empty");
		/* END parameter checking */

		Double entropy = 0.0;
		// compute the shannon probability
		for (Double probability : probabilitiesList) {
			/**
			 * Note: to compute Shannon entropy we need the log_2 of a number,
			 * java provides only the natural logarithm and the logarithm in
			 * base 10, to obtain the desired logarithm we use the change of
			 * basis
			 */
			entropy = entropy - (probability * (Math.log(probability) / Math.log(2.0)));
			// System.out.println(d + " " + sum);
		}
		// System.out.println("End probabilities");
		return entropy;
	}

	/**
	 * Computes the probability of a certain value of occurring in the dataset
	 * among all the possible values of an attribute
	 * 
	 * @param occurrences
	 *            the list of occurrences
	 * @return the list of probabilities
	 */
	public static ArrayList<Double> computeProbabilities(ArrayList<Integer> occurrences) {
		// compute the sum of all the occurrences, this is the number of elemnts
		// stored in the dataset
		double sum = 0.0;
		for (Integer i : occurrences)
			sum = sum + i;

		// for each Integer stored in the list compute its probability of
		// occurring
		ArrayList<Double> probabilities = new ArrayList<Double>();
		for (Integer i : occurrences) {
			double probability = i / sum;
			if (CCTree.DEBUG == true)
				System.out.println("Occurrences: " + i + "total: " + sum + "proabability: " + probability);
			probabilities.add(probability);
		}

		return probabilities;
	}

	/**
	 * Compute the entropy given the dataset and the index of the attribute on
	 * which we want to compute the entropy
	 * 
	 * @param dataset
	 *            the dataset to use to compute the entropy
	 * @param index
	 *            the index of the attribute on which we want to compute the
	 *            entropy
	 * @return the value of the entropy on the given attribute
	 */
	public static <T> Double entropyFromFeatures(DataSet<T> dataset, int index) {
		/* BEGIN parameter checking */
		if (dataset == null)
			ExceptionManager.throwIAE("Entropy.entropyFromFeatures the passed dataset is null");
		if (dataset.getDimensions() == 0 || index < 0)
			ExceptionManager.throwIAE("Entropy.entropyFromFeatures passed parameters are invalid");
		/* END parameter checking */

		// list used to compute the probability of a given attribute of having a
		// certain value
		ArrayList<Object> values = new ArrayList<Object>();
		ArrayList<Integer> occurrences = new ArrayList<Integer>();

		/**
		 * Counts the occurrences of a certain value for a certain attribute in
		 * the dataset
		 */
		for (int i = 0; i < dataset.getElements().size(); i++) {
			Element<T> element = dataset.getElement(i);
			// System.out.println("Size: " + element.size());
			T feature = element.getFeature(index);
			Object tmp = feature;
			/**
			 * If the feature is already stored then increment the occurrences
			 * of that value
			 */
			if (values.contains(tmp)) {
				int indexOccurrence = Utility.indexOf(tmp, values);
				int occurrence = occurrences.get(indexOccurrence);
				occurrences.set(indexOccurrence, ++occurrence);
				// occurrence++;

				// System.out.println("IO: " + indexOccurrence + " value: " +
				// tmp + " occ: " + occurrence);
				// occurrences = Utility.swap(occurrences, occurrence,
				// indexOccurrence);
			}
			/**
			 * otherwise add the value in the list of stored values, then insert
			 * a new occurrences value in the list of occurrences
			 */
			else {
				// System.out.println("Adding: " + tmp);
				values.add(tmp);
				Integer occurrence = 1;
				occurrences.add(occurrence);
			}
		}

		// System.out.println(occurrences);
		// compute the vaious probabilities
		if (CCTree.DEBUG == true)
			for (int i = 0; i < occurrences.size(); i++)
				System.out.println("Feature value: " + values.get(i) + "occurrences: " + occurrences.get(i));

		ArrayList<Double> probabilities = computeProbabilities(occurrences);
		// compute the Shannon entropy
		double entropy = computeShannonEntropy(probabilities);
		return entropy;
	}

	public static <T> Double entropyFromFeatures(ArrayList<Element<T>> elements, int index) {
		/* BEGIN parameter checking */
		if (elements == null)
			ExceptionManager.throwIAE("Entropy.entropyFromFeatures the passed dataset is null");
		if (elements.size() == 0 || index < 0)
			ExceptionManager.throwIAE("Entropy.entropyFromFeatures passed parameters are invalid");
		/* END parameter checking */

		// list used to compute the probability of a given attribute of having a
		// certain value
		ArrayList<Object> values = new ArrayList<Object>();
		ArrayList<Integer> occurrences = new ArrayList<Integer>();

		/**
		 * Counts the occurrences of a certain value for a certain attribute in
		 * the dataset
		 */
		for (int i = 0; i < elements.size(); i++) {
			Element<T> element = elements.get(i);
			// System.out.println("Size: " + element.size());
			T feature = element.getFeature(index);
			Object tmp = feature;
			/**
			 * If the feature is already stored then increment the occurrences
			 * of that value
			 */
			if (values.contains(tmp)) {
				int indexOccurrence = Utility.indexOf(tmp, values);
				int occurrence = occurrences.get(indexOccurrence);
				occurrences.set(indexOccurrence, ++occurrence);
				// occurrence++;

			}
			/**
			 * otherwise add the value in the list of stored values, then insert
			 * a new occurrences value in the list of occurrences
			 */
			else {
				// System.out.println("Adding: " + tmp);
				values.add(tmp);
				Integer occurrence = 1;
				occurrences.add(occurrence);
			}
		}

		// System.out.println(occurrences);
		// compute the vaious probabilities
		if (CCTree.DEBUG == true)
			for (int i = 0; i < occurrences.size(); i++)
				System.out.println("Feature value: " + values.get(i) + "occurrences: " + occurrences.get(i));

		ArrayList<Double> probabilities = computeProbabilities(occurrences);
		// compute the Shannon entropy
		double entropy = computeShannonEntropy(probabilities);
		return entropy;
	}

}
