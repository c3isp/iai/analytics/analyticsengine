package iit.cnr.it.classificationengine.privacy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.TYPE;

/**
 * This class provides the privacy metrics regarding a certain dataset.
 * <p>
 * The privacy metrics that can be considered are the l-diversity and the
 * e-differential.
 * 
 * <br>
 * The l-diversity in stated on this
 * <a href="https://www.cs.sfu.ca/~wangk/pub/FWCY10csur.pdf">paper</a>
 * </p>
 * 
 * @author antonio
 *
 */
public class PrivacyMetrics {

	private PrivacyMetrics() {

	}

	/**
	 * Computes the lDiversity among a dataset passed as parameter.
	 * 
	 * @param dataset
	 *            the dataset on which we want to compute the lDiversity
	 * @return the value of the lDiversity
	 */
	public static <T> double lDiversity(DataSet<T> dataset) {
		// BEGIN parameter checking
		if (dataset == null || dataset.getQualification() != TYPE.CLASSIFIER) {
			return -1;
		}
		// END parameter checking

		HashMap<String, ArrayList<Element<T>>> similar = retrieveSameClass(dataset);
		double minL = -1;
		for (Map.Entry<String, ArrayList<Element<T>>> entry : similar.entrySet()) {
			double tmpL = computeLDiversity(entry.getValue());
			if (minL == -1) {
				minL = tmpL;
			} else if (tmpL < minL) {
				minL = tmpL;
			}
		}
		return minL;
	}

	/**
	 * Retrieves all the elements having the same attributes
	 * 
	 * @param dataset
	 *            the dataset to be used
	 * @return a map in which the hash of the attributes list is the key and the
	 *         value is the list of elements having the same attributes
	 */
	private static <T> HashMap<String, ArrayList<Element<T>>> retrieveSimilar(DataSet<T> dataset) {
		HashMap<String, ArrayList<Element<T>>> map = new HashMap<>();
		for (Element<T> element : dataset.getElements()) {
			String elemAsString = element.toString();
			elemAsString = elemAsString.substring(0, elemAsString.lastIndexOf(","));
			if (map.containsKey(elemAsString)) {
				map.get(elemAsString).add(element);
			} else {
				ArrayList<Element<T>> list = new ArrayList<>();
				list.add(element);
				map.put(elemAsString, list);
			}
		}
		return map;
	}

	public static <T> HashMap<String, ArrayList<Element<T>>> retrieveSameClass(DataSet<T> dataset) {
		HashMap<String, ArrayList<Element<T>>> map = new HashMap<>();
		for (Element<T> element : dataset.getElements()) {
			String label = element.getLabelAsString();
			if (map.containsKey(label)) {
				map.get(label).add(element);
			} else {
				ArrayList<Element<T>> list = new ArrayList<>();
				list.add(element);
				map.put(label, list);
			}
		}
		return map;
	}

	private static <T> double computeLDiversity(ArrayList<Element<T>> element) {
		double total = element.size();
		ArrayList<Integer> sameAttributes = computeSameAttributes(element);
		double sum = 0.0;
		for (Integer numerator : sameAttributes) {
			sum += ((numerator / total) * Math.log10(numerator / total));
		}
		sum = 0.0 - sum;
		System.out.println("SUM: " + sum);
		double result = Math.pow(10, sum);
		return result;
	}

	/**
	 * Computes the number of elements having the same class which have the same
	 * attributes
	 * 
	 * @param elements
	 *            the set of elements that have the same class
	 * @return the list of number of elements having the same attributes
	 */
	private static <T> ArrayList<Integer> computeSameAttributes(ArrayList<Element<T>> elements) {
		ArrayList<Integer> equal = new ArrayList<Integer>();
		for (int i = 0; i < elements.size();) {
			if (elements.size() % 1000 == 0) {
				System.out.println("Index i: " + elements.size() + System.currentTimeMillis());
			}
			Element<T> element = elements.remove(i);
			String elemAsString = element.toString();
			elemAsString = elemAsString.substring(0, elemAsString.lastIndexOf(","));
			int equals = 1;
			for (int j = 0; j < elements.size();) {
				Element<T> compare = elements.get(j);
				String compareAsString = compare.toString();
				compareAsString = compareAsString.substring(0, compareAsString.lastIndexOf(","));
				if (compareAsString.equals(elemAsString)) {

					elements.remove(j);
					equals += 1;
				} else {
					j++;
				}
			}
			equal.add(equals);
		}
		return equal;
	}

}
