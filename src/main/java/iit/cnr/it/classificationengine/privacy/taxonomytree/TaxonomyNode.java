package iit.cnr.it.classificationengine.privacy.taxonomytree;

import java.util.ArrayList;

/**
 * This class represents a taxonomy node.
 * <p>
 * Basically a taxonomy node is an element of the taxonomy tree, each node in
 * the taxonomy tree stores the
 * </p>
 * 
 * @author antonio
 *
 */
public class TaxonomyNode {
	private String value;
	private ArrayList<TaxonomyNode> list = new ArrayList<>();

	public TaxonomyNode() {

	}

	public void setValue(String value) {
		if (value != null) {
			this.value = value;
		} else {
			System.err.println("Value is null");
		}
	}

	public void setList(ArrayList<TaxonomyNode> list) {
		if (list != null) {
			this.list = list;
		} else {
			System.err.println("List is null");
		}
	}

	public String getValue() {
		return value;
	}

	public ArrayList<TaxonomyNode> getList() {
		return list;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(value);
		if (list.size() == 0) {
			stringBuilder.append("\n----------\n");
		} else {
			for (TaxonomyNode tn : list) {
				stringBuilder.append("->" + tn.toString());
			}
		}
		return stringBuilder.toString();
	}

	public String generalize(String value) {
		try {
			if (!value.contains("Node")) {
				Double tmp = Double.parseDouble(value);
				value = tmp.toString();
			}
		} catch (Exception e) {

		}
		String returnValue;
		String tmp1 = null, tmp2 = null, tmp3 = null;
		if (list.size() == 0) {
			tmp1 = null;
		} else {
			for (TaxonomyNode tn : list) {
				if (tn.getValue().toString().equals(value.toString())) {
					tmp2 = this.value;
					break;
				} else {
					tmp3 = tn.generalize(value);
					if (tmp3 != null) {
						break;
					}
				}
			}
		}
		returnValue = (tmp2 != null) ? tmp2 : (tmp3 != null) ? tmp3 : null;
		return returnValue;
	}

}
