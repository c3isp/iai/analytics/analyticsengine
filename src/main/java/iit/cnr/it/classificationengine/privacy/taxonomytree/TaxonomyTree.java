package iit.cnr.it.classificationengine.privacy.taxonomytree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Utility;

/**
 * The taxonomy tree can be built in two different ways.
 * <p>
 * FORMAT of file containing the taxonomy tree: <br>
 * 9th,10th<br>
 * junior_sec;<br>
 * 11th,12th<br>
 * senior_sec;<br>
 * junior_sec,senior_sec<br>
 * secondary;<br>
 * masters,doctorate<br>
 * grad_school;<br>
 * grad_school,bachelors<br>
 * university;<br>
 * secondary,university<br>
 * any;<br>
 * </p>
 * 
 * @author antonio
 *
 */
public class TaxonomyTree {

	public static final int MAX_LEVELS = 5;
	private static int nodeId = 1;

	public enum FORMULA {
		MEDIAN, MEDIA
	}

	private TaxonomyNode root;

	private TaxonomyTree() {

	}

	/**
	 * Builds the taxonomy tree from a preformatted file
	 * 
	 * @param absolutePath
	 * @return
	 */
	public static TaxonomyTree buildFromFile(String absolutePath) {
		TaxonomyTree taxonomyTree = new TaxonomyTree();
		ArrayList<String> list = Utility.retrieveFileContent(absolutePath, ";\n");

		HashMap<String, TaxonomyNode> map = new HashMap<>();
		for (String couple : list) {
			buildTaxonomyNode(couple, map);
		}
		taxonomyTree.root = map.get("any");
		return taxonomyTree;
	}

	public static TaxonomyTree buildDefault(DataSet<String> dataset, int index) {
		ArrayList<String> features = dataset.getFeaturesByIndex(index);
		Set<String> set = new HashSet<>();
		for (String feature : features) {
			set.add(feature);
		}
		TaxonomyNode root = new TaxonomyNode();
		root.setValue("any");
		ArrayList<TaxonomyNode> nodes = new ArrayList<>();
		for (String s : set) {
			TaxonomyNode taxonomyNode = new TaxonomyNode();
			taxonomyNode.setValue(s);
			nodes.add(taxonomyNode);
		}
		root.setList(nodes);
		TaxonomyTree tree = new TaxonomyTree();
		tree.root = root;
		return tree;

	}

	public static TaxonomyTree buildDataInteger(DataSet<String> dataset, FORMULA formula, int index) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		if (formula == FORMULA.MEDIAN) {
			ArrayList<String> features = dataset.getFeaturesByIndex(index);
			for (String feature : features) {
				try {
					int d = Integer.parseInt(feature);
					list.add(d);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Set<Integer> hs = new HashSet<>();
			hs.addAll(list);
			list.clear();
			list.addAll(hs);
			Collections.sort(list);

			ArrayList<TaxonomyNode> nodes = generateLevels(list, 1);
			// System.out.println(nodes.toString());
			/*
			 * double median = list.get((list.size() / 2) - 1);
			 * ArrayList<TaxonomyNode> lowHalf = new ArrayList<>(); for (int i =
			 * 0; i < list.size() / 2; i++) { TaxonomyNode tmp = new
			 * TaxonomyNode(); tmp.setValue(list.get(i).toString());
			 * lowHalf.add(tmp); } TaxonomyNode taxonomyNode = new
			 * TaxonomyNode(); taxonomyNode.setValue("Node" + nodeId);
			 * taxonomyNode.setList(lowHalf);
			 * 
			 * ArrayList<TaxonomyNode> highHalf = new ArrayList<>(); for (int i
			 * = list.size() / 2; i < list.size(); i++) { if (list.get(i) ==
			 * median) ; else { TaxonomyNode tmp = new TaxonomyNode();
			 * tmp.setValue(list.get(i).toString()); highHalf.add(tmp); } }
			 * TaxonomyNode taxonomyNode1 = new TaxonomyNode();
			 * taxonomyNode1.setValue("Node" + nodeId);
			 * taxonomyNode1.setList(highHalf);
			 */

			TaxonomyNode root = new TaxonomyNode();
			root.setValue("any");
			root.getList().addAll(nodes);

			TaxonomyTree taxonomyTree = new TaxonomyTree();
			taxonomyTree.root = root;
			return taxonomyTree;
		}
		return null;
	}

	public static <T> TaxonomyTree buildFromDataDouble(DataSet<T> dataset, FORMULA formula, int index) {
		ArrayList<Double> list = new ArrayList<Double>();
		if (formula == FORMULA.MEDIAN) {
			ArrayList<T> features = dataset.getFeaturesByIndex(index);
			for (T feature : features) {
				try {
					double d = Double.parseDouble(feature.toString());
					list.add(d);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			Set<Double> hs = new HashSet<>();
			hs.addAll(list);
			list.clear();
			list.addAll(hs);
			Collections.sort(list);

			ArrayList<TaxonomyNode> nodes = generateLevelsd(list, 1);
			// System.out.println(nodes.toString());
			/*
			 * double median = list.get((list.size() / 2) - 1);
			 * ArrayList<TaxonomyNode> lowHalf = new ArrayList<>(); for (int i =
			 * 0; i < list.size() / 2; i++) { TaxonomyNode tmp = new
			 * TaxonomyNode(); tmp.setValue(list.get(i).toString());
			 * lowHalf.add(tmp); } TaxonomyNode taxonomyNode = new
			 * TaxonomyNode(); taxonomyNode.setValue("Node" + nodeId);
			 * taxonomyNode.setList(lowHalf);
			 * 
			 * ArrayList<TaxonomyNode> highHalf = new ArrayList<>(); for (int i
			 * = list.size() / 2; i < list.size(); i++) { if (list.get(i) ==
			 * median) ; else { TaxonomyNode tmp = new TaxonomyNode();
			 * tmp.setValue(list.get(i).toString()); highHalf.add(tmp); } }
			 * TaxonomyNode taxonomyNode1 = new TaxonomyNode();
			 * taxonomyNode1.setValue("Node" + nodeId);
			 * taxonomyNode1.setList(highHalf);
			 */

			TaxonomyNode root = new TaxonomyNode();
			root.setValue("any");
			root.getList().addAll(nodes);

			TaxonomyTree taxonomyTree = new TaxonomyTree();
			taxonomyTree.root = root;
			return taxonomyTree;
		}
		return null;
	}

	private static ArrayList<TaxonomyNode> generateLevelsd(ArrayList<Double> list, int levels) {
		if (levels == MAX_LEVELS) {
			ArrayList<TaxonomyNode> total = new ArrayList<>();
			ArrayList<TaxonomyNode> lowHalf = new ArrayList<>();
			if (list.size() == 1) {
				TaxonomyNode tmp = new TaxonomyNode();
				tmp.setValue(list.get(0).toString());
				lowHalf.add(tmp);
				TaxonomyNode taxonomyNode = new TaxonomyNode();
				nodeId += 1;
				taxonomyNode.setValue("Node" + list.get(0).toString());
				taxonomyNode.setList(lowHalf);
				total.add(taxonomyNode);
				return total;
			}

			double median = list.get((list.size() / 2) - 1);

			for (int i = 0; i < list.size() / 2; i++) {
				TaxonomyNode tmp = new TaxonomyNode();
				tmp.setValue(list.get(i).toString());
				lowHalf.add(tmp);
			}
			TaxonomyNode taxonomyNode = new TaxonomyNode();
			nodeId += 1;
			taxonomyNode.setValue("Node" + ((list.get(0) + list.get((list.size() / 2) - 1)) / 2));
			taxonomyNode.setList(lowHalf);

			ArrayList<TaxonomyNode> highHalf = new ArrayList<>();
			for (int i = list.size() / 2; i < list.size(); i++) {
				if (list.get(i) == median)
					;
				else {
					TaxonomyNode tmp = new TaxonomyNode();
					tmp.setValue(list.get(i).toString());
					highHalf.add(tmp);
				}
			}
			nodeId += 1;
			TaxonomyNode taxonomyNode1 = new TaxonomyNode();
			taxonomyNode1.setValue("Node" + ((list.get(list.size() - 1) + list.get((list.size() / 2) - 1)) / 2));
			taxonomyNode1.setList(highHalf);

			total.add(taxonomyNode1);
			total.add(taxonomyNode);
			return total;
		}

		else {
			ArrayList<TaxonomyNode> total = new ArrayList<>();
			double median = list.get((list.size() / 2) - 1);
			ArrayList<Double> lowHalf = new ArrayList<>();
			for (int i = 0; i < list.size() / 2; i++) {
				lowHalf.add(list.get(i));
			}
			TaxonomyNode taxonomyNode = new TaxonomyNode();
			nodeId += 1;
			// System.out.println("new node id: " + nodeId + "\t" + levels);
			taxonomyNode.setValue("Node" + ((list.get(0) + list.get((list.size() / 2) - 1)) / 2));
			if (lowHalf.size() > 0) {
				taxonomyNode.getList().addAll(generateLevelsd(lowHalf, levels + 1));
			}
			ArrayList<Double> highHalf = new ArrayList<>();
			for (int i = list.size() / 2 - 1; i < list.size(); i++) {
				if (list.get(i) == median)
					;
				else {
					highHalf.add(list.get(i));
				}
			}

			nodeId += 1;
			// System.out.println("new node id: " + nodeId + "\t" + levels);
			TaxonomyNode taxonomyNode1 = new TaxonomyNode();
			taxonomyNode1.setValue("Node" + ((list.get(list.size() - 1) + list.get((list.size() / 2) - 1)) / 2));
			if (highHalf.size() != 0) {
				taxonomyNode1.getList().addAll(generateLevelsd(highHalf, levels + 1));
			}

			total.add(taxonomyNode1);
			total.add(taxonomyNode);
			return total;
		}
	}

	private static ArrayList<TaxonomyNode> generateLevels(ArrayList<Integer> list, int levels) {
		if (levels == MAX_LEVELS) {
			ArrayList<TaxonomyNode> total = new ArrayList<>();
			ArrayList<TaxonomyNode> lowHalf = new ArrayList<>();
			if (list.size() == 1) {
				TaxonomyNode tmp = new TaxonomyNode();
				tmp.setValue(list.get(0).toString());
				lowHalf.add(tmp);
				TaxonomyNode taxonomyNode = new TaxonomyNode();
				nodeId += 1;
				taxonomyNode.setValue("Node" + nodeId);
				taxonomyNode.setList(lowHalf);
				total.add(taxonomyNode);
				return total;
			}

			double median = list.get((list.size() / 2) - 1);

			for (int i = 0; i < list.size() / 2; i++) {
				TaxonomyNode tmp = new TaxonomyNode();
				tmp.setValue(list.get(i).toString());
				lowHalf.add(tmp);
			}
			TaxonomyNode taxonomyNode = new TaxonomyNode();
			nodeId += 1;
			taxonomyNode.setValue("Node" + nodeId);
			taxonomyNode.setList(lowHalf);

			ArrayList<TaxonomyNode> highHalf = new ArrayList<>();
			for (int i = list.size() / 2; i < list.size(); i++) {
				if (list.get(i) == median)
					;
				else {
					TaxonomyNode tmp = new TaxonomyNode();
					tmp.setValue(list.get(i).toString());
					highHalf.add(tmp);
				}
			}
			nodeId += 1;
			TaxonomyNode taxonomyNode1 = new TaxonomyNode();
			taxonomyNode1.setValue("Node" + nodeId);
			taxonomyNode1.setList(highHalf);

			total.add(taxonomyNode1);
			total.add(taxonomyNode);
			return total;
		}

		else {
			ArrayList<TaxonomyNode> total = new ArrayList<>();
			double median = list.get((list.size() / 2) - 1);
			ArrayList<Integer> lowHalf = new ArrayList<>();
			for (int i = 0; i < list.size() / 2; i++) {
				lowHalf.add(list.get(i));
			}
			TaxonomyNode taxonomyNode = new TaxonomyNode();
			nodeId += 1;
			// System.out.println("new node id: " + nodeId + "\t" + levels);
			taxonomyNode.setValue("Node" + nodeId);
			if (lowHalf.size() > 0) {
				taxonomyNode.getList().addAll(generateLevels(lowHalf, levels + 1));
			}
			ArrayList<Integer> highHalf = new ArrayList<>();
			for (int i = list.size() / 2 - 1; i < list.size(); i++) {
				if (list.get(i) == median)
					;
				else {
					highHalf.add(list.get(i));
				}
			}

			nodeId += 1;
			// System.out.println("new node id: " + nodeId + "\t" + levels);
			TaxonomyNode taxonomyNode1 = new TaxonomyNode();
			taxonomyNode1.setValue("Node" + nodeId);
			if (highHalf.size() != 0) {
				taxonomyNode1.getList().addAll(generateLevels(highHalf, levels + 1));
			}

			total.add(taxonomyNode1);
			total.add(taxonomyNode);
			return total;
		}
	}

	/**
	 * 
	 * @param couple
	 * @param map
	 */
	private static void buildTaxonomyNode(String couple, HashMap<String, TaxonomyNode> map) {
		TaxonomyNode taxonomyNode = new TaxonomyNode();
		String key = couple.split("\n")[1].replace(";", "");
		taxonomyNode.setValue(key);
		for (String s : couple.split("\n")[0].split(",")) {
			TaxonomyNode tmp;
			if (map.containsKey(s)) {
				tmp = map.remove(s);
			} else {
				tmp = new TaxonomyNode();
				tmp.setValue(s);
			}
			taxonomyNode.getList().add(tmp);
		}
		map.put(key, taxonomyNode);
	}

	/**
	 * 
	 */
	public void print() {
		System.out.println("START\n" + root.toString());
	}

	public String generalize(String value) {
		try {
			return root.generalize(value);
		} catch (Exception e) {
			System.out.println("Generalizing : " + value);
			this.print();
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		TaxonomyTree taxonomyTree = TaxonomyTree.buildFromFile("/home/antonio/code4Papers/C3ISP_J/education.tree");
		taxonomyTree.print();
		System.out.println(taxonomyTree.generalize("9th"));
		System.out.println(taxonomyTree.generalize("10th"));
		System.out.println(taxonomyTree.generalize("11th"));
		System.out.println(taxonomyTree.generalize("12th"));
		System.out.println(taxonomyTree.generalize("junior_sec"));
		System.out.println(taxonomyTree.generalize("secondary"));

		System.out.println(taxonomyTree.generalize("masters"));
		System.out.println(taxonomyTree.generalize("doctorate"));
		System.out.println(taxonomyTree.generalize("grad_school"));
		System.out.println(taxonomyTree.generalize("bachelors"));
		System.out.println(taxonomyTree.generalize("university"));
	}
}
